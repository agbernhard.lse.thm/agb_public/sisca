SISCA (Simulation, Identification and Statistical variation in Cardiovascular Analysis)

Copyright (C) 2010 - 2018 FU Berlin (Mathematics, workgroup Prof. Christof Schütte)
Copyright (C) 2019 - 2020 THM Giessen (LSE, workgroup Prof. Stefan Bernhard)

This program is free software: you can redistribute it and/or modify it under the terms 
of the GNU Affero General Public License Version 3 as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. 
If not, see <http://www.gnu.org/licenses/>.

---

WARNING: if you load createExampleDatabaseAngiodb.sql it will drop all existing table content!

Load the file only to create a fresh database or examine the SQL-commands of the file first. 
