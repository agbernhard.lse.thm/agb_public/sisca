function basicSystemTestSISCA()
    % Check that the systems database is there and a simulation can be executed

    % SISCA (Simulation, Identification and Statistical variation in Cardiovascular Analysis)
    %
    % Copyright (C) 2010 - 2018 FU Berlin (Mathematics, workgroup Prof. Christof Schütte)
    % Copyright (C) 2019 - 2020 THM Giessen (LSE, workgroup Prof. Stefan Bernhard)
    %
    % This program is free software: you can redistribute it and/or modify it under the terms 
    % of the GNU Affero General Public License Version 3 as published by the Free Software Foundation.
    %
    % This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
    % without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    % See the GNU Affero General Public License for more details.
    %
    % You should have received a copy of the GNU Affero General Public License along with this program. 
    % If not, see <http://www.gnu.org/licenses/>.
    
    % Check that database exists
    try
        db = AngioDB;
        db.select("select schema_name as database_name " + ...
                  "from information_schema.schemata order by schema_name;");
    catch
        error("Could not reach database");
    end
    
    % load the first ArteryNet that can be found
    try 
        id = db.select("select id from ArteryNet limit 1");
        A = ArteryNet(id(1).id);
    catch
        error("System test cannot be executed without an ArteryNet \n");
    end
    
    % Make a basic solver run
    sr = A.getSolverRun;
    sr.name = "TEST_" + string(java.util.UUID.randomUUID);
    A.setSolverRun(sr);
    fprintf("Running solver for Artery net %d\n",id(1).id);
    A.RunSolver();
    
    % Check that dimensions of solution make sense
    sol = A.getSolution;
    Y = sol.Y;
    assert(size(Y,1) > 100 & size(Y,2) > 2, "Solver run solution seems not reasonable");
    fprintf("Basic test succesfull, you can run a standard solver run\n");
end
