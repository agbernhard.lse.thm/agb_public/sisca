%% Basic example how to vary one parameter an create a set of simulations

% SISCA (Simulation, Identification and Statistical variation in Cardiovascular Analysis)
%
% Copyright (C) 2010 - 2018 FU Berlin (Mathematics, workgroup Prof. Christof Schütte)
% Copyright (C) 2019 - 2020 THM Giessen (LSE, workgroup Prof. Stefan Bernhard)
%
% This program is free software: you can redistribute it and/or modify it under the terms 
% of the GNU Affero General Public License Version 3 as published by the Free Software Foundation.
%
% This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
% without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
% See the GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License along with this program. 
% If not, see <http://www.gnu.org/licenses/>.

% Define parameters
name = 'Variation of AAA';
netId=1;
paramSet=1;
runs=100;
diagnose = 3; %3 AAA
variationNodes = [34,]; %Node in the abdomen
variationPercentage = [0.15,];%Variation between 100 and 400%
multiplierNode = [2.5,]; %Aneurysm enlarged by 2.5

% Create Objects
A = ArteryNet(netId,paramSet);
sr = A.getSolverRun();
sr.solver = 12;
sr.rate =  1000;
A.setSolverRun(sr);

ss = A.NewStatisticalSet(name,' ');
[seedPS, SeedSR] = ss.getSeed();

%Build matrix of variations
dVar = zeros(runs,length(variationNodes));
for i = 1:length(variationNodes)
    mu = seedPS.nodeParam(variationNodes(i)).d*multiplierNode(i);
    sigma = mu*variationPercentage(i); %Vary by 15%
    dVar(:,i) = abs(normrnd(mu,sigma,[runs 1]));
end

% Run each simulation by adding to the statistical Set
for i = 1:runs
    newPS = seedPS;
    for j = 1:length(variationNodes)
        newPS.nodeParam(variationNodes(j)).d = dVar(i,j);
    end
    ss.Add(newPS); % This is what actually triggers a solver run
end

% Write Id of Diagnose
db = AngioDB;
sel = sprintf('UPDATE angiodb.StatisticalSet SET diagnoseId = %i WHERE id = %i',diagnose,ss.getId());
db.update(sel);    
