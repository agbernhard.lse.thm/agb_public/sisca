classdef SSModel < handle
    %% SSModel implements the statespace representation of dynamical systems based on Windkessel elements
    %          for now the implementation supports 3 types of linear systems: linear elastic, 
    %          linear visco elastic (Maxwell model) and linear visco elastic (Voigt model) 
    %          support of nonlinear systems is geared up, but not fully implemented. 
    %
    % SSModel Properties:
    %   constant props
    %      knownTypes - supported types of statespace models
    %
    %   structural props
    %      type                 - type as index into ssModel.knownTypes)
    %      matrixA              - state dynamics matrix
    %      matrixB              - input matrix 
    %      matrixC              - observation matrix
    %      matrixD              - input to output matrix
    %      matrixP              - persistency matrix
    %      matrixV              - process noise matrix
    %      matrixW              - measurement noise matrix
    %      matrixK              - noise variance
    %      modNet               - struct containing Net and Adj matrices
    %      x0                   - initial vector 
    %      Adjazenz             - adjacency matrix 
    %      Nseg                 - number of segments of net
    %      len                  - Nseg + number of boundary nodes
    %      RCL                  - net params 
    %      pqRCL                - outlet boundary conditions of net
    %      CtsIn                - inlet boundary condition of net (time dependent)
    %      u                    - boundary conditions
    %      context              - struct with extra information needed for work arounds to escape the limitations of the state space model
    %
    % class methods:
    %    FromArteryNet - constructs an object from an ArteryNet object using the OOP approach    
    %
    % instance methods:
    %    SSModel                 - Ctor, operates over an instatiated ArteryNet object
    %    dump                    - returns a list of all non-zero values of matrices A and B as compilable code lines 
    %    Matlab_Looped_StateF    - callback function for loop operations, used by SolverRun.solveMatlab 
    %    Matlab_StateF           - callback used by simple Matlab ODE solvers like ODE5 (interpretes nonlinear calls)
    %    Matlab_StateF1          - standard callback used by some Matlab ODE solvers like ODE45 (interpretes nonlinear calls)
    %    normalizeSolution       - returns the standard qp_ordered (n+2)-dim solution the includes also boundary conditions     %    dump                    - returns a list of all non-zero values of matrices A and B as compilable code lines 
    %    prep                    - callback function used by SolverRun to prepare heart (=closed loop) simulations
    %    Sundails_Looped_StateF  - callback function for heart model loop operations used by SolverRun 
    %    Sundails_StateF_sense   - fsa callback function used by SolverRun
    %    Sundails_StateF         - standard callback used by SolverRun (interpretes nonlinear calls)
    %    U                       - calculates u(t) using fft     
    %    UpdateModel_xt          - hack function to simulate dependeny of this.A on C
    %    UpdateStateSingleParam  - calculates the model state over a changed single parameter of the parameter set
    %    UpdateState             - calculates the model over a changed full parameter set    
    %    X0                      - calculates initial state vector     
 
    % SISCA (Simulation, Identification and Statistical variation in Cardiovascular Analysis)
    %
    % Copyright (C) 2010 - 2018 FU Berlin (Mathematics, workgroup Prof. Christof Schütte)
    % Copyright (C) 2019 - 2020 THM Giessen (LSE, workgroup Prof. Stefan Bernhard)
    %
    % Author: Rudolf Huttary
    %
    % This program is free software: you can redistribute it and/or modify it under the terms 
    % of the GNU Affero General Public License Version 3 as published by the Free Software Foundation.
    %
    % This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
    % without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    % See the GNU Affero General Public License for more details.
    %
    % You should have received a copy of the GNU Affero General Public License along with this program. 
    % If not, see <http://www.gnu.org/licenses/>.
    
    properties (Constant)
        ml2mmm = 1.e-6;
        mmHg2Pa = 133;            
        C2PaMl = 1.e-6/133;   % for C
        ERL2PaMl = 133/1.e-6;       % for   R, L
    end
    properties
        NodeParams;      % solver oriented props used by 'fsa' method
    end
    properties (GetAccess = public, SetAccess = private) % statespace model props
        Nx = 0;   % dim of state vector
        Ny = 0;   % dim of output vector
        Nu = 0;   % dim of input vector
        x0;       % intital state vector 
        u;        % boundary conditions,                    ToDo: code for constants and TS 
        A;        % state matrix (Nx,Nx)
        B;        % input matrix (Nx,Nu)         
        C;        % output matrix(Ny,Nx)           
        D;        % feedthrough matrix (Nx,Nx), default = zeros(
        E; 
        K;  % Initial value of the state disturbance matrix
        P;  % persistancy matrix 
        V; 
        W; 
        Asymb;   % symbolic version of A
        Bsymb;   % symbolic version of B
        nodes;      % no of nodes
        isLooped = false; 
        nonlinear = false; 
        period; 
        SundailsStateF; 
        MatlabStateF; 
        MatlabStateF1; 
        anet_id;   % points to ArteryNet object
        RCL; 
    end
    properties % (Access = private)    
        wke;        % array of WKE object (descriptors)
        argn; 
        CtsIn;    %                                         ToDo: eliminate and pack into u
        ffts; 
        heartState = 0; 
        heartParams = [10 9 100 2.0 0.045 0.4];   % default values. first three parametrize x0, last two the Elastance.
                                             % we start with filling phase
        
%         % legacy    ToDo: eliminate!
         anettype; 
%         Adjazenz; 
%         modNet; 
%         Nseg; 
%         len; 
%         equ; 
        
        % some arrays used as optimization cache for wke access 
        ca      % equations start indices of wkes
        cb      % equations last indices of wkes
        cap     % equations start indices of parent of wke
        cbp     % equations last indices of parent of wke
        cBound  % isboundary 
    end
    methods
        function ssM = SSModel(anet)  % ToDo: replace by SSModel.FromArteryNet
        %% Ctor - constructs an SSModel object from an ArteryNet object using the OOP approach
        %         generates a WKE object from each node
        %         calcs a Model fro each node 
        %               x[k+1] = Ax[k]+Bu[k]+Ke[k]
        %                 y[k] = Cx[k]+Du[k]+e[k]
        %  >> ss = SSModel(anet); 
        %  >> ss = SSModel(62); 
        % PARAMETER:
        %   'anet' instance or id of an ArteryNet object
        %                 
        % PARMETER:
        %     'anet'      - ArteryNet object
        % 
            if nargin < 1; return; end;
            if isnumeric(anet) 
                anet = ArteryNet(anet); 
            end
            if ~isa(anet, 'ArteryNet')
                error('SSModel.FromArteryNet: ArteryNet object or id expected'); 
            end
            ssM.anet_id = anet.id; 
            ssM.RCL = anet.RCL; 
            params = anet.getParamSet; 
            ssM.RCL.E = [params.nodeParam.E]; 
            ssM.RCL.l = [params.nodeParam.l]; 
            ssM.RCL.d = [params.nodeParam.d]; 
            ssM.RCL.h = [params.nodeParam.h]; 
            ssM.RCL.rho = [params.netParam.rho]; 
            ssM.RCL.eta = [params.netParam.eta]; 
            ssM.RCL.gamma = [params.netParam.gamma]; 
            ssM.RCL.nu = [params.netParam.nu]; 
            
            ssM.nodes = anet.nodeCount; 
            ssM.nonlinear = anet.p_solverRun.nonlinear; 
            boundarynodes = anet.boundaryCount; 
            
            ssM.Nx = 0; 
            % generate elements and count equations
            for i = ssM.nodes:-1:1         % create associated wke object for each node
                wk(i) = WKE(anet, i); 
                ssM.Nx = ssM.Nx + wk(i).equations; 
                p = wk(i).GetParams; 
                ssM.NodeParams.R(i) = p.R;  % vectorize this values. 
                ssM.NodeParams.C(i) = p.C; 
                ssM.NodeParams.L(i) = p.L; 
                ssM.NodeParams.b_p(i) = p.b_p; 
                ssM.NodeParams.b_q(i) = p.b_q; 
                ssM.NodeParams.b_R(i) = p.b_R; 
            end
            ssM.Ny = ssM.Nx;   % in our case they are identical.  ToDo: reduce Ny by identifying C with NormalizeMixedSolution 
            ssM.Nu = anet.boundaryCount+1; 
            ssM.u(ssM.Nu-1) = 0; 
            ssM.wke = wk; 
            
            % generate and fill connection matrices
            ssM.A(ssM.Nx, ssM.Nx) = 0; 
            ssM.B(ssM.Nx, ssM.Nu) = 0; 
            ssM.D(ssM.Ny, ssM.Nu) = 0; 
            ssM.K(ssM.Nx, ssM.Nx) = 0;   % process noise covariance matrix
            ssM.C = eye(ssM.Nx);         % observation matrix (full range)
            ssM.isLooped = ssM.wke(1).isheart; 
            ssM.anettype = anet.n_type; 
            ssM.period = 60./anet.n_heartRate; 
            
            if ssM.isLooped  % closed loop model 
                ssM.SundailsStateF = @ssM.Sundails_Looped_StateF; 
                ssM.MatlabStateF = @ssM.Matlab_Looped_StateF; 
                heartInit; 
                ssM.ConnectAB;               % create connected model
                ssM.heartParams(1:length(anet.n_alpha)) = anet.n_alpha;  % tricky: [] and other lengthes allowed !
                ssM.x0 = ssM.X0';            % initial condition for hidden state variable for faster convergence (p-type only)
            else
                ssM.SundailsStateF = @ssM.Sundails_StateF; 
                ssM.MatlabStateF = @ssM.Matlab_StateF; 
                ssM.MatlabStateF1 = @ssM.Matlab_StateF1; 
                ssM.ConnectAB;               % create connected model
                ssM.CtsIn = anet.CtsIn; 
                ssM.ffts = Signalprocessor.performFft(ssM.CtsIn.data, ssM.CtsIn.period);
                ssM.argn = ssM.ffts.n' * 2 * pi; 
                ssM.x0 = ssM.X0';            % initial condition for hidden state variable for faster convergence (p-type only)
            end
            
            function heartInit 
            % heartInit - in looped mode the first node of a structure is conversed into a heart node. 
            %             The main physiological parameters of the heart node are set here explicitly
            %             refer to: A.Ferreira, S.Chen et. al. 
            %              "A nonlinear State Space Model of a Combined Cardiovascular System and a Rotary Pump"
                ssM.wke(1).R =   0.005 * ssM.ERL2PaMl;  % R2 = mitral valve 
                ssM.wke(1).C =   4.4   * ssM.C2PaMl;    % C2 = Left Atrial
                ssM.wke(1).L =     0   * ssM.ERL2PaMl; 
                ssM.wke(1).UpdateState; 
                
                ssM.wke(2).R =  (0.001+0.0398) * ssM.ERL2PaMl; % R3+R4 = Aortic valve + Characteristic 
%                ssM.wke(2).R =  ssM.wke(2).R + 0.001 * ssM.ERL2PaMl; % R3+R4 = Aortic valve + Characteristic 
%                 ssM.wke(2).C =   1.33  * ssM.C2PaMl ; % C3 = Systemic Compliance 0.7 from literature
%                 ssM.wke(2).L =   0.0005 * ssM.ERL2PaMl ; % Aortic inertance
%                 ssM.wke(2).b_R = 1.2 * ssM.ERL2PaMl ; % systemic vascular resistance: literature 0.54-1.2. Cardiac output ~ 83ml
                 ssM.wke(2).UpdateState; 
            end
            
        end
        function str = dump(this, mode, verbose)
        %% dump - returns a list of all non-zero values of matrices A and B as compilable code lines 
        %  PARAMETER: 
        %    'mode'  (optional, default = 'sparse')
        %               'sparse' list this.Asymb and this.Bsymb as sparse matrices
        %               'full'   list this.Asymb and this.Bsymb as full matrices
        %               'both'   list both representations
        %    'verbose' (optional, default = true) prints result into commandline and clipboard
        
        if nargin < 2 
            mode = 'sparse'; 
        end
        if nargin < 3
            verbose = true; 
        end
        switch mode
            case 'sparse'
                str1 = dumpMatrix(this.Asymb, 'A'); 
                str2 = dumpMatrix(this.Bsymb, 'B'); 
                str = [str1'; {''}; str2']; 
            case 'full'
                str1 = pretty(this.Asymb, 'A'); 
                str2 = pretty(this.Bsymb, 'B'); 
                str = [str1'; {''}; str2']; 
            case 'both'
                str1 = dumpMatrix(this.Asymb, 'A'); 
                str2 = dumpMatrix(this.Bsymb, 'B'); 
                str3 = pretty(this.Asymb, 'A'); 
                str4 = pretty(this.Bsymb, 'B'); 
                str = [str1'; {''}; str2'; {''}; str3'; {''}; str4']; 
            otherwise
                error('dump: ''%s'' is not a valid mode\n%s', mode, help('SSModel.dump')); 
        end
         if verbose
             cb = ''; 
             for i = 1:length(str)
                 s = sprintf('%s\n', str{i}); 
                 cb = [cb s]; 
             end
             fprintf('%s\n',cb); 
             clipboard('copy', cb); 
         end
            
            function str = pretty(m, var)
                N = size(m,1); 
                len = max(cellfun('length', m));      % column lengthes
                linelength = sum(len)+length(len)*2;  % allow for 2 spaces
                format = sprintf('%%-%is', len+2);    % left aligned with correct columwidth
                for i = N:-1:1
                   str{i+1} = sprintf(format, m{i,:});  % one row at a time
                end
                str{1} = [var '=']; 
                
            end
            
            function str = dumpMatrix(m, var)
                str{numel(m)} = []; 
                [a, b] = size(m); 
                count = 1;
                str{count} = sprintf('%% autogenerated by SSModel.dump\n%s(%i, %i) = 0;     %% dim properly', var, a, b);
                for ii = 1:b
                    for j = 1:a
                        if ~strcmp(m{j,ii}, '0')
                            count = count + 1; 
                            str{count} = sprintf('%s(%i, %i) = %s;', var, j, ii, m{j,ii}); 
                        end
                    end
                end
                str = str(1:count); 
            end
        end 
            
        function x0 = X0(this)
        %%  X0 - calculates initial state vector  
            x0 = zeros(1,this.Nx); 
            if this.isLooped
                e = this.Elastance(0); 
                p0 = this.heartParams(3); 
                x0(3:end) = calc(2); 
                x0([1 2 4]) = [this.heartParams(1) this.heartParams(2)/e this.heartParams(3)] * SSModel.mmHg2Pa;  % 
%                x0([1 2 4]) = [10 9/e 100] * SSModel.mmHg2Pa;  % 
            else
               p0 = mean(this.CtsIn.data); 
               x0 = calc (1); 
            end
            function x0 = calc(root)
               res = this.wke(root).X0(p0, 0);  % call root wke's recursive function
               x0 = res.x0; 
                x0(isinf(x0)) = 0; 
                x0(isnan(x0)) = 0; 
            end
        end
        function UpdateStateSingleParam(this, odeP)
        %% UpdateStateSingleParam - calculates the model state over a changed single parameter of the parameter set
        %                Function is optimized to update only the state spase of involved nodes 
        % PARAMETER: 
        %   'odeP'   paramset with fields
        %            'name' (string) name of the parameter
        %            'nodes'(vector) indices of involved nodes
        %            'xxx'  parameter
        %
            f = odeP.name; 
            for j=1:length(odeP.nodes)  % we update nodes that are changed
                i = odeP.nodes(j); 
                wk = this.wke(i); 
                w_temp = wk.(f); 
                o_temp = odeP.(f)(i); 
                if w_temp ~= o_temp  % only if changes apply
                    wk.(f) = o_temp; 
                    wk.UpdateState;
                    wk.(f) = w_temp; 
                end
            end
            this.FastUpdateAB(odeP.nodes); 
        end
        function UpdateState(this, odeP)
        %% UpdateState - calculates the model over a changed full parameter set
        %                Function is optimized to update only matrix fields of involved nodes 
        % PARAMETER: 
        %   'odeP'   paramset with fields
        %            'R', 'C', 'L', 'b_R', 'b_p', 'b_q' parameter vector (dim = this.nodes )
        %            'nodes'
        %
            for j=1:length(odeP.nodes)  % we update all parameters of nodes that are changed
                i = odeP.nodes(j); 
                wk = this.wke(i); 
                wk.R = odeP.R(i); 
                wk.C = odeP.C(i); 
                wk.L = odeP.L(i); 
                wk.b_R = odeP.b_R(i); 
                wk.b_p = odeP.b_p(i); 
                wk.b_q = odeP.b_q(i); 
                wk.UpdateState; 
            end
            this.FastUpdateAB(odeP.nodes); 
        end
        function Y = normalizeSolution(this, X, T, isSens) % ToDo: create C instead using this algorithm, then eliminate
        %%  normalizeSolution - returns the standard qp_ordered (n+2)-dim solution the includes also boundary conditions 
        %   Y = this.normalizeSolution(X, T, isSens)
        %   PARAMETER
        %      'X'      - solution as returned by solver
        %      'T'      - time vector used for calculation of boundary condition time series
        %      'Y'      - normalized solution
        %      'isSens' - (optional) if given, Sensitivity normalization is done, disregarding the value!
            if nargin < 4
                isSens = 1; 
            else
                isSens = 0; 
            end
            if this.isLooped
                X(:,2) = X(:,2).*this.Elastance(T);  % back transformation
            else
                bc = real(Signalprocessor.performInvFft(this.ffts, T')); 
                bc = bc(:); 
            end
            samples = size(X, 1);  
            Y(samples, 2*(1+length(this.wke))) = 0;    
            iX = 1;  
            iY = 1; 
            iB = 1; 
            for i = 1: length(this.wke)
                equ = this.wke(i).equations;
                p = 2 * this.wke(i).parent+1; 
                switch this.wke(i).flavor
                    case 'pp'  % for now this case applies only on leave nodes
                        if this.wke(i).isboundary
                            Y(:, p) = Y(:, p) + X(:, iX);   % add to parent flow
                            Y(:, iY+2) = X(:, iX+equ-1);  % own flow 
                            if  this.isLooped
                                Y(:, iY+3) = X(:,1);         % own pressure is inpressure of root
                                Y(:,1) = Y(:,1) + Y(:,iY+2); % add up own flow to root
                            else
                                Y(:, iY+3)  = ones(samples, 1) * this.u(iB) * isSens;  % boundary condition
                            end
                            if length(this.wke) == 1
                                Y(:, 2) = bc(:,iB)*isSens;  % pressure of first node
                            end
                            iB = iB+1; 
                        else
                            error ('pp-WKE is only for leave nodes')
                        end
                    case 'pq'
                        if i==1    % root
                            Y(:, iY) = X(:, iX);  % flow
                            Y(:, iY+1) = bc(:,iB)*isSens;   % pressure boundary condition
                            Y(:, iY+3) = X(:, iX+equ-1);  % pressure of first node
                            iB = iB+1; 
                        else
                            Y(:, p) = Y(:, p) + X(:, iX);   % add to parent flow
                            Y(:, iY+3) = X(:, iX+equ-1);  % pressure of node i+1
                            if this.wke(i).isboundary     
                                Y(:, iY+2)  = ones(samples, 1) * this.u(iB)*isSens;  % flow boundary condition
                                iB = iB+1; 
                            else
                            end
                        end
                    case 'qq' % for now this case applies only to root nodes
                        if i == 1     
                            if ~this.isLooped
                                % sum of outflows of boundary nodes                                
                                Y(:, iY)   = bc(:,1)*isSens;         % flow boundary condition
                            end
                            Y(:, iY+1) = X(:, iX);        % in pressure
                            Y(:, iY+3) = X(:, iX+equ-1);  % out pressure
                            iB = iB+1; 
                        else
                            error ('pp-WKE is only for root nodes')
                        end
                end
                iX = iX+equ; 
                iY = iY+2; 
            end
        end
        function u = Udrift(this, t)
        %% U - calculates u(t) using fft 
          for i = length(t):-1:1 
                tdft=mod(t(i), this.ffts.tperiod) / this.ffts.tperiod;
                arg = this.argn * tdft; 
                drift = 133 * 15 *(1-cos(t/4));  % pressure drift 30 mmHg
                u(:, i) = real([this.ffts.a0 + this.ffts.an * cos(arg) + this.ffts.bn * sin(arg) + this.ffts.ax * cos(12 * pi * tdft)+drift; this.u(2:end)']); 
          end
        end
        function u = U(this, t)
        %% U - calculates u(t) using fft 
          for i = length(t):-1:1 
                tdft=mod(t(i), this.ffts.tperiod) / this.ffts.tperiod;
                arg = this.argn * tdft; 
                u(:, i) = real([this.ffts.a0 + this.ffts.an * cos(arg) + this.ffts.bn * sin(arg) + this.ffts.ax * cos(12 * pi * tdft); this.u(2:end)']); 
          end
        end
        
        function xd = Matlab_StateF(ss, tout, x)
        %% Matlab_StateF - callback for Matlab ODE solvers
%            xd = ss.A * x(:,1) + ss.B * ss.U1(tout);
%            xd = ss.A * x(:,1) + ss.B * ss.U(tout);
            if ss.nonlinear
                [A, B] = ZimEstimator.NonLin_model(ss.RCL, x, ss.anet_id, tout); 
                xd = (A * x + B * ss.U(tout));
            else
                xd = (ss.A * x + ss.B * ss.U(tout));
            end
        end
        function xd = Matlab_StateF1(ss, tout, x)
        %% Matlab_StateF - callback for Matlab ODE solvers
%            xd = ss.A * x(:,1) + ss.B * ss.U1(tout);
%            xd = ss.A * x(:,1) + ss.B * ss.U(tout);
            if ss.nonlinear
                [A, B] = ZimEstimator.NonLin_model(ss.RCL, x, ss.anet_id); 
                xd = (A * x + B * ss.U(tout));
            else
                xd = (ss.A * x + ss.B * ss.U(tout));
            end
        end
        function [xd, flag, new_data] = Sundails_StateF(ss, tout, x)
        %% Sundails_StateF = standard callback function used by SolverRun
        ucb = @ss.Udrift; 
        ucb = @ss.U; 
            if ss.nonlinear
                [A, B] = ZimEstimator.NonLin_model(ss.RCL, x, ss.anet_id, tout); 
                xd = (A * x + B * ucb(tout))';   % use offset drift
            else
                xd = (ss.A * x + ss.B * ucb(tout))';
            end
            flag = 0;
            new_data = [];
        end
        
        function [z use_z] = prep(ss, tout, z)
        %% prep - callback function used by SolverRun to prepare heart (=closed loop) simulations
        % testcode for valve function 
            use_z = false; 
            if ss.isLooped
                el = ss.Elastance(tout); 
                x = z;
                eq = ss.wke(2).equations; 
                hyst = 0; 
                x(2) = z(2) * el; % back transformation for case discrimination
                if x(2) < x(1) && x(2) < x(4)            % filling 
                    if ss.heartState ~= 1
                        ss.A(1:2, 1:3) = ss.wke(1).model.AB(:, 2:4);  % exchange wke(1)
                        ss.A(3:2+eq, 3:3+eq) = ss.wke(2).model.AB(1:eq,2:2+eq);    % restore wke(2)

                        ss.A(3,:) = 0;                        % reduce wke(2)
                        ss.A(4:3+eq,3) = 0;  
                        ss.heartState = 1; 
%                      use_z = true;  
                    end
                    
                    ss.A(1:2, 2) = ss.wke(1).model.AB(:, 3) * el;     % transformation
                    z(3) = 0; 
                elseif (x(2) > x(1) && x(2) > x(4))      % ejection
                    if ss.heartState ~= 2
                        ss.A(1:2, 1:3) = ss.wke(1).model.AB1(:, 2:4);    % exchange wke(1)
                        ss.A(3:2+eq, 2:3+eq) = ss.wke(2).model.AB(1:eq, 1:2+eq);    % restore wke(2)
                        
                        ss.heartState = 2; 
                        use_z = false; 
                    end
                    ss.A(3:2+eq, 2) = ss.wke(2).model.AB(1:eq,1) * el;         % transformation & connection
                    
                elseif x(2) > x(1) && x(2) < x(4)        % isovolumic
                    if ss.heartState ~= 3
                        ss.A(1:2, 1:3) = ss.wke(1).model.AB2(:, 2:4);   % exchange wke(1)
                        ss.A(3:2+eq, 3:3+eq) = ss.wke(2).model.AB(1:eq,2:2+eq);    % restore wke(2)

                        ss.A(3,:) = 0;                                  % reduce   wke(2)
                        ss.A(4:3+eq,3) = 0;  
                        ss.heartState = 3; 
%                        use_z = true;  
                    end
                    z(3)  = 0; 
                else % not possible is ignored
                    % error('not possible'); 
                end
            end
        end
        
        function xd = Matlab_Looped_StateF(ss, tout, z)
        %% Matlab_Looped_StateF - callback function for loop operations, used by SolverRun.solveMatlab 
                xd = (ss.A * z + ss.B * (ss.E * z));
        end
        
        function [xd, flag, new_data] = Sundails_Looped_StateF(ss, tout, z, ~)
        %% Sundails_Looped_StateF - callback function for loop operations used by SolverRun
            xd = (ss.A * z + ss.B * (ss.E * z))';
            flag = 0;
            new_data = [];
        end
        
        function [xd, flag, new_data] = Sundails_StateF_sense(ss, tout, x, odeP)
        % Sundails_StateF_sense - fsa callback function used by SolverRun
            ss.UpdateStateSingleParam(odeP); 
            xd = (ss.A * x + ss.B * ss.U(tout))';
            flag = 0;
            new_data = [];
        end
    end
    methods % ( Access = private)
        function e = Elastance(ssM, t)
            Emax=ssM.heartParams(4);
            Emin=ssM.heartParams(5);
            tm=mod(t, ssM.period);
            % N.Stergiopulos: Determinants of stroke volum and systolic and diastolic aortic pressure
            a = ssM.heartParams(6); % beat strength
            a1 = 0.303; 
            a2 = 0.508; 
            n1 = 1.32; 
            n2 = 21.9; 
            T1 = tm/(a1*ssM.period); 
            T2 = tm/(a2*ssM.period); 
            e=Emax * a*(T1.^n1)./(1+T1.^n1)./(1+T2.^n2)+Emin;
            % heart pump model: A.Ferreira, et al.: A Nonlinear State-Space Model of a Combined Cardiovascular System and a Rotary Pump
%            beatTimeRatio = ssM.heartParams(6);
%           tn=tm./(0.2+beatTimeRatio*ssM.period);
%            En=1.55 * ((tn/0.7).^1.9)./(1+(tn/0.7).^1.9)./(1+(tn/1.17).^21.9);
%            e = ((Emax-Emin)*En+Emin);
            e = e* SSModel.ERL2PaMl; 
        end
        function FastUpdateAB(ssM, nodes)
        % FastUpdateAB - updates matrices A, B and connections of changed nodes
        %                optimization: only involved nodes and their direct children are updated.
        % PARAMETER:
        %   'nodes'  indices of nodes to be updated

        
            % calc full node set 
            nlarge(max(length(ssM.wke))) = 0; 
            nlarge(nodes) = 1; 
            for i = 1:length(nodes)
               wk = ssM.wke(nodes(i)); 
               if ~isempty(wk.children)
                   nlarge([wk.children.nodeId]) = 1; 
               end
            end
            nodes = find(nlarge); 
        
            A = ssM.A;  %#ok<*PROP>
            B = ssM.B; 
            for i = nodes(1): nodes(end)  % over all involved nodes
                m = ssM.wke(i).model; 
                a = ssM.ca(i);                   % first cell in connection matrix, m(a,a)
                b = ssM.cb(i);                   % last  cell in connection matrix, m(b,b)
                A(a:b, a:b) = m.AB(:,2:end-1);   % core matrix A
                % all but root
                if(i>1)
                    ap = ssM.cap(i);              % first cell of parent in connection matrix, m(ap,ap)
                    bp = ssM.cbp(i);              % last  cell of parent in connection matrix, m(bp,bp) 
                    A(a:b, bp) = m.AB(:, 1);      % uplink to parent)
                    A(ap:bp, a) = A(ap:bp, bp+1); % downlink from parent
                end
                % boundaries
                if ssM.cBound(i)
                    bc = sum(ssM.cBound(1:i))+1;   % calc the boundary index
                    B(a:b, bc) = m.AB(:, end);     % downlink (bc)
                    ssM.u(bc) = m.u;            
                else
                    A(a:b, b+1) = m.AB(:,end);     % downlink to each child
                end
            end
            ssM.A = A; 
            ssM.B = B; 
        end
        function ConnectAB(ssM, ~)
        %% ConnectAB - generates connected Matrices A and B
        %
            % calc A and B 
%            nodes = length(ssM.wke); 
            StartI(ssM.nodes+1) = 0;        % tracks start row/column indices of nodes in A
            StartI(1) = 1;              % idx of first node is 1
            bCount = 1;                 % counter for Boundary conditions in B
            A(ssM.Nx,ssM.Nx) = 0; 
            B(ssM.Nx,ssM.Nu) = 0; 
            E(ssM.Nu, ssM.Nx) = 0; 
            ssM.ca(ssM.nodes) = 0;          % cache for FastUpdateAB
            ssM.cb(ssM.nodes) = 0; 
            ssM.cap(ssM.nodes) = 0; 
            ssM.cbp(ssM.nodes) = 0; 
            ssM.cBound(ssM.nodes) = 0; 

            % symbolic world
            Asymb{ssM.Nx,ssM.Nx} = {}; 
            Bsymb{ssM.Nx,ssM.Nu} = {}; 
            Asymb(1:numel(Asymb)) = {'0'};  % init all with '0'
            Bsymb(1:numel(Bsymb)) = {'0'}; 
            if ~isfield(ssM.wke(1).model, 'symb')
                symb = false;
            else
                symb = true; 
            end

            if ssM.nodes == 1                                   % one element net (heart types not possible!)
                m = ssM.wke(1).model;  % calc the model
                A = m.AB(:, 2:end-1); 
                B = [m.AB(:, 1) m.AB(:, end)]; 
                if symb
                    Asymb = m.symb(:, 2:end-1); 
                    Bsymb = [m.symb(:, 1) m.symb(:, end)]; 
                end
                ssM.wke(1).pos = 1;  %#ok<*NASGU>
                ssM.u(2) = m.u; 
            else                        % connection matrix
                for i = 1: ssM.nodes
                    wk = ssM.wke(i); 
                    m = wk.model; 
                    a = StartI(i);               % first cell in connection matrix, m(a,a)
                    ssM.ca(i) = a; 
                    b = a+wk.equations-1;    % last  cell in connection matrix, m(b,b)
                    ssM.cb(i) = b; 
                    StartI(i+1) = b+1; 
                    if wk.isroot                                % root node
                        A(a:b, a:b+1) = m.AB(:, 2:end);     % core matrix + downflow
                        B(a:b, bCount) = m.AB(:, 1);        % upward
                        if symb
                         Asymb(a:b, a:b+1) = m.symb(:, 2:end); 
                         Bsymb(a:b, bCount) = m.symb(:, 1);
                        end
                        bCount = bCount + 1; 
                    else
                        ssM.wke(wk.parent).children = [ssM.wke(wk.parent).children wk];
                        A(a:b, a:b) = m.AB(:,2:end-1);      % core matrix A
                         Asymb(a:b, a:b) = m.symb(:, 2:end-1); 
                        ap = StartI(wk.parent);             % first cell of parent in connection matrix, m(ap,ap)
                        ssM.cap(i) = ap; 
                        bp = ap+ssM.wke(wk.parent).equations-1; % last  cell of parent in connection matrix, m(bp,bp) 
                        ssM.cbp(i) = bp; 
                        % boundaries
                        A(a:b, bp) = m.AB(:, 1);            % uplink to parent)
                        A(ap:bp, a) = A(ap:bp, bp+1);       % downlink from parent
                        if symb
                            Asymb(a:b, bp) = m.symb(:, 1); 
                            Asymb(ap:bp, a) = Asymb(ap:bp, bp+1);       % downlink from parent
                        end

                        if wk.isboundary      
                            B(a:b, bCount) = m.AB(:, end);  % downlink (bc)
                            if symb
                                Bsymb(a:b, bCount) = m.symb(:, end);  % downlink (bc)
                            end
                            if ssM.isLooped
                                E(1, b) = 1; 
                            else
                                ssM.u(bCount) = m.u;            
                            end
                            ssM.cBound(i) = true; 
                            bCount = bCount + 1; 
                        else
                            A(a:b, b+1) = m.AB(:,end);      % downlink to each child
                            if symb
                                Asymb(a:b, b+1) = m.symb(:,end);      % downlink to each child
                            end
                        end
                    end
                    ssM.wke(i).pos = a; 
                end
            end
            ssM.A = A; 
            ssM.B = B; 
            ssM.Asymb = Asymb; 
            ssM.Bsymb = Bsymb; 
            if ssM.isLooped
                E(2:end, 1) = ones(ssM.Nu-1, 1); 
                ssM.E = E; 
            end
        end
        function ConnectAB1(ssM, ~)
        %% ConnectAB - generates connected Matrices A and B
        %
            % calc A and B 
%            nodes = length(ssM.wke); 
            StartI(ssM.nodes+1) = 0;        % tracks start row/column indices of nodes in A
            StartI(1) = 1;              % idx of first node is 1
            bCount = 1;                 % counter for Boundary conditions in B
            A(ssM.Nx,ssM.Nx) = 0; 
            B(ssM.Nx,ssM.Nu) = 0; 
            E(ssM.Nu, ssM.Nx) = 0; 
            ssM.ca(ssM.nodes) = 0;          % cache for FastUpdateAB
            ssM.cb(ssM.nodes) = 0; 
            ssM.cap(ssM.nodes) = 0; 
            ssM.cbp(ssM.nodes) = 0; 
            ssM.cBound(ssM.nodes) = 0; 
            if ssM.nodes == 1                                   % one element net (heart types not possible!)
                m = ssM.wke(1).model;  % calc the model
                A = m.AB(:, 2:end-1); 
                B = m.AB(:, 1) + m.AB(:, end); 
                ssM.wke(1).pos = 1;  %#ok<*NASGU>
            else                        % connection matrix
                for i = 1: ssM.nodes
                    wk = ssM.wke(i); 
                    m = wk.model; 
                    a = StartI(i);               % first cell in connection matrix, m(a,a)
                    ssM.ca(i) = a; 
                    b = a+wk.equations-1;    % last  cell in connection matrix, m(b,b)
                    ssM.cb(i) = b; 
                    StartI(i+1) = b+1; 
                    if wk.isroot                                % root node
                        A(a:b, a:b+1) = m.AB(:, 2:end);     % core matrix + downflow
                        B(a:b, bCount) = m.AB(:, 1);        % upward
                        bCount = bCount + 1; 
                    else
                        ssM.wke(wk.parent).children = [ssM.wke(wk.parent).children wk];
                        A(a:b, a:b) = m.AB(:,2:end-1);      % core matrix A
                        ap = StartI(wk.parent);             % first cell of parent in connection matrix, m(ap,ap)
                        ssM.cap(i) = ap; 
                        bp = ap+ssM.wke(wk.parent).equations-1; % last  cell of parent in connection matrix, m(bp,bp) 
                        ssM.cbp(i) = bp; 
                        % boundaries
                        A(a:b, bp) = m.AB(:, 1);            % uplink to parent)
                        A(ap:bp, a) = A(ap:bp, bp+1);       % downlink from parent

                        if wk.isboundary      
                            if ssM.isLooped
                                E(1, b) = 1; 
                                B(a:b, bCount) = m.AB(:, end);  % downlink (bc)
                            else
                                B(a:b, bCount) = m.AB(:, end);  % downlink (bc)
                                ssM.u(bCount) = m.u;            
                            end
                            ssM.cBound(i) = true; 
                            bCount = bCount + 1; 
                        else
                            A(a:b, b+1) = m.AB(:,end);      % downlink to each child
                        end
                    end
                    ssM.wke(i).pos = a; 
                end
            end
            ssM.A = A; 
            ssM.B = B; 
            if ssM.isLooped
                E(2:end, 1) = ones(ssM.Nu-1, 1); 
                ssM.E = E; 
            end
        end
    end
    methods (Static)
	end
end

