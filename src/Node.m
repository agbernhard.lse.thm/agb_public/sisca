classdef Node < handle 
    %% Node objects represent nodes in ArteryNet objects
    %    each node 
    %      - connects to one parent (except for root)
    %      - has a parameter set 
    %      - governs a subtree  (see children property)
    %        many operations are recursive therefore. 
    %      - has a GUI representation in current figure (node, edge, text)
    %      - GUI color in denotes inner state: 
    %      - is dragable by left mouse button 
    %      - offers two contextmenus (node and edge) with operations
    %
    %
    %
    %
    % Node Properties:
    %   structural props     
    %     id       - ordered numbering   
    %     parent   - parent node
    %     children - Node array
    %
    %   visual props     
    %     radius   - radius of rect (visual)
    %     position - actual position (visual)
    %     color    - color indicating state
    %
    %   GUI Handles
    %     hNode    -  handle of Rectangle drawn to visualize Node
    %     hLine    -  handle of Line drawn to visualize parent relation
    %     hText    -  handle of Text drawn in Rectangle to visualize id
    % 
    % Node Methods (Static):
    %     ContextMenus - defines the three context menus
    %     Clipboard    - mark node to be pasted
    %     Copy         - put node into clipboard
    %     Paste        - clone subtree under node
    % 
    % Node Methods (callbacks):
    %     startDragFcnRect - start drag operation of subtree
    %     draggingFcnRect  - drag subtree visually
    %     stopDragFcn      - end drag operation and update position props
    %     cmCut            - context menu command 'Cut'
    %     cmPaste          - context menu command 'Paste'
    %     cmCopy           - context menu command 'Copy'
    %     cmValidate       - implements root node's context menu command 'Validate'
    %
    % Node Methods (instance):
    %     Node           - constructor
    %     addChild       - adds a subtree to object
    %     addToFigure    - constructs GUI Representation of object
    %     delSubtreeGUI  - deletes GUI-Elements of subtree
    %     isRoot         - true if object is root node    
    %     isBoundary     - true if object is boundary node    
    %     move           - moves GUI elements of subtree in figure
    %     pos2LinePos    - calculate visible part of edge for line from a to b: a = [x1 y1], b = [x2 y2]
    %     sort           - sorts subtree using clock metric (0:00 is first elem)
    %     textvisibility - sets visibility of GUI text objects in subtree
    %     updatePosition - set position properties of subtree 

    % SISCA (Simulation, Identification and Statistical variation in Cardiovascular Analysis)
    %
    % Copyright (C) 2010 - 2018 FU Berlin (Mathematics, workgroup Prof. Christof Schütte)
    % Copyright (C) 2019 - 2020 THM Giessen (LSE, workgroup Prof. Stefan Bernhard)
    %
    % Author: Rudolf Huttary
    %
    % This program is free software: you can redistribute it and/or modify it under the terms 
    % of the GNU Affero General Public License Version 3 as published by the Free Software Foundation.
    %
    % This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
    % without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    % See the GNU Affero General Public License for more details.
    %
    % You should have received a copy of the GNU Affero General Public License along with this program. 
    % If not, see <http://www.gnu.org/licenses/>.

properties (Constant)
   rootProps2Validate = {'n_eta' 'n_rho' 'n_gamma' 'n_ctsId' 'n_heartRate'};
   nodeProps2Validate = {'E' 'l' 'd' 'h'};
   % prefix '~' prop must have value > 0 ; 
   %        '+' one prop must have value ~=[] ; 
   
   leaveProps2Validate = {'E' 'l' 'd' 'h' '~b_R' '+b_p' '+b_q'};    
   copyprops = {'type' 'E' 'd' 'l' 'h' 'R' 'C' 'L' 'b_R' 'b_C' 'b_L' 'b_p' 'b_q'};
   props2Clone = {'id' 'paramSetId' 'name' 'nodeId' 'description' 'type' 'E' 'l' 'd' 'h' 'b_id' 'b_paramSetId' 'b_name' 'b_description' ...
                'b_R' 'b_C' 'b_L' 'b_p' 'b_q'};

end

   properties
   %% structural
   
     % NodeParam
        id
        temp_id
        paramSetId
        name
        nodeId = -1; % 0 when root, id>0 when ordered, -1 when unordered, 
        description
        type = 1;    % node Paramtype
        E;
        l = 0;
        d;
        h;
        R;
        C; 
        L;
     % BoundaryParam
        b_id; 
        b_temp_id;
        b_paramSetId; 
        b_name; 
        b_description; 
        b_R = 5e-9; 
        b_C = 0; 
        b_L = 0; 
        b_p = 0;  
        b_q; 


        % design
        parent = [];   % parent node, empty when root
        children % array containing children nodes
        position 
        
        %state
        color = 'red';  % red:    no layout(unsorted) + no parameters
                        % magenta: layouted + no parameters
                        % yellow: standard layout + no parameters 
                        % cyan:   standard layout + parameters 
                        % blue:   no layout + parameters
                        % green:  layouted + parameters
        
        % display
        radius = 0.02;  % radius for visual node representation
        hNode
        hLine
        hText
        
        % moving
        startpos  % when dragging

        % used by ArteryNet.MultiSelector Dialog
        selected = false; 
        hSelect;     
        hSelectText; 
        hSelectLine; 
    end
    
    methods (Static)
        
        function clip = ClipEdge(node)
        %%  ClipEdge - copies node properties into persistent variable
            persistent edge; 
            if nargin > 0
                if ~isa(node, 'Node')
                    error('Node.PasteEdge: Node object expected')
                end
                for i=1:length(Node.copyprops)
                    edge.(Node.copyprops{i}) = node.(Node.copyprops{i}); 
                end
            else
                clip = edge; 
            end
        end
        function PasteEdge(node)
        % PasteEdge - copies node parameters from persistent variable into node    
            if ~isa(node, 'Node')
                error('Node.PasteEdge: Node object expected')
            end
            clip = Node.ClipEdge; 
            for i=1:length(Node.copyprops)
                node.(Node.copyprops{i}) = clip.(Node.copyprops{i}); 
            end
            node.validate; 
        end
        
        function clip = Clipboard(node)
        %% Clipboard - implements a clipboard for subtrees
        %   because there no static class properties ... 
        % See also: Cut, Copy, Paste. 
            persistent subtree;    % static equivalent
            if nargin > 0
                subtree = node.clone;
            end
            clip = subtree; 
        end
        
        function Copy(node)
        %% Copy - puts node into clipboard 
        %   clone is done by paste-operation
        % See also: Clipboard, Paste. 
            Node.Clipboard(node);
        end
        
        function node = Paste
        %% Paste - returns a clone of subtree from clipboard 
        % See also: Copy, Clipboard. 
            node = Node.Clipboard; 
            node = node.clone; 
        end
        
%% callbacks used by GUI elements ===================================================      
% varargin(1) denotes figure handle
        
        function startDragFcnRect(varargin)
        %% startDragFcnRect - callback for mouse button activity
        % prepares dragging of subtree to visually follow the mouse cursor
        %       distance
                st = get(gcf, 'SelectionType');
                obj = get(gco, 'UserData');
                anet = get(gcf, 'UserData'); 
                if strcmp(st ,'normal')   % only left mouse button
                    anet.EditParams(obj); 
                elseif strcmp(st ,'alt')   
                    % maintain contextmenu
                    anet.EditParams; 
                    k = get(anet.hNodeMenu, 'Children'); 
                    set(k(1), 'Enable', 'off');
                    if ~obj.isRoot && ~isempty(obj.children)
                        if size(obj.children, 2) == 1
                        set(k(1), 'Enable', 'on');
                        end
                    end
                   if isa(Node.Clipboard, 'Node')
                        m = get(anet.hRootMenu, 'Children'); 
                        if ~obj.isRoot
                            set(m(1), 'Enable', 'off');
                            k = get(anet.hNodeMenu, 'Children'); 
                            set(k(3), 'Enable', 'on');
                        else
                            if isempty(obj.children)
                                set(m(2), 'Enable', 'on');
                            end
                        end
                    end
                else
                    if anet.visualize
                        return; 
                    end
                    anet.EditParams; 
                    set(gcf, 'WindowButtonMotionFcn', @obj.draggingFcnRect); 
                    set(gcf, 'WindowButtonUpFcn', @obj.stopDragFcn); 
                    obj.textvisibility('off');   % supress draw of text fields for speed reasons
                    pos = get(gca, 'CurrentPoint'); 
                    obj.startpos = pos(1,1:2);
                    anet.modified = true; 
                end
        end

        function draggingFcnRect(varargin)
        %% draggingFcnRect - callback
        % drags subtree to visually follow the mouse cursor
        % ToDo: implement length and constrict movements to radial
        %       distance
            obj = get(gco, 'UserData');  % this-pointer
            pos = get(gca, 'CurrentPoint');
            delta = obj.startpos - pos(1,1:2);
            move(obj, delta, 1); 
        end
        
        function stopDragFcn(varargin)
        %% stopDragFcn - callback
        %   ends dragging 
        %   updates position property of subtree
            obj = get(gco, 'UserData');
            set(gcf, 'WindowButtonMotionFcn', ''); 
            set(gcf, 'WindowButtonUpFcn', ''); 
            obj.textvisibility('on');
            obj.updatePosition;
        end
                
        %% callbacks fr context menus
        function cmECopy(varargin)
        %% cmECopy - implements edge context menu command 'Copy'
            obj = get(gco, 'UserData');
            Node.ClipEdge(obj); 
        end
        %% callbacks fr context menus
        function cmEPaste(varargin)
        %% cmECopy - implements edge context menu command 'Copy'
            obj = get(gco, 'UserData');
            Node.PasteEdge(obj); 
        end
        function cmCopy(varargin)
        %% cmCopy - implements node context menu command 'Copy'
            obj = get(gco, 'UserData');
            Node.Copy(obj); 
        end
        function cmRootPaste(varargin)
        %% cmPaste - implements root node's context menu command 'Paste'
        %   inserts copy of subtree in clipboard as new child of selected node
            obj = get(gco, 'UserData');    % new parent 
            if size(obj.children,2) == 0
                obj.cmPaste; 
            end
        end 
        function cmPaste(varargin)
        %% cmPaste - implements node context menu command 'Paste'
        %   inserts copy of subtree in clipboard as new child of selected node
            obj = get(gco, 'UserData');    % new parent 
            subtree = Node.Paste;        % get clone of subtree
            subtree.parent = obj; 
            obj.addChild(subtree); 
            subtree.addToFigure;
            delta = subtree.position - obj.position - [0.1 0]; 
            subtree.move(delta,1); 
            subtree.updatePosition; 
            Node.cmSort;
        end
        
        function cmCut(varargin)
        %% cmCut - implements node context menu command 'Cut'
        %   inserts copy of subtree in clipboard as new child of selected node
        %   deletes Subtree under node
            obj = get(gco, 'UserData');
            Node.Copy(obj); 
            obj.parent.deleteChild(obj); 
            Node.cmSort;
        end
        
        function cmDeleteSubtree(varargin)
        %% cmCut - implements node context menu command 'Delete Subtree'
        %   deletes Subtree under node
        %   doesn't change clipboard
            obj = get(gco, 'UserData');
            obj.parent.deleteChild(obj); 
            Node.cmSort;
        end
        
        function cmSort(varargin)
        %% cmSort - implements root node's context menu command 'Sort'
        %   sorts children recursively and validates tree
        %   renames children deep search left to right (clockwise)
            anet = get(gcf, 'UserData');
            anet.root.sort; 
            anet.root.rename(0); 
            anet.root.validate(anet); 
            anet.structureChanged; 
        end
        
        function cmValidate(varargin)
            anet = get(gcf, 'UserData');
            anet.root.validate(anet); 
        end
        
      
        function cmDelete(varargin)
        %% cmDelete - Deletes intermediate node
        %      ignores call, if node has no or more than one successor
        %      adopts parameter length
           obj = get(gco, 'UserData');  % node to be deleted
           if size(obj.children, 2) == 1
               c = obj.children(1);  
               c.l = c.l+obj.l;         % correct length parameter
               c.parent = obj.parent; 
               for i=1:size(c.parent.children, 2) % hook child to parent
                 if obj == c.parent.children(i)
                     c.parent.children(i) = c;
                     break; 
                 end
               end
               [x y] = c.pos2LinePos(c.position, obj.parent.position); 
               set(c.hLine, 'XData', x, 'YData', y);
               delete([obj.hNode, obj.hLine, obj.hText]);  
               obj.delete; 
               % ToDo: adopt l-Parameter 
               Node.cmSort;              %  calls anet.structureChanged; 
           end
        end
        function cmEdgeMenu(varargin)
             clip = Node.ClipEdge;
             val = 'on'; 
             if isempty(clip)
                 val = 'off';
             end
             ch = get(varargin{1}, 'Children'); 
             for i=1:length(ch)
                 cmd = get(ch(i), 'Label'); 
                 if strcmp(cmd, 'Paste')
                     set(ch(i), 'Enable', val); 
                 end
             end

        end
        function cmSplit(varargin)
        %% cmSplit - Inserts intermediate node
        %  
           obj = get(gco, 'UserData');
           node = Node;
           for i=1:size(obj.parent.children, 2)        % parent 
             if obj == obj.parent.children(i)
                 obj.parent.children(i) = node;
                 break; 
             end
           end
           node.position = obj.position + (obj.parent.position - obj.position) / 2; 
           node.parent = obj.parent; 
           node.addToFigure;       % no kids allowed
           anet = get(gcf,'UserData');
           node.addChild(obj); 
           obj.parent = node; 
           for i=1:size(Node.copyprops,2)
                node.(Node.copyprops{i}) = obj.(Node.copyprops{i});
           end
           obj.l = obj.l/2; 
           node.l = node.l/2; 
          
           [x y] = obj.pos2LinePos(obj.position, node.position); 
           set(obj.hLine, 'XData', x, 'YData', y);
           Node.cmSort;              %  calls anet.structureChanged; 
        end

        function cmAdd(varargin)
            obj = get(gco, 'UserData'); 
            node = Node(-1); 
            node.parent = obj; 
            obj.addChild(node); 
            node.position = obj.position + [0.1 0]; 
            for i=1:size(Node.copyprops,2)
                node.(Node.copyprops{i}) = obj.(Node.copyprops{i});
            end            
            node.addToFigure;       
            Node.cmSort;              %  calls anet.structureChanged; 
        end
    
        

    end
%% instance methods ===================================================      
% varargin(1) denotes object
 
    methods 
        function obj = Node(id)
        %% Node - Constructor, sets id, if given
            if nargin > 0
              obj.nodeId = id; 
            end
        end
        
        function addChild(obj, child)
        %% addChild - adds a subtree to the object
           [obj.children] = [obj.children  child]; 
        end
        
        function deleteChild(obj, child)
        %% deleteChild - deletes child in subtree of the object
        %     Parameter: child - child to be deleted
        newlist = []; 
        for i=1:size(obj.children, 2)        % 1: shorten list
            if child ~= obj.children(i)
               newlist = [newlist obj.children(i)];  %#ok<AGROW>
            else
               obj.children(i).delSubtreeGUI;   % 2. delete child's subtree
            end
        end
        obj.children = newlist; 
        end
        
        function delSubtreeGUI(obj)
        %% delSubtree - deletes GUI-Elements of subtree
            if ishandle(obj.hNode); 
                delete([obj.hNode, obj.hLine, obj.hText]); 
            end
            for i=1:size(obj.children, 2)    
                obj.children(i).delSubtreeGUI; 
            end
        end
        
        function validate(obj, anet)
           bool = false; 
           if obj.isRoot
                for i=1:length(Node.rootProps2Validate)
                       bool = bool + isempty(anet.(Node.rootProps2Validate{i})); 
                end
                if bool 
                    anet.validated = false; 
                    obj.color = 'red'; 
                else
                    anet.validated = true;   % an invalid Child revokes this!
                    obj.color = 'green';
                end
           elseif obj.isBoundary
                   % prefix '+' one of the tagged props must have value > 0 ; 
                   %        '~' prop must have value ~=[] ; 
                   %        ''  prop must have value > 0
                oneshot = true;     
                for i=1:length(Node.leaveProps2Validate)
                    prop = Node.leaveProps2Validate{i}; 
                    switch prop(1)
                        case '+'
                            val = obj.(prop(2:end)); 
                            if ~isempty(val)
                                oneshot = false; 
                            end
                        case '~'
                            val = obj.(prop(2:end)); 
                            bool = bool + isempty(val); 
                        otherwise
                            val = obj.(prop); 
                            bool = bool + (isempty(val) || val <= 0); 
                    end
                end
                bool = bool + oneshot; 
           else
                for i=1:length(Node.nodeProps2Validate)
                       bool = bool + isempty(obj.(Node.nodeProps2Validate{i})); 
                end
           end
           if bool 
               if nargin < 2
                    anet.validated = false; 
               end
               obj.color = 'red'; 
           else
                if isempty (obj.id)
                   obj.color = 'yellow';
                else
                    if obj.isBoundary
                        if ~isempty(obj.b_p)
                            obj.color = 'cyan';
                        else
                            obj.color = 'blue';
                        end
                    else
                        obj.color = 'green';
                    end
                end
           end
           set (obj.hNode, 'FaceColor', obj.color); 
           if nargin > 1  
               for i=1:size(obj.children, 2)
                 obj.children(i).validate(anet);     
               end
           end
        end

        function sort(obj)
        %% sort - sorts subtree using clock metric (0:00 is first elem)
        %  recursive!
        n = size(obj.children, 2); 
        if n > 0 
            polarr(n) = 0; 
        else
            polarr = []; 
        end
            for i=1:n    
                cart = obj.children(i).position - obj.position;  % angle
                pol = cart2pol(cart(2), -cart(1)); % rotate pi/2 + clockwise
                if pol < 0 
                  pol = 2 * pi + pol;   % treat negative values
                end
                polarr(i) = pol; 
            end
            [~, y] = sort(polarr);           % calc ordering
            obj.children = obj.children(y); % re-order
            
            for i=1:size(obj.children, 2)    % recursion
                obj.children(i).sort; 
            end
        end
        
        function nextid = rename(obj, id)
        %% rename - renames subtree using deep walk clockwise
        %  recursive!
        %  params: id = new id value for object
        %          nextid = new id value for next object
           obj.nodeId = id; 
           nextid = id + 1; 
           set (obj.hText, 'String', num2str(id)); 
           for i=1:size(obj.children, 2)    % recursion
               nextid = obj.children(i).rename(nextid); 
           end
            
        end
       
        function addToFigure(obj)
        %% addToFigure - constructs GUI-Representation of object
        % generates a rectangle, line and text GUI object 
        % links context menus to GUI objects
        % ToDo: connect context commands
            
            anet = get(gcf, 'UserData');  % use backpointer
            obj.hNode = rectangle('Position',[obj.position 2*obj.radius 2*obj.radius], ...
                'FaceColor', obj.color, 'UIContextMenu', anet.hNodeMenu, ...
                'ButtonDownFcn', @obj.startDragFcnRect, 'UserData', obj, 'LineWidth', 1, 'Curvature', [1 1]); 
            obj.hText = text(obj.position(1) + obj.radius, obj.position(2) + obj.radius, num2str(obj.nodeId), ...
                'FontSize', 8, 'VerticalAlignment', 'Middle', 'HorizontalAlignment', 'Center', ...
                'UIContextMenu', anet.hNodeMenu, 'ButtonDownFcn', @obj.startDragFcnRect, 'UserData', obj);  
            if obj.parent ~= 0
                [x y] = obj.pos2LinePos(obj.position, obj.parent.position); 
                obj.hLine = line(x, y, 'UIContextMenu', anet.hEdgeMenu, 'UserData', obj);
            else
                set(obj.hNode , 'UIContextMenu', anet.hRootMenu); 
                set(obj.hText, 'UIContextMenu', anet.hRootMenu); 
            end
            
            % children
            for i=1:size(obj.children, 2)
               obj.children(i).addToFigure;
            end
        end
        
        function unvisualize(this)
            if ~this.isRoot && ~isempty(this.startpos)
                this.position = this.startpos; 
                this.startpos = []; 
                [x y] = this.pos2LinePos(this.position, this.parent.position); 
                set(this.hNode, 'Position',[this.position 2*this.radius 2*this.radius], 'Visible', 'on'); 
                set(this.hText, 'Position',[this.position(1) + this.radius this.position(2) + this.radius 0]); 
                set(this.hLine, 'XData', x, 'YData', y, 'LineWidth', 0.5);
                if ishandle(this.hSelect) 
                    set(this.hSelect, 'Position',[this.position 2*this.radius 2*this.radius], 'Visible', 'on'); 
                    set(this.hSelectText, 'Position',[this.position(1) + this.radius this.position(2) + this.radius 0]); 
                    set(this.hSelectLine, 'XData', x, 'YData', y, 'LineWidth', 0.5);
                end
            end
            if ~isempty(this.children)
                for i=1 :length(this.children)
                    this.children(i).unvisualize; 
                end
            end
        end
        
        function visualize(this)
            if isempty(this.startpos)
                this.startpos = this.position; 
            end
            if ~this.isRoot
                d1 = this.startpos-this.parent.startpos; 
                dist = this.l * 1.5 * d1/norm(d1); 
                this.position = this.parent.position + dist; 
                [x y] = this.pos2LinePos(this.position, this.parent.position); 
                set(this.hNode, 'Position',[this.position 2*this.radius 2*this.radius], 'Visible', 'off'); 
                set(this.hText, 'Position', [this.position(1) + this.radius this.position(2) + this.radius 0]); 
                set(this.hLine, 'XData', x, 'YData', y, 'LineWidth', 400*this.d);
                if ishandle(this.hSelect) 
                    set(this.hSelect, 'Position',[this.position 2*this.radius 2*this.radius], 'Visible', 'off'); 
                    set(this.hSelectText, 'Position', [this.position(1) + this.radius this.position(2) + this.radius 0]); 
                    set(this.hSelectLine, 'XData', x, 'YData', y, 'LineWidth', 400*this.d);
                end

            end
            
            if ~isempty(this.children)
                for i=1 :length(this.children)
                    this.children(i).visualize; 
                end
            end
        end
        function add2Dlg(obj, hDlg, callback, toggleroot)
        % 'toggleroot' (optional) if given toggle also root
            
            toggleroot = nargin == 4; 
            if ~obj.selected
                col = 'w'; 
            else
                col = rand(1,3)*0.9; 
            end
            obj.hSelect = rectangle('Position',[obj.position 2*obj.radius 2*obj.radius], ...
                'FaceColor', col, 'LineWidth', 1, 'Curvature', [1 1], 'ButtonDownFcn', @toggleNode, 'UserData', obj);
            if obj.parent ~= 0
                [x y] = obj.pos2LinePos(obj.position, obj.parent.position); 
               obj.hSelectLine = line(x, y);
            end
            obj.hSelectText = text(obj.position(1) + obj.radius, obj.position(2) + obj.radius, num2str(obj.nodeId), ...
                'FontSize', 8, 'VerticalAlignment', 'Middle', 'HorizontalAlignment', 'Center', ...
                'ButtonDownFcn', @toggleNode, 'UserData', obj);  
            
            % children
            for i=1:size(obj.children, 2)
               obj.children(i).add2Dlg(hDlg, callback);
            end
            function toggleNode(varargin)
                if ~isempty(obj.parent) || toggleroot % exclude root
                    obj.Select;
                    if ~isempty(callback)
                        feval(callback, obj);
                    end
                end
            end
        end
        function Select(obj, value)
            if (nargin == 2)   % set
                obj.selected = value; 
            else               % toggle
                obj.selected = ~obj.selected; 
            end
            if obj.selected 
                col = rand(1,3)*0.9; 
            else
                col = 'w'; 
            end
            if ~isempty(obj.hSelect)
                set(obj.hSelect, 'FaceColor', col); 
            end
        end

        function updatePosition(obj)
        %% updatePosition - recursive 
        %   updates position properties of subtree with position of
        %   GUI-Elements in figure
           pos = get(obj.hNode, 'position'); 
           obj.position = pos(1:2); 
           obj.startpos = obj.position; 
           for i=1:size(obj.children, 2)
               obj.children(i).updatePosition; 
           end
        end
        
        function move(obj, delta, leading)
        %% move - moves GUI-elements of subtree in figure (recursive)
        %  Parameter: delta   - distance to move
        %             leading - 1 when root of subtree, 0 when child
           pos = get(obj.hNode, 'position'); 
           newposition = obj.position - delta; 
           pos = [newposition pos(3:4)]; 
           set(obj.hNode, 'position', pos);
           set(obj.hText, 'position', [newposition + [obj.radius obj.radius] 0]); 

           % parentline
           if obj.parent ~= 0
               if leading == 1
                 [x y] = obj.pos2LinePos(newposition, obj.parent.position); 
               else
                 [x y] = obj.pos2LinePos(newposition, obj.parent.position - delta); 
               end
                 set(obj.hLine, 'XData', x, 'YData', y);
           end
           
           % children
           for i=1:size(obj.children, 2)
               obj.children(i).move(delta, 0); 
           end
        end
        
        function textvisibility(obj, value)
        %% textvisibility - sets Visibility of GUI text objects in subtree
        %   recursive!
        %   value = 'on' or 'off'
            set( obj.hText,'Visible', value); 
           for i=1:size(obj.children, 2)
               obj.children(i).textvisibility(value); 
           end
        end
        
        function [x, y] = pos2LinePos (obj, a, b)
        %% pos2LinePos - calculate visible part of edge for line from node a to node b: a = [x1 y1], b = [x2 y2]
            z = a-b; 
            z = z/norm(z) * obj.radius*1.1; 
            offset = [obj.radius obj.radius]; 
            a = a - z + offset; 
            b = b + z + offset; 
            x = [a(1) b(1)]; 
            y = [a(2) b(2)]; 
        end
        
        function node = clone(obj, parent)
        % node.clone()        : clones subtree of node
        % child.clone(parent) : clones subtree of child and links it to parent
        %     !! Tricky recursive function!
        %     obj clones itself and then asks all children to clone
            node = Node(-1);
            node.position = obj.position; 
            % node.color = obj.color; 
            if nargin > 1
                parent.addChild(node); 
                node.parent = parent; 
            end
            for i=1:length(Node.props2Clone)
                node.(Node.props2Clone{i}) = obj.(Node.props2Clone{i});
            end
            
            for i=1:size(obj.children, 2)
                obj.children(i).clone(node); 
            end
            node.color = [0.5, 0.5, 0.5]; 
        end
   
        
        function [nodeArr spArr boundArr] = toArr(obj, na, sa, ba, parentidx)
        %% recursive top down treewalk 
        %  parameter: nodeArr - Node-pointers
        %                 adj - sparse arr: parent indices
            spArr = [sa parentidx]; 
            nodeArr = [na obj]; 
            idx = size(nodeArr, 2);       % get index of this node
            if isempty(obj.children)
                boundArr = [ba idx - 1]; 
            else
                boundArr = ba; 
            end
            for i=1:size(obj.children, 2)
                [nodeArr spArr boundArr] = obj.children(i).toArr(nodeArr, spArr, boundArr, idx);
            end
        end
        function bool = isRoot(obj)
        %% isRoot - true if object is root node    
            bool = isempty(obj.parent); 
        end
        function bool = isBoundary(this)
        %% isBoundary - true if object is boundary node    
           bool = ~this.isRoot && isempty(this.children);  
        end
        function obj = anet(~)
            obj = get(gcf, 'UserData'); 
        end
    end
    
end

