classdef ParamEditor < handle
    %% ParamEditor shows ParamSet values and handles editing
    %  Author: Rudolf Huttary 7.2011
    %   - Intended to be used by an ArteryNet object anet
    %   following logic is implemented:
    %   1. anet calls InitParamSetxxx method to prepare the panel with
    %      uicontrols. From here a recursive call to a generic routine
    %      populates the panel according to a sample structure
    %      and initializes a 'Userdata' context with appropriate field 
    %      (field name and type) information
    %   2. A node object calls the ArteryNet objects EditParams() method to show (and hide) its parameter panel 
    %   3. EditParams() calls Parameter.ReadxxxValues to update uicontrols
    %      values
    %   4. Any edit input calls ProcessEdit()
    %   5. ProcessEdit() sets field values directly or calls further property
    %      windows like BlobDialog
    %
    %  ParamEditor properties:
    %
    %  instance methods:
    %
    %  class methods:
    %       BlobDialog      - shows available CTS/RTS and details of current selection
    %       InitParamSetNet - initializes panel with 'typed' elements for editing
    %       parameterPanel  - constructs nested panel structure for editing struct values
    %       ProcessEdit     - (callback) processes input data into hPropEditorNode
    %       ReadPropValues  - manages panel position and visibilty and fills them with values

    % SISCA (Simulation, Identification and Statistical variation in Cardiovascular Analysis)
    %
    % Copyright (C) 2010 - 2018 FU Berlin (Mathematics, workgroup Prof. Christof Schütte)
    % Copyright (C) 2019 - 2020 THM Giessen (LSE, workgroup Prof. Stefan Bernhard)
    %
    % Author: Rudolf Huttary
    %
    % This program is free software: you can redistribute it and/or modify it under the terms 
    % of the GNU Affero General Public License Version 3 as published by the Free Software Foundation.
    %
    % This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
    % without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    % See the GNU Affero General Public License for more details.
    %
    % You should have received a copy of the GNU Affero General Public License along with this program. 
    % If not, see <http://www.gnu.org/licenses/>.


    
    properties
        
    end
    methods (Static)
        function [a b] = InitParamSetNet(anet)
            %% InitParamSetNet - initializes NetParam panel of root node
            % d is a template struct 
            %    - fieldname is field information for input processing 
            %    - value is type information for input processing 
            % callbacks processEdit and ReadValues will later interpret the following values as
            %    1 = num value
            %    11 = num vector and [] 
            %    12 = num value with %g formated
            %    2 = readonly num value
            %    3 = string
            %    4 = readonly string
            %    5 string = = combobox (string contains values)
            %    6 = bool value in combobox readonly
            %    7 = Blob

            d.netParam.n_id = 2; 
            d.netParam.n_paramSetId = 2; 
            d.netParam.n_name = 3; 
            d.netParam.n_description = 3; 
            d.netParam.n_type = ['5 ' anet.getComboTypesFromDb('NetType')];   % values for combofield
            d.netParam.n_eta = 1; 
            d.netParam.n_rho = 1; 
            d.netParam.n_gamma = 1; 
            d.netParam.n_alpha = 11; 
            d.netParam.n_nu = 11; 
            d.netParam.n_ctsId = 7;  % Blobdialog to load CTS ! has side effect on heartRate !
            d.netParam.n_heartRate = 12; 
            
            [a,b] = ParamEditor.parameterPanel(anet.hFig, d, 'root', 'node', 80, 100, 25, 5, 100, 10, @ParamEditor.ProcessEdit); 
        end
        
        function [a b] = InitParamSetNode(anet)
            %% InitParamSetNode - initializes panel with 'typed' elements for editing
            %  !!! tricky
            % d is a sample struct 
            %    - fieldname is field information for input processing 
            %    - value is type information for input processing 
            % callback will interpret the following values as
            %    1 = num value
            %    11 = num value and [] 
            %    2 = readonly num value
            %    3 = string
            %    4 = readonly string
            %    5 string = = combobox (string contains values)
            %    6 = bool value in check readonly
            %    7 = Blob
            %    8 = CtsId -> angiodb.CharacteristicTimeSeries

            d.nodeParam.id = 2;     % structure for generic PanelEditor
            d.nodeParam.paramSetId = 2; 
            d.nodeParam.nodeId = 2; 
            d.nodeParam.name = 3; 
            d.nodeParam.description = 3; 
            d.nodeParam.type = ['5 ' anet.getComboTypesFromDb('NodeType')];   % values for combofield
            d.nodeParam.E = 1; 
            d.nodeParam.l = 1; 
            d.nodeParam.d = 1; 
            d.nodeParam.h = 1; 
            d.nodeParam.R = 2; 
            d.nodeParam.C = 2; 
            d.nodeParam.L = 2; 
            d.boundaryParam.b_id = 2; 
            d.boundaryParam.b_paramSetId = 2; 
            d.boundaryParam.b_name = 3; 
            d.boundaryParam.b_description = 3; 
            d.boundaryParam.b_R = 1;
            d.boundaryParam.b_C = 1;
            d.boundaryParam.b_L = 11;
            d.boundaryParam.b_p = 1;
            d.boundaryParam.b_q = 1;
            
            [a,b] = ParamEditor.parameterPanel(anet.hFig, d, 'node', 'node', 80, 100, 25, 5, 100, 10, @ParamEditor.ProcessEdit); 
        end
        
        function ReadPropValues(anet, node, ~)
        %%  ReadPropValues - manages panel position and visibilty and fills them with values
            
            switch nargin
                case 1 % net
                    p = get(anet.hPropEditorNet, 'Children'); 
                    node = anet;
                    panels = 1; 
                case 2 % only node
                    UpdateNodeRCL(anet, node); 
                    p = get(anet.hPropEditorNode, 'Children'); 
                    p0 = get(anet.hPropEditorNode, 'Position');
                    p1 = get(p(1), 'Position');
                    p2 = get(p(2), 'Position');
                    panels = 1;
                    if p0(4) > p1(4) + 30
                        p0(4) = p1(4)+30; 
                        p0(2) = p0(2)+p2(4); 
                        p1(2) = 5;  
                        set (p(1), 'Position', p1);
                        set (anet.hPropEditorNode, 'Position', p0); 
                        set (p(2), 'Visible', 'off');
                    end
                case 3  % node and boundary
                    UpdateNodeRCL(anet, node); 
                    p = get(anet.hPropEditorNode, 'Children'); 
                    p0 = get(anet.hPropEditorNode, 'Position');
                    panels = 2;
                    p1 = get(p(1), 'Position');
                    p2 = get(p(2), 'Position');
                    if p0(4) < p1(4) + 40
                        p0(4) = p1(4)+p2(4) + 30; 
                        p0(2) = p0(2)- p2(4); 
                        p1(2) = p2(4) + 5;  
                        set (p(1), 'Position', p1);
                        set (anet.hPropEditorNode, 'Position', p0); 
                        set (p(2), 'Visible', 'on');
                    end
            end
            for j=1:panels    % all panels
                ch = get(p(j), 'Children'); 
                doPanel;
            end
            
            function doPanel  
            %% doPanel populates panel with node param values
                for i=1:size(ch, 1)
                    ud = get(ch(i), 'UserData');
                    if ~isempty(ud)
                        f = ud{1}; 
                        switch ud{2}
                            case {1 2 7 8 9}
                                set(ch(i), 'String', '');   % number
                                if ~isempty( node.(f)) 
                                    v = node.(f); 
                                    if isa(v, 'double') && mod(v, 1) ~= 0 || abs(v) > 1e4
                                        set(ch(i), 'String', sprintf('%0.3e ', (node.(f))));  % scientific notation
                                    else
                                        set(ch(i), 'String', num2str(node.(f))); 
                                    end
                                end
                            case 11
                                set(ch(i), 'String', '');   % number
                                if ~isempty( node.(f)) 
                                    if isa(node.(f), 'double')
                                        set(ch(i), 'String', sprintf('%.4g ', (node.(f))));  % float
                                    else
                                        set(ch(i), 'String', num2str(node.(f))); 
                                    end
                                end
                            case 12
                                set(ch(i), 'String', '');   % number
                                if ~isempty( node.(f)) 
                                        set(ch(i), 'String', sprintf('%.4g', (node.(f))));  
                                end

                            case {3 4}                      % string
                                set(ch(i), 'String', ''); 
                                if ~isempty(node.(f)) 
                                    set(ch(i), 'String', node.(f)); 
                                end
                            case {5 6}
                                set(ch(i), 'Value', node.(f) ); 
                            case {10 }  
                                    set(ch(i), 'Style', 'Edit');
                                if isempty(node.(f)) 
                                    value = ''; 
                                elseif size(node.(f),1) + size(node.(f),2) == 2 
                                    value = num2str(node.(f)); 
                                else
                                    value = 'Blob';
                                    set(ch(i), 'Style', 'Pushbutton');
                                end
                                set(ch(i), 'String', value);
                        end
                    end
                end
            end
        end
 
        function ProcessEdit(varargin)
        %% ProcessEdit - (callback) processes input data into hPropEditorNode
            anet = get(gcf, 'UserData');      
            %  !!! tricky code:  part of statement originates from UserData
            context = get(varargin{1}, 'UserData');   % prepared by parameterPanel()
            field = context{1};
            ctrltype = context{2};
            value = get(varargin{1}, 'String');
            switch ctrltype
                case {1 12}   % num 
                    value = str2num(value);  
                    if isempty(value) % validate number
                        if strcmp(field, 'b_p') || strcmp(field, 'b_q')  % special case
                            anet.editObject.(field) = '';
                        else
                            set(gco, 'String', num2str(anet.editObject.(field))); 
                            uicontrol(gco); 
                            beep; 
                            return; 
                        end
                    else
                        anet.editObject.(field) = value(1,1); % truncate vectors
                        if isa(anet.editObject.(field), 'double')
                            if ctrltype == 1
                                set(gco, 'String', sprintf('%.2e ', anet.editObject.(field))); 
                            else
                                set(gco, 'String', sprintf('%.4g', anet.editObject.(field))); 
                            end
                        end
                    end
                case 11   % num > 0 + []
                    value = str2num(value);  
                    if isempty(value) % validate number
                            anet.editObject.(field) = [];
                    elseif sum(value <= 0)
                            set(gco, 'String', num2str(anet.editObject.(field))); 
                            uicontrol(gco); 
                            beep; 
                            return; 
                    else
                        anet.editObject.(field) = value; % vector possible
                        if isa(anet.editObject.(field), 'double')
                            set(gco, 'String', sprintf('%g ', anet.editObject.(field)));  % adopt notation 
                        end
                    end
                case {3}   % string
                    anet.editObject.(field) = value; 
                case {5 6}   % combo
                    anet.editObject.(field) = get(gco, 'Value'); 
                    if strcmp(field, 'type') && anet.editObject.parent.isRoot  % hack ... 
                        anet.n_ctsId = []; 
                    end
                case {7}  % Pushbutton, selects Blob from DB, deep walk possible
                        heartRate = ParamEditor.BlobDialog(anet, 'Load inbound CTS ', anet.editObject.(field), true); 
                        if heartRate
                            set (gco, 'String', num2str(anet.editObject.(field))); 
                            p = get(get(gco, 'Parent'), 'Children'); 
                            pos = find(p == gco); 
                            set (p(pos+2), 'String', num2str(heartRate));   % side-effect !!
                        else
                            return;   % cancel
                        end
                case {8}  % Pushbutton, Dialog shows infos about CTS, deep walk possible
                        ParamEditor.BlobDialog(anet, 'Details on Time Series', anet.editObject.(field), false);
            end
            if isa(anet.editObject, 'node')
             UpdateNodeRCL(anet, anet.editObject); 
            end
            anet.p_modified = true; 
        end
        
        function [hPanel myheight] = parameterPanel(hControl, Struct, name, fullname, textWidth, editWidth, lineHeight, canvas, x, yPos, cb)
            %% parameterPanel - constructs nested panel structure for editing struct values
            %    gives struct names as panels, and field names as
            %    combination of text and edit field
            %    recursive function! 
            % Parameters: 
            %   - out     hPanel     - handle of outmost panel
            %             myheight   - height of outmost panel
            %
            %   - in      hControl   - handle of parent/owner
            %             Struct     - Example of nested structure
            %             name       - name of this field
            %             fullname   - full qualified field name
            %             textWidth  - Width in pixels for static text field
            %             editWidth  - Width in pixels for edit field
            %             lineHeight - Height in pixels for fields
            %             canvas     - field border in pixels 
            %             x          - x-position of Element
            %             yPos       - y-position of Element
            %             cb         - callback evaluating edits
            % 
            %    example [a,b] = ParamEditor.rad(hFig, mystruct, 'mystruct', 'fullname.mystruct', 40, 100, 20, 5, 100, 10, @myCallback); 

            if ~isstruct(Struct) % end node -> combination of text and edit field
             % static text
              name1 = name;
              if length(name) > 2
                  if strcmp(name(2), '_')
                      name1 = name1(3:end); 
                  end
              end
              uicontrol('Parent', hControl, 'Style', 'Text', 'FontSize', 8, 'HorizontalAlignment', 'right', 'String', [name1 ' '], ...
                  'Position', [1 yPos-5 textWidth lineHeight], 'Callback', cb); 

             % input control
              comboList = ''; 
              if ischar(Struct)
                  pos = strfind(Struct, ' '); 
                  comboList = Struct(pos+1:size(Struct,2)); 
                  Struct = str2num(Struct(1:2));  %#ok<*ST2NM>
              end
              value = {name Struct}; 
              switch Struct
                  case {1, 11, 12, 3} % num + string value
                      uicontrol(hControl, 'Style', 'Edit', 'FontSize', 8, 'HorizontalAlignment', 'left', 'String', value{1}, ...
                        'Position', [textWidth+canvas yPos editWidth-canvas lineHeight], 'UserData', value, 'Callback', cb);
                  case {2, 4} % num + string value readonly
                      uicontrol(hControl, 'Style', 'Edit', 'FontSize', 8, 'HorizontalAlignment', 'left', 'String', value{1}, 'Enable', 'off', ...
                        'Position', [textWidth+canvas yPos editWidth-canvas lineHeight], 'UserData', value, 'Callback', cb);
                  case {5} % combobox
                      uicontrol(hControl, 'Style', 'Popupmenu', 'FontSize', 8, 'HorizontalAlignment', 'left', 'String', comboList, ...
                        'Position', [textWidth+canvas yPos editWidth-canvas lineHeight], 'UserData', value, 'Callback', cb);
                  case {6} % checkbox
                      uicontrol(hControl, 'Style', 'Checkbox', 'FontSize', 8, 'HorizontalAlignment', 'left', ...
                        'Position', [textWidth+canvas yPos editWidth-canvas lineHeight], 'UserData', value, 'Callback', cb, 'Enable', 'off');
                  case {7 8 9} % Button
                      uicontrol(hControl, 'Style', 'Pushbutton', 'FontSize', 8, 'HorizontalAlignment', 'left', ...
                        'Position', [textWidth+canvas yPos editWidth-canvas lineHeight], 'UserData', value, 'Callback', cb);
              end
              
              myheight = lineHeight; 
              hPanel = hControl; 
            else % struct -> Panel
              % panel
              hPanel = uipanel('Parent', hControl, 'Title', name, 'Units', 'Pixels');
              f = fieldnames(Struct); 
              height = canvas; 
              for i=size(f, 1):-1:1
              value = {fullname f(i)}; 
                    [~, hei] = ParamEditor.parameterPanel(hPanel, eval(['Struct.' char(f(i))]), char(f(i)), value{1}, ...
                      textWidth-2, editWidth-1, lineHeight, canvas, 2, height, cb); 
                  height = height + hei; 
              end
              myheight = height + lineHeight; 
              set(hPanel, 'Position', [x yPos (editWidth + textWidth + canvas) myheight]); 
            end
        end
        
        function heartRate = BlobDialog(anet, DlgTitle, currid, inbound)
        %% BlobDialog - shows available CTS/RTS and details of current selection
        % >>  heartRate = BlobDialog(anet, DlgTitle, currid, inbound)
        %    'anet'     - ArteryNet object being edited
        %    'DlgTitle' - description string
        %    'currid'   - db id of RTS/CTS to be initially shown in graph 
        %    'inbound'  - true if blob is used for root node
        %    'heartRate' - heart rate calulated from blob data
        
           if isempty(currid) 
              id = 0; 
           else
              id = currid; 
           end
            scrsz = get(0,'ScreenSize'); 
            mp = get(0, 'MonitorPositions'); 
            if size(mp,1) > 1
                scrf = scrsz(3) + (scrsz(3)-1000)/2; 
            else
                scrf = (scrsz(3)-1000)/2; 
            end

            position = [scrf, (scrsz(4)-600)/2, 1000, 600]; 
            hDlg = figure('Name', DlgTitle, 'NumberTitle', 'off', 'MenuBar','none', 'ToolBar','none', ...
                'Color', 'w', 'Position', position, 'Resize', 'off', 'WindowStyle', 'modal', 'DeleteFcn', @cbCancel);
            axes('Color', 'w', 'Position', [0.64 0.16 0.35 0.79], 'Box', 'on');
            
            % buttons  
                uicontrol(hDlg, 'Style', 'Pushbutton', 'FontSize', 8, 'HorizontalAlignment', 'right', 'String', 'Cancel', ...
                  'Position', [920 20 60, 30], 'Callback', @cbCancel); 
            if inbound 
                choose = uicontrol(hDlg, 'Style', 'Pushbutton', 'FontSize', 8, 'HorizontalAlignment', 'right', 'String', 'Choose', ...
                  'Position', [840 20 60, 30], 'Callback', @cbChoose, 'Enable', 'off'); 
            end
            anet.db.open; 
            if strcmp(id, '') 
               id = 0;
            end
            % populate 1st Table
            if inbound % root node
                command = ['select CTS.id, signalName, description, rate, TSType.typeName as type, lastmodified from CTS join TSType on type = TSType.id order by CTS.id'];
                [headers dbcontent] = anet.db.select1(command);
                if size(dbcontent, 2) == 1
                    dbcontent = cell(size(headers)); 
                end
                table = javahandle_withcallbacks.javax.swing.JTable(dbcontent, headers); % R2014a/R2015a fixed
%                warning('off', 'MATLAB:hg:JavaSetHGProperty'); 
                if ~strcmp(dbcontent{1}, '')
                    set(table, 'MouseMovedCallback', {@pDlgMouseMoved 0});  % 
                    set(table,'MousePressedCallback',@cbSelect);
                    set(table,'KeyPressedCallback',@cbSelect);
                    set(table.getDefaultEditor(table.getColumnClass(0)),'ClickCountToStart', 100) ; 
                end
            else   % other nodes
                [headers dbcontent] = anet.db.select1(['select * from viewCTS where id = ' num2str(id)]);
                if isempty(dbcontent{1})
                    dbcontent(1:size(headers,2)) = {''};
                end
                dbcontent = [headers' dbcontent'];
                headers = {'CTS.field' 'Value'}; 
                table = javahandle_withcallbacks.javax.swing.JTable(dbcontent, headers); % R2014a/R2015a fixed
                set(table, 'MouseMovedCallback', {@pDlgMouseMoved 0});  % 
                set(table,'MousePressedCallback',{@cbMore 0});
            end
            set(hDlg, 'UserData', table); 
            table.setSelectionMode(0); 
            cm = table.getColumnModel; 
            if(cm.getColumnCount > 4)
              w = cm.getTotalColumnWidth;
              cm.getColumn(0).setPreferredWidth(0.05*w); 
              cm.getColumn(1).setPreferredWidth(0.15*w); 
              cm.getColumn(2).setPreferredWidth(0.5*w); 
              cm.getColumn(3).setPreferredWidth(0.1*w); 
              cm.getColumn(4).setPreferredWidth(0.07*w); 
            end

            ScrollPane = javahandle_withcallbacks.javax.swing.JScrollPane(table);
            btLinePropsPos = [[0.02 0.50],0.57,0.45];
            [jc,btContainer] = javacomponent(ScrollPane,[0 0 1 1], hDlg); 
            set(btContainer, 'Units', 'Norm', 'Position', btLinePropsPos);
            set(ScrollPane, 'MouseMovedCallback', @pDlgMouseMoved);  % 
%            warning('off', 'MATLAB:hg:JavaSetHGProperty'); 
%             lh = addlistener(ScrollPane, 'MouseMoved', @pDlgMouseMoved); 

            
            if inbound
                % select current id in 1st table
                for i=0:table.getRowCount-1
                    if id == table.getModel.getValueAt(i,0)
                        table.changeSelection(i, 1, true,false); 
                        break; 
                    end
                end
            % populate 2nd Table

                [headers dbcontent] = anet.db.select1(['select * from viewCTS where id = ' num2str(id)]);
                if isempty(dbcontent{1})
                    dbcontent(1:size(headers,2)) = {''};
                end
                dbcontent = [headers' dbcontent'];
                headers = {'CTS.field' 'Value'}; 
            else
                [headers dbcontent] = anet.db.select1(['select * from viewRTS where id = ' num2str(id)]);
                dbcontent(1:size(headers,2)) = {''};
                dbcontent = [headers' dbcontent'];
                headers = {'RTS.field' 'Value'}; 
            end
            table1 = javahandle_withcallbacks.javax.swing.JTable(dbcontent, headers); % R2014a/R2015a fixed 
              set(table1, 'MouseMovedCallback', {@pDlgMouseMoved 1});  % for tooltip table 2
              set(table1,'MousePressedCallback',{@cbMore 1});
            table1.setSelectionMode(0); 
            columnmetrik(table1.getColumnModel); 
            ScrollPane = javax.swing.JScrollPane(table1);
            btLinePropsPos = [[0.02 0.1],0.57,0.40];
            [jc,btContainer] = javacomponent(ScrollPane,[0 0 1 1], hDlg); 
            set(btContainer, 'Units', 'Norm', 'Position', btLinePropsPos);
            cbSelect; 

            blob = [];
            type = 1; 
            uiwait(hDlg); 
            
            function columnmetrik(cm)
                if(cm.getColumnCount == 2)
                   w = cm.getTotalColumnWidth;
                   cm.getColumn(0).setPreferredWidth(0.2*w); 
                   cm.getColumn(1).setPreferredWidth(0.8*w); 
                end
            end
            
            
            function pDlgMouseMoved(varargin)
            %% pDlgMouseMoved - maintains tooltip for table in Dlg
            % expects table in 'UserData' of figure
                if nargin < 3 
                    return; 
                end
                if varargin{3}
                    tab = table1;
                else
                    tab = table; 
                end

                event = varargin{2};
                row = tab.rowAtPoint(event.getPoint); 
                col = tab.columnAtPoint(event.getPoint);
                value = tab.getModel.getValueAt(row,col);
                if ~isempty(value) && ~isa(value, 'unit8')
                    if isjava(value)
                        value = value.toString;
                    elseif isnumeric(value)
                        value = num2str(value);
                    end
                else
                    value = '';
                end
                j = 1;
                while j * 100 < size(value, 2)
                    value = sprintf('%s<br/>%s',value(1:j*100), value(j*100+1:size(value, 2)));
                    j = j+1;
                end
                tab.setToolTipText(['<html> ' char(value)]);
            end
            function cbChoose(varargin)
            %% cbOk - callback, handles Ok button
            %   closes db connection and destroys figure, without further action
                anet.n_heartRate = heartRate; 
                anet.n_ctsId = id; 
                delete(gcbf);  % triggers cbCancel because of DeleteFcn
                heartRate = anet.n_heartRate;
            end
            
            function cbCancel(varargin)
            %% cbOk - callback, handles Ok button
            %   closes db connection and destroys figure, without further action
                heartRate = 0; 
                delete(gcbf); 
            end
            
            function cbSelect(varargin)
            %% cbSelect - callback, populates Graph 
            %   closes db connection and destroys figure, without further action
                clicks = 0; 
                if size(varargin,2) == 2  % selection by double click
                    ev = varargin{1,2}; 
                    clicks = ev.getClickCount; 
                    if clicks > 1
                        cbOk; 
                        return;
                    end
                end
                r = table.getSelectedRow;
                if r >=0
                    set (choose, 'Enable', 'on'); 
                    id = table.getModel.getValueAt(r,0);         
                    [headers1 dbcontent1] = anet.db.select1(['select * from viewCTS where id = ' num2str(id)]);
                    dbcontent1 = [headers1' dbcontent1'];
                    headers1 = {'CTS.field' 'Value'}; 
                    unit = '';
                    rate = 1; 
                    for j=1:size(dbcontent1,1)
                        if strcmp(dbcontent1(j,1), 'unit')
                            unit = char(dbcontent1(j,2)); 
                        end
                        if strcmp(dbcontent1(j,1), 'rate')
                            rate = double(cell2mat(dbcontent1(j,2)));
                        end
                    end

                    tm = javax.swing.table.DefaultTableModel(dbcontent1, headers1); 
                    table1.setModel(tm); 
                    columnmetrik(table1.getColumnModel); 
                    res = anet.db.select(['select data, type from CTS where id = ' num2str(id)]);
                    blob = typecast(res.data, 'double'); 
                    if strcmp(unit, 'Pa')
                        blob = blob./133; 
                        unit = 'mmHg'; 
                    end
                    type = res.type; 
                    inc = 1/rate;
                    heartRate = double(rate)/length(blob) * 60;
                    x = 0.0: inc: length(blob)/rate-inc;
                    plot(gca, x, blob, 'black'); 
                    hr = sprintf('%g bpm\n%.2g %s', heartRate, mean(blob), unit); 
                    legend (hr); 
                    xlabel(gca,'time [s]');
                    ylabel(gca, unit); 
                end
                
            end
            
            function cbMore(varargin)
            %% cbMore - callback, generic table walk 
            %   closes db connection and destroys figure, without further action
            if length(varargin) < 3
                return; 
            end
            if varargin{3}
                tab = table1;
            else
                tab = table; 
            end
            
                r = tab.getSelectedRow;
                if r >=0
                    t = tab.getModel.getValueAt(r, 0); 
                    currPid = tab.getModel.getValueAt(r, 1); 
                    if isempty(currPid) || strcmp(currPid, '')  
                        currPid = 0;
                    end
                    pos = strfind(upper(t), 'ID'); 
                    if ~isempty(pos)  && pos > 1 % foreign key?
                        t = t(1:pos-1);
                        t(1) = upper(t(1)); 
                        [header dbcontent] = anet.db.select1(['select * from ' t ' where id = ' num2str(currPid)]); 
                        if isempty(dbcontent{1})
                            dbcontent(1:size(header,2)) = {''};
                        end
                        dbcontent = [header' dbcontent'];
                        headers = {[t '.field'] 'Value'}; 
                        tm = javax.swing.table.DefaultTableModel(dbcontent, headers); 
                        table1.setModel(tm); 
                   end
                end
            end
            
        end
    end
end
