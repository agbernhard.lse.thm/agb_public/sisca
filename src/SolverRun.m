classdef SolverRun < DbObj
    %% SolverRun implements forward simulations for several windkessel models and boundary szenarios
    %     - operates over a fully parameterized (and previously saved) ArteryNet object
    %     - runs are first temporary
    %     - runs are saved in table "SolverRun", requiring saved
    %       ArteryNet/ParamSet
    %     - please note the side effect:
    %            Any ParamSet Save operation will delete all Solutions for this ParamSet in DB
    %
    %  SolverRun officially has no public interface!
    %  Scripting should be done by means of the class ArteryNet using
    %    >> anet = ArteryNet.FromSolverRunId;
    %    >> sr = anet.getSolverRun;
    %    >> ...
    %    >> anet.setSolverRun(sr);
    %    >> anet.RunSolver;
    %    >> anet.SolverRunSave
    %
    % SolverRun Properties:
    %   constant props:
    %         knownSensPar = 'R|C|L|b_R|b_p|b_q';       % to be extended for further implementation
    %         knownSensMethods = 'none|fsa|asa';        % to be extended for further implementation
    %   dynamic props:
    %         id, name, description (inherited)
    %         paramSetId
    %         solver = 10;
    %         rate = 1000;
    %         t_end = 3;
    %         starttime = 0
    %         duration
    %         relTol = 1e-6;
    %         absTol = 1e-8;
    %         maxNumSteps = 1000;
    %         sensMtd = 'none';
    %         outMon = false;
    %         nonlinear = false; 
    %         sensPar = 'R';
    %         parScale = [];
    %         plist = 1;       % Liste der Knoten f???r die Sensitivit???t berechnet wird
    %         showlist = []    % Liste der Knoten die im Multiselector Dialog angezeigt werden
    %         lastmodified
    %         solution = [];
    %         solvers = [];
    %         saved = true;
    %    
    % class methods: 
    %    OpenSolverRunDlg - select a solution for a given ParamSet stored in the database
    %     NewSolverRunDlg - shows Dialog allowing to choose parameters for new solver run   
    %
    % instance methods:    
    %     (ctor)            - constructor allows reading stored solutions from database
    %     Open              - reads a SolverRun state from db into the object this
    %     parScale          - calculates mean value of a sensitivity parameter over all nodes
    %     plotSensitivity   - opens a Dialog with sensitivity plots for selected nodes
    %     Save              - saves SolverRun instance + last period of STS, SensTS to DB
    %     solve             - common entry point for solver run calls, selecting specific solveXXX call
    %     solveDiscretized  - solver using the common discretization scheme x = AD*x+BD*u
    %     solveLSIM         - application of LTI solver LSIM
    %     solveLSIM_nl      - application of LTI solver LSIM in a non-linear framework    
    %     solveMatlab       - common entrypoint for Matlab solver runs    
    %     solveSundails     - interfaces to Sundails CVode solver 
    
    % SISCA (Simulation, Identification and Statistical variation in Cardiovascular Analysis)
    %
    % Copyright (C) 2010 - 2018 FU Berlin (Mathematics, workgroup Prof. Christof Schütte)
    % Copyright (C) 2019 - 2020 THM Giessen (LSE, workgroup Prof. Stefan Bernhard)
    %
    % Author: Rudolf Huttary
    %
    % This program is free software: you can redistribute it and/or modify it under the terms 
    % of the GNU Affero General Public License Version 3 as published by the Free Software Foundation.
    %
    % This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
    % without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    % See the GNU Affero General Public License for more details.
    %
    % You should have received a copy of the GNU Affero General Public License along with this program. 
    % If not, see <http://www.gnu.org/licenses/>.
    
    properties (Constant)
        knownSensPar = 'R|C|L|b_R|b_p|b_q';                   % to be extended for further implementation
        knownSensMethods = 'none|fsa|asa';        % to be extended for further implementation
    end
    properties
        % inherited: id, name, description
        paramSetId
        solver = 12;
        rate = 1000;
        t_end = 3;
        starttime = 0
        duration
        relTol = 1e-6;
        absTol = 1e-8;
        maxNumSteps = 1000;
        sensMtd = 'none';
        outMon = false;
        nonlinear = false; 
        sensPar = 'R';
        parScale = [];
        plist = 1;       % Liste der Knoten f???r die Sensitivit???t berechnet wird
        showlist = []    % Liste der Knoten die im Multiselector Dialog angezeigt werden
        lastmodified
        solution = [];
        solvers = [];
        saved = true;
    end
    
    methods
        function this = SolverRun(db, paramSetid, id)
            % SolverRun - constructor allows reading stored solutions from database
            %   >> s = SolverRun;              % default instance
            %   >> s = SolverRun(db, 12);      % read SolverRun object defined by ParamSet.currentSolverRun from AngioDB.SolverRun
            %   >> s = SolverRun(db, 12, 17);  % read SolverRun object 17 from AngioDB.SolverRun
            %       - 'db' (optional, default: = AngioDB) is an existing AngioDB object
            %       - 'paramSetid' (optional, default: = currentParamSet, currentSolverRun) load SolverRun by paramsetId
            %       - 'id' (optional, default: = currentSolverRun) Load SolverRun by paramsetId and id
            this.name = 'SR'; 
            if nargin == 1  % special call!
                this.paramSetId = db;
            elseif nargin == 2 && isa(db, 'AngioDB')
                db.open;
                sid = db.select(['select currentSolverRun from ParamSet where id = ' num2str(paramSetid)]);
                if ~isempty(sid)
                    if sid.currentSolverRun > 0
                        this.Open(db, sid.currentSolverRun);
                    end
                end
                this.paramSetId = paramSetid;
            elseif nargin == 2 && ~isa(db, 'AngioDB')
                id = paramSetid; 
                paramSetid = db; 
                db = AngioDB; 
                if ~this.Open(db, id)
                    error('SolverRun.ctor: no solution with id = %i found', id); 
                end
                if this.paramSetId ~= paramSetid
                    error('SolverRun.ctor: paramSet/solution mismatch'); 
                end

            elseif nargin == 3 && id > 0    % id denotes id in table AngioDB.SolverRun
                this.Open(db, id);
                this.paramSetId = paramSetid;
            end
        end
        function solve(this, anet, cbProgress)
        %% solve  - common entry point for solver run calls, selecting specific solveXXX call
        %   >>  this.solve(anet, cbProgress);
        % PARAMETER: solve(this, anet, cbProgress)
        %    - 'anet' instance of type ArteryNet
        %    - 'cbProgress' callback function to control solver loop and indicate Progress
            if isempty(this.parScale)
                this.parScale = this.calcParScale(anet);
            end
            
            switch this.solver
                case {1 2 3 4 5 6 7 8} % ODExxx
                    this.solveMatlab(anet, cbProgress);
                case 9                 % SIM
                    if this.nonlinear
                        this.solveSim_nl(anet, cbProgress);
                    else
                        this.solveSim(anet, cbProgress);
                    end
                case 10                % CVODES
                    this.solveSundails(anet, cbProgress);
                case 11
                    this.solveDiscretized(anet, cbProgress); 
                case 12                % LTI-System 
                    if this.nonlinear
                        this.solveLSIM_nl(anet, cbProgress); 
                    else
                        this.solveLSIM(anet, cbProgress); 
                    end
            end
        end
        function solveLSIM_nl(this,anet, cbProgress)
        %% solveLSIM_nl - application of LTI solver LSIM in a non-linear framework
        %                 uses control system toolbox 
        % PARAMETER: solveLSIM_nl(this, anet, cbProgress)
        %    - 'anet' instance of type ArteryNet
        %    - 'cbProgress' callback function to control solver loop and indicate Progress
            tcpu = cputime;             % starting time of cpu
            deltat = 1.0/this.rate;                   % time step width
            T = 0:deltat:this.t_end-deltat;
            N = length(T);
            % generate state-space maodel
            ssM = SSModel(anet); 
            x0 = ssM.x0(:);             % Make a column vector.
            
            %% create periodic time domain input
            y = x0'; 
            y(N, ssM.Nx) = 0; 
            x = x0; 
            u0 = ssM.U(0);
            for i=2:N
                u1 = ssM.U(T(i));
                [A, B] = ZimEstimator.NonLin_model(anet.RCL, x, anet.id); 
                sys = ss(A, B, ssM.C, ssM.D); 
                [x, ~] = lsim(sys, [u0, u1], [0, deltat], x);
                x = x(2,:)';
                y(i, :) = x'; 
                u0 = u1; 
                %% progress
                if ~mod(i, round(4*sqrt(i+1)))
                    if ~feval(cbProgress, i, N)  % progress + cancel
                        return; 
                    end
                end
            end
            Y = ssM.normalizeSolution(y, T');  % ToDo:  Y = ss.C*X+ss.D*u
            % run was successful; prepare solution in structure
            this.solution = struct('time', T, 'Y', Y, 'X', y, 'out_rate', this.rate, 'Ys', [], 'tsol', cputime-tcpu, 'duration', ssM.period);
            
        end
        function solveLSIM(this,anet, ~)
        %% solveLSIM - application of LTI solver LSIM
        %              uses control system toolbox 
        % PARAMETER: solveLSIM(this, anet)
        %    - 'anet' instance of type ArteryNet
            tcpu = cputime;             % starting time of cpu
            deltat = 1.0/this.rate;                   % time step width
            T = 0:deltat:this.t_end-deltat;
            N = length(T);
            
            % generate state-space maodel
            ssM = SSModel(anet); 
            x0 = ssM.x0(:);             % Make a column vector.
            
            %% create periodic time domain input
            u=ssM.U(T)';
            sys = ss(ssM.A, ssM.B, ssM.C, ssM.D); 
            %% Simulate the output using the model
            [y, ~] = lsim(sys, u, T, x0);
            Y = ssM.normalizeSolution(y, T');  % ToDo:  Y = ss.C*X+ss.D*u
            % run was successful; prepare solution in structure
            this.solution = struct('time', T, 'Y', Y, 'X', y, 'out_rate', this.rate, 'Ys', [], 'tsol', cputime-tcpu, 'duration', ssM.period);
            

         end
        function solveSim(this, anet, ~)
        %% solveSIM - application of SIM solver 
        %             uses signal processing toolbox 
        % PARAMETER: solveSim(this, anet)
        %    - 'anet' instance of type ArteryNet
            tcpu = cputime;             % starting time of cpu
            deltat = 1.0/this.rate;                   % time step width
            T = 0:deltat:this.t_end-deltat;
            N = length(T);
            
            % generate state-space maodel
            ss = SSModel(anet); 
            x0 = ss.x0(:);             % Make a column vector.
            
            %% create periodic time domain input
            u=ss.U(T)';
            ts=1./this.rate;
            periodvec = ss.period * ones(ss.Nu,1);
            %% Create an iddata object with empty output in the time domain
            data = iddata([], u, 'Period', periodvec, 'Ts', ts);
            data.ti = 's';
            %% Create a continuous-time state-space model
            model_ss = idss(ss.A, ss.B, ss.C, ss.D, ss.K, x0, 'Ts',0);
            
            %% Simulate the output using the model
            [y, ~] = sim(model_ss, data, x0);
            Y = ss.normalizeSolution(y.Y, T');  % ToDo:  Y = ss.C*X+ss.D*u
            % run was successful; prepare solution in structure
            this.solution = struct('time', T, 'Y', Y, 'X', y.Y, 'out_rate', this.rate, 'Ys', [], 'tsol', cputime-tcpu, 'duration', ss.period);
            
        end
        function solveSim_nl(this, anet, cbProgress)
        %% solveSim_nl - application of SIM solver in a non-linear framework
        %             uses signal processing toolbox 
        % PARAMETER: solveSim_nl(this, anet, cbProgress)
        %    - 'anet' instance of type ArteryNet
        %    - 'cbProgress' callback function to control solver loop and indicate Progress
            tcpu = cputime;             % starting time of cpu
            deltat = 1.0/this.rate;                   % time step width
            T = 0:deltat:this.t_end-deltat;
            N = length(T);
            
            % generate state-space maodel
            ss = SSModel(anet); 
            x0 = ss.x0(:);             % Make a column vector.
            
            y = x0'; 
            y(N, ss.Nx) = 0; 
            x = x0; 
            u0 = ss.U(0);
            ts=1./this.rate;
            periodvec = ss.period * ones(ss.Nu,1);
            for i=2:N
                u1 = ss.U(T(i));
                data = iddata([], [u0, u1], 'Period', periodvec, 'Ts', ts);
                data.ti = 's';
                [A, B] = ZimEstimator.NonLin_model(ss.RCL, x, ss.anet_id); 
                model_ss = idss(A, B, ss.C, ss.D, ss.K, x, 'Ts', 0);
%                sys = ss(A, B, ssM.C, ssM.D); 
%                [x, ~] = lsim(sys, [u0, u1], [0, deltat], x);
                [x, ~] = sim(model_ss, data, x);

                x = x.Y(2,:)';
                y(i, :) = x'; 
                u0 = u1; 
                %% progress
                if ~mod(i, round(4*sqrt(i+1)))
                    if ~feval(cbProgress, i, N)  % progress + cancel
                        return; 
                    end
                end
            end
            
            Y = ss.normalizeSolution(y, T');  % ToDo:  Y = ss.C*X+ss.D*u
            % run was successful; prepare solution in structure
            this.solution = struct('time', T, 'Y', Y, 'X', y, 'out_rate', this.rate, 'Ys', [], 'tsol', cputime-tcpu, 'duration', ss.period);
            
        end
        function solveDiscretized(this, anet, cbProgress)
        %% solveDiscretized - solver using the common discretization scheme x = AD*x+BD*u
        %                     not very reliable because of matrix conditioning problem
        %                     zero-hold not guaranteed at IV
        % PARAMETER: solveDiscretized(this, anet, cbProgress)
        %    - 'anet' instance of type ArteryNet
        %    - 'cbProgress' callback function to control solver loop and indicate Progress
        
            warning('this solver has a conditioning problem'); 
            tcpu = cputime;             % starting time of cpu
            deltat = 1.0/this.rate;                   % time step width
            T = 0:deltat:this.t_end-deltat;
            N = length(T);
            
            % generate state-space maodel
            ss = SSModel(anet); 
            x0 = ss.x0(:);             % Make a column vector.
            
            %% create periodic time domain input
            u=ss.U(T)';  % BC 
            ts=1./this.rate;
            n = ss.Nx; 
            m = ss.Nu;

            A1 = [ss.A, ss.B];  % http://en.wikipedia.org/wiki/Discretization
            A1(m+n, 1) = 0; 
            
            M = expm(A1*ts); 
            Ad = M(1:n, 1:n); 
            Bd = M(1:n, n+1:end); 

            X(N, n) = 0; 
            X(1, :) = x0; 
            x = x0; 
            for i=2:N
                if this.nonlinear
                    [Ad, Bd] = ZimEstimator.NonLin_model(ss.RCL, x, ss.anet_id); 
                    A1 = [ss.A, ss.B];  % http://en.wikipedia.org/wiki/Discretization
                    A1(m+n, 1) = 0; 
                end
            
            M = expm(A1*ts); 
            Ad = M(1:n, 1:n); 
            Bd = M(1:n, n+1:end); 

                x = Ad*x + Bd*u(i,:)'; 
                X(i,:) = x; 
                
                if ~mod(i, round(4*sqrt(i)))
                    if ~feval(cbProgress, i, N)  % progress + cancel
                        return;
                    end
                end
            end
            
            Y = ss.normalizeSolution(X, T');  % ToDo:  Y = ss.C*X+ss.D*u
            % run was successful; prepare solution in structure
            this.solution = struct('time', T, 'Y', Y, 'X', X, 'out_rate', this.rate, 'Ys', [], 'tsol', cputime-tcpu, 'duration', ss.period);

        end
        function solveMatlab(this, anet, cbProgress)
        %%  solveMatlab - common entrypoint for Matlab solver runs
        %   >>  sr.solveMatlab(anet, cbProgress);
        % PARAMETER:
        %    'anet'       instance of type ArteryNet
        %    'cbProgress' callback function to control solver loop and indicate Progress
            
            
            % prepare solver run
            tcpu = cputime;             % starting time of cpu
            deltat = 1.0/this.rate;                   % time step width
            T = 0:deltat:this.t_end-deltat;
            N = length(T);
            
            % opimization for speed
            diffs = diff(T);
            ss = SSModel(anet); 
            statefn=ss.MatlabStateF;                  
            neq = ss.Nx;
            X(neq, N) = 0; 
            X(:,1) = ss.x0(:);                             % Make a column vector.
            % optimization
            if (this.outMon)
                stats='on';
            else
                stats='off';
            end
            
            success = false; 
            switch this.solver
                case 1
                    solveODE1;
                case 2
                    solveODE2;
                case 3
                    solveODE3;
                case 4
                    solveODE4;
                case 5
                    solveODE5;
                case 6
                    options=odeset('RelTol', this.relTol, 'AbsTol', this.absTol);
                    solveODE(@ode15s);
                case 7
                    options=odeset('RelTol', this.relTol, 'AbsTol', this.absTol, 'Stats', stats, 'Vectorized', 'on');
                    solveODE(@ode45);
                case 8
                    options=odeset('RelTol', this.relTol, 'AbsTol', this.absTol, 'Stats', stats, 'Vectorized', 'on');
                    statefn=ss.MatlabStateF1;                  
                    solveODE(@ode113);
                otherwise
                    error('SolverRun.solveMatlab: unknown solver'); 
            end
            
            %% compose solution struct
            if success
                Y = ss.normalizeSolution(X, T');  % ToDo:  Y = ss.C*X+ss.D*u
                this.solution = struct('time', T, 'Y', Y, 'X', X, 'Ys', [], 'out_rate', this.rate, 'tsol', cputime-tcpu, 'duration', ss.period);
            end
            
            function solveODE1
                %% solveODE1 - Solve differential equations with a non-adaptive method of order 1.
                for i = 1:N-1
                    ti = T(i); 
                    yi = X(:,i); 
                    yi = ss.prep(ti, yi); 
                    f = feval(statefn,T(i),yi); 
                    X(:,i+1) = X(:,i) + diffs(i)*f';
                    if ~mod(i, round(4*sqrt(i)))
                        if ~feval(cbProgress, i, N)  % progress + cancel
                            X = [];
                            return;
                        end
                    end
                end
                X = X.';
                success = true; 
            end
            function solveODE2
                %% solveODE2 - Solve differential equations with a non-adaptive method of order 2.
                F(neq,2) = 0;
                for i = 2:N
                    ti = T(i-1);
                    hi = diffs(i-1);
                    yi = X(:,i-1);
                    yi = ss.prep(ti, yi); 
                    F(:,1) = feval(statefn,ti,yi);
                    F(:,2) = feval(statefn,ti+hi,yi+hi*F(:,1));
                    X(:,i) = yi + (hi/2)*(F(:,1) + F(:,2));
                    if ~mod(i, round(4*sqrt(i)))
                        if ~feval(cbProgress, i, N)  % progress + cancel
                            X = [];
                            return;
                        end
                    end
                end
                X = X.';
                success = true; 
            end
            function solveODE3
                %% solveODE3 - Solve differential equations with a non-adaptive method of order 3.
                F(neq,3) = 0;
                for i = 2:N
                    ti = T(i-1);
                    hi = diffs(i-1);
                    yi = X(:,i-1);
                    yi = ss.prep(ti, yi); 
                    F(:,1) = feval(statefn,ti,yi);
                    F(:,2) = feval(statefn,ti+0.5*hi,yi+0.5*hi*F(:,1));
                    F(:,3) = feval(statefn,ti+0.75*hi,yi+0.75*hi*F(:,2));
                    X(:,i) = yi + (hi/9)*(2*F(:,1) + 3*F(:,2) + 4*F(:,3));
                    
                    if ~mod(i, round(4*sqrt(i)))
                        if ~feval(cbProgress, i, N)  % progress + cancel
                            X = [];
                            return;
                        end
                    end
                end
                X = X.';
                success = true; 
            end
            function solveODE4
                %% solveODE4  Solve differential equations with a non-adaptive method of order 4.
                F(neq,4) = 0;
                for i = 2:N
                    ti = T(i-1);
                    hi = diffs(i-1);
                    yi = X(:,i-1);
                    yi = ss.prep(ti, yi); 
                    F(:,1) = feval(statefn,ti,yi);
                    F(:,2) = feval(statefn,ti+0.5*hi,yi+0.5*hi*F(:,1));
                    F(:,3) = feval(statefn,ti+0.5*hi,yi+0.5*hi*F(:,2));
                    F(:,4) = feval(statefn,T(i),yi+hi*F(:,3));
                    X(:,i) = yi + (hi/6)*(F(:,1) + 2*F(:,2) + 2*F(:,3) + F(:,4));
                    
                    if ~mod(i, round(4*sqrt(i)))
                        if ~feval(cbProgress, i, N)  % progress + cancel
                            X = [];
                            return;
                        end
                    end
                    
                end
                X = X.';
                success = true; 
            end
            function solveODE5
            %% solveODE5 - Solve differential equations with a non-adaptive method of order 5.
                % Method coefficients -- Butcher's tableau
                %   C | A
                %   --+---
                %     | B
                
                C = [1/5; 3/10; 4/5; 8/9; 1];
                
                A = [ 1/5,          0,           0,            0,         0
                      3/40,         9/40,        0,            0,         0
                      44/45        -56/15,       32/9,         0,         0
                      19372/6561,  -25360/2187,  64448/6561,  -212/729,   0
                      9017/3168,   -355/33,      46732/5247,   49/176,   -5103/18656];
                
                B = [35/384, 0, 500/1113, 125/192, -2187/6784, 11/84];
                
                % More convenient storage
                A = A.';
                B = B(:);
                
                nstages = length(B);
                F(neq,nstages) = 0;
                for i = 2:N
                    ti = T(i-1);
                    hi = diffs(i-1);
                    yi = X(:,i-1);
                    % General explicit Runge-Kutta framework
                    yi = ss.prep(ti, yi); 
                    F(:,1) = feval(statefn,ti,yi);
                    for stage = 2:nstages
                        tstage = ti + C(stage-1)*hi;
                        ystage = yi + F(:,1:stage-1)*(hi*A(1:stage-1,stage-1));
                        F(:,stage) = feval(statefn,tstage,ystage);
                    end
                    X(:,i) = yi + F*(hi*B);
                    if ~mod(i, round(4*sqrt(i)))
                        if ~feval(cbProgress, i, N)  % progress + cancel
                            X = [];
                            return;
                        end
                    end
                end
                X = X.';
                success = true; 
            end
            function solveODE(odesolver)
            %% solveODE - Solve differential equations with a given Matlab-solver
                for i = 2:N
                    y0 = X(:,i-1); 
                    y0 = ss.prep(T(i-1), y0); 
                    xsol = odesolver(statefn,T(i-1:i),y0,options);
                    X(:,i) = xsol.y(:,end);
                    if ~mod(i, round(4*sqrt(i)))
                        if ~feval(cbProgress, i, N)  % progress + cancel
                            X = [];
                            return;
                        end
                    end
                end
                X = X.';
                success = true; 
            end
        end
        function solveSundails(this, anet, cbProgress)
        %% solveSundails - interfaces to Sundails CVode solver 
        %  >> sr.solveSundails(anet, cbProgress); 
        % PARAMETER
        %  'anet'         - fully initialized instance of ArteryNet
        %  'cbProgress'   - [] or callback displaying progress
        
            tcpu = cputime;             % starting time of cpu
            % CVode initialization
            t0 = 0.0;
            
            options = CVodeSetOptions('UserData', [], 'RelTol', this.relTol, 'AbsTol', this.absTol, 'MaxNumSteps', this.maxNumSteps);
            if this.outMon
                mondata.sol = true;
                mondata.mode = 'text';
                mondata.skip = 9;           % take only every 10th point to display
                mondata.updt = 100;         % update display every 100 points
                options = CVodeSetOptions(options, 'MonitorFn', @CVodeMonitor, 'MonitorData', mondata);
            end
            
            nout = this.rate;                % number of collocation points per second
            deltaT = 1./nout;                % time step for collocation point
            Nend = nout*this.t_end;          % number of total iterations for simPar.Pe periods
            T=zeros(Nend,1);
            tspan = 0:deltaT(1):this.t_end;
            
            ss = SSModel(anet); 
            Nseg = ss.nodes;         % determine number of segments in network
            X(1,:) = ss.x0; 
            Xs=[]; 
            success = false;   % set by solve calls
                switch this.sensMtd
                    case  'fsa'
%                        CVodeInit(@ss.SundailsStateF_sens, 'BDF', 'Newton', t0, X(1,:)', options);
                        try, fsaSolve, catch ex, CVodeFree, throw(ex), end; 
                    case  'none'
                        try, noneSolve, catch ex, CVodeFree, throw(ex), end; 
                    otherwise
                        error('method not implemented');
                end
            CVodeFree;
            
            %% compose solution struct
            if success && ~isempty(X) 
                Y = ss.normalizeSolution(X, T);  % ToDo:  Y = ss.C*X+ss.D*u
                
                pulse = 60/anet.n_heartRate*this.rate; 
                if ~isempty(Xs)
                    for k= length(this.plist):-1:1
                        Ys(:,k,:) = ss.normalizeSolution(Xs(:,k,:), T, 'sens');  % ToDo:  Y = ss.C*X+ss.D*u
                    end
                else
                    Ys = []; 
                end
                this.solution = struct('time', T', 'Y', Y, 'X', X, 'out_rate', this.rate, 'Ys', Ys, 'Xs', Xs, 'tsol', cputime-tcpu, 'duration', ss.period);
            end
            
            function fsaSolve
            %% fsaSolve - implements a 'fsa' sensitivity analysis run of this solver over the time intervall tspan[1:Nend]
            %   refer to: https://computation.llnl.gov/casc/sundials/documentation/cvs_guide/node7.html#SECTION00710000000000000000'
                Ns = length(this.plist);
                pscales=ones(1,Ns)*this.parScale;        % parameter scale
                odeP.(this.sensPar) = ss.NodeParams.(this.sensPar);
                odeP.name = this.sensPar;
                odeP.nodes = this.plist;
                options = CVodeSetOptions('UserData', odeP, ...
                    'RelTol', this.relTol, ...
                    'AbsTol', this.absTol, ...
                    'MaxNumSteps', this.maxNumSteps);
                CVodeInit(@ss.Sundails_StateF_sense, 'BDF', 'Newton', t0, ss.x0(:), options);
                FSAoptions = CVodeSensSetOptions('method', 'Simultaneous',...
                    'ErrControl', true,...
                    'ParamField', this.sensPar, ...
                    'ParamList', this.plist, ...
                    'ParamScales', pscales, ...
                    'DQtype', 'Centered');

                yS0 = zeros(ss.Nx,Ns);
                CVodeSensInit(Ns, [], yS0, FSAoptions);
                % solve ODE
                success = false;
                Xs(nout, Ns, ss.Nx) = 0;
                
                for iout=2:Nend
                    [status, t, x, yS] = CVode(tspan(iout),'Normal');
                    if ~mod(iout, round(4*sqrt(iout+1)))
                        if ~feval(cbProgress, iout, Nend)  % progress + cancel
                            return;
                        end
                    end
                    
                    % Extract statistics
                    si = CVodeGetStats;
                    if this.outMon
                        if mod(iout,nout)<=0
                            OutputStat;
                            OutputSensFsa;
                        end
                    else
                        % indicate progress!
                    end
                    
                    Xs(iout,:,:)=yS';
                    X(iout,:)=x;
                    T(iout,1)=t;
                end
                success = true;
                
                function OutputStat
                    fprintf('**********************************************************************************\n\n');
                    fprintf('t = %0.2e   order = %1d  step = %0.2e',t, si.qlast, si.hlast);
                    if(status == 2)
                        fprintf(' ... Root found  %d   %d\n',si.RootInfo.roots(1), si.RootInfo.roots(2));
                    else
                        fprintf('\n\n');
                    end
                    fprintf('Segmental solution vector for flow and pressure\n\n');
                    for i=1:Nseg
                        fprintf('Segment %i: Flow %d [ml/s]  Pressure %d [mmHg] \n', i, 1E6*x(2*i-1), 7.5E-3*x(2*i));
                    end
                    fprintf('\n\n');
                end
                function OutputSensFsa
                    fprintf('Sensitivity of %c, parameter scale %d \n', this.sensPar, this.parScale);
                    for i=1:Nseg
                        fprintf('Sensitivity in segment %i:\n', i);
                        for j=1:Ns
                            fprintf('  %c(%i): Flow %d   Pressure %d  \n', this.sensPar, j, 1E6*yS(2*i-1,j), 7.5E-3*yS(2*i,j));
                        end
                    end
                    fprintf('\n\n');
                end
            end
            function noneSolve
            %%  noneSolve - implements a standard run of this solver over the time intervall tspan[1:Nend]
                success = false;
                x = ss.x0(:); 
%               options.UserData = anet;   % to enable calls like ELDH2RCL 
                CVodeInit(ss.SundailsStateF, 'BDF', 'Newton', t0, x, options);
                for iout=2:Nend   % loop over all time steps
                    [y useY] = ss.prep(tspan(iout), x); 
                    if useY
                        CVodeReInit(t0, y, options);
                    end
                    [status, t, x] = CVode(tspan(iout),'Normal');
                    if ~mod(iout, round(4*sqrt(iout+1)))
                        if ~feval(cbProgress, iout, Nend)  % progress + cancel
                            return
                        end
                    end
                    
                    % Extract statistics
                    si = CVodeGetStats;
                    if this.outMon
                        if mod(iout,nout)<=0
                            OutputStat;
                        end
                    end
                    X(iout,:)=x;
                    T(iout,1)=t;
                end
                success = true;
                function OutputStat
                    fprintf('**********************************************************************************\n\n');
                    fprintf('t = %0.2e   order = %1d  step = %0.2e',t, si.qlast, si.hlast);
                    if(status == 2)
                        fprintf(' ... Root found  %d   %d\n',si.RootInfo.roots(1), si.RootInfo.roots(2));
                    else
                        fprintf('\n\n');
                    end
                    fprintf('Segmental solution vector for flow and pressure\n\n');
                    for i=1:Nseg
                        fprintf('Segment %i: Flow %d [ml/s]  Pressure %d [mmHg] \n', i, 1E6*x(2*i-1), 7.5E-3*x(2*i));
                    end
                    fprintf('\n\n');
                end
            end
        end
        function ps = calcParScale(this, anet)
        %% parScale - calculates mean value of a sensitivity parameter over all nodes
            RCL = anet.RCL;
            switch this.sensPar
                case 'R'
                    v = RCL.R';
                case 'C'
                    v = RCL.C';
                case 'L'
                    v = RCL.L';
                case 'b_R'
                    v = anet.pqRCL.R';
                case 'b_p'
                    v = anet.pqRCL.p';
                case 'b_q'
                    v = anet.pqRCL.q';
            end
            ps = mean(v);
            if length(ps) == 1
                ps = str2double(sprintf('%.0e',ps)); % truncate to one decimal place
            else
                error('why does this happen?'); 
                % ps(2) = str2double(sprintf('%.0e',ps(2))); % truncate to one decimal place
                % ps(1) = str2double(sprintf('%.0e',ps(1))); % truncate to one decimal place
            end
            if ps==0  % 0 values are not taken by cVode
                ps = 1; 
            end
            this.parScale = ps;
        end
        function [flowSM, flowSV, pressureSM, pressureSV] = plotSensitivity(this, showlist)
        %% plotSensitivity - opens a Dialog with sensitivity plots for selected nodes
            if isempty(showlist)
                return; 
            end
            sensl.XTick = 1:length(this.plist); 
            for i=length(this.plist):-1:1
                sensl.XTickLabel{i} = sprintf('%i', this.plist(i)); 
            end
            sensl.YTick = 1:length(showlist); 
            for i=length(showlist):-1:1
                sensl.YTickLabel{i} = sprintf('%i', showlist(i)-1); 
            end

            fullp = floor(length(this.solution.time) / (this.solution.duration * this.solution.out_rate));
            x1 = round((fullp-1) * (this.solution.duration * this.solution.out_rate)+1);
            x2 = round(double(this.solution.duration) * double(fullp) * this.solution.out_rate);
            
            Yt = this.solution.Ys(x1:x2, :, :);
            [Ns, Nsens, Nseg]=size(Yt);
            Yq(Ns, Nseg, Nsens)=0;
            for i=1:Nsens
                Yi = reshape(Yt(:, i, :), Ns, Nseg);
                Yq(:, :, i) = Yi;
            end
            time = this.solution.time(x1:x2);
            fi = (showlist*2)-1;
            pi = (showlist*2);
%             fi = 1:2:Nseg;
%             pi = 2:2:Nseg;
            Ys.YsF = Yq(:,fi,:);
            Ys.YsP = Yq(:,pi,:);
            scale = 'abs';
            
            % flow sensitivity in ml/parameter units
            % pressure sensitivity in mmHg/parameter units
            if strcmp(scale, 'abs')
                sensscale = ' abs';
                for i = length(showlist):-1:1
                   flowSM(i,:)     = mean(abs(1E6 * Ys.YsF(:,i,:)));
                   flowSV(i,:)     = var(abs(1E6 * Ys.YsF(:,i,:)));
                   pressureSM(i,:) = mean(abs(7.5E-3 * Ys.YsP(:,i,:)));
                   pressureSV(i,:) = var(abs(7.5E-3 * Ys.YsP(:,i,:)));
                end
                
            elseif strcmp(scale, 'lga')
                sensscale = ' log abs';
                flowSM(:,:)     = mean(log(abs(1E6 * Ys.YsF)));
                flowSV(:,:)     = var(log(abs(1E6 * Ys.YsF)));
                pressureSM(:,:) = mean(log(abs(7.5E-3 * Ys.YsP)));
                pressureSV(:,:) = var(log(abs(7.5E-3 * Ys.YsP)));
            end
            
            scrsz = get(0,'ScreenSize');
            mp = get(0, 'MonitorPositions');
            if size(mp,1) > 1
                scrf = 1.067 * scrsz(3);
            else
                scrf = 0.067 * scrsz(3);
            end
            figure('Name',['SolverRun: Solution=' this.name ' (id=' num2str(this.id) ')  --- Plot of Sensitivities using scale ', sensscale], 'NumberTitle','off', 'MenuBar','none', 'ToolBar','none', ...
                'Position',[scrf,  scrsz(4)/16, 7*scrsz(4)/8, 7*scrsz(4)/8])
            
            hold on
            subplot(3,2,1)
            if strcmp(scale, 'abs')
                plot(time, abs(Ys.YsF(:,:,1)));
                xlabel('time [s]')
            elseif strcmp(scale, 'lga')
                plot(time, log(abs(Ys.YsF(:,:,1))));
                xlabel('time [s]')
            end
            for i=length(showlist):-1:1
             leg{i} = sprintf('Node %i', showlist(i)-1); 
             if i>10
                 break; 
             end
            end
            legend(leg); 
            title(strcat(this.sensPar, strcat(sensscale, ' sensitivity on flow')))
            
            subplot(3,2,2)
            if strcmp(scale, 'abs')
                plot(time, abs(Ys.YsP(:,:,1)));
                xlabel('time [s]')
            elseif strcmp(scale,'lga')
                plot(time, log(abs(Ys.YsP(:,:,1))));
                xlabel('time [s]')
            end
            title(strcat(this.sensPar, strcat(sensscale, ' sensitivity on pressure')))
            legend(leg); 
            subplot(3,2,3)
            imagesc(flowSM(:,:));
            labelling; 
            title(strcat('mean ', strcat(sensscale,  ' sensitivity for flow')))
            colorbar()
            subplot(3,2,5)
            imagesc(flowSV(:,:));
            labelling; 
            title(strcat(sensscale, ' sensitivity variance for flow'))
            colorbar()
            hold on
            subplot(3,2,4)
            imagesc(pressureSM(:,:));
            labelling; 
            title(strcat('mean ', strcat(sensscale,  ' sensitivity for pressure')))
            colorbar()
            subplot(3,2,6)
            imagesc(pressureSV(:,:));
            labelling; 
            title(strcat(sensscale, ' sensitivity variance for pressure'))
            colorbar()
            hold off
            function labelling
                set(gca,'XTickMode', 'manual', 'XTick', sensl.XTick, 'XTickLabel', sensl.XTickLabel); 
                set(gca,'YTickMode', 'manual', 'YTick', sensl.YTick, 'YTickLabel', sensl.YTickLabel); 
                xtext = 'sens node:'; 
                ytext = 'selected node'; 
                xlabel(xtext); 
                ylabel(ytext)
            end
        end
        function sid = Save(this, db, saveAll, useTransaction)
            %% Save - saves SolverRun instance + last period of STS, SensTS to DB
            %  >> srID = sr.Save;                       % save SolverRun + STS + SensTS
            %  >> srID = sr.Save(db);                   %  ""
            %  >> srID = sr.Save(db, false);            % save SolverRun omit STS
            %  >> srID = sr.Save(db, saveAll, false);   % save SolverRun ext. controlled Transaction
            %       - 'db': (optional default = AngioDB) global AngioDB object
            %       - 'saveAll' (optional, default = true) if false, function omits STS, SensTS
            %       - 'useTransaction' (optional, default: = true); if false, function relies on external transaction control
            %       - 'sid' id of new SolverRun object in DB
            
            if nargin < 2
                db = AngioDB;
            end
            if nargin < 3
                saveAll = true;
            end
            if nargin < 4
                useTransaction = true;
            end
            if isempty(this.solution)
                error('SolverRun.Save: There is no solution yet. SolverRun.Solve must be called first');
            end
            db.open;
            % Write SolverRun
            period = (this.solution.duration * this.solution.out_rate); % samples per period
            fullp = floor(length(this.solution.time) / period);         % # full periods
            this.duration = period/this.solution.out_rate;
            this.starttime = (fullp-1)*this.duration;
            sid = this.id;
            if useTransaction
                db.exec('Start Transaction');
            end
            try
                saveRun;
                if saveAll
                    saveSTS;
                end
            catch ex
                ex.message
                if useTransaction
                    db.exec('Rollback');
                end
                this.saved = false;
                this.id = sid;
                % sid = 0;
                error('SolverRun.Save: operation was not successfull (->rollback)')
            end
            if useTransaction
                db.exec('Commit');
            end
            this.saved = true;
            sid = this.id;
            
            function saveSTS
                res = db.select('select last_insert_id() sid');
                this.id = res.sid;
                x1 = round((fullp-1) * period) + 1;
                x2 = round((fullp) * period);
                data = this.solution.Y(x1:x2, :)';
                l = size(data, 1);  % node #
                command = 'insert into STS (solverRunId, nodeId, rate, data) ';
                for i=2:2:l
                    blob = typecast([data(i-1,:) data(i,:)], 'uint8');
                    values = ['values(' ...
                        num2str(this.id) ',' ...
                        num2str(i/2) ',' ...
                        num2str(this.rate) ', ?)'];
                    res = db.update([command values], blob);
                    err(res , 'STS');
                end
                if isempty(this.solution.Ys)
                    return;
                end
                
                % SensTS
                command = 'insert into SensTS (solverRunId, nodeId, rate, sensCount, data) ';
                for i=2:2:l
                    data = this.solution.Ys(x1:x2,: , i-1:i); % [oneperiod node_i sensenodes]
                    [a b c] = size(data);  % [samples, nodes, sensnodes]
                    data = reshape(data, 1, a*b*c);
                    blob = typecast(data, 'uint8');
                    values = ['values(' ...
                        num2str(this.id) ',' ...
                        num2str(i/2) ',' ...
                        num2str(this.rate) ',' ...
                        num2str(b) ', ?)'];
                    res = db.update([command values], blob);
                    err(res , 'SensTs');
                end
            end
            
            function saveRun
                update = 'insert into SolverRun (paramSetId, name, description, solver, rate, type, starttime, duration, relTol, absTol, sensMtd, sensPar, parScale, plist, showlist) ';
                values = ['values(' ...
                    num2str(this.paramSetId) ',''' ...
                    this.name ''',''' ...
                    this.description ''',' ...
                    num2str(this.solver) ',' ...
                    num2str(this.rate) ',' ...
                    num2str(this.nonlinear) ',' ...
                    num2str(this.starttime) ',' ...
                    num2str(this.duration) ',' ...
                    num2str(this.relTol) ',' ...
                    num2str(this.absTol) ',''' ...
                    this.sensMtd ''',''' ...
                    this.sensPar ''',' ...
                    num2str(this.parScale) ','];
                sl = typecast(int16(this.showlist), 'uint8');
                pl = typecast(int16(this.plist), 'uint8');
                if isempty(this.plist)
                    if isempty(this.showlist)
                        values = [values 'Null, Null)'];
                        res = db.update([update values]);
                    else
                        values = [values 'Null, ?)'];
                        res = db.update([update values], sl);
                    end
                else
                    if isempty(this.showlist)
                        values = [values '?, Null)'];
                        res = db.update([update values], pl);
                    else
                        values = [values '?, ?)'];
                        res = db.update([update values], pl, sl);
                    end
                end
                err(res , 'SolverRun');
            end
            
            function err(res, table)
                if res<1
                    error(['SolverRun.Save: Save to Table ' table ' was not succesfull']);
                end
            end
        end
        function success = Open(this, db, sid)
        %% Open - reads a SolverRun state from db into the object this
        % PARAMETER: success = Open(this, db, sid)
        %   'db'      - database handle of type AngioDB
        %   'sid'     - database id of run 
            success = true;
            db.open;
            sr = db.select(['Select * from SolverRun where id = ' num2str(sid)]);
            if isempty(sr)
                success = false;
                return;   % not found?
            end
            sol = db.select(['Select * from STS where solverRunId = ' num2str(sr.id)]);
            sens = db.select(['Select * from SensTS where solverRunId = ' num2str(sr.id)]);
            [headers this.solvers] = db.select1('Select typeName from Solvers');
            this.id = sr.id;
            this.paramSetId = sr.paramSetId;
            this.name = char(sr.name);
            this.description = char(sr.description);
            this.solver = sr.solver;
            this.rate = double(sr.rate);
            this.nonlinear = logical(sr.type); 
            this.starttime = double(sr.starttime);
            this.duration = double(sr.duration);
            this.relTol = sr.relTol;
            this.absTol = sr.absTol;
            this.sensMtd = char(sr.sensMtd);
            this.sensPar = char(sr.sensPar);
            this.parScale = double(sr.parScale);
            this.lastmodified = sr.lastmodified;
            
            if ~isempty(sr.plist)
                this.plist = double(typecast(sr.plist', 'int16'));
            end
            if ~isempty(sr.showlist)
                this.showlist = double(typecast(sr.showlist', 'int16'));
            end
            
            % compute Y
            this.solution = [];
            if isempty(sol)
                return;
            end
            ts = length(sol)*2;                 % # time series
            pq = int16(length(sol(1).data)/16); % # samples [p q]
            this.solution.Y(pq, ts) = 0;
            
            for i=2:2:ts
                data = typecast(sol(i/2).data', 'double');
                data = reshape(data, pq, 2)';
                this.solution.Y(:, i-1) = data(1,:);
                this.solution.Y(:, i)   = data(2,:);
            end
            deltat = 1.0/double(this.rate);
            this.solution.duration = double(pq)/double(this.rate);
            this.solution.out_rate = this.rate;
            this.solution.time = this.starttime: deltat: this.solution.duration-deltat + this.starttime;
            
            this.saved = true;
            
            % compute Ys
            this.solution.Ys = [];
            if isempty(sens)
                return;
            end
            s = int16(sens(1).sensCount);
            sens(1).sensCount;
            this.solution.Ys(pq, s, ts) = 0;
            for i=2:2:ts
                data = typecast(sens(i/2).data', 'double');
                data = reshape(data, pq*s, 2)';
                this.solution.Ys(:, :, i-1) = reshape(data(1,:), pq, s);
                this.solution.Ys(:, :, i) =   reshape(data(2,:), pq, s);
            end
        end
    end
    
    methods (Static)
        function sid = OpenSolverRunDlg(pid, db)
        %% OpenSolverRunDlg - select a solution for a given ParamSet stored in the database
            % prepare Dialog
            scrsz = get(0,'ScreenSize');
            mp = get(0, 'MonitorPositions');
            if size(mp,1) > 1
                scrf = scrsz(3) + (scrsz(3)-1000)/2;
            else
                scrf = (scrsz(3)-1000)/2;
            end
            position = [scrf, (scrsz(4)-600)/2, 1000, 600];
            hDlg = figure('Name', 'Open SolverRun', 'NumberTitle', 'off', 'MenuBar','none', 'ToolBar','none', ...
                'Color', 'w', 'Position', position, 'Resize', 'off', 'WindowStyle', 'modal');
            
            % static text
            uicontrol(hDlg, 'Style', 'Text', 'FontSize', 14, 'HorizontalAlignment', 'left', 'String', 'Solver runs saved for current ParamSet', ...
                'Position', [20 570 500, 30], 'BackgroundColor', 'w');
            
            
            % buttons
            uicontrol(hDlg, 'Style', 'Pushbutton', 'FontSize', 8, 'HorizontalAlignment', 'right', 'String', 'Cancel', ...
                'Position', [920 20 60, 30], 'Callback', @cbCancel);
            bOpen = uicontrol(hDlg, 'Style', 'Pushbutton', 'FontSize', 8, 'HorizontalAlignment', 'right', 'String', 'Open', ...
                'Position', [840 20 60, 30], 'Callback', @cbOpen, 'Enable', 'off');
            
            % table
            db.open;
            [headers dbcontent] = db.select1(['select SolverRun.id as id, name, description, Solvers.typeName as solver, rate, type as nonlinear, sensMtd, lastmodified as saved from SolverRun join Solvers on solver = Solvers.id where paramSetId = ' num2str(pid)], true);
            if size(dbcontent, 2) == 1
                dbcontent = cell(size(headers)); 
            end
            table = javahandle_withcallbacks.javax.swing.JTable(dbcontent, headers); % R2014a/R2015a fixed
            table.setSelectionMode(0);
            cm = table.getColumnModel;
            if(cm.getColumnCount == 8)
                w = 1133;
                cm.getColumn(0).setPreferredWidth(0.04*w);
                cm.getColumn(1).setPreferredWidth(0.08*w);
                cm.getColumn(2).setPreferredWidth(0.50*w);
                cm.getColumn(3).setPreferredWidth(0.1*w);
                cm.getColumn(4).setPreferredWidth(0.08*w);
                cm.getColumn(5).setPreferredWidth(0.08*w);
                cm.getColumn(6).setPreferredWidth(0.08*w);
                cm.getColumn(7).setPreferredWidth(0.08*w);
            end
            ScrollPane = javax.swing.JScrollPane(table);
            set(hDlg, 'Userdata', table);
            btLinePropsPos = [[0.02 0.52],0.96,0.45];
            [~,btContainer] = javacomponent(ScrollPane,[0 0, 1, 1], hDlg);
            set(btContainer, 'Units', 'Norm', 'Position', btLinePropsPos);
            drawnow; 
            warning('off', 'MATLAB:hg:JavaSetHGProperty');
            if ~strcmp(dbcontent{1}, '')
                set(table, 'MouseMovedCallback', @cbMouseMoved);  % for tooltip table 1
                set(table,'MousePressedCallback',@cbSelect);      % copy values on double click
            end
            
            sid = 0;
            uiwait(hDlg);
            
            function cbMouseMoved(varargin)
                %% cbMouseMoved - maintains tooltip
                event = varargin{2};
                row = table.rowAtPoint(event.getPoint);
                col = table.columnAtPoint(event.getPoint);
                value = table.getModel.getValueAt(row,col);
                if ~isempty(value)
                    if isjava(value)
                        value = value.toString;
                    elseif isnumeric(value)
                        value = num2str(value);
                    end
                else
                    value = '';
                end
                % Multiline tooltip
                j = 1;
                while j * 100 < size(value, 2)
                    value = sprintf('%s<br/>%s',value(1:j*100), value(j*100+1:size(value, 2)));
                    j = j+1;
                end
                table.setToolTipText(['<html> ' char(value)]);
            end
            
            function cbSelect(varargin)
                %% cbSelect - callback, copies params on double Click
                %
                set(bOpen, 'Enable', 'on');
                if size(varargin,2) == 2  % selection by double click
                    ev = varargin{1,2};
                    if ev.getClickCount > 1
                        cbOpen;
                    end
                end
            end
            function cbOpen(varargin)
                sid = table.getModel.getValueAt(table.getSelectedRow, 0);
                cbCancel;
            end
            function cbCancel(varargin)
                delete(hDlg);
            end
            
        end
        function NewSolverRunDlg(anet)
            %% NewSolverRunDlg - shows Dialog allowing to choose parameters for new Solver run
            this = anet.p_solverRun;
            %RCL = anet.RCL;
            this.parScale = this.calcParScale(anet);
            db = anet.db;
            dbcontent = [];
            % prepare Dialog
            scrsz = get(0,'ScreenSize');
            mp = get(0, 'MonitorPositions');
            if size(mp,1) > 1
                scrf = scrsz(3) + (scrsz(3)-1400)/2;
            else
                scrf = (scrsz(3)-1000)/2;
            end
            position = [scrf, (scrsz(4)-600)/2, 1400, 600];
            hDlg = figure('Name', 'New SolverRun', 'NumberTitle', 'off', 'MenuBar','none', 'ToolBar','none', ...
                'Color', 'w', 'Position', position, 'Resize', 'off', 'WindowStyle', 'normal', ...
                'CreateFcn', {@ArteryNet.EnableMenus, anet, 'off'}, 'DeleteFcn', @cbCancel);
            
            % static text
            uicontrol(hDlg, 'Style', 'Text', 'FontSize', 14, 'HorizontalAlignment', 'left', 'String', 'Solver runs already saved for current ParamSet', ...
                'Position', [420 570 500, 30], 'BackgroundColor', 'w');
            uicontrol(hDlg, 'Style', 'Text', 'FontSize', 14, 'HorizontalAlignment', 'left', 'String', 'Solver Parameter ', ...
                'Position', [20 570 300, 30], 'BackgroundColor', 'w');
            
            % Panel
            hPanel = uipanel('Parent', hDlg, 'Title', '', 'Units', 'Pixels', 'Position', [20 60 380, 510], 'BackgroundColor', [0.95, 0.95, 0.95], 'BorderType', 'none');
            
            % input controls
            solvers = anet.getComboTypesFromDb('Solvers'); %#ok<*PROP>
            
            
            pos = 480;
            createTextCtrl(hPanel, 'name', [10 pos-2 150 22]);
            hName = createInputCtrl(hPanel, this.name, [165 pos 210 25], 1, 'on', {@processInput, 'name', 'String'});  pos = pos-30;
            
            createTextCtrl(hPanel, 'description', [10 pos-2 150 22]);
            hDescription = createInputCtrl(hPanel, this.description, [165 pos 210 25], 1, 'on', {@processInput, 'description', 'String'});  pos = pos-30;
            
            createTextCtrl(hPanel, 'solver', [10 pos-2 150 22]);
            hSolver = uicontrol(hPanel, 'Style', 'popupmenu', 'FontSize', 8, 'String', solvers, 'Value', this.solver, ...
                'Position', [165 pos 210, 25], 'BackgroundColor', 'w', 'Callback',  {@processInput, 'solver', 'Value'});pos = pos-30;
            
            createTextCtrl(hPanel, 'output rate', [10 pos-2 150 22]);
            hRate = createInputCtrl(hPanel, this.rate, [165 pos 210 25], 1, 'on', {@processInput, 'rate', 'String'});  pos = pos-30;
            
            createTextCtrl(hPanel, 'end time (full seconds)', [10 pos-2 150, 22]);
            createInputCtrl(hPanel, this.t_end, [165 pos 210 25], 1, 'on', {@processInput, 't_end', 'String'});  pos = pos-30;
            
            createTextCtrl(hPanel, 'relative tolerance ', [10 pos-2 150 22]);
            hRelTol = createInputCtrl(hPanel, sprintf('%.0e', this.relTol), [165 pos 210 25], 1, 'on', {@processInput, 'relTol', 'String'});  pos = pos-30;
            
            createTextCtrl(hPanel, 'absolute tolerance', [10 pos-2 150 22]);
            hAbsTol = createInputCtrl(hPanel, sprintf('%.0e', this.absTol), [165 pos 210 25], 1, 'on', {@processInput, 'absTol', 'String'});  pos = pos-30;
            
            createTextCtrl(hPanel, 'maximal # steps', [10 pos-2 150 22]);
            createInputCtrl(hPanel, this.maxNumSteps, [165 pos 210 25], 1, 'on', {@processInput, 'maxNumSteps', 'String'});  pos = pos-30;
            
            createTextCtrl(hPanel, 'output monitor', [10 pos-2 150 22]);
            uicontrol(hPanel, 'Style', 'checkbox', 'FontSize', 8, 'Value', this.outMon, ...
                'Position', [165 pos 50, 25], 'Callback',  {@processInput, 'outMon', 'Value'});
            % on same line
             createTextCtrl(hPanel, 'non linear', [210 pos-2 50 22]); 
             uicontrol(hPanel, 'Style', 'checkbox', 'FontSize', 8, 'Value', this.nonlinear, ...
                 'Position', [270 pos 50, 25], 'Callback',  {@processInput, 'nonlinear', 'Value'});pos = pos-40;

            createTextCtrl(hPanel, '-- from here sundails parameters --', [20 pos 210, 22]); pos = pos-30;

            createTextCtrl(hPanel, 'sensitivity method', [10 pos+3 150 22]);
            idx = round(strfind(SolverRun.knownSensMethods, this.sensMtd) / 5)+1;
            hSensMtd = uicontrol(hPanel, 'Style', 'popupmenu', 'FontSize', 8, 'String', SolverRun.knownSensMethods, 'Value', idx, ...
                'Position', [165 pos 210, 30], 'BackgroundColor', 'w', 'Callback',  {@processVal, 'sensMtd', 'Value'});pos = pos-35;
            
            createTextCtrl(hPanel, 'sensitivity parameter', [10 pos+3 150 22]);
            
            sep = strfind(SolverRun.knownSensPar, this.sensPar);
            idx = find(sep == strfind(['|' SolverRun.knownSensPar(1:sep)], '|'));
            hSensPar = uicontrol(hPanel, 'Style', 'popupmenu', 'FontSize', 8, 'String', SolverRun.knownSensPar, 'Value', idx, ...
                'Position', [165 pos 60, 30], 'BackgroundColor', 'w', 'Callback',  @changeSensPar); pos = pos-35;
            
            createTextCtrl(hPanel, 'parameter scale', [10 pos-2 150 22]);
            hParScale = createInputCtrl(hPanel, num2str(this.parScale), [165 pos 210 25], 1, 'on', {@processInput, 'parScale', 'String'});  pos = pos-30;
            
            createTextCtrl(hPanel, 'sensitivity node list', [10 pos-2 150 22]);
            uicontrol(hPanel, 'Style', 'pushButton', 'FontSize', 8, 'String', num2str(this.plist), ...
                'Position', [165 pos 210, 25], 'BackgroundColor', 'w', 'Callback',  @multiSelect); 
            
            % buttons
            uicontrol(hDlg, 'Style', 'Pushbutton', 'FontSize', 8, 'HorizontalAlignment', 'right', 'String', 'Cancel', ...
                'Position', [100 20 60, 30], 'Callback', @cbCancel);
            bRun = uicontrol(hDlg, 'Style', 'Pushbutton', 'FontSize', 8, 'HorizontalAlignment', 'right', 'String', 'Run', ...
                'Position', [20 20 60, 30], 'Callback', @cbRun, 'Enable', 'on');
            
            bDel = uicontrol(hDlg, 'Style', 'Pushbutton', 'FontSize', 8, 'HorizontalAlignment', 'right', 'String', 'Delete', ...
                'Position', [420 60 60, 30], 'Callback', @cbDelete, 'Enable', 'off');
            
            bCopy = uicontrol(hDlg, 'Style', 'Pushbutton', 'FontSize', 8, 'HorizontalAlignment', 'right', 'String', 'Copy Values', ...
                'Position', [500 60 80, 30], 'Callback', @cbCopy, 'Enable', 'off');
            
            uipanel(hDlg, 'Units', 'Pixels', 'BackgroundColor', [0.94 0.94 0.94], 'BorderType', 'none', 'Position', [180 20 1200, 30]);
            bProgress = uipanel(hDlg, 'Units', 'Pixels', 'BackgroundColor', [0 1 0], 'Position', [180 20 20, 30], 'Visible', 'off');
            running = false;
            
            % table
            table = javax.swing.JTable;
            if ~isempty(anet.id)
                refreshTable;
            end
            table.setSelectionMode(0);
            ScrollPane = javax.swing.JScrollPane(table);
            set(hDlg, 'Userdata', table);
            btLinePropsPos = [0.3, 0.16 ,0.69, 0.785];
            [~,btContainer] = javacomponent(ScrollPane,[0 0 1 1], hDlg);
            set(btContainer, 'Units', 'Norm', 'Position', btLinePropsPos);
            warning('off', 'MATLAB:hg:JavaSetHGProperty');
            if ~isempty(dbcontent) && ~strcmp(dbcontent{1}, '')
                set(table, 'MouseMovedCallback', @cbMouseMoved);  % for tooltip table 1
                set(table,'MousePressedCallback',@cbSelect);      % copy values on double click
            end
            
            running = false;
            
            function cbCopy(varargin)
                this.name = table.getModel.getValueAt(table.getSelectedRow,1);
                set(hName, 'String', this.name);
                this.description = table.getModel.getValueAt(table.getSelectedRow,2);
                set(hDescription, 'String', this.description);
                this.solver = table.getModel.getValueAt(table.getSelectedRow,4);
                set(hSolver, 'Value', this.solver);
                this.rate = table.getModel.getValueAt(table.getSelectedRow,5);
                set(hRate, 'String', num2str(this.rate));
                this.relTol = table.getModel.getValueAt(table.getSelectedRow,6);
                set(hRelTol, 'String', num2str(this.relTol));
                this.absTol = table.getModel.getValueAt(table.getSelectedRow,7);
                set(hAbsTol, 'String', num2str(this.absTol));
                this.sensMtd = table.getModel.getValueAt(table.getSelectedRow,8);
                idx = round(strfind(SolverRun.knownSensMethods, this.sensMtd) / 5)+1;
                set(hSensMtd, 'Value', idx);
                this.sensPar = table.getModel.getValueAt(table.getSelectedRow,9);
                idx = (strfind(SolverRun.knownSensPar, this.sensPar)+1)/2;
                set(hSensPar, 'Value', idx);
                this.parScale = table.getModel.getValueAt(table.getSelectedRow,10);
                set(hParScale, 'String', num2str(this.parScale));
            end
            
            function refreshTable
                [headers dbcontent] = db.select1(['select SolverRun.id as id, name, description, Solvers.typeName as solver, SolverRun.solver as SolverId, rate, type as nonlinear, relTol, absTol, sensMtd, parScale, lastmodified as saved from SolverRun join Solvers on solver = Solvers.id where paramSetId = ' num2str(anet.p_id)], true);
                if size(dbcontent, 2) == 1
                    dbcontent = cell(size(headers)); 
                end
                tm = javax.swing.table.DefaultTableModel(dbcontent, headers);
                table.setModel(tm);
            end
            
            function cbDelete(varargin)
                aid = table.getModel.getValueAt(table.getSelectedRow,0);
                db.exec(['delete from SolverRun where id = ' num2str(aid)]);
                refreshTable;
            end
            function run = cbProgress(varargin)
                p = get(bProgress, 'Position');
                e = varargin{1}+1;
                f = varargin{2};
                p(3) = 1200.0/f*e;
                set(bProgress, 'Visible', 'on', 'Position', p);
                drawnow;
                run = running;
            end
            
            function multiSelect(varargin)
                this.plist = anet.MultiSelector(this.plist);
                set(varargin{1}, 'String', num2str(this.plist));
            end
            
            function changeSensPar(varargin)
                v = get(varargin{1}, 'Value');
                s = get(varargin{1}, 'String');
                this.sensPar = strtrim(s(v,:));
                this.calcParScale(anet);
                set(hParScale, 'String', num2str(this.parScale));
            end
            function processVal(varargin)
                v = get(varargin{1}, 'Value');
                s = get(varargin{1}, 'String');
                this.sensMtd = deblank(s(v,:));
            end
            function processInput(obj, ~,field, prop)
                %% callback for inputfields
                %   - writes value of uicontrol property 'prop' into field 'field' of object
                %   - expects obj handle in 'UserData' of current figure
                % parameter:   obj - handle for uicontrol
                %            field - destination field of new value
                %             prop - source property of new value
                
                val = get(obj, prop);
                if ischar(val)
                    if isnumeric(this.(field))
                        this.(field) = str2num(val);  %#ok<ST2NM>
                    elseif ~strcmp(val, this.(field))
                        fullval = deblank(val(1,:));
                        for i=2:size(val, 1)       % convert multiline input into single string
                            fullval = [fullval char(10) deblank(val(i,:))];  %#ok<AGROW>
                        end
                        this.(field) = fullval;
                    end
                else
                    if val ~= this.(field)
                        this.(field) = val;
                    end
                end
            end
            function handle = createTextCtrl(hParent, text, position)
                handle = uicontrol(hParent, 'Style', 'text', 'HorizontalAlignment', 'right', 'String', [text ' '], 'Position', position, ...
                    'BackgroundColor', [0.95, 0.95, 0.95]);
            end
            function handle = createInputCtrl(hParent, text, position, max, Enable, cb)
                handle = uicontrol(hParent, 'Style', 'edit', 'HorizontalAlignment', 'left', 'String', text, 'Position', position, ...
                    'BackgroundColor', 'w', 'Max', max, 'Enable', Enable, 'Callback', cb);
            end
            function cbCancel(varargin)
                ArteryNet.EnableMenus([], [], anet, 'on');
                running = false;
                delete(hDlg);
            end
            function cbRun(varargin)
                running = ~running;
                if running
                    set(bRun, 'String', 'Stop');
                else
                    set(bRun, 'String', 'Run');
                    return;
                end
                this.solve(anet, @cbProgress);
                if ~running  % run was canceled or Window is closing
                    return
                end
                if ~isempty(this.solution)
                    this.saved = false;
                    this.showlist = anet.MultiSelector(this.plist, true, this.showlist);  % Ausgabe
                end
                delete(hDlg);
            end
            function cbMouseMoved(varargin)
                %% cbMouseMoved - maintains tooltip
                event = varargin{2};
                row = table.rowAtPoint(event.getPoint);
                col = table.columnAtPoint(event.getPoint);
                value = table.getModel.getValueAt(row,col);
                if ~isempty(value)
                    if isjava(value)
                        value = value.toString;
                    elseif isnumeric(value)
                        value = num2str(value);
                    end
                else
                    value = '';
                end
                % Multiline tooltip
                j = 1;
                while j * 100 < size(value, 2)
                    value = sprintf('%s<br/>%s',value(1:j*100), value(j*100+1:size(value, 2)));
                    j = j+1;
                end
                table.setToolTipText(['<html> ' char(value)]);
            end
            function cbSelect(varargin)
                %% cbSelect - callback, copies params on double Click
                %
                set(bDel, 'Enable', 'on');
                set(bCopy, 'Enable', 'on');
                if size(varargin,2) == 2  % selection by double click
                    ev = varargin{1,2};
                    clicks = ev.getClickCount;
                    if clicks > 1
                        return;
                    end
                end
            end
        end
    end
end

