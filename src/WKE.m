classdef WKE < handle
    %% WKE implements Windkessel elements
    %          for now the implementation supports 3 types of linear systems: 
    %             - linear elastic, 
    %             - linear visco elastic Maxwell 
    %             - linear visco elastic Voigt
    %          support of nonlinear systems is geared up, but not fully implemented. 
    %    See also: SSModel
    %
    % WKE Properties:
    %   constant props
    %      knownTypes   - {'linElastic', 'linViscoElasticMaxwell', 'linViscoElasticVoigt', } 
    %      knownFlavors - {'qq', 'pp', 'pq'};  % ToDo: qp for one element nets root
    %    
    %         pos        - equ no. (=row and col) of core matrix in connected matrix
    %
    %  read-only props
    %         model      - the model
    %         equations  - number of equations
    %         type       - one of WKE.knownTypes
    %         flavor     - one of WKE.knownFlavors
    %         parent     - index of parent element
    %         nodeId     - own index
    %         isroot     - true, if root node
    %         isboundary - true, if boundary node
    %
    % instance methods:
    %       WKE        - creates a typed and flavored WKE object based on anet's root and boundary node's types
    %
    % class methods:
    %       Equ         - returns the number of equations for an WKE of given type and flavor
    %       model_le_qq - creates matrix AB and vector u for linear elastic WKE (Pi) used as root node adapter
    %       model_le_pq - creates matrix AB and vector u for linear elastiWKE (inverted L) used as standard
    %       model_le_pp - creates matrix AB and vector u for linear elastic WKE (T) used as leave node adapter 
    %       model_lveV_qq - creates matrix AB and vector u for linear viscc o elastic Voigt WKE (Pi) used as root node adapter
    %       model_lveV_pq - creates matrix AB and vector u for linear visco elastic Voigt WKE (T) used as leave node adapter
    %       model_lveM_qq - creates matrix AB and vector u for linear visco elastic Maxwell WKE (Pi) used as root adapter
    %       model_lveM_pq - creates matrix AB and vector u for linear visco elastic Maxwell WKE (L inverted) used as standard
    %       model_lveM_pp - creates matrix AB and vector u for linear visco elastic Maxwell WKE (T) used as leave node adapter
    %       model_heart - creates matrix AB and u for heart
    
    % SISCA (Simulation, Identification and Statistical variation in Cardiovascular Analysis)
    %
    % Copyright (C) 2010 - 2018 FU Berlin (Mathematics, workgroup Prof. Christof Schütte)
    % Copyright (C) 2019 - 2020 THM Giessen (LSE, workgroup Prof. Stefan Bernhard)
    %
    % Author: Rudolf Huttary
    %
    % This program is free software: you can redistribute it and/or modify it under the terms 
    % of the GNU Affero General Public License Version 3 as published by the Free Software Foundation.
    %
    % This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
    % without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    % See the GNU Affero General Public License for more details.
    %
    % You should have received a copy of the GNU Affero General Public License along with this program. 
    % If not, see <http://www.gnu.org/licenses/>.
    
    properties (Constant)
        knownTypes = {'linElastic', 'linViscoElasticMaxwell', 'linViscoElasticVoigt', 'heartLinElastic', 'heartViscoMaxwell', 'heartViscoVoigt'}; 
        knownFlavors = {'qq', 'pq', 'pp'};  %  'qp' is missing
    end
    properties
        pos        % equ no. (=row and col) of core matrix in connected matrix
        children
        R
        C
        L
        b_R
        b_C = 0; 
        b_q
        b_p
        R2
        C2
        R3
        C3
        R4
    end
    properties (GetAccess = public, SetAccess = private)
        model      % the model
        equations  % number of equations
        type       % one of WKE.knownTypes
        flavor     % one of WKE.knownFlavors
        parent     % index of parent element
        nodeId     % own index
        isroot = false     % true, if root node
        isboundary = false % true, if boundary node
        isheart = false;   % true, if closed loop system
    end
    
    properties (Access = private)
        fpArr = {@WKE.model_le_qq   @WKE.model_le_pq   @WKE.model_le_pp ...
                 @WKE.model_lveM_qq @WKE.model_lveM_pq @WKE.model_lveM_pp ...
                 @WKE.model_lveV_qq @WKE.model_lveV_pq @WKE.model_lveV_pp};
        equArr = [3 2 3 7 4 5 3 2 3]; 
        fpModel  % function pointer for method generating model 
    end
    
    %% instance methods
    methods
        function this = WKE(anet, nodeIdx) % ctor
        %% WKE - creates a typed and flavored WKE object based on anet's root and boundary node's types
        %       - flavor is selected automatically on type of boundary conditions
        %       - 'qp' flavors are not implemented, therefore interconnecting nodes are modelled with 'pq' flavor only
        %       - being modelled as heart, 'qq' flavor is used for root node and 'pp' flavor for all boundary nodes
        %    >> wke = WKE(node);
        %  PARAMETER
        %     'anet'    ArteryNet object 
        %     'nodeIdx' index of associated node

            help = '<a href="matlab:help WKE/WKE">USAGE</a>'; 
            if nargin == 0   % standard ctor, used for arrays 
                return; 
            end
            if nargin ~= 2 && ~isa(anet, 'ArteryNet')
                error('WKE.WKE: two parameters expected\n%s\n', help); 
            end
            ps = anet.getNodeParamSet(nodeIdx); 
            fIdx = 2;        % assume pq
            tIdx = anet.n_type; 
            this.type = WKE.knownTypes{tIdx}; 
            if tIdx > length(WKE.knownTypes); 
                error('WKE.WKE: parameter error, ArteryNet object has unknown n_type: \n%i\n', tIdx); 
            end
            if ps.isroot
                if tIdx > 3  % heart?
                    if ps.isboundary
                        error('WKE.ctor: one node models of type %s are not possible', this.type); 
                    end
                    this.isheart = true; 
                    fIdx = 1;  % qq
                elseif strcmp(anet.CtsIn.type, 'q')
                       fIdx = 1;  % qq
                elseif ~strcmp(anet.CtsIn.type, 'p')
                       error('WKE.FromNode: unsupported ctsIn type for root node found'); 
                end
                if ps.isboundary 
                    if ~isempty(ps.b_p)  % ToDo: implement one element pq network for all types
                        fIdx = 3;  % pp
                    else
                        error('pq flavors for one element structures are not implemented'); 
                    end
                end
                
            elseif ps.isboundary 
                if tIdx > 3 || isempty(ps.b_q)
                    fIdx = 3;   % pp
                end
            end

            if this.isheart
                this.fpModel =   @WKE.model_heart; 
                this.equations = 2; 
            else
                idx = mod(tIdx-1,3)*3; 
                this.fpModel =   this.fpArr{ idx + fIdx};  
                this.equations = this.equArr(idx + fIdx); 
            end
            this.type = WKE.knownTypes{tIdx}; 
            this.flavor = WKE.knownFlavors{fIdx}; 
            this.isroot = ps.isroot; 
            this.isboundary = ps.isboundary; 
            this.parent = ps.parent; 
            this.nodeId = nodeIdx; 
            this.R = ps.R; 
            this.C = ps.C; 
            this.L = ps.L; 
            this.b_R = ps.b_R; 
            if ~isempty(ps.b_C)
                this.b_C = ps.b_C; 
            end
            this.b_q = ps.b_q; 
            this.b_p = ps.b_p; 
            this.R2 = ps.R2; 
            this.C2 = ps.C2; 
            this.R3 = ps.R3; 
            this.C3 = ps.C3; 
            this.R4 = ps.R4; 
            this.UpdateState;   % generate the model; 
            
            if isfield(this.model, 'symb')
                this.model.symb = SymbIdx(nodeIdx, this.model.symb); 
            end
            
            function Carr = SymbIdx(idx, Carr)
            % replaces placeholder 'i' by current node index
                for j = 1: numel(Carr)
                    if Carr{j} ~= '0'; 
                        Carr{j} = strrep(Carr{j}, 'i', num2str(idx)); 
                    end
                end
            end
            
        end
        function res = X0(this, p0, R) 
        %% X0 - calcs R and [p q] vector of subtree reigned by this element
        %       recursive function! 
        % PARAMETER:
        %   'p0' mean pressure at root (only for pp calc used) 
        %   'R'  resistance sum of parent elements 
            
            switch this.flavor
                case 'pq'
                    if this.isboundary
                        q = this.b_q; 
                        res.Z = this.R + this.b_R; 
                        p = p0-(R+res.Z)*q;            % pressure offset
                        res.x0(this.equations) = p; 
                        res.x0(1) = q; 
                    else
                        q = 0; 
                        Rsum = 0;
                        x0 = []; 
                        for i = 1: length(this.children)
                            resc = this.children(i).X0(p0, R+this.R); 
                            Rsum = Rsum + 1/resc.Z;             % R of children are parallel
                            q = q + resc.x0(1);                 % own flow is sum of children's inflows
                            x0 = [x0 resc.x0];                  % collect children's values
                        end
                        res.Z = this.R + 1/Rsum;          
                        p = p0 - (R+this.R) * q; 
                        me(this.equations) = p; 
                        me(1) = q; 
                        res.x0 = [me x0]; 
                    end
                case 'qq'   
                    % p0 is a flow in this case!
                    % we start with two estimations e1, e2 to calc p0 by a linear transformation
                    % p0 = p1+ (e2-e1)/(q2-q1)*(q-q1)
                    % then we can calc the solution
                    if this.isroot && this.isboundary
                        res.Z = this.R + this.b_R; 
                        pin = res.Z*p0; 
                        res.x0(this.equations) = 0; 
                        res.x0(1:2) = [pin p0]; 
                    elseif this.isroot
                        e1 = 1e5; 
                        e2 = 2e5; 
                        q = p0; 
                        ch = this.children(1); 
                        resc1 = ch.X0(e1, this.R); 
                        resc2 = ch.X0(e2, this.R); 
                        pin = (q-resc1.x0(1)) * (resc2.x0(2)-resc1.x0(2))/(resc2.x0(1)-resc1.x0(1))+ resc1.x0(2); 
                        resc = ch.X0(pin, this.R); 
                        pout = pin-this.R*q; 
                        res.Z = resc.Z+this.R; 
                        me(this.equations) = pout; 
                        me(1:2) = [pin p0]; 
                        if this.isheart
                            res.x0 = [p0, p0 resc.x0];  % initial condition heartbeat starts  
                        else
                            res.x0 = [me resc.x0]; 
                        end
                    else
                        error('WKE.X0: flavor ''%s''is not implemented for root nodes.', this.flavor); 
                    end
                case 'pp'
                    if this.isboundary
                        res.Z = this.R + this.b_R; 
                        R = R + res.Z; 
                        q = (p0-this.b_p) / R; 
                        p = this.b_p + this.b_R*q; 
                        res.x0(this.equations) = q; 
                        res.x0(1:2) = [q p]; 
                    else
                        error('WKE.X0: flavor ''%s''is not implemented for leaf nodes.', this.flavor); 
                    end
                        
                otherwise
                    error('WKE.X0: flavor ''%s''is not implemented.', this.flavor);   % ToDo: implement this case
            end
        end
        function UpdateState(this)
            this.model = this.fpModel(this); 
        end
        function p = GetParams(this)
        %%  GetParams - exposes public collect    
            p.R = this.R; 
            p.C = this.C; 
            p.L = this.L; 
            p.b_R = this.b_R; 
            if isempty(this.b_q)
                p.b_p = this.b_p;
                p.b_q = 0; 
            else
                p.b_p = 0; 
                p.b_q = this.b_q;
            end
        end
    end
    %% static methods
    methods(Static)
        function eq = Equ(type, flavor)
        %%  Equ - returns the number of equations for an WKE of given type and flavor
        %             intended use: calculation of matrix size for connected system
        %   >> eq = WKE.Equ(type, flavor); 
        %   >> eq = WKE.Equ('linElastic', 'pp');   
        % PARAMETER:
        %  PARAMETER
        %      'type' element of WKE.knownTypes = {'linElastic', 'linViscoElasticMaxwell', 'linViscoElasticVoigt'} 
        %    'flavor' element of WKE.knownFlavors = {'qq', 'pp', 'pq'};
        %        'eq' number of equations
        
           help = '<a href="matlab:help WKE/Equ">USAGE</a>'; 
           if nargin ~= 2 || ~ischar(flavor) || ~ischar(type)
               error('WKE.Equ: parameter error, two string types expected\n%s\n', help); 
           end
           [a tIdx] = ismember(type, WKE.knownTypes);  
           [b fIdx] = ismember(flavor, WKE.knownFlavors);
           if a && b
               eq = this.equArr{(tIdx-1)*length(WKE.knownTypes)+ fIdx}; 
           else
               error('WKE.WKE: parameter error, unknown type or flavor used\n%s\n', help); 
           end
        end
        
        % linear elastic
        function m = model_le_qq(p) % Pi-Element
        %% model_le_qq - creates matrix AB and vector u for linear elastic WKE (Pi) used as root node adapter
        %  PARAMETER: m with fields 'AB' and 'u'
        %           AB(:,1) - upstream connection
        %           AB(:,2) - upstream result
        %         AB(:,n-1) - downstream result
        %           AB(:,n) - downstream connection
        %                 u - outlet condition
            %        Qin    p0                   q         p1        Qout
            m.AB = [ 2/p.C  0        -2/p.C      0         0         % p0'
                     0      1/2/p.L  -p.R/2/p.L  -1/2/p.L  0         % q'
                     0      0        2/p.C       0         -2/p.C];  % p1'
            m.u = [p.b_q]; % outlet condition
             %        Qin        p0          q               p1           Qout
            m.symb = {'2/C(i)'   '0'         '-2/C(i)'       '0'          '0'   
                      '0'        '1/2/L(i)'  '-R(i)/2/L(i)'  '-1/2/L(i)'  '0' 
                      '0'        '0'         '2/C(i)'        '0'          '-2/C(i)'}; 
        end
        function m = model_le_pq(p) % L-inverted
        %% model_le_pq - creates matrix AB and vector u for linear elastic WKE (inverted L) used as standard
        %  PARAMETER: m with fields 'AB' and 'u'
        %           AB(:,1) - upstream connection
        %           AB(:,2) - upstream result
        %         AB(:,n-1) - downstream result
        %           AB(:,n) - downstream connection
        %                 u - outlet condition
            %        Pin    q0        p1      Qout
            m.AB = [ 1/p.L  -p.R/p.L  -1/p.L  0       % q0'
                     0      1/p.C     0       -1/p.C]; % p1'
            m.u = [p.b_q];       % outlet condition
             %        Pin        q0            p1         Qout
            m.symb = {'1/L(i)'   '-R(i)/L(i)'  '-1/L(i)'  '0' 
                      '0'        '1/C(i)'      '0'        '-1/C(i)'}; 
        end
        function m = model_le_pp(p) % T-Element
        %% model_le_pp - creates matrix AB and vector u for linear elastic WKE (T) used as leave node adapter 
        %    uses peripheral R, C 
        %  PARAMETER: m with fields 'AB' and 'u'
        %           AB(:,1) - upstream connection
        %           AB(:,2) - upstream result
        %         AB(:,n-1) - downstream result
        %           AB(:,n) - downstream connection
        %                 u - outlet condition
            c = p.C + p.b_C; 
            %           Pin    q0          p           q1            Pout
            m.AB =    [ 2/p.L  -2*p.R/p.L  -2/p.L      0             0         % q0'
                        0      1/c         0           -1/c          0         % p'
                        0      0           2/p.L       -2*p.b_R/p.L  -2/p.L];  % q1'
            m.u = [p.b_p];       % outlet condition
            %          Pin       q0                    p            q1                          Pout
            m.symb = { '2/L(i)'  '-2*R(i)/L(i)'        '-2/L(i)'    '0'                         '0'       % q0'
                       '0'       '1/(C(i)+b_C(i))'  '0'             '-1/(C(i)+b_C(i))'    '0'       % p'
                       '0'       '0'                   '2/L(i)'     '-2*b_R(i)/L(i)'            '-2/L(i)'};  % q1'
        end
        % linear visco elastic Voigt
        function m = model_lveV_qq(p)
        %% model_lveV_qq - creates matrix AB and vector u for linear visco elastic Voigt WKE (Pi) used as root node adapter
        %  PARAMETER: m with fields 'AB' and 'u'
        %           AB(:,1) - upstream connection
        %           AB(:,2) - upstream result
        %         AB(:,n-1) - downstream result
        %           AB(:,n) - downstream connection
        %                 u - outlet condition
            %        Qin            p0                    q          p1      Qout
            m.AB = [2/p.C   -2*p.R4/p.L   2*p.R4*p.R/p.L-2/p.C  2*p.R4/p.L      0   % p0'
                        0         1/p.L               -p.R/p.L      -1/p.L      0   % q'
                        0    2*p.R4/p.L   2/p.C-2*p.R4*p.R/p.L -2*p.R4/p.L -2/p.C]; % p1'
            m.u = [p.b_q];      % outlet condition
            
            %         Qin       p0              q                           p1              Qout
            m.symb = {'2/C(i)'  '-2*R4(i)/L(i)' '2*R4(i)*R(i)/L(i)-2/C(i)'  '2*R4(i)/L(i)'  '0'         % p0'
                      '0'       '1/L(i)'        '-R(i)/L(i)'                '-1/L(i)'       '0'         % q'
                      '0'       '2*R4(i)/L(i)'  '2/C(i)-2*R4(i)*R(i)/L(i)'  '-2*R4(i)/L(i)' '-2/C(i)'}; % p1'
        end
        function m = model_lveV_pq(p)
        %% model_lveV_pq - creates matrix AB and vector u for linear visco elastic Voigt WKE (L inverted) used as standard
        %  PARAMETER: m with fields 'AB' and 'u'
        %           AB(:,1) - upstream connection
        %           AB(:,2) - upstream result
        %         AB(:,n-1) - downstream result
        %           AB(:,n) - downstream connection
        %                 u - outlet condition
            %           Pin       q0                     p1          Qout
            m.AB = [    1/p.L     -(p.R)/p.L             -1/p.L      0      % q0'
                        p.R4/p.L  1/p.C-p.R4*(p.R)/p.L   -p.R4/p.L   -1/p.C];    % p1'
            m.u = [p.b_q]; % outlet condition
            %          Pin          q0                                p1             Qout
            m.symb = { '1/L(i)'     '-(R(i)+b_R(i))/L(i)'             '-1/L(i)'      '0'          % q0'
                       'R4(i)/L(i)' '1/C(i)-R4(i)*(R(i)+b_R(i))/L(i)' '-R4(i)/L(i)'  '-1/C(i)'};  % p1'
        end
        function m = model_lveV_pp(p)
        %% model_lveV_pq - creates matrix AB and vector u for linear visco elastic Voigt WKE (T) used as leave node adapter
        %  PARAMETER: m with fields 'AB' and 'u'
        %           AB(:,1) - upstream connection
        %           AB(:,2) - upstream result
        %         AB(:,n-1) - downstream result
        %           AB(:,n) - downstream connection
        %                 u - outlet condition

%         m = WKE.model_le_pp(p);  % hack: boundary elements have linear elastic coupling
%         return; 
        c = p.C + p.b_C; 
        
            %        Pin         q0                  p              q1                     Pout
            m.AB =     [ 2/p.L       -2*p.R/p.L          -2/p.L         0                      0            % q0'
                         2*p.R4/p.L  1/c-2*p.R4*p.R/p.L  -4*p.R4/p.L    2*p.R4*p.b_R/p.L-1/c   2*p.R4/p.L   % p'
                         0           0                   2/p.L          -2*p.b_R/p.L           -2/p.L    ]; % q1'
            m.u = [p.b_p];       % outlet condition
            %            Pin           q0                                   p               q1                                      Pout
            m.symb = {  '2/L(i)'       '-2*R(i)/L(i)'                       '-2/L(i)'       '0'                                     '0'              % q0'
                        '2*R4(i)/L(i)' '1/(C(i)+b_C(i))-2*R4(i)*R(i)/L(i)'  '-4*R4(i)/L(i)' '2*R4(i)*b_R(i)/L(i)-1/(C(i)+b_C(i))' '2*R4(i)/L(i)'   % p'
                        '0'            '0'                                  '2/L(i)'        '-2*b_R(i)/L(i)'                        '-2/L(i)'};      % q1'
        end
        % linear visco elastic Maxwell
        function m = model_lveM_qq(p)
        %% model_lveM_qq - creates matrix AB and vector u for linear visco elastic Maxwell WKE (Pi) used as root adapter
        %  PARAMETER: m with fields 'AB' and 'u'
        %           AB(:,1) - upstream connection
        %           AB(:,2) - upstream result
        %         AB(:,n-1) - downstream result
        %           AB(:,n) - downstream connection
        %                 u - outlet condition
        
          m.u = [p.b_p];       % outlet condition
          sumC = 2/p.C + 2/p.C2 + 2/p.C3; 
          %          q0      p0           q             p2            p3           p2_           p3_          p1       q1
          m.AB = [ sumC       0       -sumC   -1/p.R2/p.C2  -1/p.R3/p.C3             0             0           0        0   % p0'
                      0   1/p.L    -p.R/p.L              0             0             0             0      -1/p.L        0   % q'
                 2/p.C2       0     -2/p.C2   -1/p.R2/p.C2             0             0             0           0        0   % p2'
                 2/p.C3       0     -2/p.C3              0  -1/p.R3/p.C3             0             0           0        0   % p3'
                      0       0      2/p.C2              0             0  -1/p.R2/p.C2             0           0  -2/p.C2   % p2_'
                      0       0      2/p.C3              0             0             0  -1/p.R3/p.C3           0  -2/p.C3   % p3_'
                      0       0        sumC              0             0  -1/p.R2/p.C2  -1/p.R3/p.C3           0    -sumC]; % p1'
          [a, b] = size(m.AB);         
          m.symb(1:a, 1:b) = {'ToDo'}; 
        end
        function m = model_lveM_pq(p)
        %% model_lveM_pq - creates matrix AB and vector u for linear visco elastic Maxwell WKE (L inverted) used as standard
        %  PARAMETER: m with fields 'AB' and 'u'
        %           AB(:,1) - upstream connection
        %           AB(:,2) - upstream result
        %         AB(:,n-1) - downstream result
        %           AB(:,n) - downstream connection
        %                 u - outlet condition
          sumC = 1/p.C + 1/p.C2 + 1/p.C3; 
          %        Pin            q0            p2            p3            p1        Qout
          m.AB = [1/p.L     -(p.R+p.b_R)/p.L     0               0       -1/p.L        0    % q0'
                      0       1/p.C2  -1/p.C2/p.R2               0            0   -1/p.C2   % p2'
                      0       1/p.C3             0    -1/p.C3/p.R3            0   -1/p.C3   % p3'    
                      0         sumC   -1/p.C2/p.R2    -1/p.C3/p.R3       0       -sumC ] ; % p1'
          m.u = [p.b_q];       % outlet condition
          [a, b] = size(m.AB);         
          m.symb(1:a, 1:b) = {'ToDo'}; 
        end
        function m = model_lveM_pp(p)
        %% model_lveM_pp - creates matrix AB and vector u for linear visco elastic Maxwell WKE (T) used as leave node adapter
        %  PARAMETER: m with fields 'AB' and 'u'
        %           AB(:,1) - upstream connection
        %           AB(:,2) - upstream result
        %         AB(:,n-1) - downstream result
        %           AB(:,n) - downstream connection
        %                 u - outlet condition
          sumC = 1/p.C + 1/p.C2 + 1/p.C3; 
          %        Pin            q0           p            p2              p3            q1       Pout
          m.AB = [2/p.L   -2*p.R/p.L      -2/p.L             0               0             0           0   % q0'
                      0         sumC           0  -1/p.C2/p.R2     -1/p.C3/p.R3        -sumC           0   % p'
                      0       1/p.C2           0  -1/p.C2/p.R2               0       -1/p.C2           0   % p2'
                      0       1/p.C3           0             0     -1/p.C3/p.R3      -1/p.C3           0   % p3'
                      0            0       2/p.L             0               0  -2*p.b_R/p.L      -2/p.L]; % q1'
          m.u = [p.b_p];       % outlet condition
          [a, b] = size(m.AB);         
          m.symb(1:a, 1:b) = {'ToDo'}; 
        end
        function m = model_heart(p)
        %% model_heart - creates matrix AB and u for heart
        %  PARAMETER: m with fields
        %   'AB'  -           filling phase
        %   'AB1'  -           ejection phase
        %   'AB2'  -           isovolumic phase
        %           ABx(:,1) - upstream connection
        %           ABx(:,2) - upstream result
        %         ABx(:,n-1) - downstream result
        %           ABx(:,n) - downstream connection
        
            %         Qin     p0          z          Qout
            m.AB = [  1/p.C   -1/p.C/p.R  1/p.C/p.R  0    % p0' FILLING
                      0       1/p.R       -1/p.R     0];  % row z  will be multiplied with E(t)
                  
            m.AB1 = [ 1/p.C   0           0          0    % p0' EJECTION
                      0       0           0          -1];  % 
                 
            m.AB2 = [ 1/p.C   0           0          0    % p0'  ISOVOLUMIC
                      0       0           0          0];  % z'
                 
%            m.u = []; % outlet condition
          [a, b] = size(m.AB);         
          m.symb(1:a, 1:b) = {'heart'}; 
        end
    end
end
