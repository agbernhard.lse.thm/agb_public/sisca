classdef CTS < DbObj
    %% CTS represents a Characteristic Time Series (CTS) stored in db table CTS 
    %    how to generate cts from raw time series data?
    %    > peaks = Signalprocessor.peakDetect(data, 450, 0);    % extract peeks and select 'nice' period
    %    > cts = CTS;                  )                        % new obj 
    %    %> cts = CTS(id);                                       % or  get a template from db
    %    > cts.data = data(peaks(n): peaks(n+1)-1);             % extract cts data
    %    > cts.rate = 500;                                      % if ~= 1000
    %    > cts.name = 'newname';                                % select a name 
    %    > cts.description = 'desc';                            % select a description
    %    > cts.unit = 4;                                        % if ~= 4 (mmHg)
    %    > cts.type = 'p';                                      % 'p' or 'q'
    %    > cts.periodize;                                       % linear shift to fit maxima
    %    > figure; plot(cts.Cts2TS(3));                         % view result
    %    > cts.data = cts.phaseshift(pi);                       % shift phase
    %    > cts.plot;                                            % view data
    %    > cts.SaveAs;                                          % store in DB
    %
    %
    % CTS Properties:
    %     signalname  - optional name
    %     description - optional description
    %     rate        - samples per second
    %     type        - type id (refers to record in db table TSType, 1 = p, 2 = q)... 
    %     data        - root node 
    %     unit        - unit id (refers to record in db table Units)
    %     macsimId    - reserved
    %     STSId       - reserved
    %     ctsSetId    - reserved
    %
    % class methods:
    %     Delete      - deletes one or more CTS objects in database 
    %     List        - generates a list of known CTS objects in db and displays it
    %
    % instance methods: 
    %     CTS (ctor)     - Constructor, reads object with id from db table CTS
    %     cts2struct     - pack data in struct
    %     Const2TS       - generate a constant time series over a time interval 't' 
    %     Cts2TS         - repeates CTS over a time interval 't'
    %     periodize      - linear transformation to fit first and last data point
    %     plot           - plots CTS in new figure
    %     plotWith       - reads 2nd cts from db and plots both with equal (resampled) period
    %     resampleFFT    - resamples object data at given rate using fft
    %     resampleSpline - uses spline interpolation to resample data at a given rate
    %     Save           - alters existing record in db table CTS
    %     SaveAs         - inserts object as new record in db table CTS
    %     Unit2SI        - rescale data property using SI unit
    %     Smoothen       - smoothes data curve using a triangulation algorithm
    %
    % 

    % SISCA (Simulation, Identification and Statistical variation in Cardiovascular Analysis)
    %
    % Copyright (C) 2010 - 2018 FU Berlin (Mathematics, workgroup Prof. Christof Schütte)
    % Copyright (C) 2019 - 2020 THM Giessen (LSE, workgroup Prof. Stefan Bernhard)
    %
    % Author: Rudolf Huttary
    % Contributors: Stefan Bernhard, Urs Hackstein, Alexander Mair
    %
    % This program is free software: you can redistribute it and/or modify it under the terms 
    % of the GNU Affero General Public License Version 3 as published by the Free Software Foundation.
    %
    % This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
    % without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    % See the GNU Affero General Public License for more details.
    %
    % You should have received a copy of the GNU Affero General Public License along with this program. 
    % If not, see <http://www.gnu.org/licenses/>.

    properties
        % inherited: id, name, description
        RTSId = 0; 
        macsimId = 0;
        STSId = 0;
        ctsSetId = 0;
        signalName = '';
        type = 1;
        typeName = ''; 
        unit = 4;
        lastmodified;
        period = 1; 
        rate = 1000;
        data;
        fft; 
        units;
    end
    
    methods
        function this = CTS(id, db)
        %% CTS - Constructor, reads object with id from db table CTS
        %   >> s = CTS(id);      % read CTS with given numerical id from db table CTS
        %   >> s = CTS(id, db);  %     " and use given db object 
        % PARAMETER: this = CTS(id, db)
        %   'id' - id of db object
        %   'db' - (optional, default: = AngioDB) is an existing AngioDB object
        
            help = '<a href="matlab:help CTS/CTS">USAGE</a>'; 
            if nargin < 1
                this.signalName = 'unnamed'; 
                return; 
            end
            if ~isnumeric(id)
                error(sprintf('CTS.CTS: Parameter error\n%s\n', help));
            end
            if nargin < 2
                db = AngioDB;
            end
            if nargin < 3
                if isnumeric(id)
                    res = db.select(sprintf('Select * from CTS where id = %i', id)); 
                    if isempty (res)
                        error(sprintf('CTS: no object with id=%i in db', id));
                    end
                    [headers un]= db.select1('Select name from Units'); 
                    [headers t]= db.select1(['Select typeName from TSType where id = ' num2str(res.type)]); 
                    this.units = un; 
                    this.id = res.id;
                    this.RTSId = res.RTSId;
                    this.macsimId = res.macsimId;
                    this.STSId = res.STSId;
                    this.ctsSetId = res.ctsSetId;
                    this.signalName = char(res.signalName);
                    this.type = res.type;
                    this.typeName = t{1}; 
                    this.unit = res.unit;
                    this.description = char(res.description);
                    this.lastmodified = datevec(char(res.lastmodified));
                    this.rate = double(res.rate);
                    this.data = typecast(res.data, 'double');
                    this.period = length(this.data)/this.rate; 
                end
                this.Unit2SI; 
            end
        end
        
        function s = cts2struct(this)
        %%  cts2struct     - pack data in struct
            s.dbId = this.id; 
            s.signalName = this.signalName;
            s.description = this.description; 
            s.type = this.typeName;
            s.unit = this.units{this.unit}; 
            s.period = this.period; 
            s.rate = this.rate; 
            s.data = this.data; 
        end
        
        function Unit2SI(this)
        %%  Unit2SI - rescale data property using SI unit
        % PARAMETER: Unit2SI(this)
        
            switch this.unit
                case 4 % mmHg
                    this.data = this.data/7.5E-3;
                    this.unit = 5;
                case 5 % Pa
                    this.data = this.data;
                case 6 % kPa
                    this.data = this.data*1E3;
                case 7 % MPa
                    this.data = this.data*1E6;
                case 8 % m^/s
                    this.data = this.data;
                case 9 % l/s
                    this.data = this.data*1E3;
                case 10 % ml/s
                    this.data = this.data*1E6;
                case {1,2,3,11,12,13}
                    error('Unit not implemented error: Conversion not implemented');
            end
            this.unit = 5; 
        end
        
        function Smoothen(this, n)
        %%  Smoothen - smoothes data curve using a triangulation algorithm
        %   PARAMETER: Smoothen(this, n) 
        %       'n' - (optional) number of runs to be done
        
        if nargin == 1
           n = 1; 
        end
        for i = 1:n
            if mod(i, 2) == 1
                x1 = [this.data(end); this.data]; 
                x = [this.data; this.data(1)]; 
                x1 = 0.5*(x+x1); 
                this.data = x1(1:end-1); 
            else
                x = [this.data(end); this.data]; 
                x1 = [this.data; this.data(1)]; 
                x1 = 0.5*(x+x1); 
                this.data = x1(2:end); 
            end
        end
        end
        
        function Save(this, db)
        %% Save - alters existing record in db table CTS
        %  >> mycts.Save; 
        %  >> mycts.Save(db); 
        % PARAMETER: Save(this, db)
        %       'db' - (optional, default: = AngioDB) is an existing AngioDB object

            if nargin < 2
                db = AngioDB; 
            elseif ~isa(db, 'AngioDB')
                error('CTS.Save: type ''AngioDB'' for second parameter expected');
            end
            if isempty(this.id)
                warning('SaveAs is used'); 
                this.SaveAs(db); 
                return; 
            end
            update = 'replace CTS (id, RTSId, macsimId, STSId, ctsSetId, signalName, type, unit, description, rate, data) ';
            values = ['values(' ...
                num2str(this.id) ',' ...
                num2str(this.RTSId) ',' ...
                num2str(this.macsimId) ',' ...
                num2str(this.STSId) ',' ...
                num2str(this.ctsSetId) ',''' ...
                this.signalName ''',' ...
                num2str(this.type) ',' ...
                num2str(this.unit) ',''' ...
                this.description ''',' ...
                num2str(this.rate) ', ?)'];
            res = db.update([update values], typecast(this.data, 'uint8'));
            if res<1 
               error('CTS.Save: Save-Update to Table CTS was not succesfull'); 
            end

        end
        
        function id = SaveAs(this, db)
        %% SaveAs - inserts object as new record in db table CTS
        %  >> mycts.SaveAs; 
        %  >> mycts.SaveAs(db); 
        %   PARAMETER: id = SaveAs(this, db)
        %       'db' (optional, default: = AngioDB) is an existing AngioDB object
        
            if nargin < 2
                db = AngioDB; 
            elseif ~isa(db, 'AngioDB')
                error('CTS.Save: type ''AngioDB'' for second parameter expected');
            end
            update = 'replace CTS (RTSId, macsimId, STSId, ctsSetId, signalName, type, unit, description, rate, data) ';
            values = ['values(' ...
                num2str(this.RTSId) ',' ...
                num2str(this.macsimId) ',' ...
                num2str(this.STSId) ',' ...
                num2str(this.ctsSetId) ',''' ...
                this.signalName ''',' ...
                num2str(this.type) ',' ...
                num2str(this.unit) ',''' ...
                this.description ''',' ...
                num2str(this.rate) ', ?)'];
            res = db.update([update values], typecast(this.data, 'uint8'));
           if res<1 
               error('CTS.SaveAs: Inserting new row to table CTS was not succesfull'); 
           end
           res = db.select('select last_insert_id() pid');
           id = res.pid; 
           this.id = id; 
        end
        
        function samples = resampleSpline(this, newrate)
        %% resampleSpline - uses spline interpolation to resample data at a given rate
        %               explicit Save or SaveAs required for permanent access. 
        %   >> cts.resampleSpline(200);    % resample at a rate of 200; 
        %   PARAMETER: samples = resampleSpline(this, newrate)
        %      'newrate' new sample rate
        %      'samples' resampled data
           if nargin < 2
               error('CTS.resampleSpline: parameter missing\n%s', help('CTS.resampleFFT')); 
           end
           this.data = Signalprocessor.resampleSpline(this.data, this.rate, newrate);
           this.rate = newrate; 
           samples = this.data; 
        end
        
        function samples = resampleFFT(this, newrate)
        %% resampleFFT - uses fft interpolation to resample data at a given rate
        %               explicit Save or SaveAs required for permanent access. 
        %   >> cts.resampleFFT(200);    % resample at a rate of 200; 
        %   PARAMETER: samples = resampleFFT(this, newrate) 
        %      'newrate' new sample rate, value must be > 2
        %      'samples' resampled data

           if nargin < 2 || newrate < 3
               error('CTS.resampleFFT: parameter missing or newrate < 3\n%s', help('CTS.resampleFFT')); 
           end
           tperiod = length(this.data)/double(this.rate);
           time = 0:1/(newrate-1):tperiod; 
           this.data = Signalprocessor.resampleFFT(this.data, tperiod, time); 
           this.rate = newrate; 
           samples = this.data; 
        end
        function shift2mmHgSystDia(this, Systole, Diastole)
            switch this.unit
                case 5 % Pa
                    k = 133; 
                case 8 
                    k = Data*1e6; 
                otherwise
                    k = 1; 
            end
            mi = min(this.data); 
            ma = max(this.data); 
            data = (this.data - (mi+ma)/2)./(ma-mi)+0.5; % normalize
            this.data = k*(data*(Systole-Diastole)+Diastole); % apply new values
        end
        
        function plot(this)
        %% plot - plots CTS in new figure
        %  >> cts.plot; 
            Unit = this.unit; 
            Data = this.data; 
            switch Unit
                case 4 % mmHg
                    pText = 'pressure [mmHg]'; 
                case 5 % Pa
                    toggleUnit; 
                case 8 
                    pText = 'flow [ml/s]'; 
                    Data = Data*1e6; 
            end
            
%            delta = 1.0 / double(this.rate);
%            time = 0.0: delta: double(length(this.data) - 1) / double(this.rate); 
            
            time = ((1:length(this.data)) - 1) / double(this.rate); 
            Title = sprintf('CTS(id = %i, signalName = %s, rate = %i/s)', this.id, this.signalName, this.rate); 
            f = figure('Name', Title, 'NumberTitle', 'off'); 
            plotit; 
            function toggleUnit(varargin)
                switch Unit
                    case 4 % mmHg
                        Unit = 5; 
                        Data = Data/7.5E-3;
                        pText = 'pressure [Pa]'; 
                    case 5 % Pa
                        Data = Data*7.5E-3;
                        Unit = 4; 
                        pText = 'pressure [mmHg]'; 
                end
                if nargin
                    plotit; 
                end
            end
            function plotit
                mi = min(Data); 
                ma = max(Data); 
                mea = mean(Data); 
                plot(time([1,end]), [ma, ma], time([1,end]), [mi, mi], time([1,end]), [mea, mea],  time, Data); 
                legend({sprintf('syst %.2f',ma), sprintf('diast %.2f', mi), sprintf('mean %.2f', mean(Data))}); 
                xlabel(gca, 'time [s]'); 
                ylabel(gca, pText); 
                hYbl = get(gca, 'YLabel'); 
                set(hYbl, 'ButtonDownFcn', @toggleUnit); 
                title(this.description); 
            end
        end
        
        function plotWith(this, id2)
        %% plotWith - reads 2nd cts from db and plots both with equal (resampled) period
            N = length(this.data); 
            t = N/this.rate; 
            cts = CTS(id2); 
            dat = cts.data; 
            sig = Signalprocessor.resampleFFT(dat, t, 0:t/N:t-t/N); 
            switch this.unit
                case 4 % mmHg
                    pText = 'pressure [mmHg]'; 
                    f = 1;
                case 5 % Pa
                    pText = 'pressure [mmHg]'; 
                    f = 7.5E-3; 
                case 8 
                    pText = 'flow [ml/s]'; 
                    f = 1e6; 
            end
            
            figure; 
            plot((0:N-1)/this.rate, f*[this.data sig']);
            xlabel ('time [s]');  
            ylabel (pText);  
            legend(this.signalName,  cts.signalName); 
        end
        
        function periodize(this)
        %% periodize - linear transformation to fit first and last data point
        %   >> cts.periodize; 
          delta = this.data(1)-this.data(end); 
          dt = 1/double(length(this.data)); 
          this.data = this.data + (-0.5+dt/2 : dt : 0.5-dt/2)' * delta; 
        end
        
        function data = phaseshift(this, radian)
        %% cyledata 
            N = length(this.data); 
            offs = ceil(mod(radian, 2*pi)/2/pi *N); 
            I = [offs:N 1:offs-1]; 
            data = this.data(I); 
        end
        
        function TS = Const2TS(this, value, t)
        %%  Const2TS - generate a constant time series over a time interval 't' 
        %              uses given rate
        %  >> InSignal = cts.Cts2TS(10); 
        %  >> outSignal = cts.Const2TS(2000, 10);
        % PARAMETER: TS = Const2TS(this, value, t) 
        %     't'     - time interval in seconds
        %     'value' - constant value to be used
        %     'TS'    - resulting signal
           if nargin < 3
               error('CTS.Const2TS: parameter missing \n%s', '<a href="matlab:help CTS.Const2TS">USAGE</a>'); 
           end
            TS = value * ones(1, t*this.rate); 
        end
        
        function TS = Cts2TS(this, t)
        %% Cts2TS - repeates CTS over a time interval 't'
        %           explicit Save or SaveAs required for permanent access 
        %           use is intended only for periodical times series
        %  >> TS = cts.Cts2TS(10);   % repeat cts over 10s
        % PARAMETER: TS = Cts2TS(this, t)
        %     't'  - time interval in seconds
        %     'TS' - resulting signal
        %     
           N = t*this.rate; 
           M = numel(this.data); 
           TS(N) = 0; 
           L = floor(double(N)/M); 
           for i = 0: L
               TS(1+i*M:(i+1)*M) = this.data; 
           end
           TS = TS(1:N); 
            
        end
        
        function fft = FFT(this)
        %% FFT - calculates fft and sets fft property of this object
        %    >> cts.FFT; 
        %  PARAMETER: fft = FFT(this)
            fft = Signalprocessor.performFft(this.data, this.period); 
            fft.argn = fft.n' * 2 * pi; 
            this.fft = fft; 
        end
        
        
        function signal = addGaussianHRVtoCTS(this,meanPeriod,stdPeriod,Nperiods,newRate)
            %% Creates an input signal with heart rate variability
            % meanPeriod average length of a period
            % stdPeriod standard deviation 
            % newRate should be set because of otherwise high memory
            % consumption
                      
            signal = CTS.calcGaussianHRV(this.data,meanPeriod,stdPeriod,Nperiods,newRate);
            
            assert(length(signal)*8 < (65535 - 2),...
                "Signal to long to store in BLOB in database. Reduce newRate or Nperiods");
                        
            % Update Values in class
            this.rate = newRate;
            this.data = signal;
            this.period = length(this.data)/this.rate;
        
        end
    end
    methods (Static)
        function Delete(id)
        %% Delete - deletes one or more CTS objects in database 
        %           AUTO_INCREMENT is reduced if possible
        %  >> CTS.Delete([12 14 16]);   % deletes objects with ids 12, 14, 16  
        % PARAMETER: Delete(id)
        %   'id'   (vectorized) id of object to delete
           db = AngioDB; 
           for i = 1:length(id)
             db.exec(['delete from CTS where id = ' num2str(id(i))]);  
           end
           db.exec('ALTER TABLE CTS AUTO_INCREMENT = 1');  
        end
        function res = List(verbose, start, count)
        %% List  - generates a list of known CTS objects in db
        %    >> all = CTS.List;               % lists all in verbose mode and returns info in a struct array
        %    >> CTS.List(true, 3);            % list the first 3 objects in verbose mode 
        %    >> all = CTS.List(false);        % returns info in a struct array
        % PARAMETER: res = List(verbose, start, count)
        %       'verbose' (optional, default: true) if true, output to command window
        %       'start'   (optional, default = 1) first element to be listet
        %       'count'   (optional, default no restriction) restricts output to command window to maximal 'count' objects; 
        %       'res'     info array with fields: 'id', 'signalName', 'type', 'unit', 'rate', 'description'
            db = AngioDB; 
            res = db.select('select CTS.id, signalName as name, TSType.typeName as type, Units.name as unit, rate, description from CTS join TSType on CTS.type = TSType.id join Units on CTS.unit = Units.id order by CTS.id ' ); 
            if isempty(res)
                return; 
            end
            if nargin == 0
                verbose = true; 
            end
            startidx = 1; 
            len = length(res); 
            if nargin > 1
                if start == 0 || abs(start) > len
                    return; 
                end
                startidx = abs(start); 
            end
            if nargin > 2
                if count <= 0 
                    return; 
                end
                len = min(abs(startidx+count-1), len); 
            end
            if verbose
                fprintf('   id |                 Name |   Type |   Unit | Rate | Description\n'); 
                fprintf('==================================================================\n'); 
                for i = startidx : len
                    desc = res(i).description; 
                    f = strfind(desc, char(10)); 
                    desc(f) = '*'; 
%                    desc = desc(1: min(end,120)); 
                    fprintf('%5i | %20s | %6s | %6s | %4i | %s\n', res(i).id, res(i).name, res(i).type, res(i).unit, res(i).rate, desc); 
                end
            end
        end
        function cts = Open
        %% Open - construct db object using open dialog
            cts = Open@DbObj; 
            cts.plot; 
        end
        function cts = FromPeriodMax2Max(data, denoise, verbose)
        %% FromPeriodMax2Max - creates a Kalman smoothed periodic cts from a 'max to max' time series
        %  PARAMETER: 
        %    'data'      (vector) time series starting and ending mit maximum of period
        %    'denoise'   (optional, default = 3.3) Kalman filter characteristics (choose from approx. 1 to 8)
        %    'verbose'   (optional, default = false) plot result
        
            if nargin < 2
                denoise = 3.3; 
            end
            if nargin < 3
                verbose = true; 
            end
            data(:) = data; 
            N = length(data); 
            data = [data; data; data]'; 
            ctsKF = KF.SmoothSignal(data, denoise); 
            m = mean(ctsKF); 
            pos = ctsKF-m; 
            [~, mi] = min(abs(pos(1:N))); 
            cts = ctsKF(mi(1):mi(1)+N-1)'; 
            if verbose
                figure(113); 
                hold off; 
                plot(data(mi(1):mi(1)+N-1)); 
                hold on; 
                plot(cts, 'r', 'LineWidth', 2); 
                legend({'unfiltered', 'filtered'}); 
            end
            
        end
        function plotCTS(id)
            if length(id) == 1
                CTS(id).plot; 
            else
                cts = CTS(id(1)); 
                cts.plotWith(id(2)); 
            end
        end
        
        function signal = calcGaussianHRV(data,meanPeriod,stdPeriod,Nperiods,newRate)
            %% Static function to add HRV without changing the object
            time = [];               % time in array
            signal = [];             % signal in array
            t = 0;                   % start time with 0
            tstep = 1/newRate;       % seconds per sample
            
            for i = 1:Nperiods
                tnew = meanPeriod + randn(1)*stdPeriod;
                assert(tnew > 0, ...
                    "Negative period length encountered. Reduce stdPeriod or remodel the distribution"); %Rethink the distribution if that happens
                timeNew = (t):tstep:(t+tnew);
                time = [time timeNew];
                signalNew =  Signalprocessor.resampleFFT(data,tnew,timeNew-t);
                signal = [signal signalNew];
                t = t + tnew;
            end
        end
    end
end

