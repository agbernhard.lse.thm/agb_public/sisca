classdef StatisticalSet < handle
    %%  StatisticalSet represents a statical set around a SEED ParamSet object
    %     NOTE: a SEED is by default NOT an element of the statistical set. However any number of modified 
    %           or unmodified copies of the SEED may added explicitly into the set. 
    %
    %   StatisticalSet Properties:
    %      name = 'SS-'
    %      description = ''
    %
    %   instance Methods: 
    %      StatisticalSet               - Constructor with overloads for 'Id, 'ParamSet', 'New'
    %      Add                          - adds a new ParamSet object into the StatisticalSet, does a solver run, and writes both into DB
    %      Elements                     - lists all Element of StatisticalSet object; Seed element excluded
    %      Exclude                      - excludes a ParamSet object from StatisticalSet and deletes it in DB
    %      getId                        - returns the Id of StatisticalSet 
    %      getSeed                      - returns the ParamSet struct used as Seed for this StatisticalSet 
    %
    %   class methods:
    %      Delete                       - deletes StatisticalSet object in DB and all its dependencies
    %      ParamSets                    - returns the ids of all ParamSet objects grouped into StatisticalSet object 'ssId' 
    %      StatisticalSets              - returns the ids of all ParamSet objects grouped into StatisticalSet object 'ssId' 

    % SISCA (Simulation, Identification and Statistical variation in Cardiovascular Analysis)
    %
    % Copyright (C) 2010 - 2018 FU Berlin (Mathematics, workgroup Prof. Christof Schütte)
    % Copyright (C) 2019 - 2020 THM Giessen (LSE, workgroup Prof. Stefan Bernhard)
    %
    % Author: Rudolf Huttary
    %
    % This program is free software: you can redistribute it and/or modify it under the terms 
    % of the GNU Affero General Public License Version 3 as published by the Free Software Foundation.
    %
    % This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
    % without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    % See the GNU Affero General Public License for more details.
    %
    % You should have received a copy of the GNU Affero General Public License along with this program. 
    % If not, see <http://www.gnu.org/licenses/>.

    properties
        % DB
        name = 'SS-'
        description = ''
        arteryNet 
    end
    properties (Access = private)
        id
        seedParamSetId
        % state
        seedParamSet
        seedSolverRun
    end
    methods
        function this = StatisticalSet(varargin)
        %% StatisticalSet - Constructor with overloads for 'Id, 'ParamSet', 'Seed'
        %   >> this = StatisticalSet('Id', ssId, db);                           % open Set, StatisticalSet-Id given
        %   >> this = StatisticalSet('ParamSet', paramSetId, db);               % open Set, paramSetId given
        %   >> this = StatisticalSet('New', seedId, name, description, anet);   % implemented for internal use by ArteryNet only!
        %     PARAMS 
        %          - 'db': optional last parameter, denotes an AngioDB object
        %          - 'ssId': DB-Identifier of an existing StatisticalSet object
        %          - 'paramSetId': DB-Identifier of an existing ParamSet object
        %          - 'seedId': DB-Identifier of an existing ParamSet object used as seed
        %          - 'name': sets/overrides 'name' property, ignored if []
        %          - 'description': sets/overrides 'description' property, ignored if []
        %          - 'anet': instance of 
        %          - 'this' is the resulting object handle
        
            % examine first two parameters 
            if  nargin < 2 || ~ischar(varargin{1}) || ~isnumeric(varargin{2})   || ...
                ~strcmp(varargin{1}, 'Id') && ~strcmp(varargin{1}, 'New') && ~strcmp(varargin{1}, 'ParamSet') 
                err; 
            end
            % examine last parameters
            if nargin > 2  && ~strcmp(varargin{1}, 'New') && ~isa(varargin{3}, 'AngioDB')
               err; 
            end
            if  strcmp(varargin{1}, 'New') && ...
                ~(nargin == 5 && ...
                  ischar(varargin{3}) && ...
                  ischar(varargin{4}) && ...
                  isa(varargin{5}, 'ArteryNet')) 
               err; 
            end
            % db handle
            if isa(varargin{nargin}, 'AngioDB')
                db = varargin{nargin}; 
            elseif isa(varargin{nargin}, 'ArteryNet')
                anet = varargin{nargin}; 
                db = anet.db; 
            else
                db = AngioDB; 
            end
            
            switch varargin{1}  % choose between 3 possible constructions
                case {'Id'}     % load from DB, ssId given 
                    if varargin{2} == 1    % Superset
                        error('StatisticalSet(''Id'', 1, ... : StatisticalSet with id = 1 is not meant to be used'); 
                    end
                    ss = db.select(['select * from StatisticalSet where id=' num2str(varargin{2})]); 
                    if isempty(ss)
                        error(sprintf('StatisticalSet(''Id'', %i : There is no StatisticalSet object with this id\n', varargin{2})); 
                    end
                    this.id = ss.id; 
                    this.seedParamSetId = ss.seedParamSetId; 
                    this.name = char(ss.name); 
                    this.description = char(ss. description); 
                    this.arteryNet = ArteryNet.FromParamSetId(this.seedParamSetId, db);
                    
                case {'ParamSet'}   % load from ParamSet
                    ps = db.select(['select statisticalSetId from ParamSet where id=' num2str(varargin{2})]); 
                    if isempty(ps) || ps.statisticalSetId == 1
                        error(sprintf('StatisticalSet(''ParamSet'',...: either ParamSet object with id = %i does not exist or it is not grouped into a StatisticalSet', varargin{2} )); 
                    end
                    ss = db.select(['select * from StatisticalSet where id=' num2str(ps.statisticalSetId)]); 
                    if isempty(ss)
                        error(sprintf('StatisticalSet(''ParamSet''...,: There is no StatisticalSet object with id = %i\n', varargin{2})); 
                    end
                    this.id = ss.id; 
                    this.seedParamSetId = ss.seedParamSetId; 
                    this.name = char(ss.name); 
                    this.description = char(ss. description); 
                    this.arteryNet = ArteryNet.FromParamSetId(this.seedParamSetId, db);
                    
                case {'New'}    % generate new StatisticalSet in DB
                    this.seedParamSetId = varargin{2}; 
                    this.name = varargin{3}; 
                    this.description = varargin{4}; 
                    this.arteryNet = varargin{5}; 
                    values = ['values(' ...
                        num2str(this.seedParamSetId) ', ''' ...
                        this.name ''',''' ...
                        this.description ''' )']; 
                    res = db.update(['replace StatisticalSet (seedParamSetId, name, description) ' values]);
                    if res < 1
                        error('StatisticalSet(''New'', ...: Can''t write object to DB' ); 
                    end; 
                    res = db.select('select last_insert_id() sid');
                    this.id = res.sid;  
                    db.update(sprintf('update ParamSet set statisticalSetId=%i where id =%i', this.id, this.seedParamSetId));
            end
                
            this.seedParamSet = this.arteryNet.getParamSet; 
            this.seedSolverRun = this.arteryNet.getSolverRun;

            
            function err
                error(sprintf('StatisticalSet: Parameter error:\n %s', help('StatisticalSet.StatisticalSet')));
            end
        end
        
        function ssId = getId(this)
        %%  getId - returns the Id of StatisticalSet 
        %     >> ssId = sSet.getId; 
            ssId = this.id; 
        end
        
        function [SeedPS SeedSR] = getSeed(this)
        %%  getSeed - returns the ParamSet and SolverRun used as SEED for this StatisticalSet 
        %     >> [SeedPS SeedSR] = sSet.getSeed; 
        %       PARAMS
        %        - 'SeedPS' state of seed ParamSet object (as returned by anet.getParamSet)
        %        - 'SeedSR' state of seed SolverRun struct (as returned by anet.getSolverRun)
            SeedPS = this.seedParamSet; 
            SeedSR = this.seedSolverRun; 
        end
        
        function [solution paramSetId solverRunId] = Add(this, pset, srun, quiet, db)
        %%  Add - adds a new ParamSet object into the StatisticalSet, does a solver run, and writes both into DB
        %    >> [paramSetId solution]  = SSet.Add(pset);  
        %    >> [paramSetId solution]  = SSet.Add(pset, srun);  
        %    >> [paramSetId solution]  = SSet.Add(pset, srun, quiet);  
        %    >> [paramSetId solution]  = SSet.Add(pset, srun, quiet, db);  
        %       PARAMS
        %         - 'pset': state of new ParamSet object
        %         - 'srun': (optional) state of SolverRun object
        %         - 'quiet': (optional, default = false) if 'true' commandline output is omitted
        %         - 'db': optional last parameter, denotes an AngioDB object
        %         - 'solution': results of SolverRun for new ParamSet obejct
        %         - 'paramSetId':  DB id of new ParamSet object
        %         - 'solverRunId': DB id of new SolverRun object
        
            if nargin < 2
                error(sprintf('StatisticalSet.Add: Parameter error:\n %s', help('StatisticalSet.Add')));
            end
            if nargin > 2
                this.arteryNet.setSolverRun(srun); 
            end
            if nargin < 4
                quiet = false; 
            end
            if nargin < 5
                db = this.arteryNet.db; 
            end
            pset.statisticalSetId = this.id;  % force own id
            this.arteryNet.setParamSet(pset); 
            db.exec('Start Transaction'); 
            try
                paramSetId = this.arteryNet.ParamSetSaveAs(db, false, false); % save Seed ParamSet, no update CurrentArteryNetId, no transaction 
                if ~this.arteryNet.RunSolver(quiet)
                    error(sprintf('StatisticalSet.Add: SolverRun was not successful')); 
                end
                solution = this.arteryNet.getSolution; 
                solverRunId = this.arteryNet.SolverRunSave(db, true, false); % save SolverRun & solution, no transaction
            catch e
                db.exec('Rollback'); 
                error('StatisticalSet.Add: new Element was not added.\n %s', e.message); 
            end
            db.exec('Commit'); 
            db.close; 
            db.open; 
        end
        
        function Exclude(this, paramSetId, db)
        %%  Exclude - excludes a ParamSet object from StatisticalSet and deletes it in DB
        %    >> = SSet.Exclude(paramSetId);  
        %    >> = SSet.Exclude(paramSetId, db);  
        %       PARAMS
        %         - 'paramSetId': id of ParamSet object to be deleted
        %         - 'db': optional last parameter, denotes an AngioDB object
            if nargin < 3
                db = this.arteryNet.db; 
            end
            if nargin < 2 || ~isnumeric(paramSetId)
                error('StatisticalSet.Exclude: Wrong parameter type. \n%s', help('StatisticalSet.Exclude')); 
            end
            if paramSetId ~= this.seedParamSetId; 
                db.exec(sprintf('Delete from ParamSet where id = %i', paramSetId));
            end
        end
        
        function [plist] = Elements(this, db)
        %%  Elements - lists all Elements of StatisticalSet object; Seed element excluded
        %    >> plist = SSet.Elements;  
        %    >> plist = SSet.Elements(db);  
        %       PARAMS
        %        - 'db': optional last parameter, denotes an AngioDB object
            if nargin < 2
                db = this.arteryNet.db; 
            end
            plist = db.select(sprintf('select id, currentSolverRun, name, description from ParamSet where statisticalSetId = %i and id <> %i', this.id, this.seedParamSetId)); 
        end
    end
    
    methods (Static)
        function sset = StatisticalSets(varargin)
        %%  StatisticalSets  - returns the ids of all known StatisticalSet objects in the db
        %    >> sset = StatisticalSet.StatisticalSets;               % returns all known StatisticalSet objects 
        %    >> sset = StatisticalSet.StatisticalSets(db);           % returns all known StatisticalSet objects 
        %    >> sset = StatisticalSet.StatisticalSets(arteryNetId);  %     "         with filter on arteryNetId
        %    >> sset = StatisticalSet.StatisticalSets(arteryNetId, db); 
        %       PARAMS
        %        - 'db': optional last parameter, denotes an AngioDB object
        %        - 'ssId': denotes the StatisticalSet object
        %        - 'pset': vector with ids of ParamSet objects
        
            if nargin > 0 && ~isnumeric(varargin{1}) || nargin > 1 && ~isa(varargin{nargin}, 'AngioDB') 
                error(sprintf('StatisticalSet.StatisticalSets: Parameter error:\n %s', help('StatisticalSet.StatisticalSets')));
            end
            if nargin == 0 || nargin > 0 && ~isa(varargin{nargin}, 'AngioDB') 
               db1 = AngioDB; 
            else
               db1 = varargin{nargin}; 
            end
            where = 'where s.id <> 1'; 
            if nargin > 0 && isnumeric(varargin{1});
                where = ['join ParamSet as p on seedParamSetId = p.id where s.id <> 1 and p.arteryNetId =' num2str(varargin{1})]; 
            end
            request = ['Select s.id, seedParamSetId, s.name, s.description, s.lastmodified from StatisticalSet as s ' where];
            sset = db1.select(request); 
        end

        function [pset arteryNetId] = ParamSets(ssId, db)
        %%  ParamSets - returns the ids of all ParamSet objects grouped into StatisticalSet object 'ssId' 
        %    >> pset = StatisticalSet.StatisticalSets(ssId); 
        %    >> [pset arteryNetId] = StatisticalSet.StatisticalSets(ssId, db); 
        %       PARAMS
        %        - 'db': optional last parameter, denotes an AngioDB object
        %        - 'ssId': denotes the StatisticalSet object
        %        - 'pset': vector with ids of ParamSet objects
        %        - 'arteryNetId': id of the corresponding ArteryNet object. 
        
            if nargin == 0 || nargin > 0 && ~isnumeric(ssId) || nargin > 1 && ~isa(db, 'AngioDB') 
                error(sprintf('StatisticalSet.ParamSets: Parameter error:\n %s', help('StatisticalSet.ParamSets')));
            end
            if nargin == 1
               db = AngioDB; 
            end
            pset = []; 
            ps = db.select(['Select id, arteryNetId from ParamSet where statisticalSetId = ' num2str(ssId)]); 
            if ~isempty(ps)
                pset = [ps.id]; 
                arteryNetId = double(ps(1).arteryNetId); 
            end
        end
        
        function success = Delete(ssid, quietmode, db)
        %% Delete - deletes StatisticalSet object in DB and all its dependencies
        %   >> success = StatisticalSet.Delete(ssid, quietmode);
        %   >> success = StatisticalSet.Delete(ssid, quietmode, db);
        %       PARAMS
        %        - 'db': optional last parameter, denotes an AngioDB object
        %        - 'ssid': DB id of object to be deleted
        %        - 'quietmode': if missing or 'true', the method shows a quest dialog listing all dependent ParamSets that will be deleted. 
        
            success = false; 
            if nargin == 0 || ~isnumeric(ssid)
                error('StatisticalSet.Delete: Parameter error:\n %s', help('StatisticalSet.Delete'));
            end
            if ssid == 1
                error('StatisticalSet.Delete: StatisticalSet with id = 1 can''t be deleted');
            end
            if nargin < 3 
                db = AngioDB; 
            end
            if nargin < 2
                quietmode = false; 
            end
            if ~quietmode && isempty(db.select(['Select id from StatisticalSet where id = ' num2str(ssid)]))
                fprintf('StatisticalSet.Delete: StatisticalSet object with id %i didn''t exist\n', ssid);
                success = false; 
                return; 
            end
            
            list = sprintf('%i ', StatisticalSet.ParamSets(ssid, db)); 
            res = 'No'; 
            if ~quietmode
                res = questdlg(sprintf('This operation will delete also the following dependent ParamSets \n[%s] \n\n do you wish to proceed?', list), 'Are you sure?', 'Yes', 'No', 'No');
            end
            if strcmp(res, 'Yes')
                db.exec(['Delete from StatisticalSet where id = ' num2str(ssid)]); 
            end
        end
        function res = List(verbose, count, start)
        %% List  - generates a list of known StatisticalSet objects
        %    >> all = StatisticalSet.List;               % lists all in verbose mode and returns info in a struct array
        %    >> StatisticalSet.List(true, 3);            % list the first 3 objects 
        %    >> all = StatisticalSet.List(false);        % returns info in a struct array
        %    >> all = StatisticalSet.List(true, 10, 4);  % list 10 objects starting with index 4
        %       'verbose' (optional) if true, output to command window
        %       'count'   (optional) restricts output to command window to maximal 'count' objects; 
        %       'start'   (optional)     "  start index for output
            db = AngioDB; 
            res = db.select('select StatisticalSet.id, StatisticalSet.name, StatisticalSet.description, seedParamSetId, Diagnose.name as diagnose, lastmodified from StatisticalSet join Diagnose on StatisticalSet.diagnoseId = Diagnose.id order by StatisticalSet.id '); 
            if isempty(res)
                return; 
            end
            len = length(res); 
            if nargin == 0
                verbose = true; 
            end
            if nargin > 1
                len = min(count, length(res)); 
            end
            startidx = 1; 
            if nargin > 2
                startidx = min(start, len); 
                len = min(startidx+count-1, length(res)); 
            end
            if verbose
                fprintf('   id |                                     name |     diagnose | seedParamSetId |        lastmodified | description \n'); 
                fprintf('=============================================================================================================\n'); 
                for i = startidx : len
                    desc = res(i).description; 
                    f = strfind(desc, char(10)); 
                    desc(f) = '*'; 
                    desc = desc(1: min(end,120)); 
                    fprintf('%5i | %40s | %12s | %14i | %s | %s\n', res(i).id, res(i).name, res(i).diagnose, res(i).seedParamSetId, char(res(i).lastmodified.toLocaleString), desc); 
                end
            end
        end
    end
end

