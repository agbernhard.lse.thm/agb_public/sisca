classdef STS < handle
    %% STS represents a Characteristic Time Series stored in db table STS 
    %
    % class methods:
    %   FromParamSet - retrieves STS data for a given ParamSet (uses currentSolverRun )
    %
    % instance methods: 
    %   STS         - Constructor, reads object with id from db table CTS
    %   plot

    % SISCA (Simulation, Identification and Statistical variation in Cardiovascular Analysis)
    %
    % Copyright (C) 2010 - 2018 FU Berlin (Mathematics, workgroup Prof. Christof Schütte)
    % Copyright (C) 2019 - 2020 THM Giessen (LSE, workgroup Prof. Stefan Bernhard)
    %
    % Author: Rudolf Huttary
    % Contributors: Stefan Bernhard, Urs Hackstein, Alexander Mair
    %
    % This program is free software: you can redistribute it and/or modify it under the terms 
    % of the GNU Affero General Public License Version 3 as published by the Free Software Foundation.
    %
    % This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
    % without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    % See the GNU Affero General Public License for more details.
    %
    % You should have received a copy of the GNU Affero General Public License along with this program. 
    % If not, see <http://www.gnu.org/licenses/>.

    properties
        id
        applicationId
        nodeId
        solverRunId
        rate
        data
    end
    
    methods
        function this = STS(id, db)
        %% STS - Constructor, reads object with id from db table CTS
        %   >> s = STS(id);      % read CTS with given numerical id from db table CTS
        %   >> s = STS(id, db);  %     " and use given db object 
        
            if nargin < 1 || ~isnumeric(id)
                return; 
                error(sprintf('STS: Parameter error:\nUSAGE:\n %s', help('STS.STS')));
            end
            if nargin < 2
                db = AngioDB;
            end
            if nargin < 3
                if isnumeric(id)
                    res = db.select(sprintf('Select * from STS where id = %i', id)); 
                    this.id = res.id;
                    this.nodeId = res.nodeId;
                    this.applicationId = res.applicationId;
                    this.solverRunId = res.solverRunId;
                    this.rate = res.rate;
                    l = int16(length(res.data)/2); 
                    this.data.q = typecast(res.data(1:l), 'double');
                    this.data.p = typecast(res.data(l+1:end), 'double');
                end
            end
        end
        function [] = plot(this)
        %% plot - plots CTS in new figure
        %  >> mycts.plot; 
            title = sprintf('STS(id = %i, NodeId = %i, rate = %i/s)', this.id, this.nodeId, this.rate); 
            f = figure('Name', title, 'NumberTitle', 'off'); 
            Unit = 5; 
            time = ((1:length(this.data.p))-1) / double(this.rate); 
            
            switch Unit
                case 5 % mmHg
                    pText = 'pressure [Pa]'; 
                case 4 % Pa
                    pText = 'pressure [mmHg]'; 
            end
            Data = this.data.p; 
            plotit; 
            
            function toggleUnit(varargin)
                switch Unit
                    case 4 % mmHg
                        Unit = 5; 
                        Data = Data/7.5E-3;
                        pText = 'pressure [Pa]'; 
                    case 5 % Pa
                        Data = Data*7.5E-3;
                        Unit = 4; 
                        pText = 'pressure [mmHg]'; 
                end
                plotit; 
            end
            function plotit
            subplot(2, 1, 1); 
            plot(time, this.data.q); 
            xlabel(gca, 'time [s]'); 
            ylabel(gca, 'm/s'); 
            subplot(2, 1, 2); 
            plot(time, Data); 
                xlabel(gca, 'time [s]'); 
                ylabel(gca, pText); 
                hYbl = get(gca, 'YLabel'); 
                set(hYbl, 'ButtonDownFcn', @toggleUnit); 
            end
        end
    end
    methods (Static)
        function sts = FromParamSet(p_id)
        %% FromParamSet - retrieves STS data for a given ParamSet (uses currentSolverRun)
        %  sts = STS.FromParamSet(6969);      
        % PARAMETER: sts = FromParamSet(p_id)
        %   p_id  - id of ParamSet for which currentSolverRun is used. 
        %   sts - data array [2*nodes, Samples]
            if nargin < 1
                error('ParamSet id expected'); 
            end
            db = AngioDB; 
            solId = db.select(sprintf('select currentSolverRun as s from ParamSet where id = %i', p_id)); 
            sts = STS.FromSolverRun(solId.s); 
        end
        function sts = FromSolverRun(sid)
        %% FromSolverRun - retrieves STS data for a given SolverRun)
        %  >> sts = STS.FromSolverRun(6650);      
        % PARAMETER: sts = FromSolverRun(sid)
        %   sid  - id of SolverRun  
        %   sts - data array [2*nodes, Samples]
        if nargin < 1
            error('SolverRun id expected'); 
        end
        db = AngioDB; 
          res = db.select(sprintf('select id from STS where solverRunId = %i', sid)); 
          stsid = [res.id]; 
         for i = length(stsid):-1:1
            Sts = STS(stsid(i), db); 
            sts(2*i-1,:) = Sts.data.q;  
            sts(2*i,:) = Sts.data.p;  
         end
        end
    end
end

