classdef Signalprocessor < DbObj
    %% class Signalprocessor is used as superclass for classes representing signals
    %     it implements some common functionality to objects of derived classes
    %
    % Signalprocessor properties:
    %     signal     - dynamic struct, fields describe raw and cts timeseries of signal their properties
    %
    % class methods:
    %   BPFilter                    - Bandpass filter using FFT filtering
    %   LPFilter                    - First order zero-phase Lowpass filter
    %   MeanWaveExtraction          - resamples a mean wave period TS with bins samples from a multiperiod TS
    %   peakDetect                  - detects indices of peaks (maxima) of given time series
    %   performFft                  - performs DFT, calculates fourier coefficients of an n-vector
    %   performInvFft               - calculates time series from Fourier coefficients (inverse DFT)
    %   PhaseCalculation            - Wave phase calculation from a given set of Wave-peaks
    %   resampleFFT                 - resamples one period of a given signal using fft into any time base
    %   resampleSpline              - resamples time series using spline interpolation
    %   rmsd                        - calculates root mean square deviation of a time series to an estimated time series
    %   rmsFilter                   - zero phase filtering to obtain a rmsd below K
    %
    % instance methods:
    %   (ctor)                      - initializes filter parameters
    %   extractCharacteristicWave   - extract characteristic wave from raw data of dedicated signal
    %   denoise                     - filter, removes offset while keeping only dominant frequencies
    %   getfft                      - returns fft of dedicated signal
    %   getSignals                  - returns an array of signals matching one or more signal patterns
    %   getSignal                   - returns one signal as struct
    %   multiWaveAnalysis           - processes characteristic waves from raw data of all signals into cts
    %   plot                        - shows a window with plots of one, some or all signals (understands pattern)
    %   plotfft                     - plots frequencies of dedicated signal
    %   SignalSubset                - returns a subset of 'signal'
    
    % SISCA (Simulation, Identification and Statistical variation in Cardiovascular Analysis)
    %
    % Copyright (C) 2010 - 2018 FU Berlin (Mathematics, workgroup Prof. Christof Schütte)
    % Copyright (C) 2019 - 2020 THM Giessen (LSE, workgroup Prof. Stefan Bernhard)
    %
    % Author: Rudolf Huttary
    %
    % This program is free software: you can redistribute it and/or modify it under the terms 
    % of the GNU Affero General Public License Version 3 as published by the Free Software Foundation.
    %
    % This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
    % without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    % See the GNU Affero General Public License for more details.
    %
    % You should have received a copy of the GNU Affero General Public License along with this program. 
    % If not, see <http://www.gnu.org/licenses/>.
    
    properties (Constant)
        WaveAnalysisMethods = {'mean', 'median', 'mean2mean', 'median2median', 'mean2median', 'median2mean'}
    end
    properties
        signal;
        filt;
        filename = ''; 
    end
    
    methods
        function this = Signalprocessor(obj)
            %% (ctor)  - initializes filter parameters
            this.filt.K1 = 2. ; % 4th order lowpass cutoff for peak detection
            this.filt.K2 = 8e-3; % rms threshold, second filtering
            this.filt.order1 = 6;
            this.filt.order2 = 6;
            this.filt.nB = 4;  % phase bin width factor of sample rate performs downsampling of period to fs/nB bins
            this.filt.headCut = 0; % number of samples cut from beginning
            this.filt.tailCut = 0;  % number of samples cut from end
            this.filt.confidence = 0.15; % threshold to exclude 'freak' periods
            this.filt.bins = 1024;    % sample # of cts
            this.filt.obs = [0 0 0]; 
            this.filt.method = 'mean';   % element of SignalProcessor.WaveAnalysisMeth
            this.filt.HR = 75; 
            
            % read solutions
            if nargin == 1 
                if length (obj)>1
                    for i= 1: length(obj)
                        this(i) = Signalprocessor(obj(i)); 
                    end
                else
                    if isstruct(obj) && isfield(obj, 'X')
                        dim = size(obj.X, 2); 
                        this.signal.data = obj.X; 
                        this.signal.cts.data= obj.X; 
                        this.signal.cts.observeddata = obj.X; 
                        this.signal.cts.meanTime = 1:size(obj.X, 1); 
                        this.signal.cts.Phase = zeros(1, dim); 
                        this.signal.cts.Duration(1, 1:dim) = obj.duration; 
                        this.signal.rate = obj.out_rate*ones(1, dim); 
                        this.signal.signalname = cell(1,dim); 
                        this.signal.signalname(:) = {'X'}; 
                    end
                end
            end
        end
        
        function signal = SignalSubset(this, sigI)
        %% SignalSubset - returns a copy of reduceSignal
        %  >> s.SignalSubset(sigI); 
        %  PARAMETER: 
        %  'sigI'    element indices (vector)
        %  signal    copy of reduced signal set
           if length(intersect(sigI, 1:size(this.signal.data,2))) ~= length(sigI)
               error('Signalprocessor.SignalSubset: Subset is not a subset');  
           end
           signal  = this.signal; 
           signal.signalcount = length(sigI); 
           reduce('unit'); 
           reduce('type'); 
           reduce('signalname'); 
           reduce('rate'); 
           reduce('node'); 
           signal.data = []; 
           signal.data = this.signal.data(:, sigI);  
           function reduce(field)
               if isfield(signal, field)
                   signal.(field)= signal.(field)(sigI); 
               end
           end
        end
        function multiWaveAnalysis(this, verbose)
            %% multiWaveAnalysis - processes characteristic waves from raw data of all signals into cts
            %                      - stores results (ts + properties) in signal.cts
            %                      - vectorized
            %  >> ms.multiWaveAnalysis(verbose);
            %  >> ms = MacSim(12);             % read one object
            %  >> ms.multiWaveAnalysis;        % calc cts
            %  >> ms = MacSim(121:134);        % reas many object
            %  >> ms.multiWaveAnalysis(false); % calc cts for each
            % PARAMETER: 
            %   'verbose' (optional, default = true)
            
            if nargin < 2
                verbose = true;
            end
            if length(this) > 1
                for i = 1: length(this)
                    this(i).multiWaveAnalysis(verbose); 
                end
                return; 
            end
            sizeData = size(this.signal.data);
            sz = 0;
            for i = sizeData(2):-1:1
                %                 try
                [charWave(:, i), lpFcharWave(:, i), waveSd(:, i), peaksI, trusted] = this.extractCharacteristicWave(i, verbose);
                %                 catch ex
                %                     warning('%s', ex.message);
                %                 end
                % force into same # of periods
                if ~sz
                    sz = length(trusted);
                end
                minsz = min(sz, length(trusted));
                PeaksI(1:minsz, i) = peaksI(1:minsz);
                Trusted(1:minsz, i) = trusted(1:minsz);
            end
            
            
            
            this.signal.cts = [];
            this.signal.cts.data = charWave;
            this.signal.cts.observeddata = charWave;
            this.signal.cts.lpFCharWave = lpFcharWave;
            this.signal.cts.waveSd = waveSd;
            this.signal.cts.peaksI = PeaksI;
            this.signal.cts.trusted = Trusted;
            this.signal.cts.meanTime = ((1:this.filt.bins)-1)/this.filt.bins * 2 * pi - pi;
            [this.signal.cts.Phase this.signal.cts.meanSPP this.signal.cts.lag] = calcLags;
            
            for i = sizeData(2):-1:1
                this.signal.cts.Offset(i)    = mean(this.signal.data(:,i));
                this.signal.cts.Amplitude(i) = max(this.signal.cts.data(:,i)) - min(this.signal.cts.data(:,i));
                this.signal.cts.Duration(i)  = this.signal.cts.meanSPP / double(this.signal.rate(i));
            end
            
            function [phase meanSPP lag] = calcLags
                % 'phase' phase shift in radian
                % 'meanSPP' mean samples per period
                
                % correct period jumps
                t = find(Trusted(1:end-1,1));
                meanSPP = mean(PeaksI(t+1, 1) - PeaksI(t, 1));
                for k = 1:sizeData(2)
                    if PeaksI(2,k)-PeaksI(2,1) > meanSPP/2
                        Trusted(:,k) = [0; Trusted(1:end-1,k)];
                        PeaksI(:,k) = [0; PeaksI(1:end-1,k)];
                    end
                    if PeaksI(2,1)-PeaksI(2,k) > meanSPP/2
                        PeaksI(:,k) = [PeaksI(2:end,k); 0];
                        Trusted(:,k) = [Trusted(2:end,k); 0];
                    end
                end
                
                % calc all trusted times
                atr = Trusted(:,1);
                for k=2:sizeData(2)
                    atr = Trusted(:, k) & atr;
                end
                atrI = find(atr);
                for k=sizeData(2):-1:2
                    delta(:, k) = PeaksI(atrI, k) - PeaksI(atrI, 1);
                end
                lag = mean(delta, 1);  % ind samples
                phase = lag / meanSPP * 2 * pi;
            end
        end
%CD: Mache die nachfolgende Funktion MeanWaveExtraction zu static
%        function [meanWave, waveSd, peaksI, trusted] = MeanWaveExtraction(this, data, peaksI, verbose, context)
%             %% MeanWaveExtraction - resamples a mean wave period TS with bins samples from a multiperiod TS
%             %                       uses spline approximation
%             %                       algorithm chosen by 'filt.method'. 
%             %                       Implemented methods: 'mean', 'median', 'mean2mean', 'median2median', 'mean2median', 'median2mean'            
%             %  PARAMETER:
%             %    'data'       signal time series (vector)
%             %    'peaksI'     indices of maxima (periods)
%             %    'verbose'    if true, periods are plotted
%             %    'context'    string used as title of plot window
%             %
%             %    'meanWave'   characteristic wave, mean over all periods (bins vector)
%             %    'waveSd'     Standard deviation (vector)
%             %    'peaksI'     indices of period peaks (k-vector)
%             %    'trusted'    vektor of trusted period peaks (logical k-vector)
%             
%             Figtitle = 'MeanWaveExtraction: ';
%             if nargin == 5
%                 Figtitle = [Figtitle context];
%             end
%             if nargin < 4
%                 verbose = false;
%             end
%             
%             
%             bins = this.filt.bins + mod(this.filt.bins, 2);  % make even
%             time = double(bins+1);
%             half = bins/2;
%             nPeaks = length(peaksI);
%             periods.data = zeros(length(peaksI)-2, time-1);
%             
%             ResampleFold; 
% 
%             len = length(periods.peak); 
%             tpI = 1:len;
%             trusted = ones(1, len); 
%             waveSd = std(periods.data);
%             switch this.filt.method
%                 case 'median'
%                     meanWave = median(periods.data, 1);
%                 case 'mean'
%                     meanWave = mean(periods.data, 1);
%                 case 'mean2median' % sort out 'freak' periods
%                     waveSdAll = waveSd;
%                     meanAllWave = mean(periods.data, 1);
%                     cutoffLimit = norm(meanAllWave) * this.filt.confidence;                % calc threshold
%                     for q = size(periods.data, 1): -1: 1
%                         PeriodNorm(q) = norm(meanAllWave-periods.data(q, :));  % norm of differences
%                     end
%                     trusted = PeriodNorm < cutoffLimit;              % mark confident periods
%                     tpI = find(trusted);
%                     peaksI = [periods.peak];
%                     meanWave = median(periods.data(trusted, :));     % apply freak filter
%                     waveSd   = std(periods.data(trusted, :));
%                 case 'median2mean' % sort out 'freak' periods
%                     waveSdAll = waveSd;
%                     meanAllWave = median(periods.data, 1);
%                     cutoffLimit = norm(meanAllWave) * this.filt.confidence;                % calc threshold
%                     for q = size(periods.data, 1): -1: 1
%                         PeriodNorm(q) = norm(meanAllWave-periods.data(q, :));  % norm of differences
%                     end
%                     trusted = PeriodNorm < cutoffLimit;              % mark confident periods
%                     tpI = find(trusted);
%                     peaksI = [periods.peak];
%                     meanWave = median(periods.data(trusted, :));     % apply freak filter
%                     waveSd   = std(periods.data(trusted, :));
%                 case 'median2median' % sort out 'freak' periods
%                     waveSdAll = waveSd;
%                     meanAllWave = median(periods.data, 1);
%                     cutoffLimit = norm(meanAllWave) * this.filt.confidence;                % calc threshold
%                     for q = size(periods.data, 1): -1: 1
%                         PeriodNorm(q) = norm(meanAllWave-periods.data(q, :));  % norm of differences
%                     end
%                     trusted = PeriodNorm < cutoffLimit;              % mark confident periods
%                     tpI = find(trusted);
%                     peaksI = [periods.peak];
%                     meanWave = median(periods.data(trusted, :));     % apply freak filter
%                     waveSd   = std(periods.data(trusted, :));
%                 case 'mean2mean'
%                     waveSdAll = waveSd;
%                     meanAllWave = mean(periods.data, 1);
%                     cutoffLimit = norm(meanAllWave) * this.filt.confidence;                % calc threshold
%                     for q = size(periods.data, 1): -1: 1
%                         PeriodNorm(q) = norm(meanAllWave-periods.data(q, :));  % norm of differences
%                     end
%                     trusted = PeriodNorm < cutoffLimit;              % mark confident periods
%                     tpI = find(trusted);
%                     peaksI = [periods.peak];
%                     meanWave = mean(periods.data(trusted, :));     % apply freak filter
%                     waveSd   = std(periods.data(trusted, :));
%            end
%            % periodize
%            meanWave = meanWave + (-0.5 : 1/double(bins-1) : 0.5) * (meanWave(1) - meanWave(end)); % periodize transformation
%            % eliminate offset
%            meanWave = meanWave - mean(meanWave); 
%             
%             % plot 
%             if verbose
%                 visualize; 
%             end
%             
%             function visualize
%                 meanPhase = (0:bins-1) / bins * 2 * pi -pi;  % [-pi, pi[
%                 if size(get(0, 'MonitorPositions'),1) > 1
%                     pos = [1.1, 0.1, 0.8, 0.8]; 
%                 else
%                     pos = [0.1, 0.1, 0.8, 0.8]; 
%                 end
%                 figure('Name', [Figtitle ' Method: ' this.filt.method], 'Units', 'Normalized', 'Position', pos);
% 
%                 title (sprintf('All Periods, n = %i', length(periods.peak)));
%                 leg = {'stdDev', this.filt.method};
%                 if length(this.filt.method) > 6
%                     % upper plot
%                     subplot(2,1,1);
%                     plot(meanPhase, periods.data);
%                     hold on;
%                     plot(meanPhase, waveSdAll, 'b', 'LineWidth', 3);
%                     plot(meanPhase, meanAllWave, 'y', 'LineWidth', 3);
%                     plot([0, 0], [max(meanAllWave), min(meanAllWave)], 'k');
%                     two = strfind(this.filt.method, '2'); 
%                     [a b c d] = legend('stdDev', ['1.pass: ' this.filt.method(1:two-1)]);
%                     set(b(3), 'Color', 'r', 'LineWidth', 3); 
%                     set(b(5), 'Color', 'y', 'LineWidth', 3); 
%                     title (sprintf('All Periods, n = %i', length(periods.peak)));
% 
%                     % lower plot
%                     subplot(2,1,2);
%                     title (sprintf('Trusted periods, n = %i with confidence = %.2f', length(tpI), this.filt.confidence));
%                     leg = {'stdDev', ['2.pass: ' this.filt.method(two+1:end)]};
%                 end
%                 hold on;
%                 try
%                 plot(meanPhase, periods.data(tpI, :));
%                 hold on;
%                 plot(meanPhase, waveSd, 'b', 'LineWidth', 3);
%                 plot(meanPhase, meanWave, 'y', 'LineWidth', 3);
%                 plot([0, 0], [max(meanWave), min(meanWave)], 'k');
%                 [a b c d] = legend(leg);
%                 set(b(3), 'Color', 'r', 'LineWidth', 3); 
%                 set(b(5), 'Color', 'y', 'LineWidth', 3); 
%                 catch
%                 end
%             end
%             
%             function ResampleFold
%                 % normalize time base by resampling
%                 for i = 1:nPeaks - 1
%                     periodtime = peaksI(i+1) - peaksI(i);
%                     x = 0 : periodtime;                       % grid over real time
%                     xx = (0:time-1) * (periodtime / (time-1));      % grid over normalized time
%                     period = data(peaksI(i) : peaksI(i+1));   % data of this period
%                     sdata = spline(x, period, xx);
%                     if i>1                                    % shift first half period
%                         periods.data(i-1, half+1:bins) = sdata(1:half);
%                         periods.peak(i-1) = peaksI(i);
%                     end
%                     if i < nPeaks-1                           % shift second half period
%                         periods.data(i, 1:half) = sdata(half+1:bins);
%                     end
%                 end
%             end
%         end
%         
        function Signals = getSignals(this, names)
            %% getSignals  - returns an array of signals matching one or more signal patterns
            %   >> Signals = m.getSignals(names); 
            %   >> Signals = m.getSignals;                        % all signals
            %   >> Signals = m.getSignals('Ventil');              % single signal pattern
            %   >> Signals = m.getSignals({'2'});                 % signal pattern -> all signals with '2' in name
            %   >> Signals = m.getSignals({'Ventil' 'Node(36)'}); % multiple signal patterns
            %   >> Signals = m.getSignals([2 4]);                 % signals 2 and 4
            %     PARAMS
            %      'names' - (optional) single signal pattern or cell array of patterns for multiple signals
            %                (default = all signals)
            %      'Signals' struct or struct array with fields:
            %          'times'     - 1xN sample times (zero based)
            %          'data'      - Time series of signal out of this.signal.data collection
            %          'signalname - name of the signal
            help = sprintf('<a href="matlab:help %s/getSignals">USAGE</a>', class(this));
            if nargin < 2
                names = this.signal.signalname; % all signals
            end
            if ischar(names)
                names = {names};
            end
            try
                if iscell(names)
                    mark = ~logical(1:length(this(1).signal.signalname));
                    for i=length(names):-1:1 % each name is treated as pattern
                        for j = 1: length(this(1).signal.signalname)
                            if ~isempty(strfind(this(1).signal.signalname{j}, names{i}))
                                mark(j) = true;
                            end
                        end
                    end
                    search = find(mark == 1);
                    if ~isempty(search)
                        Signals = this(1).getSignals(search);  % rekursive!
                    else
                        Signals = [];
                    end
                end
                if isnumeric(names)
                    for i=length(names):-1:1
                        Signals(i) = this(1).getSignal(names(i));
                    end
                end
            catch ex
                error('Signalprocessor.getSignals: Parameter error:\n%s\n \nInner error:%s\n', help, ex.message);
            end
        end
        
        function Signal = getSignal(this, sig)
            %% getSignal - returns one signal as struct
            %   >> [signalTS time signalname] = m.getSignal(3);    % third signal
            %   >>  plot(m.getSignal(1));                          % plot 1. signal 
            %     PARAMS
            %      'this' - object or M vector of objects
            %      'sig'  - signal index
            %      'Signal' struct with fields:
            %          'times'     - 1xN sample times (zero based)
            %          'data'      - Nx1 or NxM raw data
            %          'signalname - name of the signal
            %          'ctsData'       - Nx1 or NxM cts data if available
            %          'ctsTimes'       - 1xN times, if available
            help = '<a href="matlab:help Signalprocessor/getSignal">USAGE</a>';
            if nargin<2
                error('Signalprocessor.getSignal: Parameter error:\nUSAGE:\n %s', help);
            end
            lth = length(this); 
            if lth>1   % object vector
                for th = lth:-1:1
                    s = this(th).getSignal(sig); 
                    Signal.mode = 'cts'; 
                    Signal.data(:,th) = s.ctsData; 
                    Signal.times = s.ctsTimes; 
                    Signal.rate = s.rate; 
                    Signal.signalname = s.signalname; 
                    if isfield (s, 'ctsData')
                        Signal.ctsData(:,th) = s.ctsData; 
                        Signal.ctsTimes = s.ctsTimes; 
                        Signal.duration(th) = s.duration; 
                    end
                end
                return; 
            end
            
            len = length(this.signal.signalname);
            if isscalar(sig) && sig > 0 && sig <= len
                rate = this.signal.rate(sig);
                delta = 1/double(rate);
                times = 0: delta : size(this.signal.data, 1)/double(rate)-delta;
            else
                error('Signalprocessor.getSignal: Parameter out of range\n%s\n', help);
            end
            Signal.signalname = this.signal.signalname{sig};
            Signal.rate = this.signal.rate(sig);
            Signal.times = times';
            Signal.idx = sig; 
            if isfield(this.signal, 'data') && ~isempty(this.signal.data)
                Signal.data = this.signal.data(:,sig);
            end
            if isfield(this.signal, 'unit')
                Signal.unit = this.signal.unit{sig};
            end
            if isfield(this.signal, 'cts') && ~isempty(this.signal.cts)
                Signal.ctsObserved = this.signal.cts.observeddata(:,sig); 
                Signal.ctsData = this.signal.cts.data(:, sig);
                Signal.ctsTimes = this.signal.cts.meanTime + this.signal.cts.Phase(sig);
                Signal.duration = this.signal.cts.Duration(sig); 
            end
        end
        
        function title = plot(this, siglist, subtitle, t1, t2)
            %% plot - shows a window with plots of one, some or all signals (understands pattern)
            %   - if there is more than one signal denoted, the window will split into an upper zoomed signal panel
            %       and a lower preview panel. Click into any of the preview plots (you must hit the line) to get it zoomed
            %       in the upper panel.
            %   -  use mouse mouse wheel for horizontal zooming of a plot (to select the axes click into the plot panel,
            %      not hitting the plot line)
            %   -  unknown signal names are ignored
            %   >> m.plot(siglist, subtitle); 
            %   >> m.plot;                   % plots all signals in two panels
            %   >> m.plot(3);                % plots 3. signal  in one panel
            %   >> m.plot('pg');             % plots all signals with 'pg' in name
            %   >> m.plot({'pg2' 'ekg1'});   % plots signals 'pg1' 'ekg1' in two panels
            %   >> m.plot([1, 2, 3]);        % plots 3 signals in two panels
            %  PARAMETER:
            %      'siglist'  (optional) default: all signals are plotted
            %                            index vector: indexed signals are plotted
            %                            string:  name pattern of signals to be plotted
            %                            cell array: set of name patterns of signals to be plotted (unique)
            %     'subtitle'  (optional) subtitle of Window
            %     't1'        (optional) start time for zoom of big plot
            %     't2'        (optional) end time for zoom of big plot
            
            help = '<a href="matlab:help Signalprocessor/plot">USAGE</a>';
            if nargin < 3
                subtitle = '';
            end
            if nargin < 2 || isempty(siglist)
                list = 1:length(this(1).signal.signalname);
                siglist = list;
            end
            if nargin < 4
                t1 = 0; 
                t2 = 0; 
            elseif nargin < 5
                t1 = ceil(min(size(this(1).signal.data, 1)- 1, t1*this(1).signal.rate(1)+1)); 
                t2 = 0;
            else
                t1 = ceil(min(size(this(1).signal.data, 1)- 1, t1*this(1).signal.rate(1)+1)); 
                t2 = ceil(min(size(this(1).signal.data, 1), t2*this(1).signal.rate(1)+1)); 
            end
            for i = 1:length(this)  % have all objects cts data?
                if isfield(this(i).signal, 'cts')
                    cts = true;
                else
                    cts = false; 
                    break; 
                end
            end
            if length(this) > 1
                title =  subtitle;
            else
                title =  sprintf('%s - %s', this.ToString(this.filename), subtitle);
            end
            
            % vector
            if length(this) > 1 && ~cts  % on vector call multiple windows, if cts is missing
                warning('Signalprocessor.plot: one or more objects don''t have cts data. Plotting data into multiple figures'); 
                for i=length(this):-1:1
                    this(i).plot(siglist, subtitle); 
                end
                return; 
            end
            
            % show figure
            if ~isnumeric(siglist)
                sigs = this.getSignals(siglist); 
                if isempty(sigs)
                    error('%s.plot: no signals for this pattern found', class(this));  
                end
                siglist = [sigs.idx]; 
            end
            try
                k = 1;
                if iscell(siglist)
                    list = [];
                    for j = 1 : length(siglist)
                        for i=1:length(this(1).signal.signalname)
                            if ~isempty(strfind(this(1).signal.signalname{i}, siglist{j}))
                                list(k) = i;  %#ok<AGROW>
                                k = k+1;
                            end
                        end
                    end
                    list = unique(list);
                else
                    list = siglist;
                end
            catch ex
                error('Signalprocessor.plot: signal list malformed\n%s\n', help);
            end
            switch length(list)
                case 0
                    error('Signalprocessor.plot: no matching signal found!\n%s\n', help);
                case 1
                    pos1 = [0 0 1 1];
                    pos2 = [0 0 1 0.001];
                case {2, 3}
                    pos1 = [0 0.5 1 0.5];
                    pos2 = [0 0 1 0.5];
                otherwise
                    pos1 = [0 0.7 1 0.3];
                    pos2 = [0 0 1 0.7];
            end
            pos = get(0,'ScreenSize');
            pos = [100, 100, pos(3)-200,pos(4)-200]; 
            hDlg = figure('Name', title, 'NumberTitle', 'off',  'WindowButtonMotionFcn',  @cbCursor, 'Position',pos, 'WindowScrollWheelFcn', @scale, 'WindowButtonDownFcn', @cbStartMotion);
            set (hDlg, 'Units', 'normalized'); 
            p1 = uipanel('Parent', hDlg, 'Title', '',  'Units', 'normalized', 'Position', pos1);
            p2 = uipanel('Parent', hDlg, 'Title', '',  'Units', 'normalized', 'Position', pos2);
            hHold = uicontrol(p2, 'Style', 'Pushbutton', 'FontSize', 8, 'HorizontalAlignment', 'right', 'String', 'Hold', ...
                'Units', 'normalized', 'Position', [0.9 0.05 0.05 0.03], 'Callback', @cbHold);
            if length(this) == 1 && isfield(this.signal, 'cts')
                hCts = uicontrol(p2, 'Style', 'Pushbutton', 'FontSize', 8, 'HorizontalAlignment', 'right', 'String', 'show Raw', ...
                    'Units', 'normalized', 'Position', [0.84 0.05 0.05 0.03], 'Callback', @cbCts);
            end
            if length(this) > 1
                hMean = uicontrol(p2, 'Style', 'Pushbutton', 'FontSize', 8, 'HorizontalAlignment', 'right', 'String', 'mean', ...
                    'Units', 'normalized', 'Position', [0.80 0.05 0.05 0.03], 'Callback', @cbMean);
            end
            holdplot = false;
            hbig = subplot(1,1,1, 'Parent', p1);
            set(hbig, 'Position', [0.04 0.15 0.92 0.8], 'ButtonDownFcn', @cbButtonDown);
            big = 1;
            rawTime = false;
            meanOnly = false; 
            rawX = false;
            
            % measurement
            P0 = []; 
            P1 = []; 
            htxt = []; 
            hrect = []; 
            MotionFcn =[]; 
            ButtonupFcn =[]; 
            
            doplot;
            
            function cbButtonDown(varargin)
                seltype = get(gcf, 'SelectionType'); 
                switch seltype
                    case 'alt'
                        cbClipboard; 
                    case 'extend'
                        if isempty(P1)
                            obj = findobj(hbig, 'type', 'rectangle'); 
                            delete(obj); 
                            obj = findobj(hbig, 'type', 'text'); 
                            delete(obj); 
                        end
                    case 'normal'
                        cbStartMeasure; 
                end
            end
            function cbStartMeasure(varargin)
                P0 = get(hbig, 'CurrentPoint');
                MotionFcn = get(hDlg, 'WindowButtonMotionFcn');
                ButtonupFcn = get(hDlg, 'WindowButtonUpFcn');
                set(hDlg, 'WindowButtonMotionFcn', @cbMeasure, 'WindowButtonUpFcn', @cbStopMeasure);
                hrect = rectangle('parent', hbig, 'LineWidth', 1, 'Visible', 'off', 'EdgeColor', 'r'); 
                htxt = text('parent', hbig, 'VerticalAlignment', 'Bottom', 'FontSize', 9, 'Visible', 'off', 'String', 'test');
            end
            function cbMeasure(varargin)
                 P1 = get(hbig, 'CurrentPoint');
                 if P0(1,1) <= P1(1,1)
                    x1 = P0(1,1);
                    x2 = P1(1,1);
                 else
                    x1 = P1(1,1);
                    x2 = P0(1,1);
                 end
                 if P0(1,2) <= P1(1,2)
                    y1 = P0(1,2);
                    y2 = P1(1,2);
                 else
                    y1 = P1(1,2);
                    y2 = P0(1,2);
                 end
                 dx = x2-x1; 
                 dy = y2-y1; 
                 if dx*dy==0
                     return; 
                 end
                 set(hrect, 'Position', [x1, y1, dx, dy], 'Visible', 'on'); 
                 xy = sprintf('[%.3g, %.3g]', dx, dy); 
                 set(htxt, 'Position', [x1, y2], 'Visible', 'on', 'String', xy); 
            end
            function cbStopMeasure(varargin)
                set(hDlg, 'WindowButtonMotionFcn', '', 'WindowButtonUpFcn', '');
                get(gcf, 'SelectionType');
                if ~ strcmp(get(gcf, 'SelectionType'), 'extend')
                    delete(hrect); 
                    delete(htxt); 
                end
                set(hDlg, 'WindowButtonMotionFcn', MotionFcn);
                set(hDlg, 'WindowButtonUpFcn', ButtonupFcn);
                P0 = []; 
                P1=[]; 
            end
            function cbMean(varargin)
                meanOnly = ~meanOnly; 
                set(hMean, 'BackgroundColor', [1 1 1] - get(hMean, 'BackgroundColor'), ...
                    'ForegroundColor', [1 1 1] - get(hMean, 'ForegroundColor'));        
                doplot;
            end
            
            function cbCts(varargin)
                cts = ~cts;
                if cts
                    set(hCts, 'String', 'show Raw');
                    if holdplot
                        holdplot = true;   % will be toggled
                        cbHold; 
                    else
                        doplot;
                    end
                else
                    set(hCts, 'String', 'show CTS');
                    rawTime = false; 
                    if holdplot
                        holdplot = true;   % will be toggled
                        cbHold; 
                    else
                        doplot;
                    end
                end
            end
            function cbHold(varargin)
                holdplot = ~holdplot;
                set(hHold, 'BackgroundColor', [1 1 1] - get(hHold, 'BackgroundColor'), ...
                    'ForegroundColor', [1 1 1] - get(hHold, 'ForegroundColor'));       
                if ~holdplot
                    doplot;
                end
            end
            function cbCursor(varargin)
                p = get(hDlg, 'CurrentPoint');
                if abs(p(2)-pos1(2)) < 0.01
                    set(hDlg, 'Pointer', 'top');
                else
                    set(hDlg, 'Pointer', 'arrow');
                end
            end
            function cbMotion(varargin)
                p = get(hDlg, 'CurrentPoint');
                if p(2) > 0.1 && p(2) < 0.9
                    pos1 = [pos1(1) p(2) 1 1-p(2)];
                    pos2 = [pos2(1) 0 1 p(2)];
                    set(p1, 'Position', pos1);
                    set(p2, 'Position', pos2);
                end
            end
            function cbStartMotion(varargin)
                p = get(hDlg, 'CurrentPoint');
                if abs(p(2)-pos1(2)) < 0.01
                    set(hDlg, 'WindowButtonMotionFcn', @cbMotion, 'WindowButtonUpFcn', @cbStopMotion);
                end
            end
            function cbStopMotion(varargin)
                set(hDlg, 'WindowButtonMotionFcn',  @cbCursor, 'WindowButtonUpFcn', '');
            end
            function scale(h, evnt)
                %% scale - callback for mousewheel scaling
                % transforms axes relativly to mouse position
                fac = 1 + 0.03*evnt.VerticalScrollAmount;
                if (evnt.VerticalScrollCount<0)
                    fac = 1/fac;
                end
                fac = fac - 1;
                pos = get(gca, 'CurrentPoint');
                x = xlim;
                y = ylim;
                if pos(1) < x(1) || pos(1) > x(2) || pos(3) < y(1) || pos(3) > y(2)
                    return;
                end
                x = x + (x-pos(1)) * fac;
                y = y + (y-pos(3)) * fac;
                xlim(x);
                parent = get(gca, 'Parent');
                if parent == h
                    ylim(y);
                end
            end
            
            function doplot
                persistent col
                persistent leg1; 
                if isempty(col); 
                    col = 'k'; 
                    leg1 = {}; 
                end; 
                
                if isempty(list) return; end
                sig = [];
                prepdata(list(big));
                L = length(sig.time); 
                if(t2>0)
                    sig.time = sig.time(min(t1, L) : min(t2, L));    % show time intervall
                    sig.data = sig.data(t1:t2); 
                elseif(t1>0)
                    sig.time = sig.time(min(t1, L) : L);    % show time intervall
                    sig.data = sig.data(t1:end); 
                end
                if rawTime && isfield(sig, 'duration')
                    d = sig.duration; 
                    sig.time = t1:t2-1;    % show time intervall
                    sig.time = 0: (length(sig.data)-1); 
                    sig.time = sig.time / double(length(sig.time))*d;
                end
                axes(hbig);
                if holdplot
                    hold on;
                    switch col
                        case 'b'
                            col = 'g'; 
                        case 'g'
                            col = 'r'; 
                        case 'r'
                            col = 'b'; 
                    end
                    leg1{end+1} = strrep(sig.name, '_', '\_'); 
                    
                else
                    hold off;
                    col = 'k'; 
                    leg1 = {}; 
                    leg1{1} = strrep(sig.name, '_', '\_');
                end
                if ~meanOnly
                    
                    plot(sig.time, sig.data, col, 'ButtonDownFcn', @cbButtonDown);
                    set(gca, 'ButtonDownFcn', @cbButtonDown);
                    legend (leg1); 
                else
                    plot(0,0);  
                end
                if size(sig.data, 2) > 1 % add mean, stddev when plotting vector object 
                    hold on; 
                    plot(sig.time, mean(sig.data, 2), 'r', 'LineWidth', 3);
                    plot(sig.time, std(sig.data'), 'b', 'LineWidth', 3);
                    % legend
                    if holdplot
                        delete (legend); 
                    else
                        [a b c d] = legend({sprintf('mean = %.1f', mean(mean(sig.data, 2))), sprintf('stddev = %.1f', mean(std(sig.data')))}); 
                        set(b(3), 'Color', 'r', 'LineWidth', 3); 
                        set(b(5), 'Color', 'b', 'LineWidth', 3); 
                    end
                end
                if holdplot
                    return; 
                end
                xlabel(sprintf('%s', sig.name), 'Interpreter', 'none', 'ButtonDownFcn', @cbTimeBase);
                if isfield(sig, 'unit')
                    ylabel (sig.unit); 
                end
                dimx = ceil(sqrt(length(list)));
                ll = length(list);
                if dimx * (dimx-1) >= ll
                    dimy = dimx-1;
                else
                    dimy = dimx;
                end
                if dimx > 0
                    for kk = 1:ll  %% small subplots
                        subplot(dimy, dimx, kk, 'Parent', p2);
                        idx = list(kk);
                        if ~isempty(idx)
                            prepdata(idx);
                            if idx == big
                                col = 'k';
                            else
                                col = 'b'; 
                            end
                            plot(sig.time, sig.data, col, 'ButtonDownFcn', @cb, 'UserData', idx);
                            xlabel(sprintf('%s', sig.name), 'Interpreter', 'none', 'ButtonDownFcn', @cb, 'UserData', idx);
                            if isfield(sig, 'unit')
                                ylabel (sig.unit); 
                            end
                            set(gca, 'ButtonDownFcn', @cb, 'UserData', idx);
                        end
                    end
                end
                function prepdata(idx)
                    Signal = this.getSignal(idx);
                    if isfield(Signal, 'mode')    % multi mode
                        cts = true; 
                    end
                    if isfield(Signal, 'unit')
                        sig.unit = Signal.unit;
                    end
                    if cts 
                        sig.time = Signal.ctsTimes;
                        if rawTime 
                            sig.data = Signal.ctsObserved;
                            sig.time = (0:length(sig.data)-1)/double(Signal.rate); 
                        else
                            sig.data = Signal.ctsData;
                        end
                        sig.duration = Signal.duration(1); 
                    else
                        tiInd = 1+this(1).filt.headCut : length(Signal.times)-this(1).filt.tailCut;
                        sig.data = Signal.data(tiInd,:);
                            sig.time = Signal.times(tiInd);
                        if rawX
                            sig.time = 1:size(sig.data, 1);
                        end
                    end
                    sig.name =  Signal.signalname;
                end
            end
            
            function cbTimeBase(varargin) 
            % switches view between: this.signal.cts.observeddata using a time scale (rawTime = true)
            %                   and: this.signal.cts.data using a radian scale
                if cts
                    rawTime = ~rawTime;
                else
                    rawX = ~rawX; 
                end
                if holdplot
                    cbHold; 
                else
                    doplot; 
                end
            end
            
            function cbClipboard(varargin)
                    u = get(hbig, 'units'); 
                    set(hbig, 'units', 'Pixels'); 
                    pos = get(hbig, 'Position'); 
                    set(hbig, 'units', u); 
                    h1=figure('toolbar','none', 'Position', pos); 
                    p = get(hbig, 'Parent'); 
                    q = get(p, 'Children'); 
                    copyobj([q(1) hbig],h1); 
                    print(h1,'-dbitmap','-painters')
                    close(h1);
            end
            function cb(label, varargin)
                big = get(label, 'Userdata');
                doplot;
            end
        end
        
        function data = denoise(this, sigId, order, method, verbose)
        %% denoise - experimental filter, reconstructs signal using dominant frequencies
        %  >> data = sig.denoise(6); 
        %  >> sig.denoise(6, 3, 'range', true); 
        %  >> sig.denoise(sigId, order, verbose); 
        %
        %  PARAMETER: data = denoise(this, sigId, order, method, verbose)
        %    'sigId'   - id of the object's signal to use
        %    'order'   - (optional, default = 3) number of the dominant frequencies used for ifft
        %    'method'  - (optional, default = 'dominant') one of 
        %                       'dominant' reconstruction uses 'order' dominant frequencies only 
        %                       'range'    reconstruction uses all frequencies until 'order' dominant freq 
        %                       'select'   selective reconstruction using only 'order' dominant freq 
        %                       'relrange' reconstruction uses range between first and 'order' dominant freq
        %    'verbose' - (optional, default = false) if true, a plot of denoised signal in a new figure shown
        %    'data'    -  denoised signal
        
            if nargin < 2
                error('Signalprocessor.denoise: wrong number of parameters\n%s', help('Signalprocessor.denoise')); 
            elseif nargin < 3
                order = 3; 
                verbose = false; 
            elseif nargin < 4 
                method = 'dominant'; 
            elseif nargin < 5
                verbose = false; 
            end
            
            [F0 res] = this.getfft(sigId); 
            F = abs(real(F0)); 
            % find domiant frequencies
            
            top = ceil(16/res);   % experimental
            f(order) = 0;
            halflen = floor(length(F)/2); 
            startidx = 2; 
            for i=1:order
                [~, f(i)] = max(F(startidx:top)); 
%                [~, f(i)] = max(F(startidx:halflen-1)); 
                f(i) = startidx+f(i)-1; 
                startidx = f(i)+1; 
            end
            % plot ist
            N = size(this.signal.data); 
            F1(N, 1) = 0; 
            switch method
                case 'range'
                    F1(1:f(order)) = F0(1:f(order)); 
                case 'relrange'
                    F1(f(1):f(order)) = F0(f(1):f(order)); 
                case 'select'
                    F1(f(order)) = F0(f(order)); 
                otherwise 
                    F1(f) = F0(f); 
            end
            data = real(ifft(F1)); 
            if verbose
               this.plotfft(sigId); 
               figure; plot(real(data)); 
            end
        end
        
        function [fftdata res] = getfft(this, sigId)
        %% getfft  - returns fft of dedicated signal
        %  >> [fftdata res] = sig.getfft(sigId)
        %
        %  PARAMETER: [fftdata res] = getfft(this, sigId)
        %    'sigId'   - id of object's signal to use as input
        %    'fftdata' - data of fft
        %    'res'     - frequency resolution
        
            if nargin < 2 || nargout > 2
                error('Signalprocessor.getfft: wrong number of parameters\n%s', help('Signalprocessor.getfft')); 
            end
            sig = this.getSignal(sigId); 
            if mod(length(sig.data), 2)
                x=sig.data(1:end-1);  
            else
                x=sig.data(1:end);  
            end
            rate = sig.rate; 
            N = length(x); 
            res = rate/N; 
            fftdata = fft(x); 
        end
        
        function plotfft(this, sigId, order, upperlimit, lowerlimit)
        %% plotfft - plots frequencies of dedicated signal
        % >> sig.plotfft(sigId); 
        % PARAMETER:
        %   'sigId'  - id of object's signal to be analyzed
        %   'order'  - (optional, default = 4) number of dominant frequencies to detect
        
            if nargin < 2
                error('Signalprocessor.plotfft: wrong number of parameters\n%s', help('Signalprocessor.plotfft')); 
            end
            if nargin < 3
                order = 4; 
            end
            if nargin < 4
                upperlimit = 40; 
            end
            if nargin < 5
                lowerlimit = 0; 
            end
            figure; 
            for j = 1:length(sigId)
            [F0, res] = this.getfft(sigId(j)); 
            F=abs(real(F0));
            F1=abs(imag(F0));
            hf = order;
            f(hf) = 0;
            I = 2; 
            for i=1:hf
                [~, f(i)] = max(F(I:end/2-1)); 
                f(i) = I+f(i)-1; 
                I = f(i)+1; 
            end
            if lowerlimit > 0
                range = lowerlimit:res:upperlimit; 
            else
                range = res:res:upperlimit; 
            end
            plot(range, F(round(range/res)+1), '-o'); 
%            plot(range, F(2:length(range)+1), 'bo'); 
%            plot(range, [F(2:length(range)+1), F1(2:length(range)+1)]); 
            grid on  
            xlabel('Hz'); 
            if(length(sigId) < 2); 
                leg = sprintf('f%i = %.2f\n',[1:hf;f*res]); 
            else
                leg{j} = strrep(this.signal.signalname{sigId(j)}, '_', '\_'); 
            end
            hold all; 
            legend(leg);  
            end
            set(gca, 'ButtonDownFcn', @cbClipboard); 
            
            function cbClipboard(varargin)
                   hbig = gca; 
                    u = get(hbig, 'units'); 
                    set(hbig, 'units', 'Pixels'); 
                    pos = get(hbig, 'Position'); 
                    set(hbig, 'units', u); 
                    h1=figure('toolbar','none', 'Position', pos); 
                    p = get(hbig, 'Parent'); 
                    q = get(p, 'Children'); 
                    copyobj([q(1) hbig],h1); 
                    print(h1,'-dbitmap','-painters')
                    close(h1);
            end
        end
    end
    
    methods (Static)
        function [meanWave, waveSd, meanPhase, peaksI, trusted] = MeanWaveExtraction(this, data, peaksI, verbose, context)
            %% MeanWaveExtraction - resamples a mean wave period TS with bins samples from a multiperiod TS
            %                       uses spline approximation
            %                       algorithm chosen by 'filt.method'. 
            %                       Implemented methods: 'mean', 'median', 'mean2mean', 'median2median', 'mean2median', 'median2mean'            
            %  PARAMETER:
            %    'data'       signal time series (vector)
            %    'peaksI'     indices of maxima (periods)
            %    'verbose'    if true, periods are plotted
            %    'context'    string used as title of plot window
            %
            %    'meanWave'   characteristic wave, mean over all periods (bins vector)
            %    'waveSd'     Standard deviation (vector)
            %    'meanPhase'  [- pi, pi] calculated from bins
            %    'peaksI'     indices of period peaks (k-vector)
            %    'trusted'    vektor of trusted period peaks (logical k-vector)
            
            Figtitle = 'MeanWaveExtraction: ';
            if nargin == 5
                Figtitle = [Figtitle context];
            end
            if nargin < 4
                verbose = false;
            end
            
            
            bins = this.filt.bins + mod(this.filt.bins, 2);  % make even
            time = double(bins+1);
            half = bins/2;
            nPeaks = length(peaksI);
            periods.data = zeros(length(peaksI)-2, time-1); %initialise
            
            ResampleFold; 

            len = length(periods.peak); 
            tpI = 1:len;
            trusted = ones(1, len); 
            waveSd = std(periods.data);
            this.filt.method = 'mean2mean';
            this.filt.confidence = 0.25; % war ursprünglich 0.15, dann 0.25
            switch this.filt.method
                case 'median'
                    meanWave = median(periods.data, 1);
                case 'mean'
                    meanWave = mean(periods.data, 1);
                case 'mean2median' % sort out 'freak' periods
                    waveSdAll = waveSd;
                    meanAllWave = mean(periods.data, 1);
                    cutoffLimit = norm(meanAllWave) * this.filt.confidence;                % calc threshold
                    for q = size(periods.data, 1): -1: 1
                        PeriodNorm(q) = norm(meanAllWave-periods.data(q, :));  % norm of differences
                    end
                    trusted = PeriodNorm < cutoffLimit;              % mark confident periods
                    tpI = find(trusted);
                    peaksI = [periods.peak];
                    meanWave = median(periods.data(trusted, :));     % apply freak filter
                    waveSd   = std(periods.data(trusted, :));
                case 'median2mean' % sort out 'freak' periods
                    waveSdAll = waveSd;
                    meanAllWave = median(periods.data, 1);
                    cutoffLimit = norm(meanAllWave) * this.filt.confidence;                % calc threshold
                    for q = size(periods.data, 1): -1: 1
                        PeriodNorm(q) = norm(meanAllWave-periods.data(q, :));  % norm of differences
                    end
                    trusted = PeriodNorm < cutoffLimit;              % mark confident periods
                    tpI = find(trusted);
                    peaksI = [periods.peak];
                    meanWave = median(periods.data(trusted, :));     % apply freak filter
                    waveSd   = std(periods.data(trusted, :));
                case 'median2median' % sort out 'freak' periods
                    waveSdAll = waveSd;
                    meanAllWave = median(periods.data, 1);
                    cutoffLimit = norm(meanAllWave) * this.filt.confidence;                % calc threshold
                    for q = size(periods.data, 1): -1: 1
                        PeriodNorm(q) = norm(meanAllWave-periods.data(q, :));  % norm of differences
                    end
                    trusted = PeriodNorm < cutoffLimit;              % mark confident periods
                    tpI = find(trusted);
                    peaksI = [periods.peak];
                    meanWave = median(periods.data(trusted, :));     % apply freak filter
                    waveSd   = std(periods.data(trusted, :));
                case 'mean2mean'
                    waveSdAll = waveSd;
                    meanAllWave = mean(periods.data, 1);
                    % bei sehr kleinen Amplituden sind Signale meist auch
                    % sehr verrauscht, hier größere Confidence einstellen
                    if max(meanAllWave) < 30
                        this.filt.confidence = 0.5;
                    end
                    cutoffLimit = norm(meanAllWave) * this.filt.confidence;                % calc threshold
                    for q = size(periods.data, 1): -1: 1
                        PeriodNorm(q) = norm(meanAllWave-periods.data(q, :));  % norm of differences
                    end
                    trusted = PeriodNorm < cutoffLimit;              % mark confident periods
                    tpI = find(trusted);
                    peaksI = [periods.peak];
                    meanWave = mean(periods.data(trusted, :));     % apply freak filter
                    waveSd   = std(periods.data(trusted, :));
           end
           % periodize
           meanWave = meanWave + (-0.5 : 1/double(bins-1) : 0.5) * (meanWave(1) - meanWave(end)); % periodize transformation
           % eliminate offset
           meanWave = meanWave - mean(meanWave); 
           
           meanPhase = (0:bins-1) / bins * 2 * pi -pi;  % [-pi, pi[ %  raus aus visualize
           
            % plot 
            if verbose
                visualize; 
            end
            
            function visualize
                % meanPhase = (0:bins-1) / bins * 2 * pi -pi;  % [-pi, pi[
                if size(get(0, 'MonitorPositions'),1) > 1
                    pos = [1.1, 0.1, 0.8, 0.8]; 
                else
                    pos = [0.1, 0.1, 0.8, 0.8]; 
                end
                figure('Name', [Figtitle ' Method: ' this.filt.method], 'Units', 'Normalized', 'Position', pos);

                title (sprintf('All Periods, n = %i', length(periods.peak)));
                leg = {'stdDev', this.filt.method};
                if length(this.filt.method) > 6
                    % upper plot
                    subplot(2,1,1);
                    plot(meanPhase, periods.data);
                    hold on;
                    plot(meanPhase, waveSdAll, 'b', 'LineWidth', 3);
                    plot(meanPhase, meanAllWave, 'y', 'LineWidth', 3);
                    plot([0, 0], [max(meanAllWave), min(meanAllWave)], 'k');
                    two = strfind(this.filt.method, '2'); 
                    [a b c d] = legend('stdDev', ['1.pass: ' this.filt.method(1:two-1)]);
                    set(b(3), 'Color', 'r', 'LineWidth', 3); 
                    set(b(5), 'Color', 'y', 'LineWidth', 3); 
                    title (sprintf('All Periods, n = %i', length(periods.peak)));

                    % lower plot
                    subplot(2,1,2);
                    title (sprintf('Trusted periods, n = %i with confidence = %.2f', length(tpI), this.filt.confidence));
                    leg = {'stdDev', ['2.pass: ' this.filt.method(two+1:end)]};
                end
                hold on;
                try
                plot(meanPhase, periods.data(tpI, :));
                hold on;
                plot(meanPhase, waveSd, 'b', 'LineWidth', 3);
                plot(meanPhase, meanWave, 'y', 'LineWidth', 3);
                plot([0, 0], [max(meanWave), min(meanWave)], 'k');
                [a b c d] = legend(leg);
                 set(b(3), 'Color', 'b', 'LineWidth', 3); % r -> b
                 set(b(5), 'Color', 'y', 'LineWidth', 3); 
                catch
                end
            end
            
            function ResampleFold
                % normalize time base by resampling
                for i = 1:nPeaks - 1
                    periodtime = peaksI(i+1) - peaksI(i);
                    x = 0 : periodtime;                       % grid over real time, CD: just one point? periodtime around 1s
                    xx = (0:time-1) * (periodtime / (time-1));      % grid over normalized time
                    period = data(peaksI(i) : peaksI(i+1));   % data of this period
                    sdata = spline(x, period, xx);
                    if i>1                                    % shift first half period
                        periods.data(i-1, half+1:bins) = sdata(1:half);
                        periods.peak(i-1) = peaksI(i);
                    end
                    if i < nPeaks-1                           % shift second half period
                        periods.data(i, 1:half) = sdata(half+1:bins);
                    end
                end
            end
        end
        function res = footpointDetect(ts, assp, offset)
            %% peakDetect - detects indices of negative peaks (minima) of given time series
            %     - operates recursive by splitting ts in right and left halves
            %   >> Signalprocessor.peakDetect(ts, assp, offset), 
            %   >> Signalprocessor.peakDetect(data, 600, 0);
            % PARAMETER:
            %   'ts'     time series vector
            %   'aspp'   axproximate samples per period
            %   'offset' (optional) shift of res (used in recursion)
            %   'res'    peak indices (vector)

            if nargin < 2
                  error('Signalprocessor.footpointDetect: Arguments missing\n%s', '<a href="matlab:help Signalprocessor.footpointDetect">USAGE</a>');
            end
            if nargin < 3
                offset = 0;
            end
            hp = ceil(assp/2); % half period
            res = pd(1, length(ts), offset);
            
            function res = pd(a, b, offset)
                left = [];
                right = [];
                [m i] = min(ts(a:b));   % find peak
                if i - 1.1*assp > 0    % left recursion condition
                    left = pd(a, a+i-hp, offset);
                end
                if i + 1.1*assp < b-a % right recursion condition
                    right = pd(a+i + hp-1, b, offset + i + hp-1);
                end
                res = [left i + offset right];
            end
        end
        function res = peakDetect(ts, assp, offset)
            %% peakDetect - detects indices of peaks (maxima) of given time series
            %     - operates recursive by splitting ts in right and left halves
            %   >> Signalprocessor.peakDetect(ts, assp, offset), 
            %   >> Signalprocessor.peakDetect(data, 600, 0);
            % PARAMETER:
            %   'ts'     time series vector
            %   'aspp'   approximate samples per period
            %   'offset' (optional) shift of res (used in recursion)
            %   'res'    peak indices (vector)
            if nargin < 3
                offset = 0;
            end
            hp = ceil(assp/2); % half period
            res = pd(1, length(ts), offset);
            
            function res = pd(a, b, offset)
                left = [];
                right = [];
                [m i] = max(ts(a:b));   % find peak
                if i - 1.2*assp > 0    % left recursion condition
                    left = pd(a, a+i-hp, offset);
                end
                if i + 1.2*assp < b-a % right recursion condition
                    right = pd(a+i + hp-1, b, offset + i + hp-1);
                end
                res = [left i + offset right];
            end
        end
        
        function [phase phasepos iPeriod] = PhaseCalculation(I, samples, dt)
        %% PhaseCalculation - Wave phase calculation from a given set of Wave-peaks
        %  >> [phase phasepos iPeriod] = Signalprocessor.PhaseCalculation(fpeaks, samples, dt)
        % PARAMETER:
        %    'I'   peak indices vector
        %    'samples'  number of samples
        %    'dt'
        %    'phase'    calculated phases ranging from -pi to pi. Wave-peaks at phase = 0
        %    'phasepos' calculated phases ranging from 0 to 2*pi. Wave-peaks at phasepos = 0
        %    'iPeriod'  individual period
            phasepos = zeros(samples,1);
            for i = 1:length(I)-1;
                m = I(i+1) - I(i);
                iPeriod(i) = m*dt;              % compute individual periods
                phasepos(I(i)+1:I(i+1)) = 2*pi/m : 2*pi/m : 2*pi;
            end
            m = I(2) - I(1);
            L = length(phasepos(1:I(1)));
            phasepos(1:I(1)) = 2*pi-(L-1)*2*pi/m:2*pi/m:2*pi;
            m = I(end) - I(end-1);
            L = length(phasepos(I(end)+1:end));
            phasepos(I(end)+1:end) = 2*pi/m:2*pi/m:L*2*pi/m;
            phasepos = mod(phasepos,2*pi);
            phase = phasepos;
            I = find(phasepos>pi);
            phase(I) = phasepos(I) - 2*pi;
        end
        
        function fdata = rmsFilter(ndata, K, order, rate)
        %% rmsFilter - does zero phase filtering to obtain a rmsd below K
        % >> Signalprocessor.rmsFilter(ndata, K, order, rate); 
        % PARAMETER: 
        %  'ndata' data to be filtered
        %  'K'     characteristic value
        %  'order' value out of {2, 4, 6, 8 }
        %  'rate'  sample rate 
            r=100;
            i=1;
            while r>K
                switch order
                    case {1 3 5 7}
                        error('only even orders')
                    case 2
                        fdata=Signalprocessor.LPFilter(ndata,i/rate);
                    case 4
                        fdata=Signalprocessor.LPFilter(ndata,i/rate);
                        fdata=Signalprocessor.LPFilter(fdata,i/rate);
                    case 6
                        fdata=Signalprocessor.LPFilter(ndata,i/rate);
                        fdata=Signalprocessor.LPFilter(fdata,i/rate);
                        fdata=Signalprocessor.LPFilter(fdata,i/rate);
                    case 8
                        fdata=Signalprocessor.LPFilter(ndata,i/rate);
                        fdata=Signalprocessor.LPFilter(fdata,i/rate);
                        fdata=Signalprocessor.LPFilter(fdata,i/rate);
                        fdata=Signalprocessor.LPFilter(fdata,i/rate);
                    % Wiederholung  
%                         case 2
%                         fdata=LPFilter(ndata,i/rate);
%                     case 4
%                         fdata=LPFilter(ndata,i/rate);
%                         fdata=LPFilter(fdata,i/rate);
%                     case 6
%                         fdata=LPFilter(ndata,i/rate);
%                         fdata=LPFilter(fdata,i/rate);
%                         fdata=LPFilter(fdata,i/rate);
%                     case 8
%                         fdata=LPFilter(ndata,i/rate);
%                         fdata=LPFilter(fdata,i/rate);
%                         fdata=LPFilter(fdata,i/rate);
%                         fdata=LPFilter(fdata,i/rate);
                end
                r = Signalprocessor.rmsd(ndata, fdata, 'range');    % check the rmsd
                i=1+i;
                if i>1500
                    error('large number of filter iterations');
                end
            end
        end
        
        function RMSD = rmsd(data, estimate, norm)
        %% rmsd - calculates root mean square deviation of a time series to an estimated time series
        %         Note: 'data' and 'estimate' must have same size
        % >> RMSD = Signalprocessor.rmsd(data, estimate, norm)
        % PARAMETER: 
        %   'data'     1st time series 
        %   'estimate' 2nd time series
        %   'norm'     dedicated norm to be used out of 'none', 'range', 'mean'
        %   'RMSD'     filtered data
            switch norm
                case 'none'
                    normData = 1;
                case 'range'
                    normData = max(data)-min(data);
                case 'mean'
                    normData = abs(mean(data));
                otherwise
                    error('Unknown Method');
            end
            RMSD=sqrt(mean((data-estimate).^2))/normData;
        end
        
        function y = LPFilter(x, fc)
        %% LPFilter - First order zero-phase Lowpass filter
        %  >> lpSig = Signalprocessor.LPFilter(sig, fc); 
        % PARAMETER:
        %   'sig'   vector or matrix of input data (channels x samples)
        %    'fc'   -3dB cut-off frequency normalized by the sampling frequency
        %     'y'   vector or matrix of filtered data (channels x samples)
            k = .7; % cut-off value
            alpha = (1-k*cos(2*pi*fc)-sqrt(2*k*(1-cos(2*pi*fc))-k^2*sin(2*pi*fc)^2))/(1-k);
            y = zeros(size(x));
            for i = 1:size(x,1)
                y(i,:) = fbfilter(1-alpha, [1 -alpha], x(i,:));
            end
            function y = fbfilter(b, a, x)
                sz = size(x);
                if sz(1) == 1	% a row vector
                    rotate = true;
                    x = x(:);			% make it a column vector
                else
                    rotate = false; 
                end
                lx = size(x,1);
                a = a(:).';
                b = b(:).';
                lb = length(b);
                la = length(a);
                n = max(lb, la);
                lrefl = 3 * (n - 1);
                if la < n, a(n) = 0; end
                if lb < n, b(n) = 0; end
                
                %% Compute a the initial state
                %% Likhterov & Kopeika, 2003. 'Hardware-efficient technique for
                %% minimizing startup transients in Direct Form II digital filters'
                kdc = sum(b) / sum(a);
                if (abs(kdc) < inf) % neither NaN nor +/- Inf
                    si = fliplr(cumsum(fliplr(b - kdc * a)));
                else
                    si = zeros(size(a)); % fall back to zero initialization
                end
                si(1) = [];
                
                for c = sz(1):-1:1	% filter all columns, one by one
                    v = [2 * x(1,c) - x((lrefl+1):-1:2,c); ...
                        x(:,c); 2*x(end,c)-x((end-1):-1:end-lrefl,c)]; % a column vector
                    
                    %% Do forward and reverse filtering
                    v = filter(b,a,v,si*v(1));		       %#ok<*PROP> % forward filter
                    v = flipud(filter(b,a,flipud(v),si*v(end))); % reverse filter
                    y(:,c) = v((lrefl+1):(lx+lrefl));
                end
                
                if (rotate)			% x was a row vector
                    y = rot90(y);		% rotate it back
                end
            end
        end
        
        function y = BPFilter(x,fl,fu)
        %% BPFilter - Bandpass filter using FFT filtering
            % inputs:
            % x: vector or matrix of input data (channels x samples)
            % fl: normalized lower frequency
            % fu: normalized upper frequency
            %
            % output:
            % y: vector or matrix of filtered data (channels x samples)
            %
            % Note:
            % - fl and fu are the lower and upper frequency ranges of the bandpass filter
            % normalized by the sampling frequency
            % - The filter does not perform any windowing on the data
            
            N = size(x,2);
            
            S = fft(x,N,2);
            k = 1:ceil(fl*N);
            if(~isempty(k)),
                S(:,k) = 0; S(:,N-k+2) = 0;
            end
            k = floor(fu*N):ceil(N/2)+1;
            S(:,k) = 0; S(:,N-k+2) = 0;
            
            y = real(ifft(S,N,2));
        end
        
        function ffts = performFft(signal, tperiod)
        %% performFft - performs DFT, calculates fourier coefficients of an n-vector
        %               n-vector describing signal must have an equidistant time base
        % PARAMETER:
        %   'signal'   time series to be transformed (row or column vector)
        %   'tperiod'  total time of one period
            Ns=length(signal);                     
            M = floor((Ns+1)/2);                        % only first half is used.
            ffts = [];
            ffts.fftsig = fft(signal(:)');               % Fast Fourier Transformation on column vector
            ffts.tperiod = tperiod;
            ffts.a0 = ffts.fftsig(1)/Ns;                % first absolute value
            ffts.an = 2*real(ffts.fftsig(2:M))/Ns;      % coefficiants cos
            ffts.ax = ffts.fftsig(M+1)/Ns;              % last value
            ffts.bn = -2*imag(ffts.fftsig(2:M))/Ns;     % coefficiants sin
            ffts.n = 1:length(ffts.an);
        end
        
        function iffts = performInvFft(ffts, time)
        %% performInvFft - calculates time series from Fourier coefficients (inverse DFT)
        %                   transformation of time tout in [0,360] for dft
        %                   measurement points are equidistant, so the length
        %                   of the single period tin is cruical.
        % PARAMETER:
        %   'ffts'   fft struct with fields 'tperiod', 'a0', 'an, 'bn, 'ax' to be transformed
        %   'time'   row vector of times
            tdft = mod(time, ffts.tperiod) / ffts.tperiod; 
            angle = 2*pi * ffts.n' * tdft;
            iffts = real( ffts.a0 + ffts.an * cos(angle) + ffts.bn * sin(angle) + ffts.ax * cos(12*pi * tdft));
        end
        
        function signal = resampleFFT(sig, tperiod, time)
        %%  resampleFFT - resamples one period of a given signal using fft into any time base
        %                  fft series is evaluated at each time of new time base; 
        %   PARAMETER: signal = resampleFFT(sig, tperiod, time)
        %     'sig'      samples for one period at a constant samplerate
        %     'tperiod'  period time 
        %     'time'     row vector of times to evaluate 
        %     'signal'   result
        ffts = Signalprocessor.performFft(sig, tperiod); 
        signal = Signalprocessor.performInvFft(ffts, time);
        end
        
        function [y, t0, t1] = resampleSpline(x, oldrate, newrate)
        %%  resampleSpline - resamples time series using spline interpolation
        %                    Interpolation starts at 0. Last points don't match!
        %   >> y = Signalprocessor.resampleSpline(sin(0:0.1:100), 10, 4);
        %   >> [y, t0, t1] = Signalprocessor.resampleSpline(x, oldrate, newrate);
        %   >> plot(t0, x, '-o', t1, y, '-o');
        %
        %   PARAMETER:
        %     'x'        time series to be interpolated
        %     'oldrate'  rate give for 'x'
        %     'newrate'  rate expected for 'y'
        %     'y'        resampled 'x'
        %     't0'       original times
        %     't1'       recalculated times
        help = '<a href="matlab:help Signalprocessor/resampleSpline">USAGE</a>'; 
        if nargin < 3 || nargout > 3
            error('Signalprocessor.resampleSpline: wrong number of parameters\n%s\n', help);
        end
            N = length(x); 
            d = 1/double(oldrate); 
            t0 = 0.0:d:(N-1.0)*d; 
            t1 = 0.0:1.0/newrate:t0(end); 
            y = spline(t0, x, t1); 
        end
        function data = Smooth(prior, param, t1, t2)
        %%   NormalizeSignal - normalizes all signals to: max-min=1, mean = 0
        %                      for snippets specify start and end times
        %    >> zs.NormalizeSignal; 
        % PARAMETER: NormalizeSignal(this, t1, t2)
        %   'param'   (optional) smooth granularity
        %   't1'   (optional) start time of normalised window
        %   't2'   (optional, default: end) end time of normalized window
        % see also: UnNormalizeSignal
            
            i0 = 1; 
            i1 = size(prior, 1); 
            p = 0; 
            switch nargin
                case 2
                    p = param; 
                case 3
                    i0 = t1; 
                case 4
                    i0 = t1; 
                    i1 = t2; 
            end
            
            for i = size(prior, 2):-1:1
                if p
                    data(:, i) = KF.SmoothSignal(prior(i0:i1, i), p);
                else
                    data(:, i) = KF.SmoothSignal(prior(i0:i1, i));
                end
            end
            
        end
        function data = NormalizeSignal(data, t1, t2)
        %%   NormalizeSignal - normalizes all signals to: max-min=1, mean = 0
        %                      for snippets specify start and end times
        %    >> zs.NormalizeSignal; 
        % PARAMETER: NormalizeSignal(this, t1, t2)
        %   't1'   (optional) start time of normalised window
        %   't2'   (optional, default: end) end time of normalized window
        % see also: UnNormalizeSignal
            
            i0 = 1; 
            i1 = size(data, 1); 
            switch nargin
                case 2
                    i1 = t2; 
                case 3
                    i0 = t1; 
                    i1 = t2; 
            end
            
            for i = 1: size(data, 2)
                ma = max(data(i0:i1, i)); 
                mi = min(data(i0:i1, i)); 
                mea = (ma+mi)/2; 
                data(:, i) = (data(i0:i1, i)- mea)/(ma-mi); 
            end
            
        end
    end
end