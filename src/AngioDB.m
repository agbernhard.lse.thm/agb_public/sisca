classdef AngioDB < handle
    % Class AngioDB encapsulates all DB-Activity with database angiodb
    %  Author: Rudolf Huttary 7.2011
    %   - every instance establishes its own connection on contruction and closes it, when
    %       being destroyed (destructor)
    %   - connection may also be closed and reopened explicitly at any time by calling 
    %       this.open and this.close respectively
    %   - expects to find function DatabaseCredentials in path, which delivers authentification information 
    %   - over ssh: sudo ssh -f acanthosaura.imp.fu-berlin.de -L 1000:acanthosaura.imp.fu-berlin.de:3306 -N
    %
    % AngioDB properties:
    %    dbUrl - 'jdbc:mysql://localhost:3306/';
    %    dbDb  -  'angiodb';
    %    dbUsr - not preset
    %
    % class Methods:
    %   copyConstraints               - copies all constraints of sourceDB to destDB.
    %   copyMiscellanousAngioDBTables - copies all prerequisite contents for maintaining referential integrity from sourceDB into (existing) destDB 
    %
    % instance Methods:
    %   AngioDB                       - Constructor, opens DB-Connection      
    %   copy                          - copy all or some rows of sourceDB.table into destDB.table
    %   open                          - opens Database, if not already open
    %   select                        - executes any MySQL-SELECT-statement and returns struct array
    %                                   with column headers as fields
    %   select1                       - executes any MySQL-SELECT-statement and returns 
    %                                   [header content] array
    %   update                        - executes any updating MySQL-Operation insert/replace/update/delete; up to 4 BLOB-Fields
    %   exec                          - executes any MySQL-Operation 
    %   delete                        - Destructor, closes DB-connection
    %   close                         - closes DB, if open    

    % SISCA (Simulation, Identification and Statistical variation in Cardiovascular Analysis)
    %
    % Copyright (C) 2010 - 2018 FU Berlin (Mathematics, workgroup Prof. Christof Schütte)
    % Copyright (C) 2019 - 2020 THM Giessen (LSE, workgroup Prof. Stefan Bernhard)
    %
    % Author: Rudolf Huttary
    %
    % This program is free software: you can redistribute it and/or modify it under the terms 
    % of the GNU Affero General Public License Version 3 as published by the Free Software Foundation.
    %
    % This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
    % without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    % See the GNU Affero General Public License for more details.
    %
    % You should have received a copy of the GNU Affero General Public License along with this program. 
    % If not, see <http://www.gnu.org/licenses/>.
    
    properties (GetAccess = private)
            %dbUrl = 'jdbc:mysql://acanthosaura.imp.fu-berlin.de:3306/';        
            dbPwd; 
            dbDrv = 'com.mysql.cj.jdbc.Driver'; 
    end
    properties
            dbUrl; % = 'jdbc:mysql://localhost:3306/';
            dbDb; %=  'angiodb';
            dbUsr; 
    end
    properties (Access = private)
        isopen = logical(0); 
        dbCon  % jdbc connection handle
        dbDriver = []; 
    end
    
    methods 
        function this = AngioDB(database, user, password)
        %% AngioDB - Constructor, opens DB-Connection 
        %   PROTOTYPE: this = AngioDB(database, user, password)
        %    - default values are returned by DatabaseCredentials
        %   >> db = AngioDB;  
        %   >> db = AngioDB('angiodb'); 
        %   >> db = AngioDB('angiodb', 'User', 'Pwd'); 
        %   >> db = AngioDB('angioDB_Gul'); 
        %   to personalize, please implement DatabaseCredentials (outside of svn) like this: 
        %     function conn = DatabaseCredentials
        %        conn.dbUrl = 'jdbc:mysql://acanthosaura.imp.fu-berlin.de:3306/';                    
        %        conn.dbDb =  'angiodb';
        %        conn.dbUsr = 'xxxx';  % use your own here
        %        conn.dbPwd = 'yyyy';  % use your own here
        %     end
        % 
            conn = DatabaseCredentials();
            this.dbDb = conn.dbDb; 
            this.dbUrl = conn.dbUrl; 
            this.dbUsr = conn.dbUsr; 
            this.dbPwd = conn.dbPwd; 
            if nargin > 0
                this.dbDb = database; 
            end
            if nargin > 1
                this.dbUsr = user; 
            end
            if nargin > 2
                this.dbPwd = password; 
            end
            this.open; 
        end
        
        function res = open(this)
        %% open - opens Database, if not already open
        %   PROTOTYPE: res = open(this)
        %   catches any database connection related errors presenting a modal 
        %      Catched Error-Dialog with exception information 
        % 
        %   Parameters: res =  0 indicates failure
        %                      1 indicates success
            
            if ~this.isopen
               try
                   if isempty(this.dbDriver)
                        this.dbDriver = javaObject(this.dbDrv);
                   end
                   this.dbCon = java.sql.DriverManager.getConnection(this.dbUrl, this.dbUsr, this.dbPwd);  
                   this.isopen = true; 
               catch e
                   this.isopen = false; 
                   error('AngioDB: Failed to connect to MySql:\n%s ', e.message); 
               end
               this.exec(['use ' this.dbDb]);
            end
            res = this.isopen;  
        end
        
        function rs = select(this, sqlquery)
        %% select - executes any MySQL-SELECT-statement
        %   PROTOTYPE: rs = select(this, sqlquery)
        %   - catches any sql related errors presenting a modal Catched Error-Dialog with exception information 
        %   PARAMETERS: 
        %      'rs'       result set as struct array
        %      'sqlquery' query statement as string value
        %   
        %   example code for use with double Array in BLOB Field myblob: 
        %       db = AngioDB;
        %       res = db.select('select FileName, myblob from p1 where ID=5'); 
        %       res = typecast(res.myblob', 'double');

            pstmt = this.dbCon.prepareStatement(sqlquery);
            try
                %tic
                res = pstmt.executeQuery();  % retrieve resultSet-Object
                %toc
            catch e
                error('AngioDB.select: MySql-Query failed:\n%s', e.message); 
            end
            rs = []; 
            md = res.getMetaData; 
            res.beforeFirst; 
            j = 0; 
            while (res.next)                % process all rows in resulset
                j = j+1; 
                for i=1:md.getColumnCount   % process single row
                    type = md.getColumnType(i);
                    switch type
                        case 4 % INT
                          rs(j).(char(md.getColumnLabel(i))) = int32(res.getInt(i)); %#ok<*AGROW>
                        case -5 % BIGINT
                          rs(j).(char(md.getColumnLabel(i))) = int64(res.getLong(i));
                        case 7  % 'float' 'REAL'
                          rs(j).(char(md.getColumnLabel(i))) = double(res.getFloat(i));
                        case 8  % 'DOUBLE' 'REAL'
                          rs(j).(char(md.getColumnLabel(i))) = double(res.getBigDecimal(i));
                        case 91    % DATE
                          rs(j).(char(md.getColumnLabel(i))) = res.getDate(i);
                        case 92    % TIME
                          rs(j).(char(md.getColumnLabel(i))) = res.getTime(i);
                        case 93    % TIMESTAMP
                          rs(j).(char(md.getColumnLabel(i))) = res.getTimestamp(i);
                        case -7      % BOOL
                          rs(j).(char(md.getColumnLabel(i))) = logical(res.getBoolean(i));
                        case {-1} % TEXT 
                          rs(j).(char(md.getColumnLabel(i))) = char(res.getString(i));
                        case {1, 12} % CHAR VARCHAR 
                          rs(j).(char(md.getColumnLabel(i))) = char(res.getString(i));
                        case {-3, -4} % BLOB
                          rs(j).(char(md.getColumnLabel(i))) = res.getBytes(i);
                    end
                end
            end
        end

        function [headers,  rs] = select1(this, sqlquery, cr2star)
        %% select - executes any MySQL-SELECT-statement
        %   PROTOTYPE: [headers,  data] = select1(this, sqlquery)
        %    - catches any sql related errors and prints them into the command window
        %
        %  >>   [headers data] = db.select1('select typeName from NetType');
        %   PARAMETERS: 
        %      'sqlquery' query statement as string value
        %      'headers'  string cell array with column headers
        %      'data'     string cell array with result set
        %      'cr2star'  (optional) if true, CRLF is converted to '*'

            if nargin < 3
                cr2star = false; 
            end
            pstmt = this.dbCon.prepareStatement(sqlquery);
            try 
                res = pstmt.executeQuery();  % retrieve resultSet-Object
            catch e
                headers = {'Error'}; 
                rs = {[sprintf('AngioDB.select: MySql-Query failed:\n\n ') e.message]}; 
                return; 
            end
            headers = {'no result'}; 
            rs = {''}; 

            md = res.getMetaData;
            for i=1:md.getColumnCount   % process single row
                 headers{i} = char(md.getColumnLabel(i)); 
            end
            res.beforeFirst; 
            j = 0; 
            while (res.next)                % process all rows in resulset
                j = j+1; 
                for i=1:md.getColumnCount   % process single row
                    type = md.getColumnType(i);
                    switch type
                        case 4 % INT
                          rs{j,i} = int32(res.getInt(i));
                        case -5 % BIGINT
                          rs{j,i} = int64(res.getLong(i));
                        case 8  % 'DOUBLE' 'REAL'
                          rs{j,i} = double(res.getBigDecimal(i));
                        case 91    % DATE
                          rs{j,i} = res.getDate(i);
                        case 92    % TIME
                          rs{j,i} = res.getTime(i);
                        case 93    % TIMESTAMP
                          rs{j,i} = res.getTimestamp(i);
                        case -7      % BOOL
                          rs{j,i} = logical(res.getBoolean(i));
                        case {1, 12, -1} % CHAR VARCHAR
                          str =  char(res.getString(i));
                          if cr2star
                              str(str == char(10)) = '|'; 
                          end
                          rs{j,i} = str; 
                        case {-3, -4} % BLOB TINYBLOB LONGBLOB
                          rs{j,i} = typecast(res.getBytes(i)', 'uint8');
                    end
                end
            end
        end

        
        function count = update(this, sqlstmt, blob1, blob2, blob3, blob4)
        %% update - executes any updating MySQL-Operation insert/replace/update/delete; up to 4 BLOB-Fields
        %   PROTOTYPE: count = update(this, sqlstmt, blob1, blob2, blob3, blob4)
        %    - catches any sql related errors presenting a Catched Error-Dialog with exception information 
        %
        %   PARAMETERS: 
        %     'count'   number of rows updated
        %     'sqlstmt' SQL-Statement as string, ? represents blob-values to include
        %     'blob1', ... int8-Array containing Blob-Data
        %
        %   example code for use: 
        %       db = AngioDB;
        %       cArr = 0:1:1000000; 
        %       uint8Arr = typecast(cArr, 'int8');
        %       db.update('replace p1 (id, myblob) Values(5, ? )', uint8Arr); 
        
            pstmt = this.dbCon.prepareStatement(sqlstmt);
            if nargin > 2
                blob = this.dbCon.createBlob;  %#ok<NASGU>
                try 
                   blob = this.dbCon.createBlob; 
                   blob.setBytes(1, blob1); 
                   pstmt.setBlob(1, blob);
                   if nargin > 3
                       blob = this.dbCon.createBlob; 
                       blob.setBytes(1, blob2); 
                       pstmt.setBlob(2, blob);
                   end
                   if nargin > 4
                       blob = this.dbCon.createBlob; 
                       blob.setBytes(1, blob3); 
                       pstmt.setBlob(3, blob);
                   end
                   if nargin > 5
                       blob = this.dbCon.createBlob; 
                       blob.setBytes(1, blob4); 
                       pstmt.setBlob(4, blob);
                   end
                catch e
                    error('AngioDB.update: too many blobs given :\n%s\n ', e.message); 
                    count = -1;
                    return; 
                end
            end
            try
                count = pstmt.executeUpdate();  % retrieve resultSet-Object
            catch e
               error('AngioDB.update: MySql-Statement failed:\n%s \n%s', sqlstmt ,e.message(1:200)); 
            end
        end
        
        function res = exec(this, sqlstmt)
        %% exec - executes any MySQL-Operation 
        %   PROTOTYPE: res = exec(this, sqlstmt)
        %    - catches any sql related errors presenting a Catched Error-Dialog with exception information 
        %
        %   PARAMETERS:
        %     res = result of statement; 0, if operation has a result, 1 if not
        %     sqlstmt - SQL-Statement as string, no ? interpreted
        % 
        %   example code for use: 
        %      res = db.exec('use angiodb')            % res = 0
        %      res = db.exec('select * from NetType')  % res = 1
        %         % procede 
        %      end; 
        
            pstmt = this.dbCon.prepareStatement(sqlstmt);
            try 
               res = pstmt.execute();  % retrieve resultSet-Object
            catch e
               error('AngioDB.exec: MySql-Statement failed:\n %s\n ', e.message(1:200)); 
            end

        end
       
        function close(this)
        %% close - closes DB, if open
        %   PROTOTYPE: close(this)
            if this.isopen
                this.dbCon.close; 
                this.isopen = false; 
            end
        end
        function delete(this)
        %% delete - Destructor, closes DB-connection
        this.close; 
        end
        
        function copy(this, sourceDB, destDB, table, where)
        %% copy - copy all or some rows of sourceDB.table into destDB.table
        %   PROTOTYPE: copy(this, sourceDB, destDB, table, where)
        %
        %  >> db.copy('angiodb', 'angioDB_Gul', 'Diagnose', ''); 
        %  >> db.copy('angiodb', 'angioDB_Gul', 'StatisticalSet', 'where id = 1'); 
        %   PARAMETER: 
        %     'sourceDB' database used as source
        %     'destDB'  database used as destination
        %     'table'  table to use (error thrown, if table does not exist in destDB or sourceDB)
        %     'where'  where clause or '' (error thrown, if result set for copy is empty)
        
            if nargin ~= 5
                error('AngioDB.copy: wrong number of arguments'); 
            end
            %test if table exists 
            command = sprintf('select table_name from information_schema.tables where table_schema = "%s" and table_name = "%s"', sourceDB, table); 
            if isempty(this.select(command)); 
                error('AngioDB.copy: table %s.%s not found', sourceDB, table); 
            end
            command = sprintf('select table_name from information_schema.tables where table_schema = "%s" and table_name = "%s"', destDB, table); 
            if isempty(this.select(command)); 
                error('AngioDB.copy: table %s.%s not found', destDB, table); 
            end
            command = sprintf('SELECT * FROM %s.%s %s', sourceDB, table, where);
            if isempty(this.select(command)); 
                error('AngioDB.copy: result of SELECT command is empty:\n%s', command); 
            end

            command = sprintf('INSERT INTO %s.%s SELECT * FROM %s.%s %s', destDB, table, sourceDB, table, where);
            this.exec(command);

        end
    end
    methods (Static)
        function copyMiscellanousAngioDBTables(destDB, sourceDB)
        %% copyMiscellanousAngioDBTables - copies all prerequisite contents for maintaining referential integrity 
        %                                  from sourceDB into (existing) destDB 
        %   PROTOTYPE: copyMiscellanousAngioDBTables(destDB, sourceDB)
        %  >> AngioDB.copyMiscellanousAngioDBTables('angiodb_struct');
        %  >> AngioDB.copyMiscellanousAngioDBTables('angiodb_backup', 'angiodb');
        %
        %   PARAMETER: 
        %     'sourceDB' (optional) database used as source
        %     'destDB'  database used as destination
            
            db = AngioDB; 
            if nargin == 1
                sourceDB = 'angiodb'; 
            end
            db.exec(sprintf('delete from %s.TSType', destDB)); 
            db.copy(sourceDB, destDB, 'TSType', '');
            
            db.exec(sprintf('delete from %s.NodeType', destDB)); 
            db.copy(sourceDB, destDB, 'NodeType', '');
            
            db.exec(sprintf('delete from %s.NetType', destDB)); 
            db.copy(sourceDB, destDB, 'NetType', '');
            
            db.exec(sprintf('delete from %s.ArteryNetType', destDB)); 
            db.copy(sourceDB, destDB, 'ArteryNetType', '');
            
            db.exec(sprintf('delete from %s.Diagnose', destDB)); 
            db.copy(sourceDB, destDB, 'Diagnose', '');

            db.exec(sprintf('delete from %s.StatisticalSet where id = 1', destDB)); 
            db.copy(sourceDB, destDB, 'StatisticalSet', 'where id = 1');
            
            db.exec(sprintf('delete from %s.ClinicalSet where id = 1', destDB)); 
            db.copy(sourceDB, destDB, 'ClinicalSet', 'where id = 1');
            
            db.exec(sprintf('delete from %s.ExperimentalSet where id = 1', destDB)); 
            db.copy(sourceDB, destDB, 'ExperimentalSet', 'where id = 1');
            
            db.exec(sprintf('delete from %s.Units', destDB)); 
            db.copy(sourceDB, destDB, 'Units', '');

            db.exec(sprintf('delete from %s.Solvers', destDB)); 
            db.copy(sourceDB, destDB, 'Solvers', '');
        end
        
        function copyConstraints(sourceDB, destDB)
        %% copyConstraints - copies all constraints of sourceDB to destDB.
        %   PROTOTYPE: copyConstraints(sourceDB, destDB)
        %    - apply after a structural copy of angio
        %    - throws error if constraints already exist (then all constraints must be deleted first) 
        %    - throws error if destDB is 'angiodb'.
        % >> AngioDB.copyConstraints('angiodb', 'angiodb_backup');
        %
        %   PARAMETER: 
        %     'sourceDB' (optional) database used as source
        %     'destDB'  database used as destination
        
            db = AngioDB; 
            if nargin ~=2 
                error('two arguments expected'); 
            end
            if strcmp(destDB, 'angiodb')
                error('angiodb can''t be destination. If you want this, please alter code'); 
            end
            
            try
                db.exec(['use ' sourceDB]); 
            catch e
                error('AngioDB.copyConstraints: sourceDB ''%s'' does not exist)', sourceDB); 
            end
            try
                db.exec(['use ' destDB]); 
            catch e
                error('AngioDB.copyConstraints: destDB ''%s'' does not exist)', destDB); 
            end

            cols = 'a.REFERENCED_TABLE_SCHEMA, a.TABLE_NAME, a.COLUMN_NAME, a.CONSTRAINT_NAME, a.REFERENCED_TABLE_SCHEMA, a.REFERENCED_TABLE_NAME, a.REFERENCED_COLUMN_NAME ';
            from = 'information_schema.KEY_COLUMN_USAGE as a '; 
            where = sprintf('a.REFERENCED_TABLE_SCHEMA <> ''Null'' and a.CONSTRAINT_SCHEMA = ''%s''', destDB); 
            select = sprintf('select %s from %s where %s',  cols, from, where); 
            res = db.select(select); 
            if ~isempty(res)
                error('AngioDB.copyConstraints: destDB ''%s'' has already constraints. Delete all relations first (use phpMyAdmin/Designer)', destDB); 
            end

            where = sprintf('a.REFERENCED_TABLE_SCHEMA <> ''Null'' and a.CONSTRAINT_SCHEMA = ''%s''', sourceDB); 
            select = sprintf('select %s from %s where %s',  cols, from, where); 
            res = db.select(select); 

            db.exec('Start Transaction'); 
            try 
                for i = 1:length(res); 
                    re = res(i); 
                    fprintf('%s.%s -> %s.%s\n', re.TABLE_NAME, re.COLUMN_NAME, re.REFERENCED_TABLE_NAME, re.REFERENCED_COLUMN_NAME); 
                    command = sprintf('ALTER TABLE %s.%s ', destDB, re.TABLE_NAME); 
                    command = sprintf('%s ADD CONSTRAINT FOREIGN KEY (%s) REFERENCES %s.%s (%s) ON DELETE CASCADE ON UPDATE CASCADE ', command, re.COLUMN_NAME,  destDB, re.REFERENCED_TABLE_NAME,  re.REFERENCED_COLUMN_NAME); 
                   disp(command)
                   db.exec(command); 
                end;
            catch e 
                fprintf('%s\n', e.message); 
                db.exec('Rollback'); 
                error('Fehler: %s', e.message); 
            end
            db.exec('Commit'); 
        end
    end
end

