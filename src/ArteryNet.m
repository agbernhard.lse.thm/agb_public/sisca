classdef ArteryNet < DbObj
    %% ArteryNet represents a model in a tree structure with root, node, and boundary node elements and supplies 
    %      GUI based facilities for
    %         - structure design that lets you create/modify/maintain model structures
    %         - persistent parameterizations (ParamEditor) 
    %         - calculation of forward solutions (SolverRun)
    %         - in-place viewing of synthetic and measured data (MacSim)
    %      all data are persistable to database (AngioDB)
    %
    %  See also AngioDB, MacSim, Node, ParamEditor, SolverRun, STS
    %
    % ArteryNet Properties:
    %   structural props
    %     root     - root node
    %     nodeArr  - contains all nodes (only if status is 'complete')
    %     db       - handle for AngioDB connection
    %
    % class methods:
    %   ArteryNets          - returns a list of known ArteryNets
    %   CtsFromSolution     - returns the last period of solution on a node as CTS
    %   FromParamSetId      - returns a fully instantiated ArteryNet object for ParamSet object with pset_id
    %   FromSolverRunId     - returns a fully instantiated ArteryNet object for this SolverRun
    %   Open                - shows open dialog which allows to choose an ArteryNet with a ParamSet, then opens a designer window for it.
    %   OpenDlg             - shows a dialog to interactively open a ArteryNet object with default ParamSet
    %   ParamSets           - returns an id-list of Parameter sets known for the ArteryNet object with id=anet_id
    %   List                - generates a list of known ArterNet objects
    %   calcC                - common function for calculation of compliance    
    %
    % instance methods:
    %   ArteryNet           - Constructor, provides default values for all three parameters if omitted
    %   boundaryCount       - return number of boundary nodes
    %   channel             - returns the channel from node with fromId to node with toId
    %   CtsIn               - returns the current inbound TS at root
    %   CurrentSolution     - returns result of last solver run.
    %   Designer            - opens Window, draws structure and allows editing
    %   fromWorkspace       - initialize ArteryNet object from workspace
    %   getParamSet         - returns struct with all relevant parameters of object this
    %   getSolverRun        - returns a rich set of SolverRun Properties
    %   getSolution         - returns the solution of current SolverRun object
    %   NET                 - genereates old xxxNET-struct with matrices(adj and Z)
    %   NewStatisticalSet   - generates a new StatisticalSet object in db
    %   nodeTree            - gets indices of nodes in subtree
    %   nodeCount           - return number of nodes
    %   nodeDistance        - returns sum of node lengths between two nodes
    %   nodeChannel         - returns nodes in path between node1 and node2
    %   ODE                 - establishes the ODE system resulting from the adjacent as function definition in gui_rhsfn.m
    %   ParamSetSaveAs      - save current ParamSet as new object to DB
    %   ParOld              - genereates old xxxPAR-struct
    %   RCL                 - evaluates windkessel model parameters from structural and physiological parameters
    %   ReadParamSet        - read paramSet from DB in current object
    %   rootChannel         - returns the channel from node with nodeId to root
    %   rootChannels        - returns channels from all boundary nodes to root
    %   RunSolver           - starts a new solver run
    %   scaleParamSet       - scales parameters 'dlh', 'd', 'l', 'h', 'E', 'b_R', 'b_p', 'b_q' of node list
    %   setSolverRun        - setter for the following SolverRun Properties
    %   setParamSet         - sets  paramSet of this
    %   SolverRunSave       - saves current Solverrun
    %   X0                  - computes the initial value vektor p0
    %   dump                - generates a printable dump with all net and all/dedicated node parameters, optional into file
    
    % SISCA (Simulation, Identification and Statistical variation in Cardiovascular Analysis)
    %
    % Copyright (C) 2010 - 2018 FU Berlin (Mathematics, workgroup Prof. Christof Schütte)
    % Copyright (C) 2019 - 2020 THM Giessen (LSE, workgroup Prof. Stefan Bernhard)
    %
    % Author: Rudolf Huttary
    %
    % This program is free software: you can redistribute it and/or modify it under the terms 
    % of the GNU Affero General Public License Version 3 as published by the Free Software Foundation.
    %
    % This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
    % without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    % See the GNU Affero General Public License for more details.
    %
    % You should have received a copy of the GNU Affero General Public License along with this program. 
    % If not, see <http://www.gnu.org/licenses/>.
    
    %% structural
    properties (SetAccess = private)
        root                   % root node
        nodeArr                % container for nodes (valid after 'sort')
        anet =  [];            % handle for second ArteryNet object (used by mAOpen
        
    end
    properties
        % ArteryNet db representation
        % inherited: id
        db                     % handle for AngioDB access
        temp_id
        type = 1;
        currentParamSetId
        design
        adjacency
        Z
        
        % ParamSet db representation
        p_id
        p_temp_id
        p_arteryNetId
        p_ctsSetId = 0
        p_currentSolverRun = 0;
        p_statisticalSetId = 1;
        p_name = 'Pnn'
        p_description
        p_lastmodified
        p_solverRun
        
        % NetParam (fluid) ...
        n_id
        n_temp_id
        n_paramSetId
        n_name
        n_description
        n_type = 1;
        n_eta = 4e-3;
        n_rho = 1.050e3;
        n_gamma = 2;
        n_alpha = [10 9 90 2.76, 0.08];
        n_nu = [4 6 50 70 100] ;
        n_ctsId = 1;   % ingoing TS for rootnode both types
        n_cts;
        n_heartRate = 80;
        n_typeNames = {};       % names of model types
        
        % design
        axesRect = [-1 1 -1 1];
        
        % state
        modified = true;               % tracks edit state
        p_modified = true;
        validated = false;
        solRunsExist = false;
        editObject
        visualize = false;
        MacSim = [];
    end
    properties % display
        hFig
        hAxes
        hNodeMenu
        hRootMenu
        hEdgeMenu
        hPropEditorNet
        hPropEditorNode
        hmenuArtery
        hmenuParam
        hmenuSolver
        hmVisualize
    end
    
    %% instance Methods
    methods
        function obj = ArteryNet(varargin)
            %% ArteryNet - Konstruktor, maintains its own db-connection
            %   >> anet = ArteryNet();                              - empty net
            %   >> anet = ArteryNet(id);                            - from db with 'id', uses currentParamSetId, currentSolverRun
            %   >> anet = ArteryNet(id, pid);                       -   " with specific ParamSet, uses currentSolverRun
            %   >> anet = ArteryNet(id, pid, sid);                  -   " with specific ParamSet and SolverRun
            %   >> anet = ArteryNet(id, pid, 'keepCurrent');        -   " ... currentParamSetId is not updated in DB
            %   >> anet = ArteryNet(id, pid, sid, 'keepCurrent');   -  " ...
            %   >> anet = ArteryNet(id, pid, sid, db);              -  " ... uses db, implies 'keepCurrent';
            %   >> anet = ArteryNet(xxxNet, xxxPar);                - from Workspace (for legacy stuff)
            %      - 'id'  ID of ArteryNet object in Database
            %      - 'pid' ID of ParamSet  object in Database
            %      - 'sid' ID of SolverRun object in Database
            %      - 'db'  common AngioDB object
            
            obj.p_solverRun = SolverRun;   % just a new object
            obj.name = 'none';
            obj.description = 'none';
            
            if nargin == 0            % new instance from the scratch
                obj.root = Node(0);
                obj.root.position = [0 0.8];
                newnode = Node(1);
                newnode.parent = obj.root;
                obj.root.addChild(newnode);
                newnode.position = [0 0.7];
                nodes = [obj.root  newnode];
                obj.nodeArr = nodes;
                return;
            end
            % from workspace
            if isfield(varargin{1}, 'adj')
                obj.fromWorkspace(varargin);
                return;
            end
            
            if isnumeric (varargin{1})
                % read and construct object from DB
                %           varargin{1} ArtertyNet.id
                % optional: varargin{2} ParamSet.id
                % if missing, currentParamSetId determines ParamSet
                
                %                 if nargin > 1 && ~isnumeric(varargin{2})
                %                     error('ArteryNet.Constructor: if given, second argument should denote ParamSet.id ');
                %                 end
                
                % read the structure and initialize object
                if isa(varargin{nargin}, 'AngioDB')
                    obj.db = varargin{nargin};
                else
                    obj.db = AngioDB;
                end
                anetstruct = obj.db.select(['select * from ArteryNet where id = ' num2str(varargin{1})]);
                if isempty(anetstruct)
                    error(['ArteryNet.Constructor: There is no ArteryNet object with id = ' num2str(varargin{1}) ' in database']);
                end
                obj.id = anetstruct.id;
                obj.name = char(anetstruct.name);
                
                % description, type, currentParamSetId, design, adjacency
                obj.description = char(anetstruct.description);
                obj.type = anetstruct.type;
                res = obj.db.select('select * from NetType');
                for i=length(res):-1:1
                    obj.n_typeNames{i} = res(i).typeName; 
                end
                obj.currentParamSetId = anetstruct.currentParamSetId;
                if isa (anetstruct.design(1), 'int8')
                    obj.design = typecast(anetstruct.design, 'double');
                elseif isa (anetstruct.design(1), 'double')
                    obj.design = anetstruct.design;
                end
                obj.axesRect = obj.design(1:4);
                if isa (anetstruct.adjacency(1), 'int8')
                    obj.adjacency = typecast(anetstruct.adjacency, 'double');
                elseif isa (anetstruct.design(1), 'double')
                    obj.adjacency = anetstruct.adjacency;
                end
                
                n = size(obj.adjacency,1);
                obj.nodeArr = Node(-1);
                obj.nodeArr(n) = Node(-1);
                obj.root = obj.nodeArr(1);
                for i=2:n               % knit the tree
                    parent = obj.adjacency(i);
                    obj.nodeArr(i).parent = obj.nodeArr(parent); % % parent
                    child = obj.nodeArr(i);
                    obj.nodeArr(i).parent.addChild(child); % children
                end
                for i=1:n
                    obj.nodeArr(i).position = [obj.design(2*i+3) obj.design(2*i+4)];
                end
                obj.modified = false;
                
                keep = false;
                nar = nargin;
                
                % no update if last arg is db or 'keepCurrent'
                if isa(varargin{nargin}, 'AngioDB') || ...
                        ischar(varargin{nargin}) && strcmp(varargin{nargin}, 'keepCurrent')
                    keep = true;
                    nar = nargin-1;
                end
                if nar == 1
                    obj.ReadParamSet(obj.currentParamSetId); % read current ParamSet
                elseif nar == 2
                    obj.ReadParamSet(varargin{2});           % read explicitly specified ParamSet
                    if ~keep
                        obj.db.update(['update ArteryNet set currentParamSetId = ''' num2str(varargin{2}) ''' where id=''' num2str(obj.id) '''']);
                    end
                elseif nar == 3
                    obj.ReadParamSet(varargin{2}, varargin{3});           % read explicitly specified ParamSet
                    if ~keep
                        obj.db.update(['update ArteryNet set currentParamSetId = ''' num2str(varargin{2}) ''' where id=''' num2str(obj.id) '''']);
                    end
                end
            end
        end
             
        function ReadParamSet(obj, id, sid)
            %%  ReadParamSet            - read paramSet from DB into current object
            %   >> anet.ReadParamSet(pid);       % 'select * from ParamSet where id=pid'
            %   >> anet.ReadParamSet(pid, sid);  % '   "    select * from SolverRun where id=sid'
            %      - 'pid' ID of ParamSet  object in Database
            %      - 'sid' optional (default: ParamSet.currentSolverRun) ID of SolverRun object in Database.
            %              if 0, an empty SolverRun object is used (to save loading time)
            if nargin < 2
                error('ArteryNet.ReadParamSet: al least one Parameter expected');
            elseif ~isnumeric(id)
                error('ArteryNet.ReadParamSet: Parameter must be an Integer');
            end
            if isempty(obj.db)
                obj.db = AngioDB;
            end
            obj.db.open;
            paramSet = obj.db.select(['select * from ParamSet where id = ' num2str(id)]);
            if isempty(paramSet)
                error(['ArteryNet.ReadParamSet: There is no ParamSet object with id = ' num2str(id) ' in database']);
            end
            netParam = obj.db.select(['select * from NetParam where paramSetId = ' num2str(id)]);
            if isempty(netParam)
                error(['ArteryNet.ReadParamSet: There is no NetParam object for paramSetId ' num2str(id) ' in database']);
            end
            nodeParam = obj.db.select(['select * from NodeParam where paramSetId = ' num2str(id)]);
            if isempty(nodeParam)
                error(['ArteryNet.ReadParamSet: There is no NodeParam object for paramSetId ' num2str(id) ' in database']);
            end
            boundaryParam = obj.db.select(['select * from BoundaryParam where paramSetId = ' num2str(id)]);
            if isempty(boundaryParam)
                error(['ArteryNet.ReadParamSet: There is no BoundaryParam object for paramSetId ' num2str(id) ' in database']);
            end
            
            
            obj.p_id =               paramSet.id;
            obj.p_arteryNetId =      paramSet.arteryNetId;
            obj.p_ctsSetId =         paramSet.ctsSetId;
            obj.p_name =        char(paramSet.name);
            obj.p_description = char(paramSet.description);
            obj.p_lastmodified =char(paramSet.lastmodified);
            obj.p_currentSolverRun = paramSet.currentSolverRun;
            % read SolverRun
            if nargin < 3
                obj.p_solverRun = SolverRun(obj.db, obj.p_id);
            end
            if nargin == 3
                if sid > 0
                    obj.p_solverRun = SolverRun(obj.db, obj.p_id, sid);
                else
                    obj.p_solverRun = SolverRun;  % just a new object (optimized for ssModel - looping)
                end
            end
            
            % process netParam
            obj.n_id =               netParam.id;
            obj.n_paramSetId =       netParam.paramSetId;
            obj.n_name =        char(netParam.name);
            obj.n_description = char(netParam.description);
            obj.n_type =             netParam.type;
            obj.n_eta =              netParam.eta;
            obj.n_rho =              netParam.rho;
            obj.n_gamma =            netParam.gamma;
            obj.n_alpha =            typecast(netParam.alpha, 'double');
            obj.n_nu =               typecast(netParam.nu, 'double');
            obj.n_ctsId =            netParam.inboundCtsId;
            obj.n_heartRate =        netParam.heartRate;
            
            % process nodeParam
            n = size(obj.nodeArr,2)-1;
            if n ~= size(nodeParam, 2)
                error(['ArteryNet.ReadParamSet - unexpected number of nodes. Net has:' n ' nodes, ParamSet has: ' num2str(size(nodeParam, 2)) ]);
            end;
            for i=1:n
                idx =                               nodeParam(i).nodeId +1;
                obj.nodeArr(idx).id =               nodeParam(i).id;
                obj.nodeArr(idx).paramSetId =       nodeParam(i).paramSetId;
                obj.nodeArr(idx).name =        char(nodeParam(i).name);
                obj.nodeArr(idx).nodeId =           nodeParam(i).nodeId;
                obj.nodeArr(idx).description = char(nodeParam(i).description);
                obj.nodeArr(idx).type =             nodeParam(i).type;
                obj.nodeArr(idx).E =                nodeParam(i).E;
                obj.nodeArr(idx).l =                nodeParam(i).l;
                obj.nodeArr(idx).h =                nodeParam(i).h;
                obj.nodeArr(idx).d =                nodeParam(i).d;
            end
            obj.nodeArr(1).nodeId = 0;
            
            % process BoundaryParam
            for i=1:size(boundaryParam, 2)
                idx =                               boundaryParam(i).nodeId + 1;
                obj.nodeArr(idx).b_id =             boundaryParam(i).id;
                obj.nodeArr(idx).b_paramSetId =     boundaryParam(i).paramSetId;
                obj.nodeArr(idx).b_name =      char(boundaryParam(i).name);
                obj.nodeArr(idx).description = char(boundaryParam(i).description);
                obj.nodeArr(idx).b_R =             boundaryParam(i).R;
                obj.nodeArr(idx).b_C =             boundaryParam(i).C;
                obj.nodeArr(idx).b_L =             boundaryParam(i).L;
                obj.nodeArr(idx).b_p =              boundaryParam(i).p;
                obj.nodeArr(idx).b_q =              boundaryParam(i).q;
            end
            obj.p_modified = false;
        end
        
        function fromWorkspace(obj, varargin)
            %% fromWorkspace - initialize ArteryNet object from workspace
            %    >> anet.fromWorkspace(varargin);
            %
            % varargin{1,1} = xxxNet                - structural description
            %                   .adj                - NxN-adjacency
            %                   .Z                  - NxM leave nodes
            % varargin{1,2} = xxxPar                - parameter set
            %                   .structure.E        - 1xN elastic module
            %                   .structure.h        - 1xN thickness
            %                   .structure.l        - 1xN length
            %                   .structure.d        - 1xN diameter
            %                   .fluid.nu           - 1x1
            %                   .fluid.rho          - 1x1
            %                   .fluid.mtd          - 1x1
            %                   .fluid.gama         - 1x1
            %                   .boundary.in.pin    - 1x1 name of pqArr
            %                   .boundary.in.qin    - 1x1 flow at root
            %                   .boundary.out.Z     - 2xNxM impedance at leave nodes
            %                   .boundary.out.pout  - 1x1 name of pqArr
            %                   .boundary.out.qout  - 2xNxM pressure at leave nodes
            
            
            
            args = varargin{1};
            xxxNet = args{1};
            
            if size(args, 2) > 1
                xxxPar  = args{2};
            end
            
            obj.modified = true;   % always, when loaded from workspace!
            obj.p_modified = true;   % always, when loaded from workspace!
            obj.type = 1;
            
            
            [m,n]=size(xxxNet.adj);
            if m~=n % test it first
                res = questdlg('Adjacency matrix is not square!', 'ArteryNet', 'continue with new design', 'exit with exception', 'exit with exception');
                switch res,
                    case 'continue with new design'
                        obj.root = Node(0);
                        obj.root.position = [0 0.8];
                        return;
                    case 'exit with exception'
                        error ('ArteryNet(xxxNet,...) - parameter invalid, adjacency matrix is not square!')
                end
            end
            % process xxxNet
            nodes(1,m+1) = Node(m);  % for speed
            for i=0:m-1                % put nodes into array
                nodes(1, i+1) = Node(i);
            end
            [x,y]=find(xxxNet.adj); % -> sparse matrix
            x = [1; x+1];           % take root into account
            for i=1:m               % knit the tree
                nodes(i+1).parent = nodes(x(i)); % % parent
                nodes(i+1).parent.addChild(nodes(i+1)); % children
            end
            obj.root = nodes(1, 1);
            obj.nodeArr = nodes;
            
            % test and process xxxPar
            validparameters = 0;
            if nargin > 1 && ~isempty(xxxPar)
                leaves = size(xxxNet.Z, 2);
                if m ~= size(xxxPar.structure.E, 2) || ...
                        m ~= size(xxxPar.structure.h, 2) || ...
                        m ~= size(xxxPar.structure.l, 2) || ...
                        m ~= size(xxxPar.structure.d, 2) || ...
                        leaves ~= size(xxxPar.boundary.out.Z, 2) || ...
                        leaves ~= size(xxxPar.boundary.out.pout, 2) && ...
                        leaves ~= size(xxxPar.boundary.out.qout, 2)
                    error('ArteryNet: at least one xxxNet and xxxPar dimension mismatch!');
                end
                
                % xxxPar.boundary.out
                [q w] = find(xxxNet.Z);  %#ok<*NASGU>
                if size(q, 1) ~= leaves
                    % error('ArteryNet: dimension mismach xxxNet.Z and xxxPar.boundary.out');
                else
                    validparameters = 1;
                    for i=1:leaves
                        if isfield(xxxPar.boundary, 'Z')
                        end
                        if isfield(xxxPar.boundary, 'qout')
                        end
                        if isfield(xxxPar.boundary, 'pout')
                        end
                    end
                end
                %%
                % xxxPar.fluid
                obj.n_eta = xxxPar.fluid.nu;     % Raider -> Twixx
                obj.n_rho = xxxPar.fluid.rho;
                obj.n_type = 1;
                obj.n_gamma = xxxPar.fluid.gamma;
                
                % xxxPar.structure
                idx = 0;
                for i=2:size(nodes,2)
                    nodes(1,i).E = xxxPar.structure.E(i-1);
                    nodes(1,i).h = xxxPar.structure.h(i-1);
                    nodes(1,i).l = xxxPar.structure.l(i-1);
                    nodes(1,i).d = xxxPar.structure.d(i-1);
                    % boundary
                    if isempty(nodes(1,i).children)
                        idx = idx + 1;
                        nodes(1,i).b_R = xxxPar.structure.Z(idx);
                    end
                end
                
            end
            
            % test and process xxxDesign
            % Standardlayout
            [x, y] = treelayout([0 x']);
            x = -2*x + 1;
            y = 2*y - 1;
            for i=1:m+1
                nodes(i).position = [x(i) y(i)];
                if validparameters
                    nodes(i).color = 'cyan';    % standard layout + parameters
                else
                    nodes(i).color = 'yellow';  % standard layout + no parameters
                end
            end
            obj.collectParams;
        end
        
        function structureChanged(this)
            this.id = [];
            this.p_id = [];
            this.modified = true;
            this.p_modified = true;
            this.p_solverRun = SolverRun;
            
            res = Arrange(1, this.root); 
            this.nodeArr = this.nodeArr(1:res-1); 

            function res = Arrange (start, node)
                this.nodeArr(start) = node; 
                res = start+1; 
                for i=1:length(node.children)
                   res = Arrange(res, node.children(i)); 
                end
            end
        end
        
        function setDesignerTitle(this)
            set (this.hFig, 'Name', this.title);
        end
        
        function str = title(this)
            solver = '-';
            if this.p_solverRun.id
                solver=num2str(this.p_solverRun.id);
            end
            str = sprintf('ArteryNet(%i, %i, %s) - %s [%s]', this.id, this.p_id, solver, this.name ,this.p_name);
        end
        
        function MaintainAMenu(this, varargin)
            this.MaintainMenu('ArteryNet', 'Save ...', 'Enable', 'on');
            if isempty(this.id) || ~this.modified
                this.MaintainMenu('ArteryNet', 'Save ...', 'Enable', 'off');
            end
        end
        function MaintainPMenu(this, varargin)
            this.MaintainMenu('ParameterSet', 'Open ...', 'Enable', 'on');
            this.MaintainMenu('ParameterSet', 'Save ...', 'Enable', 'on');
            this.MaintainMenu('ParameterSet', 'Save as ...', 'Enable', 'on');
            this.MaintainMenu('ParameterSet', 'Delete ...', 'Enable', 'on');
            if isempty(this.id)  % ArteryNet structure was changed
                this.MaintainMenu('ParameterSet', 'Open ...', 'Enable', 'off');
                this.MaintainMenu('ParameterSet', 'Save ...', 'Enable', 'off');
                this.MaintainMenu('ParameterSet', 'Save as ...', 'Enable', 'off');
                this.MaintainMenu('ParameterSet', 'Delete ...', 'Enable', 'off');
            end
            if ~this.p_modified
                this.MaintainMenu('ParameterSet', 'Save ...', 'Enable', 'off');
                this.MaintainMenu('ParameterSet', 'Save as ...', 'Enable', 'off');
            end
            if ~strcmp(num2str(this.p_solverRun.id), num2str(this.p_currentSolverRun))
                this.MaintainMenu('ParameterSet', 'Save ...', 'Enable', 'on');
                this.MaintainMenu('ParameterSet', 'Save as ...', 'Enable', 'on');
            end
        end
        
        function MaintainSMenu(this, varargin)
            this.root.validate(this);
            this.MaintainMenu('SolverRun', 'New ...', 'Enable', 'on');
            this.MaintainMenu('SolverRun', 'Show ...', 'Enable', 'on');
            this.MaintainMenu('SolverRun', 'Open ...', 'Enable', 'on');
            this.MaintainMenu('SolverRun', 'Save ...', 'Enable', 'on');
            this.MaintainMenu('SolverRun', 'Delete ...', 'Enable', 'on');
            
            if ~this.validated
                this.MaintainMenu('SolverRun', 'New ...', 'Enable', 'off');
            end
            if isempty(this.id) || this.p_modified
                this.MaintainMenu('SolverRun', 'Open ...', 'Enable', 'off');
                this.MaintainMenu('SolverRun', 'Save ...', 'Enable', 'off');
                this.MaintainMenu('SolverRun', 'Delete ...', 'Enable', 'off');
            end
            if isempty(this.p_solverRun.solution)
                this.MaintainMenu('SolverRun', 'Save ...', 'Enable', 'off');
                this.MaintainMenu('SolverRun', 'Show ...', 'Enable', 'off');
            end
            if this.p_solverRun.saved
                this.MaintainMenu('SolverRun', 'Save ...', 'Enable', 'off');
            end
            if  isempty(this.p_id) || isempty(this.db.select(['select id from SolverRun where paramSetId =' num2str(this.p_id) ])) 
                this.MaintainMenu('SolverRun', 'Open ...', 'Enable', 'off');
                this.MaintainMenu('SolverRun', 'Delete ...', 'Enable', 'off');
            end
        end
        function MaintainVMenu(this, varargin)
            this.MaintainMenu('View', 'Bind measurements ...', 'Enable', 'on');
            if  isempty(this.id) || isempty(this.db.select(['select id from MacSim where arteryNetId =' num2str(this.id) ])) 
                this.MaintainMenu('View', 'Bind measurements ...', 'Enable', 'off');
            end
        end
        
        
        function value = MaintainMenu(this, Menu, MenuCommand, Prop, newValue)
            %% MaintainMenu - get/set property Prop of MenuCommand in Menu
            ch = get(this.hFig, 'Children');
            value = [];
            for i=length(ch):-1:1  % find menu
                type = get(ch(i), 'Type');
                if ~strcmp(type, 'uimenu')
                    continue;
                end
                label = get(ch(i), 'Label');
                if strcmp(label, Menu)
                    commands = get(ch(i), 'Children');
                    for j=length(commands):-1:1  % find command in menu
                        lbl = get(commands(j), 'Label');
                        if strcmp(lbl, MenuCommand)
                            if nargin == 4
                                value = get(commands(j), Prop);
                            else
                                set(commands(j), Prop, newValue);
                            end
                        end
                    end
                end
            end
        end
        
        function Designer(obj, name)
            %% Designer - opens Window, draws structure and allows editing
            %   >> anet.Designer(name);   - opens Window with title 'name', draws the structure and allows editing
            
            if nargin < 2
                name = obj.name;
            end
            scrsz = get(0,'ScreenSize');
            mp = get(0, 'MonitorPositions');
            if size(mp,1) > 1
                scrf = 1.067 * scrsz(3);
            else
                scrf = 0.067 * scrsz(3);
            end
            obj.hFig = figure('NumberTitle', 'off', 'Visible', 'off');
            if verLessThan('matlab', '8.4')
                offs = obj.hFig * 20; 
            else
                offs = obj.hFig.Number*20; 
            end
            obj.setDesignerTitle;
            pos = [scrf + offs, scrsz(4)/16 - offs, 7*scrsz(4)/8, 7*scrsz(4)/8];
            set(obj.hFig, 'Position', pos, 'CloseRequest', @ArteryNet.mCloseRequest);
            
            % construct menus
            hMenu = uimenu(obj.hFig, 'Label', 'ArteryNet', 'Callback', @obj.MaintainAMenu);
            
            obj.hmenuArtery = hMenu;
            uimenu(hMenu, 'Label', 'New',         'Accelerator', 'N', 'Callback', @obj.mANew);
            uimenu(hMenu, 'Label', 'Open ...',    'Accelerator', 'O', 'Callback', @obj.mAOpen);
            uimenu(hMenu, 'Label', 'Save ...',    'Accelerator', 'S', 'Callback', @obj.mASave);
            uimenu(hMenu, 'Label', 'Save as ...', 'Accelerator', 'A', 'Callback', @obj.mASaveAs);
            uimenu(hMenu, 'Label', 'Delete ...',  'Accelerator', 'D', 'Callback', @ArteryNet.mADelete);
            uimenu(hMenu, 'Label', 'Quit',        'Accelerator', 'Q', 'Callback', @ArteryNet.mCloseRequest);
            
            hMenu = uimenu(obj.hFig, 'Label', 'ParameterSet', 'Callback', @obj.MaintainPMenu);
            obj.hmenuParam = hMenu;
            uimenu(hMenu, 'Label', 'Open ...',    'Accelerator', '1', 'Callback', @obj.mPOpen);
            uimenu(hMenu, 'Label', 'Save ...',    'Accelerator', '2', 'Callback', @obj.mPSave);
            uimenu(hMenu, 'Label', 'Save as ...', 'Accelerator', '3', 'Callback', @obj.mPSaveAs);
            uimenu(hMenu, 'Label', 'Delete ...',  'Accelerator', '4', 'Callback', @ArteryNet.mPDelete);
            
            hMenu = uimenu(obj.hFig, 'Label', 'SolverRun', 'Callback', @obj.MaintainSMenu);
            obj.hmenuSolver = hMenu;
            uimenu(hMenu, 'Label', 'New ...',     'Accelerator', '5', 'Callback', @obj.mSNew);
            state = 'on';
            if ~isempty(obj.p_solverRun)
                if isempty(obj.p_solverRun.solution)
                    state = 'off';
                end
            end
            uimenu(hMenu, 'Label', 'Show ...',    'Accelerator', '6', 'Callback', @obj.mSShow, 'Enable', state);
            uimenu(hMenu, 'Label', 'Open ...',    'Accelerator', '7', 'Callback', @obj.mSOpen);
            uimenu(hMenu, 'Label', 'Save ...',    'Accelerator', '8', 'Callback', @obj.mSSave, 'Enable', 'off');
            uimenu(hMenu, 'Label', 'Delete ...',  'Accelerator', '9', 'Callback', @obj.mSDelete);
            
            hMenu = uimenu(obj.hFig, 'Label', 'View', 'Callback', @obj.MaintainVMenu);
            obj.hmVisualize = uimenu(hMenu, 'Label', 'Visualize',  'Accelerator', 'V', 'Callback', @obj.mVVisualize);
            uimenu(hMenu, 'Label', 'Bind measurements ...',  'Accelerator', 'B', 'Callback', @obj.mVBindMacSim);
            uimenu(hMenu, 'Label', 'Show Matlab Bars',  'Accelerator', 'M', 'Callback', @obj.mVShowMatlab);
            
            
            set(obj.hFig, 'WindowScrollWheelFcn', @obj.scale, 'MenuBar','none', 'ToolBar','none');
            obj.hAxes = axes('Xlim', obj.axesRect(1:2), 'Ylim', obj.axesRect(3:4), 'Position', [0 0 1 1]);
            set(obj.hFig, 'CurrentAxes', obj.hAxes, 'UserData', obj);   % set backpointer
            set(obj.hAxes, 'XTick', [], 'YTick', []);
            % initialize parameter editors
            % NetParam
            po = get(obj.hFig, 'Position');
            if isempty(obj.db)
                obj.db = AngioDB;   % open Database connection
            end
            if isempty(obj.db)
                obj.db = AngioDB;
            end
            obj.db.open;
            [obj.hPropEditorNet height] = ParamEditor.InitParamSetNet(obj);
            po1 = get(obj.hPropEditorNet, 'Position');
            po = [3 po(4)-po1(4) po1(3) po1(4)];
            set(obj.hPropEditorNet, 'Visible', 'off', 'Position', po);
            
            % BoundaryParam and NodeParam
            [obj.hPropEditorNode height] = ParamEditor.InitParamSetNode(obj);
            
            po1 = get(obj.hPropEditorNode, 'Position');
            po = [3 po(4)-po1(4) po1(3) po1(4)];
            set(obj.hPropEditorNode, 'Visible', 'off', 'Position', po);
            
            % define context menus for nodes
            obj.hNodeMenu = uicontextmenu;
            uimenu(obj.hNodeMenu, 'Label', 'Add Child', 'Callback', @Node.cmAdd);
            uimenu(obj.hNodeMenu, 'Label', 'Cut Subtree', 'Callback', @Node.cmCut);
            uimenu(obj.hNodeMenu, 'Label', 'Copy Subtree', 'Callback', @Node.cmCopy);
            uimenu(obj.hNodeMenu, 'Label', 'Paste Subtree', 'Callback', @Node.cmPaste, 'Enable', 'off');
            uimenu(obj.hNodeMenu, 'Label', 'Delete Subtree', 'Callback', @Node.cmDeleteSubtree);
            uimenu(obj.hNodeMenu, 'Label', 'Delete Node', 'Callback', @Node.cmDelete);
            
            obj.hRootMenu = uicontextmenu;
            uimenu(obj.hRootMenu, 'Label', 'Visualize', 'Callback', @obj.mVVisualize);
            uimenu(obj.hRootMenu, 'Label', 'Sort', 'Callback', @Node.cmSort);
            uimenu(obj.hRootMenu, 'Label', 'Validate', 'Callback', @Node.cmValidate);
            uimenu(obj.hRootMenu, 'Label', 'Paste Subtree', 'Callback', @Node.cmRootPaste, 'Enable', 'off');
            
            obj.hEdgeMenu = uicontextmenu('Callback', @Node.cmEdgeMenu);
            uimenu(obj.hEdgeMenu, 'Label', 'Split', 'Callback', @Node.cmSplit);
            uimenu(obj.hEdgeMenu, 'Label', 'Copy', 'Callback', @Node.cmECopy);
            uimenu(obj.hEdgeMenu, 'Label', 'Paste', 'Callback', @Node.cmEPaste, 'Enable', 'off');
            
            
            % put tree into figure
            obj.root.addToFigure;
            obj.root.validate(obj);
            set(obj.hFig, 'Visible', 'on', 'DeleteFcn', @deleteFig, 'ResizeFcn', @resize);
%            set(obj.hFig, 'HandleVisibility', 'callback');
             resize; 
            
            function deleteFig(varargin)
                set(obj.hFig, 'UserData', []); 
            end
            
            function resize(varargin)
                %% resize - callback treats resizing of figure while trying to keep aspect
                p = get(obj.hFig, 'Position');
                ratio1 = p(3)/ p(4);
                x = xlim;
                y = ylim;
                ratio2 = (x(2) - x(1))/(y(2)-y(1));
                if x(2)-x(1) > y(2)-y(1)
                    xlim(x * ratio1 / ratio2);
                else
                    ylim(y / ratio1 * ratio2);
                end
                % Property Editor
                if ~isempty(obj.hPropEditorNode)
                    pos = get(obj.hPropEditorNode, 'Position');
                    pos(2) = p(4)- pos(4);
                    set(obj.hPropEditorNode, 'Position', pos, 'Units', 'Pixels');
                    pos = get(obj.hPropEditorNet, 'Position');
                    pos(2) = p(4)- pos(4);
                    set(obj.hPropEditorNet, 'Position', pos, 'Units', 'Pixels');
                end
                
                
            end
            
        end
        function types = getComboTypesFromDb(this, table)
            % retrieves types from Db
            if isempty(this.db)
                this.db = AngioDB;
            end
            this.db.open;
            [~, v] = this.db.select1(['select typeName from ' table, ' order by id']);
            types = v{1};
            for i=2: size(v,1)
                types = [types '|' v{i}]; %#ok<AGROW>
            end
        end
        
        function delete(this)
            if ~isempty(this.db )
                this.db.close;
            end
%             for i = 1:length(this.nodeArr)
%                 this.root = []; 
%                 this.nodeArr(i).parent = [];
%                 this.nodeArr(i).children = [];
%                 this.nodeArr(i).delete;
%             end
            
            %disp ('ArteryNet: delete');
        end
        
        function EditParams(obj, node)
            %% EditParams - shows and hides hPropEditorNode-Panel and maintains node-fields
            %     Parameter: node
            %
            active = get(obj.hPropEditorNode, 'UserData');   % retrieve last calling node
            % hide panels
            if nargin < 2 || nargin > 2 || (nargin == 2 && ~isempty(active) && active == node)
                set(obj.hPropEditorNode, 'Visible', 'off');
                set(obj.hPropEditorNet, 'Visible', 'off');
                if ~isempty(active)
                    set (active.hNode, 'LineWidth', 1);
                    set(obj.hPropEditorNode, 'UserData', []);
                end
                return;
            end
            %show panel
            if ~isempty(active)
                set (active.hNode, 'LineWidth', 1);
            end
            set(obj.hPropEditorNode, 'UserData', node);   % store calling node
            set (node.hNode, 'LineWidth', 2.5);
            
            % init Values and process through callback
            if isempty(node.parent)                                % root
                set(obj.hPropEditorNode, 'Visible', 'off');
                obj.editObject = obj;
                ParamEditor.ReadPropValues(obj);
                set(obj.hPropEditorNet, 'Visible', 'on');
            else
                set(obj.hPropEditorNet, 'Visible', 'off');
                obj.editObject = node;
                if node.isBoundary
                    ParamEditor.ReadPropValues(obj, node, 'boundary');
                else
                    ParamEditor.ReadPropValues(obj, node);
                end
                set(obj.hPropEditorNode, 'Visible', 'on');
            end
        end%             for i = 1:length(this.nodeArr)
%                 this.root = []; 
%                 this.nodeArr(i).parent = [];
%                 this.nodeArr(i).children = [];
%                 this.nodeArr(i).delete;
%             end

        
        function nparam = getNodeParamSet(this, nodeIdx)
            if ~isnumeric(nodeIdx) || nodeIdx < 1 || nodeIdx >= length(this.nodeArr)
                error('ArteryNet.GetNodeParamSet: nodeIdx out of range'); 
            end
            nu = this.n_nu; 
            eta = this.n_eta; 
            node = this.nodeArr(nodeIdx+1); 
            nparam.isroot = nodeIdx == 1; 
            nparam.isboundary = isempty(node.children); 
            nparam.parent = node.parent.nodeId; 
            if isempty(node.children)
                nparam.children = []; 
            else
                nparam.children = [node.children.nodeId]; 
            end
            nparam.rho = this.n_rho; 
            nparam.eta = eta; 
            nparam.nu = nu; 
            nparam.alpha = this.n_alpha; 
            nparam.Eldh = struct('E', node.E, 'l', node.l, 'd', node.d, 'h', node.h); 
            if nparam.isboundary
                nparam.b_R = node.b_R; 
                nparam.b_C = node.b_C; 
                nparam.b_L = node.b_L; 
                nparam.b_p = node.b_p; 
                nparam.b_q = node.b_q; 
            else
                nparam.b_R = 0; 
                nparam.b_C = 0; 
                nparam.b_L = 0; 
                nparam.b_p = 0; 
                nparam.b_q = 0; 
            end
            r = node.d/2;           % radius of vessel
            nparam.Ao=pi*r^2;  % cross sectional area [m^2]
            nparam.co = sqrt( (node.E * node.h) / 2 / nparam.rho / r);
            nparam.R = (8 * pi * nparam.eta * node.l)/ (pi * (node.d/2)^2)^2;
            nparam.L = (nparam.rho * node.l) / (pi * r^2);
%            nparam.C = (2*pi*r^3*node.l) / (node.E * node.h);
            nparam.C = ArteryNet.calcC(r, node.l, node.h, node.E);
            %maxwell
            a = nu([1,3]);
            b = nu([2,4]);
            nparam.C2 = nparam.C * a(1)*a(2)*(b(2)-b(1)) / (b(2)*(b(1)-a(1)) * (a(2)-b(1)));
            nparam.C3 = nparam.C * a(1)*a(2)*(b(2)-b(1)) / (b(1)* (b(2)-a(1)) * (b(2)-a(2)));
            nparam.R2 = 1./b(1)/nparam.C2;
            nparam.R3 = 1./b(2)/nparam.C3;
            % voigt
            nt = 1;             % just to test consistency to elastic case if nt large
            nu = nt*nu(5);    % give nu in Hz
            tc4 = 1/nu;      % give nu in Hz conversion to SI units
            nparam.R4 = tc4 /nparam.C ;
        end
        function pset = getParamSet(this)
            %% getParamSet - returns struct with all relevant parameters of object this
            %   >> pset = anet.getParamSet;
            %        - 'pset' is a struct with fields
            %            .id, .statisticalSetId, .currentSolverRun, .name, .description, .lastmodified
            %            .netParam: [1x1 struct]
            %                .id, .name, .description, .type, .eta, .rho, .gamma, .inboundCtsId
            %            .nodeParam: [1xn struct]
            %                .id, .name, .description, .type, .E, .l, .h, .d
            %            .boundaryParam: [1xm struct]
            %               .id, .nodeId, .name, .decription, .Zr, .Zi, .p, .q
            
            % ParamSet
            pset.id = this.p_id;
            pset.statisticalSetId = this.p_statisticalSetId;
            pset.currentSolverRun = this.p_currentSolverRun;
            pset.name = this.p_name;
            pset.description = this.p_description;
            pset.lastmodified = this.p_lastmodified;
            % NetParam
            pset.netParam.id            = this.n_id;
            pset.netParam.name          = this.n_name;
            pset.netParam.description   = this.n_description;
            pset.netParam.type          = this.n_type;
            pset.netParam.eta           = this.n_eta;
            pset.netParam.rho           = this.n_rho;
            pset.netParam.gamma         = this.n_gamma;
            pset.netParam.alpha         = this.n_alpha;
            pset.netParam.nu            = this.n_nu;
            pset.netParam.inboundCtsId  = this.n_ctsId;
            pset.netParam.heartRate     = this.n_heartRate;
            % NodeParam
            n = this.nodeCount;
            m = this.boundaryCount;
            
            j = 0;
            for i=1:n
                node = [];
                boundary = [];
                node.id = this.nodeArr(i+1).id;
                node.name = this.nodeArr(i+1).name;
                node.description = this.nodeArr(i+1).description;
                node.type = this.nodeArr(i+1).type;
                node.E = this.nodeArr(i+1).E;
                node.l = this.nodeArr(i+1).l;
                node.h = this.nodeArr(i+1).h;
                node.d = this.nodeArr(i+1).d;
                if this.nodeArr(i+1).isBoundary
                    j = j+1;
                    boundary.id = this.nodeArr(i+1).b_id;
                    boundary.nodeId = i;
                    boundary.name = this.nodeArr(i+1).b_name;
                    boundary.description = this.nodeArr(i+1).b_description;
                    boundary.R = this.nodeArr(i+1).b_R;
                    boundary.C = this.nodeArr(i+1).b_C;
                    boundary.L = this.nodeArr(i+1).b_L;
                    boundary.p = this.nodeArr(i+1).b_p;
                    boundary.q = this.nodeArr(i+1).b_q;
                    if j == 1
                        pset.boundaryParam(m) = boundary;
                    end
                    pset.boundaryParam(j) = boundary;
                end
                if i== 1
                    pset.nodeParam(n) = node;
                end
                pset.nodeParam(i) = node;
            end
            
        end
        function setParamSet(this, pset)
            %% setParamSet - sets  paramSet of this
            %    >> anet.setParamSet(pset);
            %        - 'pset' is a struct with fields
            %            .currentSolverRun, .name, .description, .lastmodified
            %            .netParam: [1x1 struct]
            %                .name, .description, .type, .nu, .rho, .gamma, .inboundCtsId
            %            .nodeParam: [1xn struct]
            %                .name, .description, .type, .E, .l, .h, .d
            %            .boundaryParam: [1xm struct]
            %               .name, .decription, .Zr, .Zi, .p, .q
            %        satisfiying
            %             n = anet.nodeCount
            %             m = anet.boundaryCount
            
            if nargin < 1
                error (sprintf('ArteryNet.setParamSet: parameter missing.\n usage: anet.getParamSet(pset)'));
            end
            n = this.nodeCount;
            m = this.boundaryCount;
            if length(pset.nodeParam) ~= n
                error('ArteryNet.setParamSet: dimension of nodeParam array wrong. Structural changes are not permitted.');
            end
            if length(pset.boundaryParam) ~= m
                error('ArteryNet.setParamSet: dimension of boundaryParam array wrong. Structural changes are not permitted.');
            end
            % ParamSet
            this.p_statisticalSetId     = pset.statisticalSetId;   % ???
            this.p_currentSolverRun     = pset.currentSolverRun;
            this.p_name                 = pset.name;
            this.p_description          = pset.description;
            this.p_lastmodified         = pset.lastmodified;
            % NetParam
            this.n_name         = pset.netParam.name;
            this.n_description  = pset.netParam.description;
            this.n_type         = pset.netParam.type ;
            this.n_eta          = pset.netParam.eta ;
            this.n_rho          = pset.netParam.rho ;
            this.n_gamma        = pset.netParam.gamma ;
            this.n_alpha        = pset.netParam.alpha ;
            this.n_nu           = pset.netParam.nu ;
            this.n_ctsId        = pset.netParam.inboundCtsId;
            this.n_heartRate    = pset.netParam.heartRate;
            j = 0;
            % NodeParam
            for i=1:n
                this.nodeArr(i+1).name = pset.nodeParam(i).name ;
                this.nodeArr(i+1).description = pset.nodeParam(i).description;
                this.nodeArr(i+1).type = pset.nodeParam(i).type ;
                this.nodeArr(i+1).E = pset.nodeParam(i).E ;
                this.nodeArr(i+1).l = pset.nodeParam(i).l ;
                this.nodeArr(i+1).h = pset.nodeParam(i).h ;
                this.nodeArr(i+1).d = pset.nodeParam(i).d ;
                if this.nodeArr(i+1).isBoundary
                    j = j+1;
                    this.nodeArr(i+1).b_name        = pset.boundaryParam(j).name;
                    this.nodeArr(i+1).b_description = pset.boundaryParam(j).description;
                    this.nodeArr(i+1).b_R           = pset.boundaryParam(j).R;
                    this.nodeArr(i+1).b_C           = pset.boundaryParam(j).C;
                    this.nodeArr(i+1).b_L           = pset.boundaryParam(j).L;
                    this.nodeArr(i+1).b_p           = pset.boundaryParam(j).p;
                    this.nodeArr(i+1).b_q           = pset.boundaryParam(j).q;
                end
            end
            this.p_modified = true;
        end
        function srun = getSolverRun(this)
            %% getSolverRun - returns a rich set of SolverRun Properties
            %   >> srun = anet.getSolverRun;
            %      - 'srun' is a struct with fields:
            %       .id, .paramSetId, .name, .description, .solver, .rate, .t_end, .starttime, .duration, .relTol,
            %       .absTol, .outMon, .maxNumSteps, .sensMtd, .sensPar, .parScale, .lastmodified, .plist, .showlist, .solvers
            props = {'id', 'paramSetId', 'name', 'description', 'solver', 'rate', 't_end', ...
                'starttime', 'duration', 'relTol', 'absTol', 'outMon', 'maxNumSteps', 'sensMtd', 'sensPar', ...
                'parScale', 'lastmodified', 'plist', 'showlist', 'solvers', 'nonlinear'};
            for i=1:length(props)
                srun.(props{i})= this.p_solverRun.(props{i});
            end
            if isempty(srun.parScale)
                srun.parScale = this.p_solverRun.calcParScale(this);
            end
            if isempty(this.db)
                this.db = AngioDB;
            end
            this.db.open;
            [~, srun.solvers] = this.db.select1(['select typeName from Solvers order by id']);
        end
        
        function solution = getSolution(this)
            %   getSolution  - returns the solution of current SolverRun object
            %    >> solution = anet.getSolverRun;
            solution = this.p_solverRun.solution;
            solution.model = this.n_typeNames{this.n_type}; 
        end
        
        function setSolverRun(this, srun)
            %% setSolverRun - setter for the following SolverRun Properties
            %   >> anet.setSolverRun(srun);
            %      - 'srun' is a struct with fields
            %        .name, .description, .solver, .rate, .t_end, .absTol, .maxNumSteps,
            %        .outMon, .sensMtd, .sensPar, .parScale, .plist, .showlist
            if nargin < 1
                error (sprintf('ArteryNet.setSolverRun: parameter missing.\n usage: anet.setSolverRun(srun)'));
            end
            props = {'name', 'description', 'solver', 'rate', 't_end', ...
                'relTol', 'absTol', 'maxNumSteps', 'outMon', 'sensMtd', 'sensPar', ...
                'parScale', 'plist', 'showlist', 'nonlinear'};
            for i=1:length(props)
                if isfield(srun, props{i})
                    this.p_solverRun.(props{i}) = srun.(props{i});
                end
            end
        end
        function success = RunSolver(this, quiet)
            %% RunSolver - starts a new solver run
            %   >> success = anet.RunSolver;        'success' is boolean
            %   >> success = anet.RunSolver(true);  ' no feedback on commandline
            if nargin < 2
                quiet = false;
            end
            sr = this.p_solverRun;
            sr.solve(this, @cbProgress);
            success = ~isempty(sr.solution);
            if success
                sr.saved = false;
            end
            if ~quiet
                fprintf('\n');
            end
            function  res = cbProgress(varargin)
                res = 1;
                if ~quiet
                    fprintf('|');
                end
            end
            
        end
        function solverRunId = SolverRunSave(this, db, saveAll, useTransaction)
            %% SolverRunSave - saves current Solverrun
            %  >> solverRunId = anet.SolverRunSave(db);                   %  ""
            %  >> solverRunId = anet.SolverRunSave(db, false);            % save SolverRun omit STS
            %  >> solverRunId = anet.SolverRunSave(db, saveAll, false);   % save SolverRun ext. controlled Transaction
            %       - 'db': (optional default = AngioDB) global AngioDB object
            %       - 'saveAll' (optional, default = true) if false, function omits STS, SensTS
            %       - 'useTransaction' (optional, default: = true); if false, function relies on external transaction control
            %       - 'id' id of new SolverRun object in DB
            if nargin < 2
                db = this.db;
            elseif ~isa(db, 'AngioDB')
                error(sprintf('ArteryNet.SolverRunSave: parameter has wrong type\n usage: success = anet.SolverRunSave(db)'));
            end
            if nargin < 3
                saveAll = true;
            end
            if nargin < 4
                useTransaction = true;
            end
            if isempty(this.p_solverRun.solution)
                error('ArteryNet.SolverRunSave: there is no solution to. Execute anet.RunSolver first');
            end
            solverRunId = this.p_solverRun.Save(db, saveAll, useTransaction);
            this.db.update(['update ParamSet set currentSolverRun = ''' num2str(solverRunId) ''' where id=''' num2str(this.p_id) '''']);
            this.p_currentSolverRun = solverRunId;
        end
        
        function sSet = NewStatisticalSet(this, ssName, ssDescription)
            %% NewStatisticalSet - generates a new StatisticalSet object in db, writes a copy of CurrentParamSet as Seed
            %   >> sSet = anet.NewStatisticalSet(ssName, ssDescription);
            %     - 'ssName' sets name of StatisticalSet object
            %     - 'ssDescription' sets description of StatisticalSet object
            
            if nargin < 3
                error ('ArteryNet.NewStatisticalSet: too few Parametes.\nUSAGE:\n%s', help('ArteryNet.NewStatisticalSet'));
            end
            if isempty(this.p_solverRun)
                error ('ArteryNet.NewStatisticalSet: No SolverRun object defined');
            end
            this.db.exec('Start Transaction');
            try
                seedId = this.ParamSetSaveAs(this.db, false, false);  % save Seed ParamSet, no update CurrentArteryNetId, no transaction
                this.RunSolver;
                sid = this.p_solverRun.Save(this.db, true, false);         % save SolverRun with solution, no transaction
                this.db.update(['update ParamSet set currentSolverRun = ''' num2str(sid) ''' where id=''' num2str(seedId) '''']);
                sSet = StatisticalSet('New', seedId, ssName, ssDescription, this);
            catch ex
                this.db.exec('Rollback');
                error ('ArteryNet.NewStatisticalSet: Could not create new StatisticalSet object \n%s', ex.message);
            end
            this.db.exec('Commit');
        end
        function ss = ssModel(this)
            ss = SSModel(this); 
        end
    end
    
    methods (Access=private)
        %% private
        
        function collectParams(this)
            %% collectParams - gethers parameters of current designstate
            % adjacency
            [this.nodeArr this.adjacency this.Z] = this.root.toArr([], [], [], 0);
            sz = size(this.nodeArr,2);
            % design
            try
                this.design = [];
                this.design(sz+4) = 0;    % optimize speed
                x = get(this.hAxes, 'XLim');
                y = get(this.hAxes, 'YLim');
                if isempty(x)
                    x = this.axesRect(1:2);
                    y = this.axesRect(3:4);
                end
                this.design(1:4) = [x y];
                for i=1:sz
                    this.design(2*i+3) = this.nodeArr(i).position(1);
                    this.design(2*i+4) = this.nodeArr(i).position(2);
                end
            catch
            end
            % fluid
        end
        
        function doPSaveAs(this, aid, db)
            %% helper that does all the savingAs work for the current ParamSet of ArteryNet object 'aid'
            %    anet.doPSaveAs(aid);
            %       - 'aid' is the temp_id of ArteryNet anet
            % insert ParamSet
            values = ['values(' ...
                num2str(aid) ',' ...
                num2str(this.p_ctsSetId) ',' ...
                num2str(this.p_statisticalSetId) ',''' ...
                this.p_name ''',''' ...
                this.p_description ''')'];
            res = db.update(['insert into ParamSet (arteryNetId, ctsSetId, statisticalSetId, name, description) ' values]);
            if res < 1; 
                error('ArteryNet.doPSaveAs:  could write ParamSet');
            end;
            res = db.select('select last_insert_id() pid'); % get id of new object
            pid = res.pid;
            this.p_temp_id = pid;
            
            
            % insert NetParam
            values = ['values(' ...
                num2str(pid) ',''' ...
                num2str(this.n_name) ''',''' ...
                num2str(this.n_description) ''',' ...
                num2str(this.n_type) ',' ...
                dbstr(this.n_eta) ',' ...
                dbstr(this.n_rho) ',' ...
                dbstr(this.n_gamma) ',' ...
                dbstr(this.n_heartRate) ',' ...
                dbstr(this.n_ctsId) ')'];
            res = db.update(['insert into NetParam (paramSetId, name, description, type, eta, rho, gamma, heartRate, inboundCtsId) ' values]);
            if res < 1;
                error('ArteryNet.doPSaveAs: could not write NetParam');
            end
            res = db.select('select last_insert_id() nid');
            this.n_temp_id = res.nid;
            if ~isempty(this.n_alpha)
                blob = typecast(this.n_alpha, 'uint8');
                db.update(['update NetParam set alpha=? where id =' num2str(res.nid)], blob);
            end
            if ~isempty(this.n_nu)
                blob = typecast(this.n_nu, 'uint8');
                db.update(['update NetParam set nu=? where id =' num2str(res.nid)], blob);
            end
            
            
            %NodeParam
            for i=2:size(this.nodeArr, 2) % 2, because root is virtual
                values = ['values(''' ...
                    num2str(pid) ''',''' ...
                    num2str(this.nodeArr(i).name) ''',''' ...
                    num2str(this.nodeArr(i).nodeId) ''',''' ...
                    num2str(this.nodeArr(i).description) ''',''' ...
                    num2str(this.nodeArr(i).type) ''',' ...
                    dbstr(this.nodeArr(i).E) ',' ...
                    dbstr(this.nodeArr(i).l) ',' ...
                    dbstr(this.nodeArr(i).h) ',' ...
                    dbstr(this.nodeArr(i).d) ')'];
                res = db.update(['insert into NodeParam (paramSetId, name, nodeId, description, type, E, l, h, d) ' values]);
                if res < 1;
                    error('ArteryNet.doPSaveAs: could not write NodeParam for NodeId = %i', this.nodeArr(i).nodeId);
                end
                res = db.select('select last_insert_id() nid');
                this.nodeArr(i).temp_id = res.nid;
                if this.nodeArr(i).isBoundary
                    values = ['values(''' ...
                        num2str(pid) ''',''' ...
                        num2str(this.nodeArr(i).b_name) ''',''' ...
                        num2str(this.nodeArr(i).nodeId) ''',''' ...
                        num2str(this.nodeArr(i).b_description) ''',' ...
                        dbstr(this.nodeArr(i).b_R) ',' ...
                        dbstr(this.nodeArr(i).b_C) ',' ...
                        dbstr(this.nodeArr(i).b_L) ',' ...
                        dbstr(this.nodeArr(i).b_p) ',' ...
                        dbstr(this.nodeArr(i).b_q) ')'];
                    res = db.update(['insert into BoundaryParam (paramSetId, name, nodeId, description, R, C, L, p, q) ' values]);
                    if res < 1;
                        error('ArteryNet.doPSaveAs: could not write BoundaryParam for NodeId = %i', this.nodeArr(i).nodeId);
                    end
                    res = db.select('select last_insert_id() bid');
                    this.nodeArr(i).b_temp_id = res.bid;
                end
            end
            
            function res = dbstr(val)
                if isempty(val)
                    res = 'Null';
                else
                    res = num2str(val);
                end
            end
            
        end
        
    end
    
    methods (Access = public)
        function Save(this)
            %% Save - save current ArteryNet to DB
            %  not implemented yet;
            error('ToDo: not implemented yet');
            
        end
        
        function SaveAs(this, db1)
            %% SaveAs - save current ArteryNet as new object to db1
            %  PARAMETER:
            %   'db1' (optional) if given, the object is saved to this database
            olddb = this.db;
            if nargin > 1
                this.db = db1;
            end
            try
                this.saveCommonDlg;
            catch e
                this.db = olddb;
                error('ArteryNet.SaveAs: Operation failed!\n%s', e.message);
            end
            this.db = olddb;
        end
        
        function ParamSetSave(this)
            %% ParamSetSave - save current ParamSet to DB
            %  not implemented yet;
            error('ToDo: not implemented yet');
            
        end
        
        function pid = ParamSetSaveAs(this, db, updateCurrent, useTransaction)
            %% ParamSetSaveAs - save current ParamSet as new object to DB
            %    >> pid = anet.ParamSetSaveAs;
            %    >> pid = anet.ParamSetSaveAs(db);
            %    >> pid = anet.ParamSetSaveAs(db, updateCurrent);
            %    >> pid = anet.ParamSetSaveAs(db, updateCurrent, useTransaction);
            %       - 'db' (optional, default: = AngioDB) is an existing AngioDB object
            %       - 'updateCurrent' (optional, default: = true). If false, this.currentParamSetId will not be updated in DB.
            %       - 'useTransaction' (optional, default: = true). If false, function relies on external transaction control
            %       - 'pid': id of the new ParamSet object in db
            
            if nargin < 2
                db = this.db;
            end
            if nargin < 3
                if ~isa(db, 'AngioDB')
                    error('ArteryNet.ParamSetSaveAs: AngioDB object expected\nUSAGE\n%s', help('ArteryNet.ParamSetSaveAs'));
                end
                updateCurrent = true;
            end
            if nargin < 4
                useTransaction = true;
            end
            db.open;
            % initialize variables for transaction
            this.p_temp_id = -1;
            this.n_temp_id = -1;
            for h=1:size(this.nodeArr,2)
                this.nodeArr(h).temp_id = -1;
                this.nodeArr(h).b_temp_id = -1;
            end
            if useTransaction
                db.exec('Start Transaction');
            end
            try
                this.doPSaveAs(this.id, db)    % actual Save operation
                if updateCurrent
                    db.update(['update ArteryNet set currentParamSetId = ''' num2str(this.p_temp_id) ''' where id=''' num2str(this.id) '''']);
                end
                if useTransaction
                    db.exec('Commit');
                end
            catch ex
                if useTransaction
                    db.exec('Rollback');
                end
                error(sprintf('ArteryNet.saveCommonDlg: operation was not successfull (->rollback) \n\n%s', ex.message));
            end
            if this.p_temp_id >-1 && this.n_temp_id >1
                this.p_modified = false;
                % refresh ids
                this.p_id = this.p_temp_id;
                this.n_id = this.n_temp_id;
                this.n_paramSetId = this.p_id;
                if ~isempty(this.p_solverRun)
                    this.p_solverRun.paramSetId = this.p_id; 
                end
                for h=1:size(this.nodeArr,2)
                    this.nodeArr(h).id = this.nodeArr(h).temp_id;
                    this.nodeArr(h).b_id = this.nodeArr(h).b_temp_id;
                    this.nodeArr(h).paramSetId = this.p_id;
                    this.nodeArr(h).b_paramSetId = this.p_id;
                end
                this.p_solverRun.solution = [];
                this.p_solverRun.paramSetId = this.p_id;   % next solutions will be owned by this guy
                pid = this.p_id;
            end
        end
        
        function saveCommonDlg(this, mode, status)
            %% saveCommonDlg - dialog for saving ArteryNet objects to database
            %    - modal dialog
            %    - opens and closes AngioDB
            %  parameters: mode    - title of Dlg, used to distinguish function
            %              status  - status information presented in bottom part of dialog
            persistent types
            commandline = false;
            if nargin == 1;  % treat scripting via Method
                commandline = true;
                cbASaveAs;
                return;
            end
            
            if isempty(this.db)
                this.db = AngioDB;
            end
            this.db.open;
            if isempty(types)
                types = this.getComboTypesFromDb('ArteryNetType');
            end
            
            APanelEnable = 'on';
            statusColor = [0 , 0.6 , 0];
            if strfind(status, 'Warning')
                statusColor = 'r';
            end
            cbSave = @cbASave;
            bSaveAsOffset = 0;
            bSaveText = 'Save to DB';
            if strfind(mode, 'ArteryNet: Save as')
                bSaveText = 'Save ArteryNet as new to DB';
                bSaveAsOffset = 50;
                cbSave=@cbASaveAs;
            elseif strfind(mode, 'ParamSet: Save as')
                APanelEnable = 'off';
                bSaveText = 'Save ParamSet as new to DB';
                bSaveAsOffset = 50;
                cbSave=@cbPSaveAs;
            elseif strfind(mode, 'ParamSet: Save')
                APanelEnable = 'off';
                cbSave=@cbPSave;
            end
            
            % figure
            sz = get(0,'ScreenSize');
            mp = get(0, 'MonitorPositions');
            if size(mp,1) > 1
                scrf = sz(3) + (sz(3)-1000)/2;
            else
                scrf = (sz(3)-1000)/2;
            end
            
            position = [scrf, (sz(4)-600)/2, 1000, 600];
            hDlg = figure('Name', mode, 'NumberTitle', 'off', 'MenuBar','none', 'ToolBar','none', ...
                'Color', 'w', 'Position', position, 'Resize', 'off', 'WindowStyle', 'normal', ...
                'CloseRequestFcn', @cbCancel, 'UserData', this);
            
            % panel titles
            uicontrol(hDlg, 'Style', 'Text', 'FontSize', 14, 'HorizontalAlignment', 'left', 'String', 'ArteryNet', ...
                'Position', [20 570 300, 30], 'BackgroundColor', 'w');
            uicontrol(hDlg, 'Style', 'Text', 'FontSize', 14, 'String', 'ParamSet', ...
                'Position', [510 570 300, 30], 'BackgroundColor', 'w', 'HorizontalAlignment', 'left');
            
            % status text
            hstatus = uicontrol(hDlg, 'Style', 'text', 'FontSize', 10, 'ForegroundColor', statusColor, 'String', status, ...
                'Position', [20 20 720, 22], 'HorizontalAlignment', 'left');
            
            % buttons
            save = uicontrol(hDlg, 'Style', 'Pushbutton', 'FontSize', 8, 'HorizontalAlignment', 'right', 'String', bSaveText, ...
                'Position', [800-bSaveAsOffset 20 110+bSaveAsOffset, 30], 'Callback', cbSave, 'KeyPressFcn', @cbkeySave);
            cancel = uicontrol(hDlg, 'Style', 'Pushbutton', 'FontSize', 8, 'HorizontalAlignment', 'right', 'String', 'Cancel', ...
                'Position', [920 20 60, 30], 'Callback', @cbCancel, 'KeyPressFcn', @cbkeyCancel);
            
            
            % Panels
            hAPanel = uipanel('Parent', hDlg, 'Title', '', 'Units', 'Pixels', 'Position', [20 60 470, 500], 'BackgroundColor', [0.95, 0.95, 0.95], 'BorderType', 'none');
            hPPanel = uipanel('Parent', hDlg, 'Title', '', 'Units', 'Pixels', 'Position', [510 60 470, 500], 'BackgroundColor', [0.95, 0.95, 0.95], 'BorderType', 'none');
            
            % static text
            createTextCtrl(hAPanel, 'id (auto)', [20 460 110, 22]);
            createTextCtrl(hAPanel, 'name', [20 420 110, 22]);
            createTextCtrl(hAPanel, 'type', [20 380 110, 22]);
            createTextCtrl(hAPanel, 'currentParameterSetId (auto)', [20 330 110, 32]);
            createTextCtrl(hAPanel, 'description', [20 300 110, 22]);
            
            createTextCtrl(hPPanel, 'id (auto)', [20 460 110, 22]);
            createTextCtrl(hPPanel, 'arteryNetId (auto)', [20 420 110, 22]);
            createTextCtrl(hPPanel, 'name', [20 380 110, 22]);
            createTextCtrl(hPPanel, 'lastmodified (auto)', [20 330 110, 32]);
            createTextCtrl(hPPanel, 'Description', [20 300 110, 22]);
            
            % edit controls
            createInputCtrl(hAPanel, num2str(this.id), [140 460 300, 30], 1, 'off','');
            createInputCtrl(hAPanel, this.name, [140 420 300, 30], 1, APanelEnable, {@processInput, 'name', 'String', 'modified'});
            uicontrol(hAPanel, 'Style', 'popupmenu', 'FontSize', 8, 'String', types, 'Value', this.type, ...
                'Position', [140 380 300, 22], 'BackgroundColor', 'w', 'Enable', APanelEnable, 'Callback',  {@processInput, 'type', 'Value', 'modified'});
            createInputCtrl(hAPanel, num2str(this.currentParamSetId), [140 340 300, 30], 1, 'off', '');
            createInputCtrl(hAPanel, this.description, [140 20 300, 310], 2, APanelEnable, {@processInput, 'description', 'String', 'modified'});
            
            createInputCtrl(hPPanel, num2str(this.p_id), [140 460 300, 30], 1, 'off','');
            createInputCtrl(hPPanel, num2str(this.id), [140 420 300, 30], 1, 'off', '');
            createInputCtrl(hPPanel, num2str(this.p_name), [140 380 300, 22], 1, 'on', {@processInput, 'p_name', 'String', 'p_modified'});
            createInputCtrl(hPPanel, this.p_lastmodified, [140 340 300, 30], 1, 'off', '');
            createInputCtrl(hPPanel, this.p_description, [140 20 300, 310], 2, 'on', {@processInput, 'p_description', 'String', 'p_modified'});
            
            if strfind(mode, ' as')
                uicontrol(cancel);
            else
                uicontrol(save);
            end
            
            % initialize variables for transaction
            transactionstarted = false;
            aid = this.id;
            pid = this.p_id;
            this.temp_id = -1;    % ArteryNet.id
            this.p_temp_id = -1;  % ParamSet.id
            this.n_temp_id = -1;  % NetParam.id
            for h=1:size(this.nodeArr,2)
                this.nodeArr(h).temp_id = -1;    % NodeParam.id
                this.nodeArr(h).b_temp_id = -1;  % BoundaryParam.id
            end
            uiwait(hDlg);   % wait for user input
            if ~transactionstarted % on error or cancel just return
                return;
            end
            
            % getting here means, there was a successful transaction
            this.db.exec('Commit');
            % commit also changes in the state ... 
            if this.temp_id >-1         % ArteryNet.id
                this.modified = false;
                this.id = this.temp_id;
            end
            if this.p_temp_id >-1 && this.n_temp_id >-1       % new ParamSet.id NetParam.id
                this.p_modified = false;
                this.p_id = this.p_temp_id;
                this.currentParamSetId = this.p_id;
                this.n_id = this.n_temp_id;
                this.n_paramSetId = this.p_id;
                % SolverRun object
                if ~isempty(this.p_solverRun)
                    this.p_solverRun.paramSetId = this.p_id; 
                    this.p_solverRun.solution = [];
                end

                for h=1:size(this.nodeArr,2)     % NodeParam.id
                    if this.nodeArr(h).temp_id > -1
                        this.nodeArr(h).id = this.nodeArr(h).temp_id;
                        this.nodeArr(h).paramSetId = this.p_id;
                    end
                    if this.nodeArr(h).b_temp_id > -1;    % BoundaryParam.id
                        this.nodeArr(h).b_id = this.nodeArr(h).b_temp_id;
                        this.nodeArr(h).b_paramSetId = this.p_id;
                    end
                end
            end
            this.setDesignerTitle;
            
            %% functions
            function processInput(obj, ~, field, prop, mod)
                %% callback for inputfields
                %   - writes value of uicontrol property 'prop' into field 'field' of object
                %     and sets field 'modified' of object to true
                %   - expects obj handle in 'UserData' of current figure
                % parameter:   obj - handle for uicontrol
                %                e - unused
                %            field - destination field of new value
                %             prop - source property of new value
                %              mod - modified field set true on change
                
                this = get(gcf, 'UserData');
                val = get(obj, prop);
                if ischar(val)
                    if ~strcmp(val, this.(field))
                        this.(mod) = true;
                        fullval = deblank(val(1,:));
                        for i=2:size(val, 1)       % convert multiline input into single string
                            fullval = [fullval char(10) deblank(val(i,:))];  %#ok<AGROW>
                        end
                        this.(field) = fullval;
                    end
                else
                    if val ~= this.(field)
                        this.(mod) = true;
                        this.(field) = val;
                    end
                end
            end
            function handle = createTextCtrl(hParent, text, position)
                handle = uicontrol(hParent, 'Style', 'text', 'HorizontalAlignment', 'right', 'String', [text ' '], 'Position', position, ...
                    'BackgroundColor', [0.95, 0.95, 0.95]);
            end
            function handle = createInputCtrl(hParent, text, position, max, Enable, cb)
                handle = uicontrol(hParent, 'Style', 'edit', 'HorizontalAlignment', 'left', 'String', text, 'Position', position, ...
                    'BackgroundColor', 'w', 'Max', max, 'Enable', Enable, 'Callback', cb);
            end
            
            %% callbacks
            function cbkeyCancel(hObj, eventdata, ~)
                if strcmp(eventdata.Key,'return')
                    cbCancel(hObj);
                end
            end
            function cbkeySave(hObj, eventdata, ~)
                if strcmp(eventdata.Key,'return')
                    cbSave(hObj);
                end
            end
            function cbASave(varargin)
                %% cbASave - callback, handles "Save" button of 'ArteryNet: Save' dialog
                %   - updates design state of object in table ArteryNet
                %   - updates paramSet state of object if paramSet was modified
                %   - maintains fields this.modified, this.p_modified
                %   - destroys figure, without further action
                this.db.open;
                this.db.exec('Start Transaction');
                transactionstarted = true;
                try
                    d = typecast(this.design, 'uint8');
                    a = typecast(this.adjacency, 'uint8');
                    values = ['Update ArteryNet set name=''' this.name ''',' ...
                        'description=''' this.description ''',' ...
                        'currentParamSetId=' dbstr(this.currentParamSetId) ',' ...
                        'type=' dbstr(this.type) ',' ...
                        'design = ? , adjacency = ? where id = ' dbstr(this.id)];
                    res = this.db.update(values, d, a);
                    err(res, 'ArteryNet', 'cbASave');
                    
                    this.temp_id = this.id;
                    if this.p_modified
                        if isempty(this.p_id)
                            cbPSaveAs;
                        else
                            cbPSave;
                        end
                        return;
                    end
                    if ~commandline
                        delete(gcf);
                    end
                catch ex
                    traperror(ex);
                end
            end
            function cbASaveAs(varargin)
                %% cbASaveAs - callback, handles "Save As new to DB" button of 'ArteryNet: Save as' dialog
                %   - inserts state of object as new row into table ArteryNet
                %   - inserts current ParamSet of object as new ParamSet
                %   - maintains fields this.modified, this.p_modified
                %   - destroys figure, without further action
                this.db.open;
                this.db.exec('Start Transaction');
                transactionstarted = true;
                try
                    d = typecast(this.design, 'uint8');
                    a = typecast(this.adjacency, 'uint8');
                    values = ['values(''' ...
                        this.name ''',''' ...
                        this.description ''',''' ...
                        num2str(this.type) ''', ?, ?)'];
                    res = this.db.update(['insert into ArteryNet (name, description, type, design, adjacency) ' values], d, a);
                    err(res , 'ArteryNet', 'cbASaveAs');
                    res = this.db.select('select last_insert_id() aid');
                    aid = res.aid;
                    this.temp_id = aid;
                    cbPSaveAs;
                catch ex
                    traperror(ex);
                end
            end
            function cbPSave(varargin)
                %% cbPSave - callback, handles Save button of Save dialog
                %   destroys figure, without further action
                PSave;
                if ~commandline
                    delete(gcf);
                end
            end
            function cbPSaveAs(varargin)
                %% cbPSaveAs - callback, handles Save as button of Save dialog
                %
                PSaveAs;
                if ~commandline
                    delete(gcf);
                end
            end
            function PSave
                %% SaveParams - helper. does all the saving work
                % if bSaveAs is true, function creates new ids
                if ~transactionstarted
                    this.db.open;
                    this.db.exec('Start Transaction');
                    transactionstarted = true;
                end
                
                try
                    % update ParamSet
                    pid = this.p_id;
                    this.p_currentSolverRun = 0;  % all SolverRuns are deleted later!
                    command = ['Update ParamSet SET arteryNetId=' dbstr(aid) ',' ...
                        'ctsSetId=' dbstr(this.p_ctsSetId) ',' ...
                        'currentSolverRun=' dbstr(this.p_currentSolverRun) ',' ...
                        'name=''' this.p_name ''',' ...
                        'description=''' this.p_description ''' ' ...
                        'where id = ' dbstr(pid)];
                    res = this.db.update(command);
                    err(res , 'ParamSet.update', 'PSave');
                    
                    this.p_temp_id = this.p_id;
                    
                    % delete SolverRun
                    this.db.exec(['delete from SolverRun where paramSetId = ' num2str(pid)]);
                    
                    % update NetParam
                    command = ['Update NetParam SET '...
                        'paramSetId=' dbstr(pid) ',' ...
                        'name=''' this.n_name ''',' ...
                        'description=''' this.n_description ''', ' ...
                        'type=' dbstr(this.n_type) ', ' ...
                        'eta=' dbstr(this.n_eta) ', ' ...
                        'rho=' dbstr(this.n_rho) ', ' ...
                        'gamma=' dbstr(this.n_gamma) ', ' ...
                        'heartRate=' dbstr(this.n_heartRate) ', ' ...
                        'alpha=Null, ' ...
                        'nu=Null, '...
                        'inBoundCtsId=' dbstr(this.n_ctsId) ' '...
                        'where id = ' dbstr(this.n_id)];
                    res = this.db.update(command);
                    err(res, 'NetParam', 'PSave');
                    % write the two blobs
                    if ~isempty(this.n_alpha)
                        blob = typecast(this.n_alpha, 'uint8');
                        res = this.db.update(['update NetParam set alpha=? where id =' num2str(this.n_id)], blob);
                        err(res, 'NetParam.alpha', 'PSave');
                    end
                    if ~isempty(this.n_nu)
                        blob = typecast(this.n_nu, 'uint8');
                        res = this.db.update(['update NetParam set nu=? where id =' num2str(this.n_id)], blob);
                        err(res, 'NetParam.nu', 'PSave');
                    end
                    this.n_temp_id = this.n_id;
                    
                    % update NodeParam and BoundaryParam
                    for i=2:size(this.nodeArr, 2) % 2, because root is virtual
                        command = ['Update NodeParam SET '...
                            'paramSetId=' dbstr(pid) ',' ...
                            'name=''' this.nodeArr(i).name ''',' ...
                            'nodeId=' dbstr(this.nodeArr(i).nodeId) ',' ...
                            'description=''' this.nodeArr(i).description ''', ' ...
                            'type=' dbstr(this.nodeArr(i).type) ', ' ...
                            'E=' dbstr(this.nodeArr(i).E) ', '...
                            'l=' dbstr(this.nodeArr(i).l) ', '...
                            'h=' dbstr(this.nodeArr(i).h) ', '...
                            'd=' dbstr(this.nodeArr(i).d) ' '...
                            'where id = ' num2str(this.nodeArr(i).id)];
                        res = this.db.update(command);
                        err(res, 'NodeParam', 'PSave');
                        
                        this.nodeArr(i).temp_id = this.nodeArr(i).id;
                        
                        if this.nodeArr(i).isBoundary
                            command = ['Update BoundaryParam SET '...
                                'paramSetId=' dbstr(pid) ',' ...
                                'name=''' this.nodeArr(i).b_name ''',' ...
                                'nodeId=' dbstr(this.nodeArr(i).nodeId) ',' ...
                                'description=''' this.nodeArr(i).b_description ''', ' ...
                                'R=' dbstr(this.nodeArr(i).b_R) ', '...
                                'C=' dbstr(this.nodeArr(i).b_C) ', '...
                                'L=' dbstr(this.nodeArr(i).b_L) ', '...
                                'p=' dbstr(this.nodeArr(i).b_p) ', '...
                                'q=' dbstr(this.nodeArr(i).b_q) ' '...
                                'where id = ' num2str(this.nodeArr(i).b_id)];
                            res = this.db.update(command);
                            err(res, 'BoundaryParam', 'PSave');
                            this.nodeArr(i).b_temp_id = this.nodeArr(i).b_id;
                        end
                    end
                catch ex
                    traperror(ex);
                end
            end
            
            function PSaveAs
                %% PSaveAs - helper. does all the saving work
                if ~ transactionstarted
                    this.db.open;
                    this.db.exec('Start Transaction');
                    transactionstarted = true;
                end
                try
                    this.p_temp_id = -1;
                    this.n_temp_id = -1;
                    
                    % insert ParamSet
                    values = ['values(' ...
                        dbstr(aid) ',' ...
                        dbstr(this.p_ctsSetId) ',' ...
                        dbstr(this.p_currentSolverRun) ',''' ...
                        this.p_name ''',''' ...
                        this.p_description ''')'];
                    res = this.db.update(['insert into ParamSet (arteryNetId, ctsSetId, currentSolverRun, name, description) ' values]);
                    err(res, 'ParamSet', 'PSaveAs');
                    res = this.db.select('select last_insert_id() pid');
                    pid = res.pid;
                    this.p_temp_id = pid;
                    res = this.db.update(['update ArteryNet set currentParamSetId= ''' num2str(pid) ''' where id=''' num2str(aid) '''']);
                    err(res, 'ArteryNet', 'PSaveAs');
                    
                    % insert NetParam
                    values = ['values(''' ...
                        num2str(pid) ''',''' ...
                        num2str(this.n_name) ''',''' ...
                        num2str(this.n_description) ''',''' ...
                        num2str(this.n_type) ''',' ...
                        dbstr(this.n_eta) ',' ...
                        dbstr(this.n_rho) ',' ...
                        dbstr(this.n_gamma) ',' ...
                        dbstr(this.n_heartRate) ',' ...
                        dbstr(this.n_ctsId) ')'];
                    res = this.db.update(['insert into NetParam (paramSetId, name, description, type, eta, rho, gamma, heartRate, inboundCtsId) ' values]);
                    err(res, 'NetParam', 'PSaveAs');
                    res = this.db.select('select last_insert_id() nid');
                    this.n_temp_id = res.nid;
                    % write the two blobs
                    if ~isempty(this.n_alpha)
                        blob = typecast(this.n_alpha, 'uint8');
                        res = this.db.update(['update NetParam set alpha=? where id =' num2str(this.n_temp_id)], blob);
                        err(res, 'NetParam.alpha', 'PSaveAs');
                    end
                    if ~isempty(this.n_nu)
                        blob = typecast(this.n_nu, 'uint8');
                        res = this.db.update(['update NetParam set nu=? where id =' num2str(this.n_temp_id)], blob);
                        err(res, 'NetParam.nu', 'PSaveAs');
                    end
                    
                    %NodeParam
                    for i=2:size(this.nodeArr, 2) % 2, because root is virtual
                        values = ['values(''' ...
                            num2str(pid) ''',''' ...
                            num2str(this.nodeArr(i).name) ''',''' ...
                            num2str(this.nodeArr(i).nodeId) ''',''' ...
                            num2str(this.nodeArr(i).description) ''',''' ...
                            num2str(this.nodeArr(i).type) ''',' ...
                            dbstr(this.nodeArr(i).E) ',' ...
                            dbstr(this.nodeArr(i).l) ',' ...
                            dbstr(this.nodeArr(i).h) ',' ...
                            dbstr(this.nodeArr(i).d) ')'];
                        res = this.db.update(['insert into NodeParam (paramSetId, name, nodeId, description, type, E, l, h, d) ' values]);
                        err(res, 'NodeParam', 'PSaveAs');
                        res = this.db.select('select last_insert_id() nid');
                        this.nodeArr(i).temp_id = res.nid;
                        if this.nodeArr(i).isBoundary
                            values = ['values(''' ...
                                num2str(pid) ''',''' ...
                                num2str(this.nodeArr(i).b_name) ''',''' ...
                                num2str(this.nodeArr(i).nodeId) ''',''' ...
                                num2str(this.nodeArr(i).b_description) ''',' ...
                                dbstr(this.nodeArr(i).b_R) ',' ...
                                dbstr(this.nodeArr(i).b_C) ',' ...
                                dbstr(this.nodeArr(i).b_L) ',' ...
                                dbstr(this.nodeArr(i).b_p) ',' ...
                                dbstr(this.nodeArr(i).b_q) ')'];
                            res = this.db.update(['insert into BoundaryParam (paramSetId, name, nodeId, description, R, C, L, p, q) ' values]);
                            err(res, 'BoundaryParam', 'PSaveAs');
                            res = this.db.select('select last_insert_id() bid');
                            this.nodeArr(i).b_temp_id = res.bid;
                        end
                    end
                catch ex
                    traperror(ex);
                end
            end
            
            function res = dbstr(val)
                if isempty(val)
                    res = 'Null';
                else
                    res = num2str(val);
                end
            end
            
            function traperror(ex)
                if transactionstarted
                    this.db.exec('Rollback');
                    transactionstarted = false;
                end
                set(hstatus, 'String', 'Operation failed! For more information note console output', 'ForegroundColor', 'k', 'BackgroundColor', 'r'); 
                fprintf('%s\n', ex.message);
            end
            
            function err(res, table, context)
                % err - tests for error, does rollback and throws error
                if nargin < 3
                    context = '';
                end
                if res<1
                    this.db.exec('Rollback');
                    transactionstarted = false;
                    error('ArteryNet: Save operation %s to Table ''%s'' was not successful, (->rollback)\n', context, table);
                end
            end
            
            function cbCancel(varargin)
                %% aSaveDlgCancel - callback, handles Cancel button of Open dialog
                %   destroys figure, without further action
                delete(gcf);
            end
        end
        
        function co_plot(this, node)
        %% co_plot - plots wave velocities from root to node     
            co = this.co; 
            path = sort(this.rootChannel(node)); 
            figure; plot(co(path), 'bo'); 
            title(sprintf('ArteryNet(%i): %s', this.id, this.name));  
            xlabel(sprintf('rootChannel(%i)',node)); 
            ylabel('v [m/s]'); 
        end
        
        function c = co(this, select)
        %%  co - calculates 0D wave velocity along Cap/Capova
        %   'select' (optional) vector of node ids, default: all nodes
        %    'c'  wave velocity 
            period = this.CtsIn.period;  % 1/freq
            len = [this.getParamSet.nodeParam.l]; 
            rcl = this.RCL; 
            omega = 2 * pi / period; 
            phase2time = period/pi/2;    % 1/omega 
            for p = this.nodeCount:-1:1

                     ZL = rcl.R(p) + (1i * omega) * rcl.L(p); 
                     YT = (1i * omega) * rcl.C(p); 

                     K = sqrt(ZL*YT); 
                     ptt = imag(K)  * phase2time; 
                     c(p) = len(p)/ptt; 
            end
            if nargin > 1
                c = c(select); 
            end
        end
        
        function ptt = PTT(this)
        % PTT - calculate pulse transit times for all nodes
            l = [this.getParamSet.nodeParam.l]; 
            co = this.co; 
%            co = this.RCL.co; 
            t = l./co; 
            for k = this.nodeCount:-1:1 
                ch = this.rootChannel(k); 
                ptt(k) = sum(t(ch)); 
            end
        end
        function RCL = RCL(this)
            %% RCL - evaluates windkessel model parameters from structural and physiological parameters
            %    >> RCL = anet.RCL;
            %           anet.n_type == 1 <-'kasb'    parameter analogy after K Al Zoukra S. Bernhard
            %           anet.n_type == 2 <-'sher'    parameter analogy after Sherwin
            %           anet.n_type == 3 <-'john'    parameter analogy after John
            %     - 'RCL' is a struct with fields
            %            .R, .C., .L  - the RCL model parameters,
            %            .a0          - the cross-section Ao [m^2],
            %            .co          - the pulse wave velocity co [m/s] in the model segments
            
            % ensure to mirror actual design state
            [this.nodeArr this.adjacency] = this.root.toArr([], [], [], 0);
            
            n = length(this.nodeArr)-1;                  % root node is ignored
            % initialize arrays
            Ao(n) = 0;
            co(n) = 0;
            R(n) = 0;
            L(n) = 0;
            C(n) = 0;
            b_R(n) = 0; 
            b_C(n) = 0; 
            
            switch this.n_type
                case {1 4} % linear elastic
                    for i=2:n+1                           % number of segments
                        AocoRCL();
                    end
                    RCL=struct('R',R,'C',C,'L',L, 'b_R', b_R, 'b_C', b_C, 'Ao', Ao, 'co',co);
                    
                case {2 5} % linear visco-elastic maxwell
                    nt = 1;             % just to test consistency to elastic case if nt large
                    nu=nt*this.n_nu;    % give nu in Hz
                    
                    %maxwell
                    C2(n) = 0;
                    C3(n) = 0;
                    R2(n) = 0;
                    R3(n) = 0;
                    tc2(n) = 0;
                    tc3(n) = 0;
                    a = nu([1,3]);
                    b = nu([2,4]);
                    
                    c2fac = a(1)*a(2)*(b(2)-b(1)) / (b(2)*(b(1)-a(1)) * (a(2)-b(1)));
                    c3fac = a(1)*a(2)*(b(2)-b(1)) / (b(1)* (b(2)-a(1)) * (b(2)-a(2)));
                    
                    for i=2:n+1                           % number of segments
                        AocoRCL();
                        
                        % maxwell
                        C2(i-1)=C(i-1)*c2fac;
                        C3(i-1)=C(i-1)*c3fac;
                        R2(i-1)=1./b(1)/C2(i-1);
                        R3(i-1)=1./b(2)/C3(i-1);
                        tc2(i-1)=C2(i-1)*R2(i-1);        % time constant in s    % 160 ms
                        tc3(i-1)=C3(i-1)*R3(i-1);        % time constant in s    % 14 ms
                    end
                    RCL=struct('R', R, 'C', C, 'L', L, 'R2' ,R2, 'C2', C2, 'R3', R3, 'C3', C3, 'b_R', b_R, 'b_C', b_C, 'Ao', Ao, 'co', co);
                    
                case {3 6} % linear visco-elastic voigt
                    nt = 1;             % just to test consistency to elastic case if nt large
                    nu=nt*this.n_nu;    % give nu in Hz
                    
                    % voigt
                    tc4 = 1/nu(5);      % give nu in Hz conversion to SI units
                    R4(n) = 0;          % voigt resistance values
                    
                    for i=2:n+1                           % number of segments
                        
                        AocoRCL();
                        
                        % voigt
                        R4(i-1)=tc4/C(i-1);
                    end
                    RCL=struct('R', R, 'C', C, 'L', L, 'R4', R4, 'b_R', b_R, 'b_C', b_C, 'Ao', Ao, 'co', co);
                    
                otherwise 
                    error('ArteryNet.RCL: Not implemented type = %i', this.n_type);
            end
            
            function AocoRCL()
                r = this.nodeArr(i).d/2;           % radius of vessel
                Ao(i-1)=pi*r^2;  % cross sectional area [m^2]
                
                % co = sqrt( (E*h)/2/rho/r )     = wave velocity [m/s]
                co(i-1)=sqrt( (this.nodeArr(i).E * this.nodeArr(i).h) / (2 * this.n_rho * r));
                % mathematical equivalent expression, but different results! numerical reasons?
                % co = sqrt( (E*h)/(2*rho*r) )     = wave velocity [m/s]
                % co(i-1)=sqrt( (this.nodeArr(i).E * this.nodeArr(i).h) / ( 2 * this.n_rho * r));
                
                % R = (8*pi*nu*l)/(r^2*pi)^2   = arterial resistance [kg s^-1 ns^-4]
                R(i-1)=(8 * pi * this.n_eta*this.nodeArr(i).l)/ Ao(i-1)^2;
                
                % L = (rho*l) / (r^2*pi)       = arterial inductance [kg s^-4]
                L(i-1) = (this.n_rho * this.nodeArr(i).l) / Ao(i-1);
                
                % C = (2pi*r^3 * l) / (E*h)        = arterial compliance [s^4 s kg^-1]
%                C(i-1) = (2*pi*r^3*this.nodeArr(i).l) / (this.nodeArr(i).E * this.nodeArr(i).h);
                 C(i-1) = ArteryNet.calcC(r, this.nodeArr(i).l, this.nodeArr(i).h, this.nodeArr(i).E); 
                
                if this.nodeArr(i).isBoundary
                    if ~isempty(this.nodeArr(i).b_R)
                        b_R(i-1) = this.nodeArr(i).b_R;
                    else
                        b_R(i-1) = 0; 
                    end
                    if ~isempty(this.nodeArr(i).b_C)
                        b_C(i-1) = this.nodeArr(i).b_C;
                    else
                        b_C(i-1) = 0; 
                    end
                end

            end
        end
        
        function UpdateNodeRCL(this, node)
                if isempty(node.d)
                    return; 
                end
                r = node.d/2;           % radius of vessel
                Ao=pi*r^2;  % cross sectional area [m^2]
                node.R  = (8 * pi * this.n_eta * node.l)/ Ao^2;
                node.L  = (this.n_rho * node.l) / Ao;
%                node.C  = (2*pi*r^3*node.l) / (node.E * node.h);
                node.C = ArteryNet.calcC(r, node.l, node.h, node.E);
        end

        function str = dump(this, I, fn)
            %% dump - generates a printable dump with all net and all/dedicated node parameters, optional into file
            %     >> anet.dump         % full dump to console
            %     >> str = anet.dump;  % full dump to variable
            %     >> anet.dump(fn);    % full dump to file
            %     >> anet.dump(I, fn); % dump nodes indexed by I to file fn
            %     >> anet.dump(I)      % dump Nodes indexed by I to console
            % PARAMETER: 
            %     'fn'   valid filename for target of dump. Content is overwritten!
            %     'I'    indices of nodes to be dumped
            %     'str'  cell arr containing lines of report
            N = this.nodeCount; 
            if nargin < 2
                I = 1:N;  % full dump
                fn = []; 
            end
            if ischar(I)
                fn = I; 
                I = 1:N;  % full dump
            else
                if nargin == 2
                    fn = []; 
                end
            end
            ps = this.getParamSet; 
            rcl = this.RCL; 
            str = netdump; 
            len = length(str); 
            warn(length(I)) = 0;
            wi = 1; 
            for j = 1:length(I) 
                if I(j)>N
                    warn(wi) = I(j); 
                    wi = wi+1; 
                    continue; 
                end
                strnode = nodedump(I(j)); 
                lennode = length(strnode); 
                str(len+1:len+lennode) = strnode; 
                len = len+lennode; 
            end
            if wi > 1
               warning('not existing node(s) %s skipped', sprintf('%i,', warn(1:wi-1)));
            end

            
            if ~isempty(fn)
                f = fopen(fn, 'w'); 
                fprintf(f, '%s\n', str{1:end}); 
                fclose(f); 
            end
                
            
            function str = netdump
              str = {
                 '========================================================'
                 this.title
                 sprintf('dumping nodes: %s', sprintf('%i,', I))
                 '========================================================'
                 'ArteryNet:'
                 sprintf('   name, id         =''%s'', %i', this.name, this.id)
                 sprintf('   description      =''%s''', this.description)
                 sprintf('   nodes            =%i', this.nodeCount)
                 sprintf('   type             =%i', this.type)
                 sprintf('   currentParamSetID=%i', this.currentParamSetId)
                 sprintf('   currentSolverRun =%i', this.p_currentSolverRun)
                 'ParamSet:'
                 sprintf('   name, id         =''%s'', %i', this.p_name, this.p_id)
                 sprintf('   description      =''%s''', this.p_description)
                 sprintf('   lastmodified     =%s', this.p_lastmodified)
                 sprintf('   statisticalSetId =%i', this.p_statisticalSetId)
                 sprintf('   currentPD =%i', this.currentParamSetId)
                 'NetParam:'
                 sprintf('   name, id         =''%s'', %i', this.n_name, this.n_id)
                 sprintf('   description      =''%s''', this.n_description)
                 sprintf('   type, typeName   =%i, ''%s''', this.n_type, this.n_typeNames{this.n_type})
                 sprintf('   eta, rho, gamma  =%f, %f, %f', this.n_eta, this.n_rho, this.n_gamma)
                 sprintf('   alpha            =%s', sprintf('%f, ', this.n_alpha))
                 sprintf('   nu               =%s', sprintf('%f, ', this.n_nu))
                 sprintf('   ctsId            =%i', this.n_ctsId)
                 sprintf('   heart rate       =%f', this.n_heartRate)
                 };
            end
            function str = nodedump(i)
              str = {
                 sprintf('NodeParam %i:', i) 
                 sprintf('   name, id         =''%s'', %i', ps.nodeParam(i).name, ps.nodeParam(i).id)
                 sprintf('   description      =''%s''', ps.nodeParam(i).description)
                 sprintf('   type             =%i', ps.nodeParam(i).type)
                 sprintf('   E, E*h           =%e, %e', ps.nodeParam(i).E, ps.nodeParam(i).E*ps.nodeParam(i).h)
                 sprintf('   l [m]            =%f', ps.nodeParam(i).l)
                 sprintf('   d [m]            =%f', ps.nodeParam(i).d)
                 sprintf('   h [m]            =%f', ps.nodeParam(i).h)
                 sprintf('   R [Pa*s/m�]      =%e', rcl.R(i))
                 sprintf('   C [m�/Pa]        =%e', rcl.C(i))
                 sprintf('   L [Pa*s/m�]      =%e', rcl.L(i))
                 sprintf('   Ao [m�]          =%e', rcl.Ao(i))
                 sprintf('   co [m/s]         =%f', rcl.co(i))
                 }; 
             k = find([ps.boundaryParam.nodeId] == i); 
             if k
                 str1 = {
                  sprintf('  BoundaryParam %i:', k)
                  sprintf('   R                =%e', ps.boundaryParam(k).R)
                  sprintf('   C                =%e', ps.boundaryParam(k).C)
                  sprintf('   p                =%e', ps.boundaryParam(k).p)
                  sprintf('   q                =%e', ps.boundaryParam(k).q)
                  }; 
              ll = length(str); 
              str(ll+1: ll+5) = str1; 
             end

            end
        end
            
        function erg = ParOld(this)
            %% ParOld - genereates old xxxPAR-struct
            %   >> erg = anet.ParOld;
            len = this.nodeCount;
            E(len) = 0;
            h(len) = 0;
            l(len) = 0;
            d(len) = 0;
            first = true;
            for i=1:len
                E(i) = this.nodeArr(i+1).E;
                l(i) = this.nodeArr(i+1).l;
                h(i) = this.nodeArr(i+1).h;
                d(i) = this.nodeArr(i+1).d;
                if this.nodeArr(i+1).isBoundary && first
                    pout = this.nodeArr(i+1).b_p;
                    first = false;
                end
            end
            erg.E = E;
            erg.h = h;
            erg.l = l;
            erg.d = d;
            erg.d = d;
            erg.Z = this.pqRCL.R;   % Raider -> Twixx
            erg.pout = pout;
            erg.nu = this.n_eta;    % Raider -> Twixx
            erg.rho = this.n_rho;
            [header data] = this.db.select1('select typeName from NetType');
            erg.mtd = data{this.n_type};
            erg.gamma = this.n_gamma;
        end
        
        function erg = Net(this)
            %% NET - genereates old xxxNET-struct with matrices(adj and Z)
            %   >> erg = anet.Net;
            this.collectParams;  % generate thisadjaceny, this.Z
            n = length(this.adjacency)-1;
            erg.adj(n,n) = 0;
            for i=2:n
                erg.adj(this.adjacency(i+1)-1, i) = 1;
            end
            m = length(this.Z);
            erg.Z(n,m) = 0;
            for i=1:m
                erg.Z(this.Z(i), i) = 1;
            end
        end
        
        function count = nodeCount(this)
            %% nodeCount - return number of nodes
            %   >> count = anet.nodeCount;
            count = length(this.nodeArr)-1;
        end
        
        function count = boundaryCount(this)
            %% boundaryCount - return number of boundary nodes
            %   >> count = anet.boundaryCount;
            
            count = 0;
            for i=2:length(this.nodeArr);
                if this.nodeArr(i).isBoundary
                    count = count+1;
                end
            end
        end
        
        function peripheralResistances(this)
            net = this.Net;
            [a b]  = find(net.Z);
            params = this.getParamSet;
            for i=1:length(a)
                fprintf('NodeId: %i, R = %e\n', a(i), params.boundaryParam(i).R);
            end
        end
        
        function cts = CtsFromSolution(this, node, type)
        %% CtsFromSolution - returns the last period of solution on a node as CTS
        % >> cts = CtsFromSolution(node, type); 
        % >> cts = CtsFromSolution(12, 'p'); 
        % PARAMETER
        %   'node'  node number
        %   'type'   'p' or 'q'
        
          if isempty(this.p_solverRun) || isempty(this.p_solverRun.solution)
              error('ArteryNet.CtsFromSolution: no solution available, run solver first'); 
          end
          if nargin ~=3
              error('ArteryNet.CtsFromSolution: 2 parameter expected'); 
          end
          if ~isnumeric(node) || node < 0 || node > this.nodeCount
              error('ArteryNet.CtsFromSolution: node out of range'); 
          end
          if ~strcmp(type, 'p') && ~strcmp(type, 'q')
              error('ArteryNet.CtsFromSolution: type be ''p'' or ''q'' requested'); 
          end
          cts = CTS; 
          if strcmp(type, 'q')  % flow
              cts.type = 2; 
              cts.unit = 8; 
              idx = 2*(node+1)-1; 
          else                  % pressure
              cts.type = 1; 
              idx = 2*(node+1); 
              cts.unit = 5;
          end
          cts.rate = this.p_solverRun.solution.out_rate; 
          cts.description = sprintf('from node(%i)@ArteryNet(%i, %i)', node, this.id, this.p_id); 
          cts.signalName = 'STS derivation'; 
          dur = ceil(this.p_solverRun.solution.duration * cts.rate); 
          %last full period
          samples = size(this.p_solverRun.solution.Y, 1); 
          fullperiods = floor(samples/dur)-1; 
          cts.data = this.p_solverRun.solution.Y(fullperiods*dur:(fullperiods+1)*dur, idx);  
        end
        function res = CtsIn(this)
            %% CtsIn - returns the current inbound TS at root
            %   >> res = anet.CtsIn;
            if isempty(this.db)
                this.db = AngioDB;
            end
            this.db.open;
            res = this.db.select(['select typeName as type, rate, data, unit from CTS join TSType on type = TSType.id where CTS.id = ' num2str(this.n_ctsId)]);
            res.data = typecast(res.data, 'double')';
            %            res.period = double(length(res.data)) / double(res.rate);
            res.period = 60.0/this.n_heartRate;
            switch res.unit
                case 4 % mmHg
                    res.data = res.data/7.5E-3;
                    res.unit = 5;
                case 5 % Pa
                    res.data = res.data;
                case 6 % kPa
                    res.data = res.data*1E3;
                case 7 % MPa
                    res.data = res.data*1E6;
                case 8 % m^/s
                    res.data = res.data;
                case 9 % l/s
                    res.data = res.data*1E3;
                case 10 % ml/s
                    res.data = res.data*1E6;
                case {1,2,3,11,12,13}
                    error('Unit not implemented error: Conversion not implemented');
            end
        end
        
        function res = pqRCL(this)
            % pqRCL packs boundary properties for solver
            l = this.boundaryCount;
            res.p(l) = 0;
            res.q(l) = 0;
            res.R(l) = 0;
            res.C(l) = 0;
            res.L(l) = 0;
            idx = 0;
            props = {'p' 'q' 'R' 'C' 'L'};
            for i=2:length(this.nodeArr)
                if this.nodeArr(i).isBoundary
                    idx = idx+1;
                    if isempty(this.nodeArr(i).b_R) || ...
                            (isempty(this.nodeArr(i).b_p) && ...
                            isempty(this.nodeArr(i).b_q))
                        error(['ArteryNet.pqZ: node ' num2str(i)  ' nodeid=' num2str(this.nodeArr(i).nodeId) ' has an empty value (Zr, Zi, p, q)']);
                    end
                    for j = 1: length(props)
                        val = this.nodeArr(i).(['b_' props{j}]);
                        if ~isempty(val)
                            res.(props{j})(idx) = val;
                        end
                    end
                end
            end
        end
        
        function ch = rootChannel(this, nodeId)
            %% rootChannel - returns the channel from node with nodeId to root
            %     >> ch = rootChannel(nodeId);
            %        - 'ch' is a nodeId vector describing the path from node 'nodeId' to the root; nodeId and root.nodeId included
            
            l = length(this.nodeArr);
            c(l) = 0;
            i = 1;
            if nodeId > l-1
                error('This ArteryNet has only %i nodes! Node with nodeId = %i does not exist.', l-1, nodeId);
            end
            node = this.nodeArr(nodeId+1);
            while ~node.isRoot
                c(i) = node.nodeId;
                node = node.parent;
                i = i+1;
            end
            ch = c(1:i-1);
        end
        
        function path = nodeChannel(this, node1, node2)
        %% nodeChannel - gets nodes in path between node1 and node2
        %  PARAMETER: path = nodePath(this, node1, node2)
        %    node1, node2 - the two nodes 
        %    path         - (vector) node indizes
                n1 = this.rootChannel(node1); 
                n2 = this.rootChannel(node2); 
                offs = 0; 
                for i = 0: min(length(n1), length(n2))-1 % common path
                    if n1(end-i) == n2(end-i)
                        offs = i+1; 
                    end
                end
                path = [ n1(1:end-offs) n2(1:end-offs)]; 
                if ~any(path == node1)
                    path = [path, node1]; 
                end
               
        end
        function nodelist = nodeTree(this, n_idx)
        %% nodeTree - gets indices of nodes in subtree
        %  PARAMETER: nodelist = nodeTree(this, node)
        %    node     - the two nodes 
        %    n_idx   - (vector) node indizes in subtree
           children = this.nodeArr(n_idx+1).children; 
           nodelist = n_idx; 
           if ~isempty(children)
               for i = 1:length(children)
                   nodelist = [nodelist this.nodeTree(children(i).nodeId)];
               end 
           end
        end
        function len = nodeDistance(this, node1, node2)
            %%   nodedistance  - returns sum of node lengths between two nodes or from node to root
            %  l = anet.nodedistance(118, 85);   % distance between to nodes
            %  l = anet.nodedistance(118);       % distance from root  
            % PARMETER: 
            %   node1 - nodeid of first node
            %   node2 - (optional, default = root) nodeid of second node
            %   len   - distance [m]
            params = this.getParamSet; 
            if nargin== 3
                n = this.nodeChannel(node1, node2); 
            elseif nargin == 2
                n = this.rootChannel(node1); 
            else
                error('at least one node argument expected')
            end
            len = sum([params.nodeParam(n).l]); 

        end
        
        function scaleParamSet(this, param, factor, nodes)
        %% scaleParamSet - scales parameters 'dlh', 'd', 'l', 'h', 'E', 'b_R', 'b_p', 'b_q' of node list
        %  >>  anet.scaleParamSet('dl', .89);                   % scale globally
        %  >>  anet.scaleParamSet('dl', .89, [1, 2, 3, 4]);     % scale nodes in list only
        %  >>  anet.scaleParamSet('dl', .89, 4;                 % scale nodes in subtree of node 4 only
        % PARAMETER: scaleParamSet(this, param, factor) 
        %  param  - 'dlh', 'ldh', 'lhd', 'dhl', 'dh', 'hd', 'd', 'l', 'h', 'E', 'b_R', 'b_p', 'b_q'
        %  factor - multiplier for current values 
        %  nodes  - (optional, Default = all nodes) 
        %                      if integer rootnode of subtree to be altered
        %                      if vector list of nodes be altered

            n = this.nodeCount; 
            if nargin < 4
                nodes = 1:n; 
            else
                if length(nodes) == 1
                    nodes = this.nodeTree(nodes); 
                end
            end
            switch(param)
                case {'dlh', 'ldh', 'lhd', 'dhl'}
                    for i=2:n+1
                        if any(nodes == i-1) % restrict to nodes list
                            this.nodeArr(i).l = this.nodeArr(i).l * factor; 
                            this.nodeArr(i).h = this.nodeArr(i).h * factor; 
                            this.nodeArr(i).d = this.nodeArr(i).d * factor; 
                        end
                    end
                case {'dh', 'hd'}
                    for i=2:n+1
                        if any(nodes == i-1) % restrict to nodes list
                            this.nodeArr(i).h = this.nodeArr(i).h * factor; 
                            this.nodeArr(i).d = this.nodeArr(i).d * factor; 
                        end
                    end
                case {'E', 'd','l', 'h'}
                    for i=2:n+1
                        if any(nodes == i-1) % restrict to nodes list
                            this.nodeArr(i).(param) = this.nodeArr(i).(param) * factor; 
                        end
                    end
                case {'b_R', 'b_p', 'b_q'}
                    for i=2:n+1
                        if this.nodeArr(i).isBoundary && any(nodes == i-1) % restrict to nodes list
                            this.nodeArr(i).(param) = this.nodeArr(i).(param) * factor; 
                        end
                    end
                otherwise
                    error('ArteryNet.scaleParameterSet: unknown Parameter "%s"\n', param); 
                    
            end
            this.p_modified = true; 
        end
        
        function ch = channel(this, fromId, toId)
            %% channel - returns the channel from node with fromId to node with toId
            %   >> ch = anet.channel(fromId, toId);
            %        - 'ch' is a nodeId vector describing the path from node 'fromId' to node 'toId'; fromId and toId included
            %        - 'fromId' id of first node in channel
            %        - 'toId    id of last node in channel
            c1 = this.rootChannel(fromId);
            c2 = this.rootChannel(toId);
            l1 = length(c1);
            l2 = length(c2);
            l = min(l1, l2);
            ch(l1+l2) = 0;
            for i=0:l-1
                if c1(l1-i) == c2(l2-i)
                    cc = c1(l1-i);
                    c1(l1-i) = 0;
                    c2(l2-i) = 0;
                else
                    break;
                end
            end
            c1(l1-i+1) = cc;
            f1 = find(c1);
            c2 = sort(c2);
            f2 = find(c2);
            if isempty(f2)
                ch = c1(f1);
                return;
            end
            ch = [c1(f1) c2(f2)];
        end
        
        function ch = rootChannels(this)
            %% rootChannels - returns channels from all boundary nodes to root
            %   >> ch = anet.rootChannels;
            %       - 'ch' is a cell array of channels from all boundary nodes to root node
            
            l = length(this.nodeArr);
            j = 1;
            for i=2:l
                if this.nodeArr(i).isBoundary
                    ch{j} = this.rootChannel(i-1);
                    j = j+1;
                end
            end
        end
        
        function [x0] = X0(this, p0)
            %% X0 - computes the initial value vektor p0
            %  >> x0 = anet.X0;
            %  >> x0 = anet.X0(p0);
            %              -  option 'p0' denotes the pressure value at node 1
            cts = this.CtsIn; 
            if nargin < 2
                p0 = mean(cts.data);
                if strcmp(cts.type, 'q')      % we calc a flow by using linear dependency (p ~ q)
                    q0 = p0; 
                    xtemp = this.X0(1e6);     % have a try with a "good guess for pressure"; 
                    ratio = q0/xtemp(1);      % get the factor for linear dependency
                    x0 = this.X0(ratio*1e6); % recalc using p1 as estimation for p0
                    return; 
                end
            end
            
            
            l = length(this.nodeArr);
            bound(l) = 0;
            j = 1;
            bound(j) = 1;
            for i=2:l                   % collect boundary nodes
                if this.nodeArr(i).isBoundary
                    j = j+1;
                    bound(j) = i-1;
                end
            end
            bound = bound(1:j);
            channels{(j*(j-1)/2)} = {};  % collect channels between all out/in nodes
            c = 1;
            for i=1: j
                for k=i+1:j
                    channels{c} = this.channel(bound(i), bound(k));
                    c=c+1;
                end
            end
            
            R = this.RCL.R;
            l = length(channels);
            ch_R(l) = 0;
            ch_Q(l) = 0;
            lq = length(this.nodeArr)-1;
            x0(2*lq) = 0;
            if lq<2
                return;
            end
            node_Q(lq) = 0;   % flow
            node_P(lq) = 0;   % pressure
            
            this.nodeArr(2).b_R = 0;
            this.nodeArr(2).b_p = p0;   % for root node
            for i=1:l  % calculate node flow by summing up channel flows
                % calc channel resistance
                ch_R(i) = sum(R(channels{i})) + this.nodeArr(channels{i}(1)+1).b_R + this.nodeArr(channels{i}(end)+1).b_R;
                % calc channel flow
                ch_Q(i) = (this.nodeArr(channels{i}(1)+1).b_p - this.nodeArr(channels{i}(end)+1).b_p) / ch_R(i);
                node_Q(channels{i}) =  node_Q(channels{i}) + ch_Q(i);
            end
            x0(1) = node_Q(1);                            % first flow
            x0(2) = this.nodeArr(2).b_p - R(1)*node_Q(1); % first pressure
            for i = 2:lq                                  %
                j = this.nodeArr(i+1).parent.nodeId;
                x0(2*i-1) = node_Q(i);
                x0(2*i) = x0(2*j) - R(i)*node_Q(i);
            end
            
        end
        
        function sel = MultiSelector(obj, selection, plotAll, showlist)
            %% MultiSelector - opens Window, draws structure and allows selecting nodes in ArteryNet object
            %  Parameter
            %      in:  selection - preselection as list of nodeIds
            %     out   sel       - selection done by user as list of nodeIds
            %   eg.  userSelection = myAnet.MultiSelector([2,4,5]);
            
            % prepare parameter and data
            seed = 91;    % determines coloring of nodes
            if nargin>1
                for i=1:length(obj.nodeArr)
                    obj.nodeArr(i).selected = false;
                end
                if nargin == 2  % select nodes in selection
                    for i=1:length(selection)
                        obj.nodeArr(selection(i)+1).selected = true;
                    end
                    sel = selection;
                end
                if nargin == 4  % select nodes to be plotted
                    for i=1:length(showlist)
                        obj.nodeArr(showlist(i)+1).selected = true;
                    end
                    sel = intersect(showlist, 0:length(obj.nodeArr)); 
                end
            end
            rand('seed', seed); %#ok<*RAND>
            scrsz = get(0,'ScreenSize');
            mp = get(0, 'MonitorPositions');
            if size(mp,1) > 1
                scrf = 1.067 * scrsz(3);
            else
                scrf = 0.067 * scrsz(3);
            end
            plotview = false;
            ratio = 1;
            pos = [scrf scrsz(4)/16  7*scrsz(4)/8   7*scrsz(4)/8];
            titl = obj.name;
            titl = sprintf('ArteryNet(%i, %i, %i)',obj.id, obj.p_id, obj.p_solverRun.id);
            
            if nargin > 2  % display results of a solver run?
                plotview = true;
                ratio = 0.33;
                pos = [scrf scrsz(4)/16  11*scrsz(4)/8   7*scrsz(4)/8];
                view = 0;
                cputime = 'unknown';
                if isfield(obj.p_solverRun.solution, 'tsol')
                    cputime = num2str(obj.p_solverRun.solution.tsol);
                end
                res = obj.db.select(['Select typeName as solver from Solvers where id = ' num2str(obj.p_solverRun.solver)]); 
                titl = sprintf('ArteryNet(%i, %i, %i) - Heart rate=%g, CPU-time=%s, solver=%s',obj.id, obj.p_id, obj.p_solverRun.id, obj.n_heartRate, cputime, res.solver);
                % solutions
                plotAll = obj.p_solverRun.solution;
                plotOne = []; % data of last full period
                fullp = floor(length(plotAll.time) / (plotAll.duration * plotAll.out_rate)); % full period
                x1 = round((fullp-1) * (plotAll.duration * plotAll.out_rate)+1);
                x2 = round(double(plotAll.duration) * double(fullp) * plotAll.out_rate);
                try
                    plotOne.Y = plotAll.Y(x1:x2, :);
                    plotOne.time = plotAll.time(x1:x2);
                    plotOne.mean = mean(plotOne.Y);
                catch e
                end
                plotTwo = []; % data of last two full periods
                x1 = round((fullp-2) * (plotAll.duration * plotAll.out_rate)+1);
                x2 = round((fullp) * plotAll.duration * plotAll.out_rate);
                try
                    plotTwo.Y = plotAll.Y(x1:x2, :);
                    plotTwo.time = plotAll.time(x1:x2);
                catch
                    plotTwo = plotOne;
                end
                
                % rootnode
                ctsIn = obj.CtsIn;
                delta = obj.n_heartRate / 60.0 / length(ctsIn.data);
                plotRootOne.Y = ctsIn.data;
                plotRootOne.time = 0:delta: obj.n_heartRate / 60.0 -delta;
                
            end
            hDlg = figure('Name', ['ArteryNet-Multiselector - ' titl], 'NumberTitle', 'off', 'WindowStyle', 'normal', ...
                'Position',pos, 'WindowScrollWheelFcn', @obj.scale, 'ToolBar','none', 'Visible', 'off', ...
                'ResizeFcn', @resize, 'DeleteFcn', {@ArteryNet.EnableMenus, obj, 'on'}, 'CreateFcn', {@ArteryNet.EnableMenus, obj, 'off'});
            hAxes = axes('Xlim', obj.axesRect(1:2), 'Ylim', obj.axesRect(3:4), 'XTick', [], 'YTick', [], 'Position', [0 0 ratio 1]);
            
            if plotview
                
                obj.root.add2Dlg(hDlg, @changePlot, true);   % populate with nodes & register callback for mouseclick
                pos = [0.33 0.0 0.67 1];
                hPanel = uipanel('Parent', hDlg, 'Title', '', 'Units', 'Normalized', 'Position', pos, 'BackgroundColor', 'w', 'BorderType', 'none');
                axes('Parent', hPanel);
                
                if fullp>1
                    hToggle = uicontrol(hDlg, 'Style', 'Pushbutton', 'FontSize', 8,  'HorizontalAlignment', 'right', 'String', 'last period', ...
                        'Position', [pos(3)-160 20 75, 30], 'Callback', @cbToggle);
                end
                hDesel = uicontrol(hDlg, 'Style', 'Pushbutton', 'FontSize', 8,  'HorizontalAlignment', 'right', 'String', 'deselect all', ...
                    'Position', [pos(3)-230 20 65, 30], 'Callback', @cbDesel);
                if isempty(obj.p_solverRun.solution.Ys)
                    hSensitivities = [];
                else
                    hSensitivities = uicontrol(hDlg, 'Style', 'Pushbutton', 'FontSize', 8,  'HorizontalAlignment', 'right', 'String', 'Sensitivities', ...
                        'Position', [pos(3)-300 20 65, 30], 'Callback', @cbSens);
                end

                
                pos1 = [0.1 0.55 0.85 0.4];
                pos2 = [0.1 0.1 0.85 0.4];
                subplot(2,1,1, 'Parent', hPanel, 'Position', pos1);
                if obj.p_solverRun.nonlinear
                    title('Flow (linear)', 'Fontsize', 20);
                else
                    title('Flow (non-linear)', 'Fontsize', 20);
                end
                subplot(2,1,2, 'Parent', hPanel, 'Position', pos2);
                if obj.p_solverRun.nonlinear
                    title('Pressure (non-linear)', 'Fontsize', 20);
                else
                    title('Pressure (linear)', 'Fontsize', 20);
                end
                
            else
                obj.root.add2Dlg(hDlg, []);   % populate with nodes
                uicontrol(hDlg, 'Style', 'Pushbutton', 'FontSize', 8, 'BackgroundColor', 'green', ...
                    'HorizontalAlignment', 'right', 'String', 'select All', 'Position', [10 pos(4)-40 60 25], 'Callback', {@cbAll, true});
                uicontrol(hDlg, 'Style', 'Pushbutton', 'FontSize', 8, 'BackgroundColor', [0.8 0.8 0.8], ...
                    'HorizontalAlignment', 'right', 'String', 'deselect all', 'Position', [10 pos(4)-70 60 25], 'Callback', {@cbAll, false});
            end
            % buttons
            hOK = uicontrol(hDlg, 'Style', 'Pushbutton', 'FontSize', 8, 'HorizontalAlignment', 'right', 'String', 'OK', ...
                'Position', [pos(3)-80 20 65, 30], 'Callback', @cbOK);
            
            fUnit = 1e6;
            pUnit =  7.5E-3;
            set(hDlg, 'CurrentAxes', hAxes, 'Visible', 'on');
            
            
            if ~plotview
                uiwait(hDlg);
            end
            
            
            function cbSens(varargin)
                obj.p_solverRun.plotSensitivity(sel);
            end
            
            function cbToggle(varargin)
                view = mod(view+1, 3);
                switch view
                    case 0
                        set(hToggle, 'String', 'last period');
                    case 1
                        set(hToggle, 'String', 'last 2 periods');
                    case 2
                        set(hToggle, 'String', 'all periods');
                end
                changePlot;
            end
            
            function changePlot(varargin)
                figure(hDlg);                 % set current figure
                norm_offset = 1; 
                if (size(plotAll.Y, 2)) == 2* length(obj.nodeArr)  % normalized data?
                    sel = zeros(1, length(obj.nodeArr));
                    for j=1:length(obj.nodeArr)
                        sel(j) = obj.nodeArr(j).selected;
                    end
                    norm_offset = 0; 
                else % not normalized
                    sel = zeros(1, length(obj.nodeArr)-1);
                    for j=1:length(obj.nodeArr)-1
                        sel(j) = obj.nodeArr(j+1).selected;
                    end
                end
                sel = find(sel);
                
                axes('Parent', hPanel);
                pos1 = [0.1 0.57 0.85 0.38];
                pos2 = [0.1 0.1 0.85 0.38];
                len = length(sel);
                subplot(2,1,1,  'Position', pos1);
                ylabel(gca, 'flow [ml/s]');
                xlabel(gca, 'time [s]');
                if obj.p_solverRun.nonlinear
                    title('Flow (non-linear)', 'Fontsize', 20);
                else
                    title('Flow (linear)', 'Fontsize', 20);
                end
                hold on
                for j=1:len
                        c = get(obj.nodeArr(sel(j)+norm_offset).hSelect, 'FaceColor');
                    switch view
                        case 0
                            try
                                plot(plotAll.time, fUnit*plotAll.Y(:,2*sel(j)-1), 'Color', c);
                            catch ex
                            end
                        case 1
                            try
                                plot(plotOne.time, fUnit*plotOne.Y(:,2*sel(j)-1), 'Color', c);
                            catch ex
                            end
                        case 2
                            try
                                plot(plotTwo.time, fUnit*plotTwo.Y(:,2*sel(j)-1), 'Color', c);
                            catch ex
                            end
                    end
                end
                if len
                    try
                    for j=1:len
                            leg{j} = num2str(fUnit*plotOne.mean(2*sel(j)-1));
                    end
                    legend(leg, 'Location','NorthEast');
                    catch ex
                    end

                end
                subplot(2,1,2, 'Position', pos2);
                hold on
                if pUnit == 1
                    ylabel(gca, 'pressure [Pa]');
                else
                    ylabel(gca, 'pressure [mmHg]');
                end
                xlabel(gca, 'time [s]');
                hYbl = get(gca, 'YLabel');
                set(hYbl, 'ButtonDownFcn', @toggleUnit);
                
                if obj.p_solverRun.nonlinear
                    title('Pressure (non-linear)', 'Fontsize', 20);
                else
                    title('Pressure (linear)', 'Fontsize', 20);
                end
                for j=1:length(sel)
                    c = get(obj.nodeArr(sel(j)+norm_offset).hSelect, 'FaceColor');
                    switch view
                        case 0
                            try
                            plot(plotAll.time, pUnit*plotAll.Y(:,2*sel(j)), 'Color', c);
                            catch ex
                            end
                        case 1
                            try
                                plot(plotOne.time, pUnit*plotOne.Y(:,2*sel(j)), 'Color', c);
                            catch ex
                            end
                        case 2
                            try
                                plot(plotTwo.time, pUnit*plotTwo.Y(:,2*sel(j)), 'Color', c);
                            catch ex
                            end
                    end
                end
                if len
                    try
                        for j=1:len
                            leg{j} = num2str(pUnit*plotOne.mean(2*sel(j)));
                        end
                        legend(leg, 'Location','NorthEast');
                    catch ex
                    end
                end
                function toggleUnit(varargin)
                    if pUnit == 1
                        pUnit = 7.5E-3;
                    else
                        pUnit = 1;
                    end
                    changePlot;
                end
                
            end
            function cbDesel(varargin)
                for j=2:length(obj.nodeArr)
                    obj.nodeArr(j).Select(false);
                end
                rand('seed', seed);
                changePlot;
            end
            function cbAll(varargin)
                rand('seed', seed);
                for j=2:length(obj.nodeArr)
                    obj.nodeArr(j).Select(varargin{3});
                end
            end
            
            function resize(varargin)
                %% resize - callback treats resizing of figure while trying to keep aspect
                p = get(hDlg, 'Position');
                axes(hAxes);  %#ok<MAXES>
                ratio1 = p(3)/ p(4) * ratio;
                x = xlim;
                y = ylim;
                ratio2 = (x(2) - x(1))/(y(2)-y(1));
                if (x(2)-x(1))*3 > y(2)-y(1)
                    xlim(x * ratio1 / ratio2);
                else
                    ylim(y / ratio1 * ratio2);
                end
                set (hOK, 'Position', [p(3)-80, 20, 60, 30]);
                if plotview
                    if fullp>1
                        set (hToggle, 'Position', [p(3)-160, 20, 75, 30]);
                    end
                    set (hDesel, 'Position', [p(3)-230, 20, 65, 30]);
                    if ~isempty(hSensitivities)
                        set (hSensitivities, 'Position', [p(3)-300, 20, 65, 30]);
                    end
                    
                    changePlot;
                end
            end
            function cbOK(varargin)
                sel = find([obj.nodeArr.selected])-1;
                if isempty(sel)
                    sel = 1; 
                end
                if plotview
                    obj.p_solverRun.showlist = sel;
                end
                delete(gcbf);
            end
            
        end
        function SignalViewer(this, objWithSignals)
            %% SignalViewer - opens Window, draws structure and allows to view signals attached to nodes
            %  Parameter
            %      in:  selection - preselection as list of nodeIds
            %     out   sel       - selection done by user as list of nodeIds
            %   eg.  userSelection = myAnet.MultiSelector([2,4,5]);
            
            seed = 91;    % determines coloring of nodes
            rand('seed', seed); %#ok<*RAND>
            scrsz = get(0,'ScreenSize');
            mp = get(0, 'MonitorPositions');
            if size(mp,1) > 1
                scrf = 1.067 * scrsz(3);
            else
                scrf = 0.067 * scrsz(3);
            end
            hOK = []; 
            ratio = 0.33;
            pos = [scrf scrsz(4)/16  11*scrsz(4)/8   7*scrsz(4)/8];
            
            fUnit = 1e6;
            pUnit = 1; 
            signal = objWithSignals(1).signal; 
            times = objWithSignals(1).getSignal(1).times; 
            hflow = []; 
            hpress = []; 
            xF =[]; 
            xP =[]; 
            zoomview = false; 
            ctsview = true; 
            keep = false; 
            animateIdx = 1; 
            
            % multiview
            id = objWithSignals.ToString; 
            
            titl = sprintf('ArteryNet(%i, %i) -> %s',this.id, this.p_id, id);
            
            hDlg = figure('Name', ['ArteryNet-Multiselector - ' titl], 'NumberTitle', 'off', 'WindowStyle', 'normal', ...
                'Position',pos, 'WindowScrollWheelFcn', @scroll, 'ToolBar','none', 'Visible', 'off', ...
                'ResizeFcn', @resize, 'DeleteFcn', {@ArteryNet.EnableMenus, this, 'on'}, 'CreateFcn', {@ArteryNet.EnableMenus, this, 'off'});
            hAxes = axes('Xlim', this.axesRect(1:2), 'Ylim', this.axesRect(3:4), 'XTick', [], 'YTick', [], 'Position', [0 0 ratio 1]);
            
            % draw net
            for j=1:length(this.nodeArr)-1
                this.nodeArr(j+1).selected = false;
            end
            this.root.add2Dlg(hDlg, @changePlot, 'toggleroot');   % populate with nodes & register callback for mouseclick
            objnodes = objWithSignals(1).signal.node; 
            for j=1: length(objnodes)
                node = this.nodeArr(objnodes(j)+1);
                set(node.hSelect, 'LineWidth', 2.5);
                set(node.hSelect, 'Curvature', [0 0]);
            end
            
            % panels
            pos3 = [0.33 0.0 0.67 1];
            hPanel = uipanel('Parent', hDlg, 'Title', '', 'Units', 'Normalized', 'Position', pos3, 'BackgroundColor', 'w', 'BorderType', 'none');
            axes('Parent', hPanel);
            pos1 = [0.1 0.55 0.85 0.4];
            pos2 = [0.1 0.1 0.85 0.4];
            subplot(2,1,1, 'Parent', hPanel, 'Position', pos1);
            title('Flow', 'Fontsize', 20);
            subplot(2,1,2, 'Parent', hPanel, 'Position', pos2);
            title('Pressure', 'Fontsize', 20);

            % buttons
            hOK = uicontrol(hDlg, 'Style', 'Pushbutton', 'FontSize', 8, 'HorizontalAlignment', 'right', 'String', 'OK', ...
                'Position', [pos(3)-80 20 65, 30], 'Callback', @cbOK);
            
            if length(objWithSignals) == 1 && isfield(objWithSignals(1).signal, 'cts')
                hZoom = uicontrol(hDlg, 'Style', 'Pushbutton', 'FontSize', 8, 'HorizontalAlignment', 'right', 'String', '5 s', ...
                'Position', [pos(3)-160 20 65, 30], 'Callback', @cbZoom);
                hcts = uicontrol(hDlg, 'Style', 'Pushbutton', 'FontSize', 8, 'HorizontalAlignment', 'right', 'String', 'raw', ...
                 'Position', [pos(3)-240 20 65, 30], 'Callback', @cbCts);
            else
                ctsview = true; 
            end
            set(hDlg, 'CurrentAxes', hAxes, 'Visible', 'on');

            
            changePlot; 
            
            function scroll(varargin)
                keep = true; 
                this.scale(varargin{1}, varargin{2}); 
                if gca == hflow
                    set(hpress, 'XLim', get(hflow, 'Xlim')); 
                end
                if gca == hpress
                    set(hflow, 'XLim', get(hpress, 'Xlim')); 
                end
            end
            
            function changePlot(varargin)
                % ensure that only valid nodes are selectable
                if nargin>0
                    n = varargin{1};
                    if isempty(find(objnodes == n.nodeId, 1))
                        n.Select(false);
                        return;
                    end
                end
                
                % remember x-scale
                xF = get(hflow, 'XLim'); 
                
                % find selected nodes
                sel = zeros(1, length(this.nodeArr));
                for j=1:length(this.nodeArr)
                    sel(j) = this.nodeArr(j).selected;
                end
                sel = find(sel) -1;
                
                axes('Parent', hPanel);
                pos1 = [0.1 0.57 0.85 0.3];
                pos2 = [0.1 0.1 0.85 0.3];
                len = length(sel);

                % flow
                % ToDo: flow plot not yet implemented
                hflow = subplot(2,1,1,  'Position', pos1);
              
                ylabel(hflow, 'flow [ml/s]');
                xlabel(hflow, 'time [s]');
                title('Flow', 'Fontsize', 20);
                hold on
                if ~isempty(sel)
                    for j=1:len
                        c = get(this.nodeArr(sel(j)+1).hSelect, 'FaceColor');
                    end
                end
                
                % pressure
                hpress = subplot(2,1,2, 'Position', pos2);
                hold on
                if pUnit == 1
                    ylabel(gca, 'pressure [Pa]');
                else
                    ylabel(gca, 'pressure [mmHg]');
                end
                hYbl = get(gca, 'YLabel');
                set(hYbl, 'ButtonDownFcn', @toggleUnit);
                
                title('Pressure', 'Fontsize', 20);
                legendPressure = string([]);
                legendFlow = string([]);
                if ~isempty(sel)
                    for j=1:length(sel)
                        %pressure
                        sigIdx = find(objnodes == sel(j) & contains(objWithSignals.signal.type,'p')); 
                        if ~isempty(sigIdx)
                            c = get(this.nodeArr(sel(j)+1).hSelect, 'FaceColor');
                            sig = objWithSignals.getSignal(sigIdx); 
                            if ctsview 
                                xlabel(hpress, 'phase angle [radian]');
                                if isfield(sig, 'ctsTimes')
                                    times = sig.ctsTimes; 
                                    sig = sig.ctsData; 
                                else
                                    warning('obj has no cts data!')
                                    times = 0; 
                                    sig = 0; 
                                end
                            else
                                xlabel(hpress, 'time [s]');
                                times = sig.times; 
                                sig = sig.data; 
                            end
                            plot(hpress,times, sig*pUnit, 'Color', c);
                            legendPressure = [legendPressure num2str(mean(sig)*pUnit,3)];
                        end
                        %flow
                        sigIdx = find(objnodes == sel(j) & contains(objWithSignals.signal.type,'q')); 
                        if ~isempty(sigIdx)
                            c = get(this.nodeArr(sel(j)+1).hSelect, 'FaceColor');
                            sig = objWithSignals.getSignal(sigIdx); 
                            if ctsview 
                                xlabel(hflow, 'phase angle [radian]');
                                if isfield(sig, 'ctsTimes')
                                    times = sig.ctsTimes; 
                                    sig = sig.ctsData; 
                                else
                                    warning('obj has no cts data!')
                                    times = 0; 
                                    sig = 0; 
                                end
                            else
                                xlabel(hflow, 'time [s]');
                                times = sig.times; 
                                sig = sig.data; 
                            end
                            plot(hflow,times, sig*fUnit, 'Color', c);
                            legendFlow = [legendFlow num2str(mean(sig)*fUnit,3)];
                        end
                    end
                end
                
                legend(hpress,legendPressure);
                legend(hflow,legendFlow);
                
                if zoomview
                    set([hflow hpress], 'XLim', xF); 
                end
            end
            function toggleUnit(varargin)
                if pUnit == 1
                    pUnit = 7.5E-3;
                else
                    pUnit = 1;
                end
                changePlot;
            end
            
            function resize(varargin)
                %% resize - callback treats resizing of figure while trying to keep aspect
                p = get(hDlg, 'Position');
                axes(hAxes);  %#ok<MAXES>
                ratio1 = p(3)/ p(4) * ratio;
                x = xlim;
                y = ylim;
                ratio2 = (x(2) - x(1))/(y(2)-y(1));
                if (x(2)-x(1))*3 > y(2)-y(1)
                    xlim(x * ratio1 / ratio2);
                else
                    ylim(y / ratio1 * ratio2);
                end
                if ~isempty (hOK)
                    set (hOK, 'Position', [p(3)-80, 20, 60, 30]);
                end
            end
            function cbZoom(varargin)
                set([hflow hpress], 'XLim', [0 5]); 
                zoomview = true; 
                if ctsview
                    ctsview = false; 
                    set(hcts, 'String', 'cts'); 
                    changePlot; 
                end
            end
            function cbCts(varargin)
                ctsview = ~ctsview; 
                if ctsview
                    set(hcts, 'String', 'raw'); 
                else
                    set(hcts, 'String', 'cts'); 
                end
                zoomview = false; 
                changePlot; 
                
            end
            function cbOK(varargin)
                delete(gcbf);
            end
            
        end
    end
    
    %% callbacks
    methods (Static)
        
        function testDbIntegrity
            db = AngioDB;
            res = db.select('SELECT ParamSet.id FROM ParamSet WHERE not exists (select paramSetId from NetParam where NetParam.paramSetId=ParamSet.id)');
            if ~isempty (res)
                fprintf('ParamSet(%i): no NetParam record\n', res.id);
            end
            res = db.select('SELECT ParamSet.id FROM ParamSet WHERE not exists (select paramSetId from NodeParam where NodeParam.paramSetId=ParamSet.id)');
            if ~isempty (res)
                fprintf('ParamSet(%i): no NodeParam record\n', res.id);
            end
            res = db.select('SELECT ParamSet.id FROM ParamSet WHERE not exists (select paramSetId from BoundaryParam where BoundaryParam.paramSetId=ParamSet.id)');
            if ~isempty (res)
                fprintf('ParamSet(%i): no BoundaryParam record\n', res.id);
            end
            
            res = db.select('SELECT paramSetId FROM NetParam group by paramSetId having count(paramSetId) = 2');
            if ~isempty (res)
                fprintf('ParamSet(%i) has multiple NetParam records \n', res.paramSetId');
                Multiple = res.paramSetId;
                for j = 1:length(Multiple)
                    res = db.select(sprintf('SELECT id, paramSetId FROM NetParam where paramSetId = %i', Multiple(j)));
                    for i = 1:length(res)
                        fprintf('-> NetParam(%i) refers to ParamSet(%i)\n', res(i).id, res(i).paramSetId);
                    end
                end
            end
            
            
            
        end
        
        function mANew(varargin)
            %% mANew - callback, handles ArteryNet menu command 'New'
            a = ArteryNet;
            a.Designer;
        end
        
        function this = Open(title)
            %% Open - shows open dialog which allows to choose an ArteryNet with a ParamSet, then opens a designer window for it.
            if nargin < 1
                this = ArteryNet.openCommonDlg([] ,'ArteryNet: Open');
            else
                this = ArteryNet.openCommonDlg([] ,['ArteryNet: ' title]);
            end
            if ~isempty(this)
                if nargin < 1
                    this.Designer;
                    this.setDesignerTitle;
                end
            end
        end
        function Delete
            ArteryNet.delCommonDlg([], 'ArteryNet: Delete');
        end
        
        function mAOpen(varargin)
            %% mAOpen - callback, handles ArteryNet menu command 'Open'
            %   shows open dialog and opens Designer for selected ArteryNet object
            this = get(get(get(varargin{1}, 'parent'), 'parent'), 'UserData');
            obj = ArteryNet.openCommonDlg(this, 'ArteryNet: Open');
            if ~isempty(obj)
                obj.Designer;
                obj.setDesignerTitle;
            end
        end
        function mASaveAs(varargin)
            %% mASave - callback, handles ArteryNet menu command 'Save as'
            this = get(get(get(varargin{1}, 'parent'), 'parent'), 'UserData');
            this.collectParams;
            message = '';
            if ischar(varargin{1})
                message = varargin{1};
            elseif nargin >1 && ischar(varargin{2})
                message = varargin{2};
            end
            this.saveCommonDlg('ArteryNet: Save as', message);
        end
        function mADelete(varargin)
            this = get(get(get(varargin{1}, 'parent'), 'parent'), 'UserData');
            ArteryNet.delCommonDlg(this, 'ArteryNet: Delete');
        end
        
        function mPDelete(varargin)
            this = get(get(get(varargin{1}, 'parent'), 'parent'), 'UserData');
            ArteryNet.delCommonDlg(this, 'ParamSet: Delete');
        end
        function mASave(varargin)
            %% mASave - callback, handles ArteryNet menu command 'Save'
            this = get(get(get(varargin{1}, 'parent'), 'parent'), 'UserData');
            if isempty(this.id)
                ArteryNet.mASaveAs('Message: ArteryNet object is not known in DB. "Save as" is used.');
                return;
            end
            this.collectParams;
            this.saveCommonDlg('ArteryNet: Save', '');
        end
        function mPOpen(varargin)
            %% mAOpen - callback, handles ArteryNet menu command 'Open'
            this = get(get(get(varargin{1}, 'parent'), 'parent'), 'UserData');
            ArteryNet.openCommonDlg(this, 'ParamSet: Open');
            this.setDesignerTitle;
        end
        function mPSave(varargin)
            %% mASave - callback, handles ArteryNet menu command 'Save'
            this = get(get(get(varargin{1}, 'parent'), 'parent'), 'UserData');
            if isempty(this.id)
                ArteryNet.mASaveAs(varargin{1}, 'Warning: ArteryNet object is not known in DB. "ArteryNet.Save as" is used.');
                return;
            end
            if isempty(this.p_id)
                ArteryNet.mPSaveAs('Message: ParamSet is new. "Save as" is used');
                return;
            end
            this.collectParams;
            this.saveCommonDlg('ParamSet: Save', '');
        end
        function mPSaveAs(varargin)
            %% mASave - callback, handles ArteryNet menu command 'Save as'
            this = get(get(get(varargin{1}, 'parent'), 'parent'), 'UserData');
            this.collectParams;
            if isempty(this.id)
                ArteryNet.mASaveAs('Warning: ArteryNet object is not from DB. "ArteryNet.Save as" is used.');
                return;
            end
            message = '';
            if ischar(varargin{1})
                message = varargin{1};
            end
            this.saveCommonDlg('ParamSet: Save as', message);
        end
        function mSNew(varargin)
            anet = get(get(get(varargin{1}, 'parent'), 'parent'), 'UserData');
            try
                SolverRun.NewSolverRunDlg(anet);
            catch e
                e.message
            end
        end
        function mSShow(varargin)
            anet = get(get(get(varargin{1}, 'parent'), 'parent'), 'UserData');
            if ~isempty(anet.p_solverRun)
                if ~isempty(anet.p_solverRun.solution)
                    anet.p_solverRun.showlist = anet.MultiSelector(anet.p_solverRun.plist, true, anet.p_solverRun.showlist);  % Ausgabe
                end
            end
        end
        function mSOpen(varargin)
            anet = get(get(get(varargin{1}, 'parent'), 'parent'), 'UserData');
            sid = SolverRun.OpenSolverRunDlg(anet.p_id, anet.db);
            if sid > 0
                if anet.p_solverRun.Open(anet.db, sid)
                    if ~isempty(anet.p_solverRun.solution)
                        ArteryNet.mSShow(varargin{1});
                    end
                end
            end
            anet.setDesignerTitle;
        end
        function mSSave(varargin)
            anet = get(get(get(varargin{1}, 'parent'), 'parent'), 'UserData');
            if anet.modified
                warnDlg('The ArteryNet object was modified. It must be saved to database first', 'Can''t save now');
                return;
            end
            if anet.p_modified
                warnDlg('The ParamSet object was modified. It must be saved to database first', 'Can''t save now');
                return;
            end
            sid = anet.p_solverRun.Save(anet.db);
            if sid>0
                anet.p_currentSolverRun = sid;
                anet.db.update(['Update ParamSet Set currentSolverRun = ' num2str(sid) ' where id = ' num2str(anet.p_id)]);
                anet.MaintainMenu('SolverRun', 'Save ...', 'Enable', 'off');
                anet.setDesignerTitle;
            end
        end
        function mSDelete(varargin)
            this = get(get(get(varargin{1}, 'parent'), 'parent'), 'UserData');
            ArteryNet.delCommonDlg(this, 'SolverRun: Delete');
        end
        
        function RCL = Elhd2RCL(Elhd, rho, eta)
            %% Elhd2RCL - converts windkessel model parameters to electrical parameters
            %  see also: ArteryNet.Elhd2RCL2, ArteryNet.Elhd2RCL1
            %   uses parameter analogy after K Al Zoukra S. Bernhard (anet.n_type == 1 <-'kasb' )
            %    >> RCL = ArteryNet.Elhd2RCL(Elhd);
            %     - 'Elhd': n-dim struct array with fields 'E', 'l', 'd', 'h', 'rho', 'eta'
            %     - 'RCL':  n-dim struct array 'Eldh' extended with fields
            %            .R, .C., .L  - the RCL model parameters,
            %            .Ao          - the cross-section Ao [m^2],
            %            .co          - the pulse wave velocity co [m/s] in the model segments
            
            if nargin == 0 || ~isstruct(Elhd)
                error('ArteryNet.Elhd2RCL: parameter error\nUSAGE:\n%s', help('ArteryNet.Elhd2RCL'));
            end
            if nargin == 1
                rho = Elhd(1).rho; 
                eta = Elhd(1).eta; 
            end
            n = length(Elhd);
            
            for i=1:n                           % number of segments
                r = Elhd(i).d/2;           % radius of vessel
                Elhd(i).Ao=pi*r^2;  % cross sectional area [m^2]
                
                % co = sqrt( (E*h)/2/rho/r )     = wave velocity [m/s]
                Elhd(i).co = sqrt( (Elhd(i).E * Elhd(i).h) / 2 / rho / r);
                
                % R = (8*pi*eta*l)/(pi*(d/2)^2)^2   = arterial resistance [kg s^-1 ns^-4]
                Elhd(i).R = (8 * pi * eta * Elhd(i).l)/ (pi * (Elhd(i).d/2)^2)^2;
                
                % L = (rho*l) / (pi*(d/2)^2)       = arterial inductance [kg s^-4]
                Elhd(i).L = (rho * Elhd(i).l) / (pi * (Elhd(i).d/2)^2);
                
                % C = (2pi*r^3 * l) / (E h)        = arterial compliance [s^4 s kg^-1]
%                Elhd(i).C = (2*pi*r^3*Elhd(i).l) / (Elhd(i).E * Elhd(i).h);
                 Elhd(i).C = ArteryNet.calcC(r, Elhd(i).l, Elhd(i).h, Elhd(i).E);
            end
            RCL = Elhd;
        end
        
        function RCL = Elhd2RCL1(Elhd, rho, eta)
            %% Elhd2RCL - converts windkessel model parameters to electrical parameters
            %   uses parameter analogy after K Al Zoukra S. Bernhard (anet.n_type == 1 <-'kasb' )
            %  see also: ArteryNet.Elhd2RCL2, ArteryNet.Elhd2RCL
            %    >> RCL = ArteryNet.Elhd2RCL(Elhd);
            %     - 'Elhd': n-dim struct array with fields 'E', 'l', 'd', 'h', 'rho', 'eta'
            %     - 'RCL':  struct 'Eldh' extended with vector fields
            %            .R, .C., .L  - the RCL model parameters,
            %            .Ao          - the cross-section Ao [m^2],
            %            .co          - the pulse wave velocity co [m/s] in the model segments
            
            if nargin == 0 || ~isstruct(Elhd)
                error('ArteryNet.Elhd2RCL: parameter error\nUSAGE:\n%s', help('ArteryNet.Elhd2RCL'));
            end
            if nargin == 1
                rho = Elhd(1).rho; 
                eta = Elhd(1).eta; 
            end
            n = length(Elhd);
            RCL.R(n) = 0; 
            RCL.C(n) = 0; 
            RCL.L(n) = 0; 
            RCL.Ao(n) = 0; 
            RCL.co(n) = 0; 
            
            for i=1:n                           % number of segments
                r = Elhd(i).d/2;           % radius of vessel
                RCL.Ao(i)=pi*r^2;  % cross sectional area [m^2]
                
                % co = sqrt( (E*h)/2/rho/r )     = wave velocity [m/s]
                RCL.co(i) = sqrt( (Elhd(i).E * Elhd(i).h) / 2 / rho / r);
                
                % R = (8*pi*eta*l)/(pi*(d/2)^2)^2   = arterial resistance [kg s^-1 ns^-4]
                RCL.R(i) = (8 * pi * eta * Elhd(i).l)/ (pi * (Elhd(i).d/2)^2)^2;
                
                % L = (rho*l) / (pi*(d/2)^2)       = arterial inductance [kg s^-4]
                RCL.L(i) = (rho * Elhd(i).l) / (pi * (Elhd(i).d/2)^2);
                
                % C = (2pi*r^3 * l) / (E h)        = arterial compliance [s^4 s kg^-1]
                % RCL.C(i) = (2*pi*r^3*Elhd(i).l) / (Elhd(i).E * Elhd(i).h);
                RCL.C(i) = ArteryNet.calcC(r, Elhd(i).l, Elhd(i).h, Elhd(i).E);

            end
        end
        function RCL = Elhd2RCL2(Eldh, rho, eta)
            %% Elhd2RCL2 - converts windkessel model parameters to electrical parameters
            %   fast Matlab version of ArteryNet.Elhd2RCL
            r = Eldh.d/2;   % radius of vessel
            RCL.Ao  = pi * r.^2;  % cross sectional area [m^2]
            eh = Eldh.E .* Eldh.h; 
            RCL.co = sqrt( eh / 2 / rho ./ r);
            RCL.R = (8 * pi * eta * Eldh.l) ./ RCL.Ao .^2;
            RCL.L = (rho * Eldh.l) ./ RCL.Ao ;
            % RCL.C = (2 * pi * r.^3 .* Eldh.l) ./ eh;
            RCL.C = ArteryNet.calcC(r, Elhd.l, Elhd.h, Elhd.E);
            
        end
        
        function scale(h, evnt)
            %% scale - callback for mousewheel scaling
            % transforms axes relativly to mouse position
            fac = 1 + 0.03*evnt.VerticalScrollAmount;
            if (evnt.VerticalScrollCount<0)
                fac = 1/fac;
            end
            fac = fac - 1;
            pos = get(gca, 'CurrentPoint');
            x = xlim;
            y = ylim;
            if pos(1) < x(1) || pos(1) > x(2) || pos(3) < y(1) || pos(3) > y(2)
                return;
            end
            x = x + (x-pos(1)) * fac;
            y = y + (y-pos(3)) * fac;
            xlim(x);
            parent = get(gca, 'Parent');
            if parent == h
                ylim(y);
            end
        end
        
        function EnableMenus(varargin)
            anet = varargin{3};
            val = varargin{4};
            if ~isempty(anet.hmenuArtery)
                set(anet.hmenuArtery, 'Enable', val);
                set(anet.hmenuParam, 'Enable', val);
                set(anet.hmenuSolver, 'Enable', val);
            end
        end
        
        
        function mCloseRequest(varargin)
            %% mCloseRequest - checks for unsafed modifications and quests user what to do
            
            if isempty(gcbf)
                return;
            end;
            obj = get(varargin{1}, 'UserData');
            if isempty(obj) 
                delete (gcbf); return; 
            end
            if obj.modified && obj.p_modified
                ret = questdlg(['Modifications to current ArteryNet and ParamSet objects were not saved to database!' char(10) char(10) 'Close anyway?'],'ArteryNet: Close without save?', 'Close', 'Cancel', 'Cancel');
                switch ret
                    case 'Cancel'
                        return;
                end
            elseif obj.modified
                ret = questdlg(['Modifications to current ArteryNet object were not saved to database!'  char(10) char(10) 'Close anyway?'],'ArteryNet: Close without save?', 'Close', 'Cancel', 'Cancel');
                switch ret
                    case 'Cancel'
                        return;
                end
            elseif obj.p_modified
                ret = questdlg(['Modifications to current Parameter Set were not saved to database!'  char(10) char(10) 'Close anyway?'],'ParamSet: Close without save?', 'Close', 'Cancel', 'Cancel');
                switch ret
                    case 'Cancel'
                        return;
                end
            end
            delete(gcbf);
        end
        
        function resize(varargin)
            %% resize - callback treats resizing of figure while trying to keep aspect
            
            p = get(gcf, 'Position');
            ratio1 = p(3)/ p(4);
            x = xlim;
            y = ylim;
            ratio2 = (x(2) - x(1))/(y(2)-y(1));
            if x>y
                xlim(x * ratio1 * ratio2);
            else
                ylim(y / ratio1 * ratio2);
            end
            % Property Editor
            obj = get(gcf, 'UserData');
            if ~isempty(obj.hPropEditorNode)
                pos = get(obj.hPropEditorNode, 'Position');
                pos(2) = p(4)- pos(4);
                set(obj.hPropEditorNode, 'Position', pos, 'Units', 'Pixels');
                pos = get(obj.hPropEditorNet, 'Position');
                pos(2) = p(4)- pos(4);
                set(obj.hPropEditorNet, 'Position', pos, 'Units', 'Pixels');
            end
            
            
        end
        
        function delCommonDlg(this, title)
            db = [];  %#ok<*PROP>
            if isempty(this)
                db = AngioDB;
            else
                if isempty(this.db)
                    this.db = AngioDB;
                end
                db = this.db;
            end
            db.open;
            
            mode = 0;   % ArteryNet
            target = 'ArteryNet';
            tabletitle = target;
            if strfind(title, 'ParamSet')
                target = 'ParamSet';
                tabletitle  = target;
            elseif strfind(title, 'SolverRun')
                target = 'SolverRun';
                tabletitle = 'All SolverRuns saved for this ArteryNet';
            end
            
            scrsz = get(0,'ScreenSize');
            mp = get(0, 'MonitorPositions');
            if size(mp,1) > 1
                scrf = scrsz(3) + (scrsz(3)-1000)/2;
            else
                scrf = (scrsz(3)-1000)/2;
            end
            
            
            position = [scrf, (scrsz(4)-600)/2, 1200, 600];
            hDlg = figure('Name', title, 'NumberTitle', 'off', 'MenuBar','none', 'ToolBar','none', ...
                'Color', 'w', 'Position', position, 'Resize', 'off', 'WindowStyle', 'normal');
            
            % static text over panels
            uicontrol(hDlg, 'Style', 'Text', 'FontSize', 14, 'HorizontalAlignment', 'left', 'String', tabletitle, ...
                'Position', [20 570 800, 30], 'BackgroundColor', 'w');
            % buttons
            hOk = uicontrol(hDlg, 'Style', 'Pushbutton', 'FontSize', 8, 'HorizontalAlignment', 'right', 'Enable', 'off', 'String', 'Delete', ...
                'Position', [1050 20 60, 30], 'Callback', @DelDlgOk);
            
            hCancel = uicontrol(hDlg, 'Style', 'Pushbutton', 'FontSize', 8, 'HorizontalAlignment', 'right', 'String', 'Cancel', ...
                'Position', [1120 20 60, 30], 'Callback', @DlgCancel);
            
            % table
            table = [];
            [headers, dbcontent] = refreshTable;
            if size(dbcontent, 2) == 1
                dbcontent = cell(size(headers)); 
            end
            table = javahandle_withcallbacks.javax.swing.JTable(dbcontent, headers); % R2014a/R2015a fixed
            table.setSelectionMode(0);
            cm = table.getColumnModel;
            if(cm.getColumnCount > 3)
                w = cm.getTotalColumnWidth;
                cm.getColumn(0).setWidth(0.04*w);
                cm.getColumn(1).setPreferredWidth(0.06*w);
                cm.getColumn(2).setPreferredWidth(0.06*w);
                cm.getColumn(3).setPreferredWidth(0.3*w);
            end
            ScrollPane = javax.swing.JScrollPane(table);
            set(hDlg, 'Userdata', table);
            btLinePropsPos = [[0.02 0.1],0.96,0.85];
            [~,btContainer] = javacomponent(ScrollPane,[0 0 1 1], hDlg);
            set(btContainer, 'Units', 'Norm', 'Position', btLinePropsPos);
            
            warning('off', 'MATLAB:hg:JavaSetHGProperty');
            if length(dbcontent) > 1
                set(table, 'MouseMovedCallback', {@delDlgMouseMoved, 1});  % for tooltip table 1
                set(table,'MousePressedCallback',@delDlgSelection);
            end
            
            table.setAutoCreateRowSorter(true);
            ec = table.getDefaultEditor(table.getColumnClass(0));
            set(ec,'ClickCountToStart', 100) ;
            % select current ArteryNet object in table
            idx = [];
            if ~isempty(this)
                if ~isempty(this.id)
                    idx = find(cell2mat(dbcontent(:,1))==this.id)-1;
                end
            end
            if ~isempty(idx)
                table.changeSelection(idx, 1, true,false);
            end
            
            function [headers, dbcontent] = refreshTable
                where = '';
                switch target
                    case 'ArteryNet'
                        if ~isempty(this.id)
                            where =  [' where ArteryNet.id <> ' num2str(this.id)];
                        end
                        [headers dbcontent] = db.select1(['Select ArteryNet.id as id, currentParamSetId, name, description, a.typeName as type, lastmodified as saved from ArteryNet join ArteryNetType as a on a.id = type ' where], true);
                    case 'ParamSet'
                        if ~isempty(this)
                            if ~isempty(this.id)
                                where = ['where statisticalSetId = 1 and arteryNetId = ' num2str(this.id) ' and id <> ' num2str(this.p_id)];
                            end
                        end
                        [headers dbcontent] = db.select1(['Select id, arteryNetId, name, description, currentSolverRun, lastmodified as saved from ParamSet ' where]);
                    case 'SolverRun'
                        sel = ['Select s.id, paramSetId, s.name as name, s.description as description, sv.typeName as solver, rate, type as nonlinear, duration, relTol, absTol, sensMtd, sensPar, s.lastmodified as saved ' ...
                            ' from SolverRun as s join ParamSet as p on p.id = s.paramSetId join Solvers as sv on sv.id = solver' ...
                            ' where p.arteryNetId = ' num2str(this.id)];
                        [headers dbcontent] = db.select1(sel, true);
                end
                if length(dbcontent) > 1 && ~isempty(table)
                    set(table, 'MouseMovedCallback', {@delDlgMouseMoved, 1});  % for tooltip table 1
                    set(table,'MousePressedCallback',@delDlgSelection);
                end
            end
            
            function DelDlgOk(varargin)
                r = table.getSelectedRow;
                id = table.getModel.getValueAt(r,0);
                switch target
                    case 'ArteryNet'
                        db.exec(['delete from ArteryNet where id = ' num2str(id)]);
                    case 'ParamSet'
                        db.exec(['delete from ParamSet where id = ' num2str(id)]);
                    case 'SolverRun'
                        db.exec(['delete from SolverRun where id = ' num2str(id)]);
                end
                refreshTable;
                tm = javax.swing.table.DefaultTableModel(dbcontent, headers);
                table.setModel(tm);
                sdb = size(dbcontent, 1);
                if isempty(dbcontent{1})
                    set(hOk, 'Enable', 'off');
                    DlgCancel;
                    return;
                end
                if  r == sdb
                    r = sdb-1;
                end
                table.changeSelection(r, 0, true,false);
                set(hCancel , 'String', 'Close');
            end
            function DlgCancel(varargin)
                delete(hDlg);
            end
            function delDlgSelection(varargin)
                r = table.getSelectedRow;
                aid = table.getModel.getValueAt(r,0);
                if ~isempty(aid)
                    set(hOk, 'Enable', 'on');
                end
            end
            function delDlgMouseMoved(varargin)
                %% delDlgMouseMoved - maintains tooltip
                event = varargin{2};
                row = table.rowAtPoint(event.getPoint);
                col = table.columnAtPoint(event.getPoint);
                value = table.getModel.getValueAt(row,col);
                if ~isempty(value)
                    if isjava(value)
                        value = value.toString;
                    elseif isnumeric(value)
                        value = num2str(value);
                    end
                else
                    value = '';
                end
                % Multiline tooltip
                j = 1;
                while j * 100 < size(value, 2)
                    value = sprintf('%s<br/>%s',value(1:j*100), value(j*100+1:size(value, 2)));
                    j = j+1;
                end
                table.setToolTipText(['<html> ' char(value)]);
            end
            
        end
        
        function ret = openCommonDlg(this, title)
            %% aOpenDlg - double purpose dialog for opening ArteryNet and/or ParamSet objects from database
            %    - modal dialog
            %    - can be used either as instance or static method
            %    - populates two JTables with contents of ArteryNet and ParamSet
            %      tables in angiodb and maintains them according to user input
            %    - opens and closes AngioDB
            %    - dialog load ParamSet in current design, if title string contains 'ParamSet'
            
            % figure
            paramsetmode = false;
            if strfind(title, 'ParamSet')
                paramsetmode = true;
                if isempty(this.id)
                    warndlg({'ArteryNet.openCommonDlg: ' 'Object is unknown to DB; no parameter sets exist in DB'});
                    return;
                end
            end
            db = [];
            if ~isempty(this)
                if isempty(this.db)
                    this.db = AngioDB;
                end
                db = this.db;
            else
                db = AngioDB;
            end
            db.open;
            
            
            ret = [];
            scrsz = get(0,'ScreenSize');
            mp = get(0, 'MonitorPositions');
            if size(mp,1) > 1
                scrf = scrsz(3) + (scrsz(3)-1000)/2;
            else
                scrf = (scrsz(3)-1000)/2;
            end
            
            position = [scrf, (scrsz(4)-600)/2, 1200, 800];
            hDlg = figure('Name', title, 'NumberTitle', 'off', 'MenuBar','none', 'ToolBar','none', ...
                'Color', 'w', 'Position', position, 'Resize', 'off', 'WindowStyle', 'normal', ...
                'CloseRequestFcn', @aOpenDlgCancel);
            
            % static text over panels
            uicontrol(hDlg, 'Style', 'Text', 'FontSize', 14, 'HorizontalAlignment', 'left', 'String', 'ArteryNet', ...
                'Position', [20 770 300, 30], 'BackgroundColor', 'w');
            uicontrol(hDlg, 'Style', 'Text', 'FontSize', 14, 'String', 'ParamSet', ...
                'Position', [20 370 300, 30], 'BackgroundColor', 'w', 'HorizontalAlignment', 'left');
            
            % filter controls
            hFilter = uicontrol(hDlg, 'Style', 'checkbox', 'FontSize', 8, 'HorizontalAlignment', 'right', 'Value', 1, 'String', 'Filter: WHERE statisticalSetId = ', ...
                'Position', [20 20 200, 20], 'Callback', @toggleFilter, 'BackgroundColor', 'w');
            hFilterValue = uicontrol(hDlg, 'Style', 'edit', 'FontSize', 8, 'HorizontalAlignment', 'left', 'String', '1', ...
                'Position', [200 20 50, 20], 'Callback', @setFilterValue, 'BackgroundColor', 'w');
            statSetFilter = true;
            
            % buttons
            hOk = uicontrol(hDlg, 'Style', 'Pushbutton', 'FontSize', 8, 'HorizontalAlignment', 'right', 'Enable', 'off', 'String', 'Open', ...
                'Position', [1050 20 60, 30], 'Callback', @aOpenDlgOpen, 'KeyPressFcn', @aOpenDlgOpen);
            if paramsetmode
                set(hOk, 'Callback', @pOpenDlgOpen);
            end
            
            hCancel = uicontrol(hDlg, 'Style', 'Pushbutton', 'FontSize', 8, 'HorizontalAlignment', 'right', 'String', 'Cancel', ...
                'Position', [1120 20 60, 30], 'Callback', @aOpenDlgCancel);
            
            % tables
            % ArteryNet
            select1 = 'Select a.id as id, a.name as name, a.description as description, t.typeName as type, a.currentParamSetId as currPS, a.lastmodified as saved from ArteryNet as a, ArteryNetType as t';
            where1  = ' where a.type = t.id';
            if paramsetmode
                where1  = [where1 ' and a.id = ' num2str(this.id)];
            end
            [headers dbcontent] = db.select1([select1 where1], true);
            if size(dbcontent, 2) == 1
                dbcontent = cell(size(headers)); 
            end
            table = javahandle_withcallbacks.javax.swing.JTable(dbcontent, headers); % R2014a/R2015a fixed
            table.setSelectionMode(0);
            cm = table.getColumnModel;
            if(cm.getColumnCount == 6)
                w = cm.getTotalColumnWidth;    % magic number because cm.getTotalColumnWidth has wrong value in first call
                cm.getColumn(0).setPreferredWidth(0.04*w);
                cm.getColumn(1).setPreferredWidth(0.15*w);
                cm.getColumn(2).setPreferredWidth(0.61*w);
                cm.getColumn(3).setPreferredWidth(0.04*w);
                cm.getColumn(4).setPreferredWidth(0.04*w);
                cm.getColumn(5).setPreferredWidth(0.08*w);
            end
            ScrollPane = javax.swing.JScrollPane(table);
            set(hDlg, 'Userdata', table);
            btLinePropsPos = [[0.02 0.52],0.96,0.45];
            [~,btContainer] = javacomponent(ScrollPane,[0 0 1 1], hDlg);
            set(btContainer, 'Units', 'Norm', 'Position', btLinePropsPos);
            
            warning('off', 'MATLAB:hg:JavaSetHGProperty');
            
            set(table,'MousePressedCallback',@aOpenDlgSelection);
            set(table,'KeyPressedCallback',@aOpenDlgSelection);
            set(table, 'MouseMovedCallback', {@aOpenDlgMouseMoved, 1});  % for tooltip table 1
            
            
            table.setAutoCreateRowSorter(true);
            ec = table.getDefaultEditor(table.getColumnClass(0));
            set(ec,'ClickCountToStart', 100) ;
            % select current ArteryNet object in table
            idx = [];
            if ~isempty(this)
                if ~isempty(this.id)
                    idx = find(cell2mat(dbcontent(:,1))==this.id)-1;
                end
            end
            if ~isempty(idx)
                table.changeSelection(idx, 1, true,false);
                idx = this.id;
                pid = this.p_id;
            else
                idx = 0;
                pid = 0;
            end
            where = '';
            if ~isempty(dbcontent{1})
                where = [' where arteryNetId = ' num2str(idx)];
                if statSetFilter
                    where = [where ' and statisticalSetId = 1'];
                end
            end
            
            % ParamSet
            table1 = javax.swing.JTable;
            table1.setSelectionMode(0);
            if idx > 0
                aOpenDlgSelection;  % synchronize tables
                ec = table1.getDefaultEditor(table1.getColumnClass(0));
                set(ec,'ClickCountToStart', 100);
            end
            ScrollPane = javax.swing.JScrollPane(table1);
            btLinePropsPos = [[0.02 0.07],0.96,0.4];
            [~,btContainer] = javacomponent(ScrollPane,[0 0 1 1],hDlg);
            set(btContainer, 'Units','Norm', 'Position',btLinePropsPos);
            uicontrol(hOk);
            uiwait(hDlg);
            
            %% callbacks
            function toggleFilter(varargin)
                %% toggleFilter - callback, called when user toggles filter checkbox
                set(hFilter, 'Value', ~statSetFilter);
                statSetFilter = ~statSetFilter;
                aOpenDlgSelection;
            end
            function setFilterValue(varargin)
                %% setFilterValue - callback, called when user changes value of filter edit field
                val = get(hFilterValue, 'String');
                val = str2num(val);
                if isempty(val)
                    set(hFilterValue, 'String', 1);
                else
                    val = val(1);
                    set(hFilterValue, 'String', val);
                    aOpenDlgSelection;
                end
            end
            
            function aOpenDlgSelection(varargin)
                %% aOpenDlgSelection - callback, called when user selects row in ArteryNet table
                %    - updates dbcontent in ParamSet table to fit selected ArteryNet row
                %    - selects row according to currentParamSetId value
                set(hOk, 'Enable', 'off');
                r = table.getSelectedRow;
                aid = table.getModel.getValueAt(r,0);
                pid = table.getModel.getValueAt(r,4);
                where = [' where arteryNetId = ' num2str(aid)];
                if statSetFilter
                    val = get(hFilterValue, 'String');
                    where = [where ' and statisticalSetId = ' val];
                end
                [headers dbcontent] = db.select1(['Select id, statisticalSetId, name, description , lastmodified as saved from ParamSet' where], true);
                set(table1, 'MouseMovedCallback', '');  % for tooltip table 2
                
                if ~strcmp(dbcontent{1}, '')
                    set(table1, 'MouseMovedCallback', {@aOpenDlgMouseMoved, 2});  % for tooltip table 2
                    set(table1,'MousePressedCallback',@aOpenDlgSelection1);
                    set(table1,'KeyPressedCallback',  @aOpenDlgSelection1);
                end
                
                
                tm = javax.swing.table.DefaultTableModel(dbcontent, headers);
                table1.setModel(tm);
                table1.setAutoCreateRowSorter(true);
                % select current ParamSet in Table
                currentid = table.getModel.getValueAt(r,4);
                for i=0:table1.getRowCount-1
                    if currentid == tm.getValueAt(i,0)
                        table1.changeSelection(i, 1, false,false);
                        set(hOk, 'Enable', 'on');
                        break;
                    end
                end
                cm = table1.getColumnModel;
                if(cm.getColumnCount > 4)
                    w = 1133;                                   % magic because cm.getTotalColumnWidth has wrong result on first call
                    cm.getColumn(0).setPreferredWidth(0.04*w);
                    cm.getColumn(1).setPreferredWidth(0.08*w);
                    cm.getColumn(2).setPreferredWidth(0.2*w);
                    cm.getColumn(3).setPreferredWidth(0.6*w);
                    cm.getColumn(4).setPreferredWidth(0.1*w);
                end
                if ~strcmp(dbcontent{1}, '')
                    set(table1, 'MouseMovedCallback', {@aOpenDlgMouseMoved, 2});  % for tooltip table 2
                end
            end
            function aOpenDlgSelection1(varargin)
                set(hOk, 'Enable', 'on');
                if size(varargin,2) == 2  % selection by double click
                    ev = varargin{1,2};
                    if isa(ev, 'java.awt.event.MouseEvent') && ev.getClickCount > 1
                        if paramsetmode
                            pOpenDlgOpen;
                        else
                            aOpenDlgOpen;
                        end
                    end
                end
                
            end
            function aOpenDlgMouseMoved(varargin)
                %% aOpenDlgMouseMoved1 - maintains tooltip for both tables
                event = varargin{2};
                t = varargin{3};
                tables = {table, table1};
                row = tables{t}.rowAtPoint(event.getPoint);
                col = tables{t}.columnAtPoint(event.getPoint);
                value = tables{t}.getModel.getValueAt(row,col);
                if ~isempty(value)
                    if isjava(value)
                        value = value.toString;
                    elseif isnumeric(value)
                        value = num2str(value);
                    end
                else
                    value = '';
                end
                % Multiline tooltip
                j = 1;
                while j * 100 < size(value, 2)
                    value = sprintf('%s<br/>%s',value(1:j*100), value(j*100+1:size(value, 2)));
                    j = j+1;
                end
                tables{t}.setToolTipText(['<html> ' char(value)]);
            end
            function aOpenDlgCancel(varargin)
                %% aOpenDlgCancel - callback, handles Cancel button of Open dialog
                %   closes db connection and destroys figure, without further action
                %   context = {'table', 'table1', 'this'}
                delete(gcf);
            end
            function aOpenDlgOpen(varargin)
                %% aOpenDlgOpen - callback, handles Open button of Open dialog
                % retrieves selected values from db and opens new Designer for it
                currAid = table.getModel.getValueAt(table.getSelectedRow, 0);
                r = table1.getSelectedRow;
                currPid = table1.getModel.getValueAt(r, 0);
                ret = ArteryNet(currAid, currPid);
                delete(gcf);
            end
            function pOpenDlgOpen(varargin)
                %% pOpenDlgOpen - callback, handles Open button of dialog and loads ParamSet into current design
                % retrieves selected values from db and opens new Designer for it
                r = table1.getSelectedRow;
                currPid = table1.getModel.getValueAt(r, 0);
                delete(gcf);
                this.ReadParamSet(currPid);
                command = ['update ArteryNet set currentParamSetId= ''' num2str(this.p_id) ''' where id=''' num2str(this.id) ''''];
                this.db.update(command);
                this.root.validate(this);
                this.currentParamSetId = currPid;
            end
        end
        
        %% Sets
        function [ids names] = ArteryNets(db)
            %% ArteryNets - returns a list of known ArteryNets
            %   >> [ids names] = ArteryNet.ArteryNets;
            %   >> [ids names] = ArteryNet.ArteryNets(db);
            %       - 'db' (optional, default: = AngioDB) is an existing AngioDB object
            if nargin < 1
                db = AngioDB;
            end
            if ~isa(db, 'AngioDB')
                error(sprintf('ArteryNet.ArteryNets: Parameter has wrong type. \n usage: psets = ArteryNet.ArteryNets(db)'));
            end
            ids = [];
            ids = [];
            a = db.select('select id, name from ArteryNet order by id');
            if ~isempty(a)
                ids = [a.id];
                names = {a.name};
            end
        end
        function psets = ParamSets(anet_id, ssId, db)
            %% ParamSets - returns an id-list of Parameter sets known for the ArteryNet object with id=anet_id
            %   >> psets = ArteryNet.ParamSets(anet);      % from instance
            %   >> psets = ArteryNet.ParamSets(anet_id);   % from instance id
            %   >> psets = ArteryNet.ParamSets(anet, db);
            %   >> psets = ArteryNet.ParamSets(anet_id, db);
            %   >> psets = ArteryNet.ParamSets(anet_id, 1, db); filter for StatisticalSetId ssId
            %       - 'db' (optional, default: = AngioDB) is an existing AngioDB object
            % PARAMETER: 
            %   'anet_id' ArteryNet object or id 
            %   'ssId'    (optional) filter statisticalSetId 
            %   'db'      (optional) an AngioDB object
            switch nargin
                case 0 
                    error(sprintf('ArteryNet.ParamSets: At least one Parameter expected. \n usage: psets = ArteryNet.ParamSets(anet_id, db)'));
                case 1
                    where = '';  
                    db = AngioDB;
                    if isa(anet_id, 'ArteryNet')
                        anet_id = anet_id.id;
                    elseif ~isnumeric(anet_id)
                        error(sprintf('ArteryNet.ParamSets: first Parameter has a wrong type. \n usage: psets = ArteryNet.ParamSets(anet_id, db)'));
                    end
                case 2
                    if isa(ssId, 'AngioDB')
                        db = ssId; 
                        where = '';  
                    elseif isnumeric(ssId)
                        db = AngioDB;
                        where = sprintf(' and statisticalSetId = %i', ssId);  
                    else
                        error(sprintf('ArteryNet.ParamSets: At least one Parameter has a wrong type. \n usage: psets = ArteryNet.ParamSets(anet_id, db)'));
                    end
                case 3
                    if ~isa(db, 'AngioDB') || ~isnumeric(ssId) || ~isnumeric(anet_id)
                        error(sprintf('ArteryNet.ParamSets: At least one Parameter has a wrong type. \n usage: psets = ArteryNet.ParamSets(anet_id, db)'));
                    else
                        where = sprintf(' and statisticalSetId = %i', ssId);  
                    end
            end
            psets = db.select(sprintf('select id, name, description, statisticalSetId, currentSolverRun, lastmodified from ParamSet where arteryNetId = %i %s', anet_id, where));
        end
        function sruns = SolverRuns(pset_id, db)
            %% SolverRuns - returns an id-list of SolverRuns known for the ParamSet object with id=pset_id
            %   >> sruns = ArteryNet.SolverRuns(pset_id);
            %   >> sruns = ArteryNet.SolverRuns(pset_id, db);
            %       - 'db' (optional, default: = AngioDB) is an existing AngioDB object
            %       - 'pset_id' denotes the id of the parent ParamSet object
            if nargin < 1
                error(sprintf('ArteryNet.SolverRuns: At least one Parameter expected. \n usage: sruns = ArteryNet.SolverRuns(pset_id, db)'));
            elseif nargin < 2
                db = AngioDB;
            end
            if ~isa(db, 'AngioDB') || ~isnumeric(pset_id)
                error(sprintf('ArteryNet.SolverRuns: At least one Parameter has a wrong type. \n usage: sruns = ArteryNet.SolverRuns(pset_id, db)'));
            end
            sruns = [];
            a = db.select(['select s.id from SolverRun as s join ParamSet as p on p.currentSolverRun = s.id where p.id = ' num2str(pset_id)]);
            if ~isempty(a)
                l = length(a);
                sruns(l) = 0;
                for i=1:l
                    sruns(i) = a(i).id;
                end
            end
        end
        
        %% From Constructors
        function anet = FromSolverRunId(srun_id, db)
            %% FromSolverRunId - returns a fully instantiated ArteryNet object for this SolverRun
            %   >> anet = ArteryNet.FromSolverRunId(srun_id);
            %   >> anet = ArteryNet.FromSolverRunId(srun_id, db);
            %       - 'srun_id' id of SolverRun object in DB:
            %       - 'db' (optional, default: = AngioDB) is an existing AngioDB object
            if nargin < 1
                error(sprintf('ArteryNet.FromSolverRunId: At least one Parameter expected. \n usage: anet = ArteryNet.FromSolverRunId(srun_id, db)'));
            elseif nargin < 2
                db = AngioDB;
            end
            if ~isnumeric(srun_id)
                error(sprintf('ArteryNet.FromSolverRunId: Parameter srun_id must have numeric type'));
            end
            command = ['select A.id as a_id, P.id as p_id  from ArteryNet as A join ParamSet as P on P.arteryNetId = A.id join SolverRun as S on P.id = S.paramSetId where S.id = ' num2str(srun_id)];
            anet = db.select(command);
            if ~isempty(anet)
                anet = ArteryNet(anet.a_id, anet.p_id, srun_id, 'keepCurrent');
            end
        end
        function anet = FromParamSetId(pset_id, db)
            %% FromParamSetId - returns a fully instantiated ArteryNet object for ParamSet object with pset_id
            %   >> anet = ArteryNet.FromParamSetId(pset_id);
            %   >> anet = ArteryNet.FromParamSetId(pset_id, db);
            %       - 'pset_id' id of ParamSet object in DB:
            %       - 'db' (optional, default: = AngioDB) is an existing AngioDB object
            if nargin < 1
                error(sprintf('ArteryNet.FromParamSetId: At least one Parameter expected. \n usage: anet = ArteryNet.FromParamSetId(pset_id, db)'));
            elseif nargin < 2
                db = AngioDB;
            end
            if ~isnumeric(pset_id)
                error(sprintf('ArteryNet.FromParamSetId: Parameter pset_id must have numeric type'));
            end
            command = ['select A.id from ArteryNet as A join ParamSet as P on P.arteryNetId = A.id where P.id = ' num2str(pset_id)];
            anet = db.select(command);
            if ~isempty(anet)
                anet = ArteryNet(anet.id, pset_id, 'keepCurrent');
            end
        end
        
        function anet = OpenDlg(db)
            %%  OpenDlg - shows a Dialog to interactively open a ArteryNet object
            %   >> anet = OpenDlg(db);   % show Dialog
            %   >> anet = OpenDlg;       %
            % PARAMETER
            %    'db' (optional) AngioDB object;
            %
            help = '<a href="matlab:help ArteryNet/OpenDlg">USAGE</a>';
            where = '';
            if nargin < 2
                db = AngioDB;
            end
            % prepare and call Dlg
            select = 'select a.id, name, description, b.typeName as type, lastmodified from ArteryNet as a join ArteryNetType as b on a.type = b.id ';
            title = 'ArteryNet Selector';
            id = DbObj.SelectDlg(title, select, [1 3 10 1 1], db);
            if id > 0
                anet = ArteryNet(id);
                anet.Designer;
            else
                anet = [];
            end
        end
        
        function res = List(verbose, start, count)
        %% List  - generates a list of known ArterNet objects
        %    >> all = ArteryNet.List;               % lists all in verbose mode and returns info in a struct array
        %    >> ArteryNet.List(true, 3);            % list the first 3 objects 
        %    >> all = ArteryNet.List(false);        % returns info in a struct array
        %    >> all = ArteryNet.List(true, 10, 4);  % list 10 objects starting with index 4
        %       'verbose' (optional) if true, output to command window
        %       'start'   (optional) output to command window starts at 'start' index
        %       'count'   (optional) output to command window is restricted to 'count' objects; 
            db = AngioDB; 
            res = db.select('select ArteryNet.id, name, description, typeName, currentParamSetId, lastmodified from ArteryNet join ArteryNetType on ArteryNetType.id = ArteryNet.type order by ArteryNet.id '); 
            if isempty(res)
                return; 
            end
            if nargin == 0
                verbose = true; 
            end
            startidx = 1; 
            len = length(res); 
            if nargin > 1
                if start == 0 || abs(start) > len
                    return; 
                end
                startidx = abs(start); 
            end
            if nargin > 2
                if count <= 0 
                    return; 
                end
                len = min(abs(startidx+count-1), len); 
            end
            if verbose
                fprintf('   id |                                     name |   type | currentParamSetId |        lastmodified | description \n'); 
                fprintf('=======================================================================================================================\n'); 
                for i = startidx : len
                    desc = res(i).description; 
                    f = strfind(desc, char(10)); 
                    desc(f) = '*'; 
                    desc = desc(1: min(end,120)); 
                    fprintf('%5i | %40s | %6s | %17i | %15s | %s\n', res(i).id, res(i).name, res(i).typeName, res(i).currentParamSetId, char(res(i).lastmodified.toLocaleString), desc); 
                end
            end
        end
        function C = calcC(r,l,h,E)
        %%  calcC - common function for calculation of compliance
             % C = (2*pi*r^3*l) / (E * h); % old 
              C  = (3*pi*r.^2.*(r./h+1).^2 .* l) ./ (E .* (2*r./h+1)); % Noordergraf
        end

    end
    
    methods
        function mVVisualize(this, varargin)
            m = varargin{1};
            if strcmp(get(m, 'Checked'), 'off')
                set(m, 'Checked', 'on');
                this.visualize = true;
                this.root.visualize;
            else
                set(m, 'Checked', 'off');
                this.visualize = false;
                this.root.unvisualize;
            end
            
        end
        function mVBindMacSim(this, varargin)
            % test implementation only
            ms = MacSim.OpenDlg(this.id, this.db);
            if ~isempty(ms)
                this.MacSim = ms; 
                this.SignalViewer(ms);
            end
        end
        function mVShowMatlab(this, varargin)
            m = varargin{1};
            if strcmp(get(m, 'Checked'), 'off')
                set(m, 'Checked', 'on');
                set(this.hFig, 'MenuBar', 'figure', 'Toolbar', 'figure'); 
            else
                set(m, 'Checked', 'off');
                set(this.hFig, 'MenuBar', 'none', 'Toolbar', 'none'); 
            end
        end
        
        function solution = CurrentSolution(this)
            %% CurrentSolution - returns result of last solver run.
            solution = this.p_solverRun.solution;
        end
        
        function compareParamSet(this, ps_id)
            ps1 = this.getParamSet; 
            for i=1: length(ps_id)
                this.ReadParamSet(ps_id(i)); 
                ps2 = this.getParamSet;
                comp_struct(ps1.netParam, ps2.netParam, num2str(ps1.id), num2str(ps2.id)); 
                comp_struct(ps1.nodeParam, ps2.nodeParam, num2str(ps1.id), num2str(ps2.id)); 
                comp_struct(ps1.boundaryParam, ps2.boundaryParam, num2str(ps1.id), num2str(ps2.id)); 
            end
            this.ReadParamSet(ps1.id);  
        end
    end
end
